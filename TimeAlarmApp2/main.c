#include "main.h"

uint16_t buz_count = 0;
uint16_t buz_period = 1000;
uint8_t push_once = 1;

void delay_us(uint8_t del)
{
	while(del--);
}

/********************
 * EEPROM Functions *
 ********************/
void EEPROM_config(void)
{
	FLASH_DeInit();
	FLASH_Unlock(FLASH_MemType_Data);
	FLASH_SetProgrammingTime(FLASH_ProgramTime_Standard);
}

void EEPROM_writeRoutine(uint8_t w_data)
{
	FLASH_ProgramByte(0x0013FF, w_data);
	while(!(FLASH_GetFlagStatus(FLASH_FLAG_EOP)));
}

uint8_t EEPROM_readRoutine(void)
{
	uint8_t r_data;
	r_data = FLASH_ReadByte(0x0013FF);
	return r_data;
}

/******************************
 * Clock source configuration *
 ******************************/
void CLK_config(void)
{
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
  CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
  while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}
}

void setSeg(int segNum)
{
	GPIOC->ODR &= ~(GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3);
	GPIOD->ODR &= ~GPIO_Pin_0;
	
	switch (segNum)
	{
		case 0:
			GPIOC->ODR |= GPIO_Pin_SEG1;
			break;
		case 1:
			GPIOC->ODR |= GPIO_Pin_SEG2;
			break;
		case 2:
			GPIOC->ODR |= GPIO_Pin_SEG3;
			break;
		default:
			GPIOC->ODR &= ~(GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3);
			break;
	}
}

void writeNumSym(uint8_t num, uint8_t seg)
{
	// Choosing segment position
	setSeg(seg);
	
	// Choosing numeric to write
	GPIO_SetBits(GPIOB, GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G | GPIO_Pin_DP);
	switch (num)
	{
		case 0: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B |GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E |GPIO_Pin_F);	
			break;
		case 1: 
			GPIOB->ODR &= ~(GPIO_Pin_B | GPIO_Pin_C);
			break;
		case 2: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_G | GPIO_Pin_D | GPIO_Pin_E);
			break;
		case 3: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_G);
			break;
		case 4: 
			GPIOB->ODR &= ~(GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_F | GPIO_Pin_G);
			break;
		case 5: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_G);
			break;
		case 6: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_E | GPIO_Pin_G);
			break;
		case 7: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C);
			break;
		case 8: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
			break;
		case 9: 
			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_G);
			break;
		default:
			GPIOB->ODR |= (GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
			break;
	}
}

void writeNumeric(uint8_t numeric)
{
	uint8_t num[3];
	uint8_t i;
	
	num[0] = numeric % 10;
	num[1] = (numeric / 10) % 10;
	num[2] = numeric / 100;
	for(i = 0; i < 3; i++) 
	{
		writeNumSym(num[i], i);
		delay_us(250);
		delay_us(250);
		delay_us(250);
	}
	writeNumSym(10, 3);
}

void writeNumericPos(uint8_t numeric, uint8_t pos)
{
	uint8_t num[3];
	uint8_t i;
	
	num[0] = numeric % 10;
	num[1] = (numeric / 10) % 10;
	num[2] = numeric / 100;

	writeNumSym(num[pos], pos);
}

void GPIO_config(void)
{
	 /* 
	  * 7-segment display pins
		*
		*      +-------+
		*      |   A   |
		*  +---+-------+---+
		*  |   |       |   |
		*  | F |       | B |
		*  |   |       |   |
		*  +---+-------+---+
		*      |   G   |
		*  +---+-------+---+
		*  |   |       |   |
		*  | E |       | C |
		*  |   |       |   |
		*  +---+-------+---+  +----+
		*      |   D   |      | DP |
		*      +-------+      +----+
		*
		* A 		- PB0
		* B 		- PB1 - PC4
		* C 		- PB2 - PC1
		* D 		- PB3 - PC0
		* E 		- PB4 - PB7
		* F 		- PB5 - PB6
		* G 		- PB6 - PB5
		* DP		- PB7 - PB4
		* ------------------
		* SEG1	- PC0 - PB3
		* SEG2	- PC1 - PB2
		* SEG3	- PC4 - PB1
	  */
	GPIO_Init(GPIOB, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6| GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3 | GPIO_Pin_BUZZ, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOD, GPIO_Pin_0,  GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_2 | GPIO_Pin_3, GPIO_Mode_In_PU_No_IT);
}

void TIM2_config(void)
{
	/*
	uint16_t TIM2_Period = 100;//31250;
	TIM2->CR1 &= ~TIM_CR1_CEN;
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM2;	//CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);	
	
	TIM2->PSCR &= ~TIM_PSCR_PSC;
	TIM2->PSCR |= 7;
	TIM2->CR1 |= (TIM_CR1_DIR | TIM_CR1_ARPE);
	TIM2->ARRH = (uint8_t)(TIM2_Period >> 8) ;
  TIM2->ARRL = (uint8_t)(TIM2_Period);
	
	//TIM2->IER |= TIM_IER_CC1IE;
	TIM2->CR1 |= TIM_CR1_CEN;
	*/	
	TIM2_DeInit();	
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);	
	TIM2_TimeBaseInit(TIM2_Prescaler_128, TIM2_CounterMode_Up, 100);
	TIM2_ARRPreloadConfig(ENABLE);		
	TIM2_ITConfig(TIM2_IT_Update, ENABLE);
	
	TIM2_Cmd(ENABLE);
}

void TIM3_config(void)
{	
	TIM3_DeInit();	
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);	
	TIM3_TimeBaseInit(TIM3_Prescaler_128, TIM3_CounterMode_Up, 12500);
	TIM3_ARRPreloadConfig(ENABLE);		
	TIM3_ITConfig(TIM3_IT_Update, ENABLE);
	
	TIM3_Cmd(ENABLE);
}

void EXTI_config(void)
{
	EXTI_DeInit();
	EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling);
}

void PWRCNTRL_config(void)
{
	PWR_DeInit();
	PWR_PVDLevelConfig(PWR_PVDLevel_2V05);
	PWR_PVDITConfig(ENABLE);
	PWR_PVDCmd(ENABLE);
}

void PWRCNTRL_routine(void)
{
	
}

uint8_t co = 25;
uint8_t i = 0;
uint8_t alarm_state = 0;
uint8_t alarm_type = 0;
uint8_t disp_state = 0;
uint8_t count_state = 1;
uint8_t save_st, save_pre_st;
uint8_t save_st2, save_pre_st2;
int main()
{
	GPIOB->ODR |= (GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
	GPIOC->ODR |= GPIO_Pin_BUZZ;
	
	enableInterrupts();
	CLK_config();
	TIM2_config();
	TIM3_config();
	GPIO_config();
	EEPROM_config();
	PWRCNTRL_config();
	//EXTI_config();
	counter1 = 0;
	
	T_TIMEOUT = EEPROM_readRoutine();
	if (T_TIMEOUT == 0) T_TIMEOUT = 20;
	co = T_TIMEOUT;
	alarm_state = 0;
	while (1) 
	{
		save_pre_st = save_st;
		save_st = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3);
		
		save_pre_st2 = save_st2;
		save_st2 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2);
		
		if ((!save_st) && (save_st!= save_pre_st) && (disp_state == 1)) T_TIMEOUT++;
		if ((!save_st2) && (save_st2!= save_pre_st2) && (disp_state == 1)) T_TIMEOUT--;
		
		if ((!save_st) && (disp_state != 1)) disp_state = 1;		
		if ((!save_st2) && (disp_state != 1)) disp_state = 1;
	}
}

INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
{
	EXTI_ClearITPendingBit(EXTI_IT_Pin2);
}

INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
{
	EXTI_ClearITPendingBit(EXTI_IT_Pin3);
}

short	disp_pos = 0;
INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
{
	if (disp_state) 
	{
		
		writeNumericPos(T_TIMEOUT, disp_pos);
		disp_pos++;
		if (disp_pos > 2) disp_pos = 0;
		//writeNumeric(T_TIMEOUT);
	}
	else writeNumericPos(T_TIMEOUT, 3);
	
	if (alarm_state)
	{
		buz_count++;
		switch (alarm_type)
		{
			case 0:
				if (buz_count < buz_period / 5) GPIOC->ODR ^= GPIO_Pin_BUZZ;
				break;
			case 1:
				if (buz_count < buz_period / 5 * 4) GPIOC->ODR ^= GPIO_Pin_BUZZ;
				break;
			default: break;
		}
		if (buz_count > buz_period) buz_count = 0;
	}
	TIM2_ClearITPendingBit(TIM2_IT_Update);	/* ������� �������� ������� �����������!!!*/
}

uint8_t s1, s2, s_count = 0, off_count = 0;
uint8_t mul = 0, mul_max = 10;
INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
{
	s1 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3);
	s2 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2);
	
	if (count_state)
	{
		mul++;
		if (mul >= mul_max)
		{
			co--;
			if (co <= 0) 
			{
				alarm_state = 1;
			}
			mul = 0;
		}
	}
	
	if ((!(s1)) || (!(s2)))
	{
		off_count = 0;
		disp_state = 1;
		count_state = 0;
		alarm_state = 0;
		if (s_count <  36) s_count++;
		
		if (!(s1))
		{
			if (s_count >= 6) T_TIMEOUT++;
		}
		if (!(s2))
		{
			if (s_count >= 6) T_TIMEOUT--;
		}
		
		co = T_TIMEOUT;
		EEPROM_writeRoutine(T_TIMEOUT);
	}
	else 
	{
		s_count = 0;
		count_state = 0;
		if (off_count <  30) off_count++;
		if (off_count >= 30) 
		{
			disp_state = 0;
			count_state = 1;
		}
	}
	TIM3_ClearITPendingBit(TIM3_IT_Update);	/* ������� �������� ������� �����������!!!*/
}

INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
{
	//if (PWR_PVDGetITStatus() == PWR_FLAG_PVDIF)
	//{
		alarm_type = 1;
	//}
	PWR_PVDClearITPendingBit();
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) *
  /* Infinite loop */
  while (1) {}
}
#endif