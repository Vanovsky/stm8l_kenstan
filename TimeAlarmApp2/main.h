#ifndef MAIN_H
#define MAIN_H

#include "stm8l15x.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_gpio.h"
#include "stm8l15x_tim2.h"
#include "stm8l15x_tim3.h"
#include "stm8l15x_exti.h"
#include "stm8l15x_it.h"

	 /* 
		* 7-segment display pins
		*
		*    +-------+
		*    |   A   |
		*  +-+-------+-+
		*  | |       | |
		*  |F|       |B|
		*  | |       | |
		*  +-+-------+-+
		*    |   G   |
		*  +-+-------+-+
		*  | |       | |
		*  |E|       |C|
		*  | |       | |
		*  +-+-------+-+ +----+
		*    |   D   |   | DP |
		*    +-------+   +----+
		*
	  * A 		- PB0
		* B 		- PB1 - PC4
		* C 		- PB2 - PC1
		* D 		- PB3 - PC0
		* E 		- PB4 - PB7
		* F 		- PB5 - PB6
		* G 		- PB6 - PB5
		* DP		- PB7 - PB4
		* ------------------
		* SEG1	- PC0 - PB3
		* SEG2	- PC1 - PB2
		* SEG3	- PC4 - PB1
	  *
#define GPIO_Pin_A	 GPIO_Pin_0
#define GPIO_Pin_B	 GPIO_Pin_4
#define GPIO_Pin_C	 GPIO_Pin_1
#define GPIO_Pin_D	 GPIO_Pin_7
#define GPIO_Pin_E	 GPIO_Pin_6
#define GPIO_Pin_F	 GPIO_Pin_5
#define GPIO_Pin_G	 GPIO_Pin_4
#define GPIO_Pin_DP	 GPIO_Pin_7
*/
#define GPIO_Pin_A	 GPIO_Pin_0
#define GPIO_Pin_B	 GPIO_Pin_1
#define GPIO_Pin_C	 GPIO_Pin_2
#define GPIO_Pin_D	 GPIO_Pin_3
#define GPIO_Pin_E	 GPIO_Pin_4
#define GPIO_Pin_F	 GPIO_Pin_5
#define GPIO_Pin_G	 GPIO_Pin_6
#define GPIO_Pin_DP	 GPIO_Pin_7

#define GPIO_Pin_SEG3	GPIO_Pin_0
#define GPIO_Pin_SEG2	GPIO_Pin_1
#define GPIO_Pin_SEG1	GPIO_Pin_4
#define GPIO_Pin_BUZZ	GPIO_Pin_6

void EEPROM_config(void);
void EEPROM_writeRoutine(uint8_t w_data);
uint8_t EEPROM_readRoutine(void);
void CLK_config(void);
void GPIO_config(void);
void TIM2_config(void);
void TIM3_config(void);
void EXTI_config(void);
void PWRCNTRL_config(void);
void PWRCNTRL_routine(void);


static uint8_t counter1 = 0;
//static uint8_t co = 25;

static uint8_t T_TIMEOUT = 20;

int main();
void delay_us(uint8_t del);
void writeNumSym(uint8_t num, uint8_t seg);
void writeNumeric(uint8_t numeric);
void writeNumericPos(uint8_t numeric, uint8_t pos);

#endif
