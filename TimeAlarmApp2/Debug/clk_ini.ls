   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  43                     ; 3 void CLK_INIT(void)
  43                     ; 4 {
  45                     	switch	.text
  46  0000               _CLK_INIT:
  50                     ; 5 	CLK->ECKCR |= CLK_ECKCR_HSEON;
  52  0000 721050c6      	bset	20678,#0
  54  0004               L32:
  55                     ; 7 	while (!(CLK->ECKCR & CLK_ECKCR_HSERDY));
  57  0004 c650c6        	ld	a,20678
  58  0007 a502          	bcp	a,#2
  59  0009 27f9          	jreq	L32
  60                     ; 8 }
  63  000b 81            	ret
  76                     	xdef	_CLK_INIT
  95                     	end
