   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  15                     .const:	section	.text
  16  0000               _HSIDivFactor:
  17  0000 01            	dc.b	1
  18  0001 02            	dc.b	2
  19  0002 04            	dc.b	4
  20  0003 08            	dc.b	8
  49                     ; 68 void CLK_DeInit(void)
  49                     ; 69 {
  51                     	switch	.text
  52  0000               _CLK_DeInit:
  56                     ; 70   CLK->CKDIVR = CLK_CKDIVR_RESET_VALUE;
  58  0000 350350c0      	mov	20672,#3
  59                     ; 71   CLK->PCKENR = CLK_PCKENR_RESET_VALUE;
  61  0004 725f50c3      	clr	20675
  62                     ; 72   CLK->CCOR  = CLK_CCOR_RESET_VALUE;
  64  0008 725f50c5      	clr	20677
  65                     ; 73 }
  68  000c 81            	ret
 124                     ; 80 void CLK_CCOCmd(FunctionalState NewState)
 124                     ; 81 {
 125                     	switch	.text
 126  000d               _CLK_CCOCmd:
 128  000d 88            	push	a
 129       00000000      OFST:	set	0
 132                     ; 83   assert_param(IS_FUNCTIONAL_STATE(NewState));
 134  000e 4d            	tnz	a
 135  000f 2704          	jreq	L21
 136  0011 a101          	cp	a,#1
 137  0013 2603          	jrne	L01
 138  0015               L21:
 139  0015 4f            	clr	a
 140  0016 2010          	jra	L41
 141  0018               L01:
 142  0018 ae0053        	ldw	x,#83
 143  001b 89            	pushw	x
 144  001c ae0000        	ldw	x,#0
 145  001f 89            	pushw	x
 146  0020 ae0004        	ldw	x,#L74
 147  0023 cd0000        	call	_assert_failed
 149  0026 5b04          	addw	sp,#4
 150  0028               L41:
 151                     ; 85   if (NewState != DISABLE)
 153  0028 0d01          	tnz	(OFST+1,sp)
 154  002a 2706          	jreq	L15
 155                     ; 88     CLK->CCOR |= CLK_CCOR_CCOEN;
 157  002c 721050c5      	bset	20677,#0
 159  0030 2004          	jra	L35
 160  0032               L15:
 161                     ; 93     CLK->CCOR &= (uint8_t)(~CLK_CCOR_CCOEN);
 163  0032 721150c5      	bres	20677,#0
 164  0036               L35:
 165                     ; 96 }
 168  0036 84            	pop	a
 169  0037 81            	ret
 275                     ; 113 void CLK_PeripheralClockConfig(CLK_Peripheral_TypeDef CLK_Peripheral, FunctionalState NewState)
 275                     ; 114 {
 276                     	switch	.text
 277  0038               _CLK_PeripheralClockConfig:
 279  0038 89            	pushw	x
 280       00000000      OFST:	set	0
 283                     ; 116     assert_param(IS_FUNCTIONAL_STATE(NewState));
 285  0039 9f            	ld	a,xl
 286  003a 4d            	tnz	a
 287  003b 2705          	jreq	L22
 288  003d 9f            	ld	a,xl
 289  003e a101          	cp	a,#1
 290  0040 2603          	jrne	L02
 291  0042               L22:
 292  0042 4f            	clr	a
 293  0043 2010          	jra	L42
 294  0045               L02:
 295  0045 ae0074        	ldw	x,#116
 296  0048 89            	pushw	x
 297  0049 ae0000        	ldw	x,#0
 298  004c 89            	pushw	x
 299  004d ae0004        	ldw	x,#L74
 300  0050 cd0000        	call	_assert_failed
 302  0053 5b04          	addw	sp,#4
 303  0055               L42:
 304                     ; 117     assert_param(IS_CLK_PERIPHERAL(CLK_Peripheral));
 306  0055 7b01          	ld	a,(OFST+1,sp)
 307  0057 a580          	bcp	a,#128
 308  0059 2607          	jrne	L62
 309  005b 0d01          	tnz	(OFST+1,sp)
 310  005d 2703          	jreq	L62
 311  005f 4f            	clr	a
 312  0060 2010          	jra	L03
 313  0062               L62:
 314  0062 ae0075        	ldw	x,#117
 315  0065 89            	pushw	x
 316  0066 ae0000        	ldw	x,#0
 317  0069 89            	pushw	x
 318  006a ae0004        	ldw	x,#L74
 319  006d cd0000        	call	_assert_failed
 321  0070 5b04          	addw	sp,#4
 322  0072               L03:
 323                     ; 119     if (NewState != DISABLE)
 325  0072 0d02          	tnz	(OFST+2,sp)
 326  0074 270a          	jreq	L121
 327                     ; 122       CLK->PCKENR |= ((uint8_t)CLK_Peripheral);
 329  0076 c650c3        	ld	a,20675
 330  0079 1a01          	or	a,(OFST+1,sp)
 331  007b c750c3        	ld	20675,a
 333  007e 2009          	jra	L321
 334  0080               L121:
 335                     ; 127       CLK->PCKENR &= (uint8_t)(~(uint8_t)CLK_Peripheral);
 337  0080 7b01          	ld	a,(OFST+1,sp)
 338  0082 43            	cpl	a
 339  0083 c450c3        	and	a,20675
 340  0086 c750c3        	ld	20675,a
 341  0089               L321:
 342                     ; 129 }
 345  0089 85            	popw	x
 346  008a 81            	ret
 422                     ; 141 void CLK_MasterPrescalerConfig(CLK_MasterPrescaler_TypeDef CLK_MasterPrescaler)
 422                     ; 142 {
 423                     	switch	.text
 424  008b               _CLK_MasterPrescalerConfig:
 426  008b 88            	push	a
 427       00000000      OFST:	set	0
 430                     ; 144   assert_param(IS_CLK_MASTER_PRESCALER(CLK_MasterPrescaler));
 432  008c 4d            	tnz	a
 433  008d 270c          	jreq	L63
 434  008f a101          	cp	a,#1
 435  0091 2708          	jreq	L63
 436  0093 a102          	cp	a,#2
 437  0095 2704          	jreq	L63
 438  0097 a103          	cp	a,#3
 439  0099 2603          	jrne	L43
 440  009b               L63:
 441  009b 4f            	clr	a
 442  009c 2010          	jra	L04
 443  009e               L43:
 444  009e ae0090        	ldw	x,#144
 445  00a1 89            	pushw	x
 446  00a2 ae0000        	ldw	x,#0
 447  00a5 89            	pushw	x
 448  00a6 ae0004        	ldw	x,#L74
 449  00a9 cd0000        	call	_assert_failed
 451  00ac 5b04          	addw	sp,#4
 452  00ae               L04:
 453                     ; 146   CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_HSIDIV);
 455  00ae c650c0        	ld	a,20672
 456  00b1 a4fc          	and	a,#252
 457  00b3 c750c0        	ld	20672,a
 458                     ; 147   CLK->CKDIVR = (uint8_t)CLK_MasterPrescaler;
 460  00b6 7b01          	ld	a,(OFST+1,sp)
 461  00b8 c750c0        	ld	20672,a
 462                     ; 148 }
 465  00bb 84            	pop	a
 466  00bc 81            	ret
 540                     ; 161 void CLK_CCOConfig(CLK_Output_TypeDef CLK_Output)
 540                     ; 162 {
 541                     	switch	.text
 542  00bd               _CLK_CCOConfig:
 544  00bd 88            	push	a
 545       00000000      OFST:	set	0
 548                     ; 164   assert_param(IS_CLK_OUTPUT(CLK_Output));
 550  00be 4d            	tnz	a
 551  00bf 270c          	jreq	L64
 552  00c1 a102          	cp	a,#2
 553  00c3 2708          	jreq	L64
 554  00c5 a104          	cp	a,#4
 555  00c7 2704          	jreq	L64
 556  00c9 a106          	cp	a,#6
 557  00cb 2603          	jrne	L44
 558  00cd               L64:
 559  00cd 4f            	clr	a
 560  00ce 2010          	jra	L05
 561  00d0               L44:
 562  00d0 ae00a4        	ldw	x,#164
 563  00d3 89            	pushw	x
 564  00d4 ae0000        	ldw	x,#0
 565  00d7 89            	pushw	x
 566  00d8 ae0004        	ldw	x,#L74
 567  00db cd0000        	call	_assert_failed
 569  00de 5b04          	addw	sp,#4
 570  00e0               L05:
 571                     ; 167   CLK->CCOR &= (uint8_t)(~CLK_CCOR_CCOSEL);
 573  00e0 c650c5        	ld	a,20677
 574  00e3 a4f9          	and	a,#249
 575  00e5 c750c5        	ld	20677,a
 576                     ; 170   CLK->CCOR |= ((uint8_t)CLK_Output);
 578  00e8 c650c5        	ld	a,20677
 579  00eb 1a01          	or	a,(OFST+1,sp)
 580  00ed c750c5        	ld	20677,a
 581                     ; 173   CLK->CCOR |= CLK_CCOR_CCOEN;
 583  00f0 721050c5      	bset	20677,#0
 584                     ; 174 }
 587  00f4 84            	pop	a
 588  00f5 81            	ret
 641                     ; 181 uint32_t CLK_GetClockFreq(void)
 641                     ; 182 {
 642                     	switch	.text
 643  00f6               _CLK_GetClockFreq:
 645  00f6 5209          	subw	sp,#9
 646       00000009      OFST:	set	9
 649                     ; 183   uint32_t clockfrequency = 0;
 651                     ; 184   uint8_t tmp = 0, presc = 0;
 655                     ; 186   tmp = (uint8_t)(CLK->CKDIVR & CLK_CKDIVR_HSIDIV);
 657  00f8 c650c0        	ld	a,20672
 658  00fb a403          	and	a,#3
 659  00fd 6b09          	ld	(OFST+0,sp),a
 661                     ; 187   presc = HSIDivFactor[tmp];
 663  00ff 7b09          	ld	a,(OFST+0,sp)
 664  0101 5f            	clrw	x
 665  0102 97            	ld	xl,a
 666  0103 d60000        	ld	a,(_HSIDivFactor,x)
 667  0106 6b09          	ld	(OFST+0,sp),a
 669                     ; 188   clockfrequency = HSI_VALUE / presc;
 671  0108 7b09          	ld	a,(OFST+0,sp)
 672  010a b703          	ld	c_lreg+3,a
 673  010c 3f02          	clr	c_lreg+2
 674  010e 3f01          	clr	c_lreg+1
 675  0110 3f00          	clr	c_lreg
 676  0112 96            	ldw	x,sp
 677  0113 1c0001        	addw	x,#OFST-8
 678  0116 cd0000        	call	c_rtol
 681  0119 ae2400        	ldw	x,#9216
 682  011c bf02          	ldw	c_lreg+2,x
 683  011e ae00f4        	ldw	x,#244
 684  0121 bf00          	ldw	c_lreg,x
 685  0123 96            	ldw	x,sp
 686  0124 1c0001        	addw	x,#OFST-8
 687  0127 cd0000        	call	c_ludv
 689  012a 96            	ldw	x,sp
 690  012b 1c0005        	addw	x,#OFST-4
 691  012e cd0000        	call	c_rtol
 694                     ; 190   return((uint32_t)clockfrequency);
 696  0131 96            	ldw	x,sp
 697  0132 1c0005        	addw	x,#OFST-4
 698  0135 cd0000        	call	c_ltor
 702  0138 5b09          	addw	sp,#9
 703  013a 81            	ret
 728                     	xdef	_HSIDivFactor
 729                     	xdef	_CLK_GetClockFreq
 730                     	xdef	_CLK_CCOConfig
 731                     	xdef	_CLK_MasterPrescalerConfig
 732                     	xdef	_CLK_PeripheralClockConfig
 733                     	xdef	_CLK_CCOCmd
 734                     	xdef	_CLK_DeInit
 735                     	xref	_assert_failed
 736                     	switch	.const
 737  0004               L74:
 738  0004 5f6c69625c73  	dc.b	"_lib\src\stm8l10x_"
 739  0016 636c6b2e6300  	dc.b	"clk.c",0
 740                     	xref.b	c_lreg
 741                     	xref.b	c_x
 761                     	xref	c_ltor
 762                     	xref	c_ludv
 763                     	xref	c_rtol
 764                     	end
