   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
 484                     ; 11 void MFRC_WriteReg(MfrcRegTypedef MfrcReg, uint8_t value)
 484                     ; 12 {
 486                     	switch	.text
 487  0000               _MFRC_WriteReg:
 489  0000 89            	pushw	x
 490       00000000      OFST:	set	0
 493                     ; 13 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 495  0001 4b10          	push	#16
 496  0003 ae500f        	ldw	x,#20495
 497  0006 cd0000        	call	_GPIO_ResetBits
 499  0009 84            	pop	a
 501  000a               L732:
 502                     ; 15 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 504  000a 4b02          	push	#2
 505  000c ae5200        	ldw	x,#20992
 506  000f cd0000        	call	_SPI_GetFlagStatus
 508  0012 5b01          	addw	sp,#1
 509  0014 4d            	tnz	a
 510  0015 27f3          	jreq	L732
 511                     ; 16 	SPI_SendData(SPI1, MfrcReg);
 513  0017 7b01          	ld	a,(OFST+1,sp)
 514  0019 88            	push	a
 515  001a ae5200        	ldw	x,#20992
 516  001d cd0000        	call	_SPI_SendData
 518  0020 84            	pop	a
 520  0021               L542:
 521                     ; 17 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 523  0021 4b02          	push	#2
 524  0023 ae5200        	ldw	x,#20992
 525  0026 cd0000        	call	_SPI_GetFlagStatus
 527  0029 5b01          	addw	sp,#1
 528  002b 4d            	tnz	a
 529  002c 27f3          	jreq	L542
 530                     ; 18 	SPI_SendData(SPI1, value);
 532  002e 7b02          	ld	a,(OFST+2,sp)
 533  0030 88            	push	a
 534  0031 ae5200        	ldw	x,#20992
 535  0034 cd0000        	call	_SPI_SendData
 537  0037 84            	pop	a
 539  0038               L352:
 540                     ; 20 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 542  0038 4b80          	push	#128
 543  003a ae5200        	ldw	x,#20992
 544  003d cd0000        	call	_SPI_GetFlagStatus
 546  0040 5b01          	addw	sp,#1
 547  0042 4d            	tnz	a
 548  0043 26f3          	jrne	L352
 549                     ; 21 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 551  0045 4b10          	push	#16
 552  0047 ae500f        	ldw	x,#20495
 553  004a cd0000        	call	_GPIO_SetBits
 555  004d 84            	pop	a
 556                     ; 22 }
 559  004e 85            	popw	x
 560  004f 81            	ret
 618                     ; 28 void MFRC_WriteRegs(MfrcRegTypedef MfrcReg, uint8_t *values, uint8_t count)
 618                     ; 29 {
 619                     	switch	.text
 620  0050               _MFRC_WriteRegs:
 622  0050 88            	push	a
 623       00000000      OFST:	set	0
 626                     ; 30 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 628  0051 4b10          	push	#16
 629  0053 ae500f        	ldw	x,#20495
 630  0056 cd0000        	call	_GPIO_ResetBits
 632  0059 84            	pop	a
 634  005a               L703:
 635                     ; 31 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 637  005a 4b02          	push	#2
 638  005c ae5200        	ldw	x,#20992
 639  005f cd0000        	call	_SPI_GetFlagStatus
 641  0062 5b01          	addw	sp,#1
 642  0064 4d            	tnz	a
 643  0065 27f3          	jreq	L703
 644                     ; 32 	SPI_SendData(SPI1, MfrcReg);
 646  0067 7b01          	ld	a,(OFST+1,sp)
 647  0069 88            	push	a
 648  006a ae5200        	ldw	x,#20992
 649  006d cd0000        	call	_SPI_SendData
 651  0070 84            	pop	a
 653  0071 2020          	jra	L513
 654  0073               L323:
 655                     ; 35 		while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 657  0073 4b02          	push	#2
 658  0075 ae5200        	ldw	x,#20992
 659  0078 cd0000        	call	_SPI_GetFlagStatus
 661  007b 5b01          	addw	sp,#1
 662  007d 4d            	tnz	a
 663  007e 27f3          	jreq	L323
 664                     ; 36 		SPI_SendData(SPI1, *values++);
 666  0080 1e04          	ldw	x,(OFST+4,sp)
 667  0082 1c0001        	addw	x,#1
 668  0085 1f04          	ldw	(OFST+4,sp),x
 669  0087 1d0001        	subw	x,#1
 670  008a f6            	ld	a,(x)
 671  008b 88            	push	a
 672  008c ae5200        	ldw	x,#20992
 673  008f cd0000        	call	_SPI_SendData
 675  0092 84            	pop	a
 676  0093               L513:
 677                     ; 33 	while(count--)
 679  0093 7b06          	ld	a,(OFST+6,sp)
 680  0095 0a06          	dec	(OFST+6,sp)
 681  0097 4d            	tnz	a
 682  0098 26d9          	jrne	L323
 684  009a               L133:
 685                     ; 38 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 687  009a 4b80          	push	#128
 688  009c ae5200        	ldw	x,#20992
 689  009f cd0000        	call	_SPI_GetFlagStatus
 691  00a2 5b01          	addw	sp,#1
 692  00a4 4d            	tnz	a
 693  00a5 26f3          	jrne	L133
 694                     ; 39 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 696  00a7 4b10          	push	#16
 697  00a9 ae500f        	ldw	x,#20495
 698  00ac cd0000        	call	_GPIO_SetBits
 700  00af 84            	pop	a
 701                     ; 40 }
 704  00b0 84            	pop	a
 705  00b1 81            	ret
 754                     ; 46 uint8_t MFRC_ReadReg(MfrcRegTypedef MfrcReg)
 754                     ; 47 {
 755                     	switch	.text
 756  00b2               _MFRC_ReadReg:
 758  00b2 88            	push	a
 759  00b3 88            	push	a
 760       00000001      OFST:	set	1
 763                     ; 49 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 765  00b4 4b10          	push	#16
 766  00b6 ae500f        	ldw	x,#20495
 767  00b9 cd0000        	call	_GPIO_ResetBits
 769  00bc 84            	pop	a
 771  00bd               L163:
 772                     ; 50 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 774  00bd 4b02          	push	#2
 775  00bf ae5200        	ldw	x,#20992
 776  00c2 cd0000        	call	_SPI_GetFlagStatus
 778  00c5 5b01          	addw	sp,#1
 779  00c7 4d            	tnz	a
 780  00c8 27f3          	jreq	L163
 781                     ; 51 	SPI_SendData(SPI1, MfrcReg);
 783  00ca 7b02          	ld	a,(OFST+1,sp)
 784  00cc 88            	push	a
 785  00cd ae5200        	ldw	x,#20992
 786  00d0 cd0000        	call	_SPI_SendData
 788  00d3 84            	pop	a
 789                     ; 52 	data = SPI_ReceiveData(SPI1);
 791  00d4 ae5200        	ldw	x,#20992
 792  00d7 cd0000        	call	_SPI_ReceiveData
 794  00da 6b01          	ld	(OFST+0,sp),a
 797  00dc               L173:
 798                     ; 53 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 800  00dc 4b80          	push	#128
 801  00de ae5200        	ldw	x,#20992
 802  00e1 cd0000        	call	_SPI_GetFlagStatus
 804  00e4 5b01          	addw	sp,#1
 805  00e6 4d            	tnz	a
 806  00e7 26f3          	jrne	L173
 807                     ; 54 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 809  00e9 4b10          	push	#16
 810  00eb ae500f        	ldw	x,#20495
 811  00ee cd0000        	call	_GPIO_SetBits
 813  00f1 84            	pop	a
 814                     ; 55 	return data;
 816  00f2 7b01          	ld	a,(OFST+0,sp)
 819  00f4 85            	popw	x
 820  00f5 81            	ret
 823                     	bsct
 824  0000               L573_address:
 825  0000 00            	dc.b	0
 826  0001               L773_index:
 827  0001 00            	dc.b	0
 828                     	switch	.ubsct
 829  0000               L104_mask:
 830  0000 00            	ds.b	1
 831  0001               L304_value:
 832  0001 00            	ds.b	1
 934                     ; 62 void MFRC_ReadRegs(MfrcRegTypedef MfrcReg, uint8_t count, uint8_t* values, uint8_t rxAlign)
 934                     ; 63 {
 935                     	switch	.text
 936  00f6               _MFRC_ReadRegs:
 938  00f6 89            	pushw	x
 939  00f7 88            	push	a
 940       00000001      OFST:	set	1
 943                     ; 68 	rxAlign = 0;
 945  00f8 0f08          	clr	(OFST+7,sp)
 946                     ; 70 	if (count == 0) {
 948  00fa 9f            	ld	a,xl
 949  00fb 4d            	tnz	a
 950  00fc 2603          	jrne	L62
 951  00fe cc01a3        	jp	L42
 952  0101               L62:
 953                     ; 71 		return;
 955                     ; 74 	address = 0x80 | MfrcReg;				// MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
 957  0101 7b02          	ld	a,(OFST+1,sp)
 958  0103 aa80          	or	a,#128
 959  0105 b700          	ld	L573_address,a
 960                     ; 75 	index = 0;							// Index in values array.
 962  0107 3f01          	clr	L773_index
 963                     ; 77 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin); //digitalWrite(_chipSelectPin, LOW);		// Select slave
 965  0109 4b10          	push	#16
 966  010b ae500f        	ldw	x,#20495
 967  010e cd0000        	call	_GPIO_ResetBits
 969  0111 84            	pop	a
 970                     ; 78 	count--;								// One read is performed outside of the loop
 972  0112 0a03          	dec	(OFST+2,sp)
 974  0114               L364:
 975                     ; 80 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 977  0114 4b02          	push	#2
 978  0116 ae5200        	ldw	x,#20992
 979  0119 cd0000        	call	_SPI_GetFlagStatus
 981  011c 5b01          	addw	sp,#1
 982  011e 4d            	tnz	a
 983  011f 27f3          	jreq	L364
 984                     ; 81 	SPI_SendData(SPI1, address);
 986  0121 3b0000        	push	L573_address
 987  0124 ae5200        	ldw	x,#20992
 988  0127 cd0000        	call	_SPI_SendData
 990  012a 84            	pop	a
 991                     ; 83 	if (rxAlign) {		// Only update bit positions rxAlign..7 in values[0]
 993  012b 0d08          	tnz	(OFST+7,sp)
 994  012d 2753          	jreq	L374
 995                     ; 85 		mask = (0xFF << rxAlign) & 0xFF;
 997  012f 7b08          	ld	a,(OFST+7,sp)
 998  0131 5f            	clrw	x
 999  0132 97            	ld	xl,a
1000  0133 a6ff          	ld	a,#255
1001  0135 5d            	tnzw	x
1002  0136 2704          	jreq	L41
1003  0138               L61:
1004  0138 48            	sll	a
1005  0139 5a            	decw	x
1006  013a 26fc          	jrne	L61
1007  013c               L41:
1008  013c a4ff          	and	a,#255
1009  013e b700          	ld	L104_mask,a
1010                     ; 87 		value = SPI_Transceive(address);
1012  0140 b600          	ld	a,L573_address
1013  0142 cd0000        	call	_SPI_Transceive
1015  0145 b701          	ld	L304_value,a
1016                     ; 89 		values[0] = (values[0] & ~mask) | (value & mask);
1018  0147 b601          	ld	a,L304_value
1019  0149 b400          	and	a,L104_mask
1020  014b 6b01          	ld	(OFST+0,sp),a
1022  014d b600          	ld	a,L104_mask
1023  014f 43            	cpl	a
1024  0150 1e06          	ldw	x,(OFST+5,sp)
1025  0152 f4            	and	a,(x)
1026  0153 1a01          	or	a,(OFST+0,sp)
1027  0155 1e06          	ldw	x,(OFST+5,sp)
1028  0157 f7            	ld	(x),a
1029                     ; 90 		values[0] = (values[0] & ~mask) | (value & mask);
1031  0158 b601          	ld	a,L304_value
1032  015a b400          	and	a,L104_mask
1033  015c 6b01          	ld	(OFST+0,sp),a
1035  015e b600          	ld	a,L104_mask
1036  0160 43            	cpl	a
1037  0161 1e06          	ldw	x,(OFST+5,sp)
1038  0163 f4            	and	a,(x)
1039  0164 1a01          	or	a,(OFST+0,sp)
1040  0166 1e06          	ldw	x,(OFST+5,sp)
1041  0168 f7            	ld	(x),a
1042                     ; 91 		index++;
1044  0169 3c01          	inc	L773_index
1045  016b 2015          	jra	L374
1046  016d               L174:
1047                     ; 94 		values[index] = SPI_Transceive(address);	// Read value and tell that we want to read the same address again.
1049  016d 7b06          	ld	a,(OFST+5,sp)
1050  016f 97            	ld	xl,a
1051  0170 7b07          	ld	a,(OFST+6,sp)
1052  0172 bb01          	add	a,L773_index
1053  0174 2401          	jrnc	L02
1054  0176 5c            	incw	x
1055  0177               L02:
1056  0177 02            	rlwa	x,a
1057  0178 89            	pushw	x
1058  0179 b600          	ld	a,L573_address
1059  017b cd0000        	call	_SPI_Transceive
1061  017e 85            	popw	x
1062  017f f7            	ld	(x),a
1063                     ; 95 		index++;
1065  0180 3c01          	inc	L773_index
1066  0182               L374:
1067                     ; 93 	while (index < count) {
1069  0182 b601          	ld	a,L773_index
1070  0184 1103          	cp	a,(OFST+2,sp)
1071  0186 25e5          	jrult	L174
1072                     ; 97 	values[index] = SPI_Transceive(0);			// Read the final uint8_t. Send 0 to stop reading.
1074  0188 7b06          	ld	a,(OFST+5,sp)
1075  018a 97            	ld	xl,a
1076  018b 7b07          	ld	a,(OFST+6,sp)
1077  018d bb01          	add	a,L773_index
1078  018f 2401          	jrnc	L22
1079  0191 5c            	incw	x
1080  0192               L22:
1081  0192 02            	rlwa	x,a
1082  0193 89            	pushw	x
1083  0194 4f            	clr	a
1084  0195 cd0000        	call	_SPI_Transceive
1086  0198 85            	popw	x
1087  0199 f7            	ld	(x),a
1088                     ; 98 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
1090  019a 4b10          	push	#16
1091  019c ae500f        	ldw	x,#20495
1092  019f cd0000        	call	_GPIO_SetBits
1094  01a2 84            	pop	a
1095                     ; 100 }
1096  01a3               L42:
1099  01a3 5b03          	addw	sp,#3
1100  01a5 81            	ret
1155                     ; 105 void MFRC_SetRegBitMask(MfrcRegTypedef MfrcReg, uint8_t mask)
1155                     ; 106 {
1156                     	switch	.text
1157  01a6               _MFRC_SetRegBitMask:
1159  01a6 89            	pushw	x
1160  01a7 88            	push	a
1161       00000001      OFST:	set	1
1164                     ; 108 	tmp = MFRC_ReadReg(MfrcReg);
1166  01a8 9e            	ld	a,xh
1167  01a9 cd00b2        	call	_MFRC_ReadReg
1169  01ac 6b01          	ld	(OFST+0,sp),a
1171                     ; 109 	MFRC_WriteReg(MfrcReg, tmp & (~mask));		// clear bit mask
1173  01ae 7b03          	ld	a,(OFST+2,sp)
1174  01b0 43            	cpl	a
1175  01b1 1401          	and	a,(OFST+0,sp)
1176  01b3 97            	ld	xl,a
1177  01b4 7b02          	ld	a,(OFST+1,sp)
1178  01b6 95            	ld	xh,a
1179  01b7 cd0000        	call	_MFRC_WriteReg
1181                     ; 110 }
1184  01ba 5b03          	addw	sp,#3
1185  01bc 81            	ret
1241                     ; 115 void MFRC_ClearRegBitMask(MfrcRegTypedef MfrcReg, uint8_t mask)
1241                     ; 116 {
1242                     	switch	.text
1243  01bd               _MFRC_ClearRegBitMask:
1245  01bd 89            	pushw	x
1246  01be 88            	push	a
1247       00000001      OFST:	set	1
1250                     ; 118 	tmp = MFRC_ReadReg(MfrcReg);
1252  01bf 9e            	ld	a,xh
1253  01c0 cd00b2        	call	_MFRC_ReadReg
1255  01c3 6b01          	ld	(OFST+0,sp),a
1257                     ; 119 	MFRC_WriteReg(MfrcReg, tmp & (~mask));		// clear bit mask
1259  01c5 7b03          	ld	a,(OFST+2,sp)
1260  01c7 43            	cpl	a
1261  01c8 1401          	and	a,(OFST+0,sp)
1262  01ca 97            	ld	xl,a
1263  01cb 7b02          	ld	a,(OFST+1,sp)
1264  01cd 95            	ld	xh,a
1265  01ce cd0000        	call	_MFRC_WriteReg
1267                     ; 120 }
1270  01d1 5b03          	addw	sp,#3
1271  01d3 81            	ret
1417                     ; 127 StatusCode MFRC_CalculateCRC(uint8_t *data, uint8_t length, uint8_t* result)
1417                     ; 128 {
1418                     	switch	.text
1419  01d4               _MFRC_CalculateCRC:
1421  01d4 89            	pushw	x
1422  01d5 5203          	subw	sp,#3
1423       00000003      OFST:	set	3
1426                     ; 130 	MFRC_WriteReg(CommandReg, MFRC_Idle);		// Stop any active command.
1428  01d7 ae0200        	ldw	x,#512
1429  01da cd0000        	call	_MFRC_WriteReg
1431                     ; 131 	MFRC_WriteReg(DivIrqReg, 0x04);				// Clear the CRCIRq interrupt request bit
1433  01dd ae0a04        	ldw	x,#2564
1434  01e0 cd0000        	call	_MFRC_WriteReg
1436                     ; 132 	MFRC_WriteReg(FIFOLevelReg, 0x80);			// FlushBuffer = 1, FIFO initialization
1438  01e3 ae1480        	ldw	x,#5248
1439  01e6 cd0000        	call	_MFRC_WriteReg
1441                     ; 133 	MFRC_WriteRegs(FIFODataReg, data, length);	// Write data to the FIFO
1443  01e9 7b08          	ld	a,(OFST+5,sp)
1444  01eb 88            	push	a
1445  01ec 1e05          	ldw	x,(OFST+2,sp)
1446  01ee 89            	pushw	x
1447  01ef a612          	ld	a,#18
1448  01f1 cd0050        	call	_MFRC_WriteRegs
1450  01f4 5b03          	addw	sp,#3
1451                     ; 134 	MFRC_WriteReg(CommandReg, MFRC_CalcCRC);		// Start the calculation
1453  01f6 ae0203        	ldw	x,#515
1454  01f9 cd0000        	call	_MFRC_WriteReg
1456                     ; 140 	for (i = 5000; i > 0; i--) {
1458  01fc ae1388        	ldw	x,#5000
1459  01ff 1f02          	ldw	(OFST-1,sp),x
1461  0201               L736:
1462                     ; 142 		uint8_t n = MFRC_ReadReg(DivIrqReg);
1464  0201 a60a          	ld	a,#10
1465  0203 cd00b2        	call	_MFRC_ReadReg
1467  0206 6b01          	ld	(OFST-2,sp),a
1469                     ; 143 		if (n & 0x04) {									// CRCIRq bit set - calculation done
1471  0208 7b01          	ld	a,(OFST-2,sp)
1472  020a a504          	bcp	a,#4
1473  020c 271a          	jreq	L546
1474                     ; 144 			MFRC_WriteReg(CommandReg, MFRC_Idle);	// Stop calculating CRC for new content in the FIFO.
1476  020e ae0200        	ldw	x,#512
1477  0211 cd0000        	call	_MFRC_WriteReg
1479                     ; 146 			result[0] = MFRC_ReadReg(CRCResultRegL);
1481  0214 a644          	ld	a,#68
1482  0216 cd00b2        	call	_MFRC_ReadReg
1484  0219 1e09          	ldw	x,(OFST+6,sp)
1485  021b f7            	ld	(x),a
1486                     ; 147 			result[1] = MFRC_ReadReg(CRCResultRegH);
1488  021c a642          	ld	a,#66
1489  021e cd00b2        	call	_MFRC_ReadReg
1491  0221 1e09          	ldw	x,(OFST+6,sp)
1492  0223 e701          	ld	(1,x),a
1493                     ; 148 			return STATUS_OK;
1495  0225 4f            	clr	a
1497  0226 200d          	jra	L63
1498  0228               L546:
1499                     ; 140 	for (i = 5000; i > 0; i--) {
1501  0228 1e02          	ldw	x,(OFST-1,sp)
1502  022a 1d0001        	subw	x,#1
1503  022d 1f02          	ldw	(OFST-1,sp),x
1507  022f 1e02          	ldw	x,(OFST-1,sp)
1508  0231 26ce          	jrne	L736
1509                     ; 152 	return STATUS_TIMEOUT;
1511  0233 a603          	ld	a,#3
1513  0235               L63:
1515  0235 5b05          	addw	sp,#5
1516  0237 81            	ret
1577                     ; 162 void MFRC_Init(void) 
1577                     ; 163 {
1578                     	switch	.text
1579  0238               _MFRC_Init:
1581  0238 88            	push	a
1582       00000001      OFST:	set	1
1585                     ; 165 	hardReset = 0;
1587                     ; 168 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
1589  0239 4b10          	push	#16
1590  023b ae500f        	ldw	x,#20495
1591  023e cd0000        	call	_GPIO_SetBits
1593  0241 84            	pop	a
1594                     ; 176 			GPIO_ResetBits(GPIOD, MFRC_RST_Pin);		// Make shure we have a clean LOW state.
1596  0242 4b20          	push	#32
1597  0244 ae500f        	ldw	x,#20495
1598  0247 cd0000        	call	_GPIO_ResetBits
1600  024a 84            	pop	a
1601                     ; 178 			GPIO_SetBits(GPIOD, MFRC_RST_Pin);		// Exit power down mode. This triggers a hard reset.
1603  024b 4b20          	push	#32
1604  024d ae500f        	ldw	x,#20495
1605  0250 cd0000        	call	_GPIO_SetBits
1607  0253 84            	pop	a
1608                     ; 180 			delay_us(1); //delay(50);
1610  0254 ae0001        	ldw	x,#1
1611  0257 89            	pushw	x
1612  0258 ae0000        	ldw	x,#0
1613  025b 89            	pushw	x
1614  025c cd0000        	call	_delay_us
1616  025f 5b04          	addw	sp,#4
1617                     ; 181 			hardReset = 1;
1619                     ; 190 	MFRC_WriteReg(TxModeReg, 0x00);
1621  0261 ae2400        	ldw	x,#9216
1622  0264 cd0000        	call	_MFRC_WriteReg
1624                     ; 191 	MFRC_WriteReg(RxModeReg, 0x00);
1626  0267 ae2600        	ldw	x,#9728
1627  026a cd0000        	call	_MFRC_WriteReg
1629                     ; 193 	MFRC_WriteReg(ModWidthReg, 0x26);
1631  026d ae4826        	ldw	x,#18470
1632  0270 cd0000        	call	_MFRC_WriteReg
1634                     ; 198 	MFRC_WriteReg(TModeReg, 0x80);			// TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
1636  0273 ae5480        	ldw	x,#21632
1637  0276 cd0000        	call	_MFRC_WriteReg
1639                     ; 199 	MFRC_WriteReg(TPrescalerReg, 0xA9);		// TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25?s.
1641  0279 ae56a9        	ldw	x,#22185
1642  027c cd0000        	call	_MFRC_WriteReg
1644                     ; 200 	MFRC_WriteReg(TReloadRegH, 0x03);		// Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
1646  027f ae5803        	ldw	x,#22531
1647  0282 cd0000        	call	_MFRC_WriteReg
1649                     ; 201 	MFRC_WriteReg(TReloadRegL, 0xE8);
1651  0285 ae5ae8        	ldw	x,#23272
1652  0288 cd0000        	call	_MFRC_WriteReg
1654                     ; 203 	MFRC_WriteReg(TxASKReg, 0x40);		// Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
1656  028b ae2a40        	ldw	x,#10816
1657  028e cd0000        	call	_MFRC_WriteReg
1659                     ; 204 	MFRC_WriteReg(ModeReg, 0x3D);		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
1661  0291 ae223d        	ldw	x,#8765
1662  0294 cd0000        	call	_MFRC_WriteReg
1664                     ; 205 	MFRC_AntennaOn();						// Enable the antenna driver pins TX1 and TX2 (they were disabled by the reset)
1666  0297 ad1a          	call	_MFRC_AntennaOn
1668                     ; 206 } // End MFRC_Init()
1671  0299 84            	pop	a
1672  029a 81            	ret
1675                     	bsct
1676  0002               _count:
1677  0002 00            	dc.b	0
1701                     ; 213 void MFRC_Reset(void) 
1701                     ; 214 {
1702                     	switch	.text
1703  029b               _MFRC_Reset:
1707                     ; 215 	MFRC_WriteReg(CommandReg, MFRC_SoftReset);	// Issue the SoftReset command.
1709  029b ae020f        	ldw	x,#527
1710  029e cd0000        	call	_MFRC_WriteReg
1712  02a1               L707:
1713                     ; 223 	} while ((MFRC_ReadReg(CommandReg) & (1 << 4)) && (++count) < 3);
1715  02a1 a602          	ld	a,#2
1716  02a3 cd00b2        	call	_MFRC_ReadReg
1718  02a6 a510          	bcp	a,#16
1719  02a8 2708          	jreq	L317
1721  02aa 3c02          	inc	_count
1722  02ac b602          	ld	a,_count
1723  02ae a103          	cp	a,#3
1724  02b0 25ef          	jrult	L707
1725  02b2               L317:
1726                     ; 224 } // End MFRC_Reset()
1729  02b2 81            	ret
1765                     ; 230 void MFRC_AntennaOn() 
1765                     ; 231 {
1766                     	switch	.text
1767  02b3               _MFRC_AntennaOn:
1769  02b3 88            	push	a
1770       00000001      OFST:	set	1
1773                     ; 232 	uint8_t value = MFRC_ReadReg(TxControlReg);
1775  02b4 a628          	ld	a,#40
1776  02b6 cd00b2        	call	_MFRC_ReadReg
1778  02b9 6b01          	ld	(OFST+0,sp),a
1780                     ; 233 	if ((value & 0x03) != 0x03) {
1782  02bb 7b01          	ld	a,(OFST+0,sp)
1783  02bd a403          	and	a,#3
1784  02bf a103          	cp	a,#3
1785  02c1 270b          	jreq	L337
1786                     ; 234 		MFRC_WriteReg(TxControlReg, value | 0x03);
1788  02c3 7b01          	ld	a,(OFST+0,sp)
1789  02c5 aa03          	or	a,#3
1790  02c7 ae2800        	ldw	x,#10240
1791  02ca 97            	ld	xl,a
1792  02cb cd0000        	call	_MFRC_WriteReg
1794  02ce               L337:
1795                     ; 236 } // End MFRC_AntennaOn()
1798  02ce 84            	pop	a
1799  02cf 81            	ret
1904                     ; 244 StatusCode MFRC_TransceiveData(uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
1904                     ; 245 													uint8_t sendLen,		///< Number of uint8_ts to transfer to the FIFO.
1904                     ; 246 													uint8_t *backData,		///< nullptr or pointer to buffer if data should be read back after executing the command.
1904                     ; 247 													uint8_t *backLen,		///< In: Max number of uint8_ts to write to *backData. Out: The number of uint8_ts returned.
1904                     ; 248 													uint8_t *validBits,	///< In/Out: The number of valid bits in the last uint8_t. 0 for 8 valid bits. Default nullptr.
1904                     ; 249 													uint8_t rxAlign,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
1904                     ; 250 													bool checkCRC		///< In: True => The last two uint8_ts of the response is assumed to be a CRC_A that must be validated.
1904                     ; 251 								 ) 
1904                     ; 252 {
1905                     	switch	.text
1906  02d0               _MFRC_TransceiveData:
1908  02d0 89            	pushw	x
1909  02d1 88            	push	a
1910       00000001      OFST:	set	1
1913                     ; 254 	rxAlign = 0, 
1913                     ; 255 	checkCRC = 0;
1915  02d2 0f0d          	clr	(OFST+12,sp)
1916  02d4 0f0e          	clr	(OFST+13,sp)
1917                     ; 256 	*validBits = 0;
1919  02d6 1e0b          	ldw	x,(OFST+10,sp)
1920  02d8 7f            	clr	(x)
1921                     ; 258 	waitIRq = 0x30;		// RxIRq and IdleIRq
1923                     ; 259 	return MFRC_CommunicateWithPICC(MFRC_Transceive, waitIRq, sendData, sendLen, backData, backLen, validBits, rxAlign, checkCRC);
1925  02d9 4b00          	push	#0
1926  02db 4b00          	push	#0
1927  02dd 1e0d          	ldw	x,(OFST+12,sp)
1928  02df 89            	pushw	x
1929  02e0 1e0d          	ldw	x,(OFST+12,sp)
1930  02e2 89            	pushw	x
1931  02e3 1e0d          	ldw	x,(OFST+12,sp)
1932  02e5 89            	pushw	x
1933  02e6 7b0e          	ld	a,(OFST+13,sp)
1934  02e8 88            	push	a
1935  02e9 1e0b          	ldw	x,(OFST+10,sp)
1936  02eb 89            	pushw	x
1937  02ec ae0c30        	ldw	x,#3120
1938  02ef cd07ba        	call	_MFRC_CommunicateWithPICC
1940  02f2 5b0b          	addw	sp,#11
1943  02f4 5b03          	addw	sp,#3
1944  02f6 81            	ret
1991                     ; 268 StatusCode PICC_RequestA(uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
1991                     ; 269 												uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
1991                     ; 270 										) 
1991                     ; 271 {
1992                     	switch	.text
1993  02f7               _PICC_RequestA:
1995  02f7 89            	pushw	x
1996       00000000      OFST:	set	0
1999                     ; 272 	return PICC_REQA_or_WUPA(PICC_CMD_REQA, bufferATQA, bufferSize);
2001  02f8 1e05          	ldw	x,(OFST+5,sp)
2002  02fa 89            	pushw	x
2003  02fb 1e03          	ldw	x,(OFST+3,sp)
2004  02fd 89            	pushw	x
2005  02fe a626          	ld	a,#38
2006  0300 ad13          	call	_PICC_REQA_or_WUPA
2008  0302 5b04          	addw	sp,#4
2011  0304 85            	popw	x
2012  0305 81            	ret
2059                     ; 281 StatusCode PICC_WakeupA(	uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
2059                     ; 282 							uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
2059                     ; 283 										) 
2059                     ; 284 {
2060                     	switch	.text
2061  0306               _PICC_WakeupA:
2063  0306 89            	pushw	x
2064       00000000      OFST:	set	0
2067                     ; 285 	return PICC_REQA_or_WUPA(PICC_CMD_WUPA, bufferATQA, bufferSize);
2069  0307 1e05          	ldw	x,(OFST+5,sp)
2070  0309 89            	pushw	x
2071  030a 1e03          	ldw	x,(OFST+3,sp)
2072  030c 89            	pushw	x
2073  030d a652          	ld	a,#82
2074  030f ad04          	call	_PICC_REQA_or_WUPA
2076  0311 5b04          	addw	sp,#4
2079  0313 85            	popw	x
2080  0314 81            	ret
2156                     ; 294 StatusCode PICC_REQA_or_WUPA(	uint8_t command, 		///< The command to send - PICC_CMD_REQA or PICC_CMD_WUPA
2156                     ; 295 															uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
2156                     ; 296 															uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
2156                     ; 297 											) 
2156                     ; 298 {
2157                     	switch	.text
2158  0315               _PICC_REQA_or_WUPA:
2160  0315 88            	push	a
2161  0316 89            	pushw	x
2162       00000002      OFST:	set	2
2165                     ; 302 	if ((bufferATQA == 0) || (*bufferSize < 2)) {	// The ATQA response is 2 bytes long.
2167  0317 1e06          	ldw	x,(OFST+4,sp)
2168  0319 2707          	jreq	L3111
2170  031b 1e08          	ldw	x,(OFST+6,sp)
2171  031d f6            	ld	a,(x)
2172  031e a102          	cp	a,#2
2173  0320 2404          	jruge	L1111
2174  0322               L3111:
2175                     ; 303 		return STATUS_NO_ROOM;
2177  0322 a604          	ld	a,#4
2179  0324 202b          	jra	L65
2180  0326               L1111:
2181                     ; 305 	MFRC_ClearRegBitMask(CollReg, 0x80);		// ValuesAfterColl=1 => Bits received after collision are cleared.
2183  0326 ae1c80        	ldw	x,#7296
2184  0329 cd01bd        	call	_MFRC_ClearRegBitMask
2186                     ; 306 	validBits = 7;									// For REQA and WUPA we need the short frame format - transmit only 7 bits of the last (and only) byte. TxLastBits = BitFramingReg[2..0]
2188  032c a607          	ld	a,#7
2189  032e 6b01          	ld	(OFST-1,sp),a
2191                     ; 307 	status = MFRC_TransceiveData(&command, 1, bufferATQA, bufferSize, &validBits, 0, 0);
2193  0330 4b00          	push	#0
2194  0332 4b00          	push	#0
2195  0334 96            	ldw	x,sp
2196  0335 1c0003        	addw	x,#OFST+1
2197  0338 89            	pushw	x
2198  0339 1e0c          	ldw	x,(OFST+10,sp)
2199  033b 89            	pushw	x
2200  033c 1e0c          	ldw	x,(OFST+10,sp)
2201  033e 89            	pushw	x
2202  033f 4b01          	push	#1
2203  0341 96            	ldw	x,sp
2204  0342 1c000c        	addw	x,#OFST+10
2205  0345 ad89          	call	_MFRC_TransceiveData
2207  0347 5b09          	addw	sp,#9
2208  0349 6b02          	ld	(OFST+0,sp),a
2210                     ; 308 	if (status != STATUS_OK) {
2212  034b 0d02          	tnz	(OFST+0,sp)
2213  034d 2705          	jreq	L5111
2214                     ; 309 		return status;
2216  034f 7b02          	ld	a,(OFST+0,sp)
2218  0351               L65:
2220  0351 5b03          	addw	sp,#3
2221  0353 81            	ret
2222  0354               L5111:
2223                     ; 311 	if ((*bufferSize != 2) || (validBits != 0)) {		// ATQA must be exactly 16 bits.
2225  0354 1e08          	ldw	x,(OFST+6,sp)
2226  0356 f6            	ld	a,(x)
2227  0357 a102          	cp	a,#2
2228  0359 2604          	jrne	L1211
2230  035b 0d01          	tnz	(OFST-1,sp)
2231  035d 2704          	jreq	L7111
2232  035f               L1211:
2233                     ; 312 		return STATUS_ERROR;
2235  035f a601          	ld	a,#1
2237  0361 20ee          	jra	L65
2238  0363               L7111:
2239                     ; 314 	return STATUS_OK;
2241  0363 4f            	clr	a
2243  0364 20eb          	jra	L65
2513                     ; 334 StatusCode PICC_Select(	Uid_str *uid0,			///< Pointer to Uid struct. Normally output, but can also be used to supply a known UID.
2513                     ; 335 												uint8_t validBits		///< The number of known UID bits supplied in *uid. Normally 0. If set you must also supply uid->size.
2513                     ; 336 										 ) 
2513                     ; 337 {
2514                     	switch	.text
2515  0366               _PICC_Select:
2517  0366 89            	pushw	x
2518  0367 5218          	subw	sp,#24
2519       00000018      OFST:	set	24
2522                     ; 341 	uint8_t cascadeLevel = 1;
2524  0369 a601          	ld	a,#1
2525  036b 6b04          	ld	(OFST-20,sp),a
2527                     ; 382 	if (validBits > 80) {
2529  036d 7b1d          	ld	a,(OFST+5,sp)
2530  036f a151          	cp	a,#81
2531  0371 2504          	jrult	L3131
2532                     ; 383 		return STATUS_INVALID;
2534  0373 a606          	ld	a,#6
2536  0375 2019          	jra	L261
2537  0377               L3131:
2538                     ; 387 	MFRC_ClearRegBitMask(CollReg, 0x80);		// ValuesAfterColl=1 => Bits received after collision are cleared.
2540  0377 ae1c80        	ldw	x,#7296
2541  037a cd01bd        	call	_MFRC_ClearRegBitMask
2543                     ; 390 	uidComplete = 0;
2545  037d 0f03          	clr	(OFST-21,sp)
2548  037f acd206d2      	jpf	L7131
2549  0383               L5131:
2550                     ; 393 		switch (cascadeLevel) {
2552  0383 7b04          	ld	a,(OFST-20,sp)
2554                     ; 414 				break;
2556  0385 4a            	dec	a
2557  0386 270b          	jreq	L3211
2558  0388 4a            	dec	a
2559  0389 2722          	jreq	L5211
2560  038b 4a            	dec	a
2561  038c 273b          	jreq	L7211
2562  038e               L1311:
2563                     ; 412 			default:
2563                     ; 413 				return STATUS_INTERNAL_ERROR;
2565  038e a605          	ld	a,#5
2567  0390               L261:
2569  0390 5b1a          	addw	sp,#26
2570  0392 81            	ret
2571  0393               L3211:
2572                     ; 394 			case 1:
2572                     ; 395 				buffer[0] = PICC_CMD_SEL_CL1;
2574  0393 a693          	ld	a,#147
2575  0395 6b0f          	ld	(OFST-9,sp),a
2577                     ; 396 				uidIndex = 0;
2579  0397 0f07          	clr	(OFST-17,sp)
2581                     ; 397 				useCascadeTag = validBits && uid0->size > 4;	// When we know that the UID has more than 4 bytes
2583  0399 0d1d          	tnz	(OFST+5,sp)
2584  039b 270b          	jreq	L26
2585  039d 1e19          	ldw	x,(OFST+1,sp)
2586  039f f6            	ld	a,(x)
2587  03a0 a105          	cp	a,#5
2588  03a2 2504          	jrult	L26
2589  03a4 a601          	ld	a,#1
2590  03a6 2001          	jra	L46
2591  03a8               L26:
2592  03a8 4f            	clr	a
2593  03a9               L46:
2594  03a9 6b0d          	ld	(OFST-11,sp),a
2596                     ; 398 				break;
2598  03ab 2026          	jra	L5231
2599  03ad               L5211:
2600                     ; 400 			case 2:
2600                     ; 401 				buffer[0] = PICC_CMD_SEL_CL2;
2602  03ad a695          	ld	a,#149
2603  03af 6b0f          	ld	(OFST-9,sp),a
2605                     ; 402 				uidIndex = 3;
2607  03b1 a603          	ld	a,#3
2608  03b3 6b07          	ld	(OFST-17,sp),a
2610                     ; 403 				useCascadeTag = validBits && uid0->size > 7;	// When we know that the UID has more than 7 bytes
2612  03b5 0d1d          	tnz	(OFST+5,sp)
2613  03b7 270b          	jreq	L66
2614  03b9 1e19          	ldw	x,(OFST+1,sp)
2615  03bb f6            	ld	a,(x)
2616  03bc a108          	cp	a,#8
2617  03be 2504          	jrult	L66
2618  03c0 a601          	ld	a,#1
2619  03c2 2001          	jra	L07
2620  03c4               L66:
2621  03c4 4f            	clr	a
2622  03c5               L07:
2623  03c5 6b0d          	ld	(OFST-11,sp),a
2625                     ; 404 				break;
2627  03c7 200a          	jra	L5231
2628  03c9               L7211:
2629                     ; 406 			case 3:
2629                     ; 407 				buffer[0] = PICC_CMD_SEL_CL3;
2631  03c9 a697          	ld	a,#151
2632  03cb 6b0f          	ld	(OFST-9,sp),a
2634                     ; 408 				uidIndex = 6;
2636  03cd a606          	ld	a,#6
2637  03cf 6b07          	ld	(OFST-17,sp),a
2639                     ; 409 				useCascadeTag = 0;						// Never used in CL3.
2641  03d1 0f0d          	clr	(OFST-11,sp)
2643                     ; 410 				break;
2645  03d3               L5231:
2646                     ; 418 		currentLevelKnownBits = validBits - (8 * uidIndex);
2648  03d3 7b07          	ld	a,(OFST-17,sp)
2649  03d5 48            	sll	a
2650  03d6 48            	sll	a
2651  03d7 48            	sll	a
2652  03d8 101d          	sub	a,(OFST+5,sp)
2653  03da 40            	neg	a
2654  03db 6b0e          	ld	(OFST-10,sp),a
2656                     ; 419 		if (currentLevelKnownBits < 0) {
2658  03dd 9c            	rvf
2659  03de 7b0e          	ld	a,(OFST-10,sp)
2660  03e0 a100          	cp	a,#0
2661  03e2 2e02          	jrsge	L7231
2662                     ; 420 			currentLevelKnownBits = 0;
2664  03e4 0f0e          	clr	(OFST-10,sp)
2666  03e6               L7231:
2667                     ; 423 		index = 2; // destination index in buffer[]
2669  03e6 a602          	ld	a,#2
2670  03e8 6b18          	ld	(OFST+0,sp),a
2672                     ; 424 		if (useCascadeTag) {
2674  03ea 0d0d          	tnz	(OFST-11,sp)
2675  03ec 2714          	jreq	L1331
2676                     ; 425 			buffer[index++] = PICC_CMD_CT;
2678  03ee 96            	ldw	x,sp
2679  03ef 1c000f        	addw	x,#OFST-9
2680  03f2 1f01          	ldw	(OFST-23,sp),x
2682  03f4 7b18          	ld	a,(OFST+0,sp)
2683  03f6 97            	ld	xl,a
2684  03f7 0c18          	inc	(OFST+0,sp)
2686  03f9 9f            	ld	a,xl
2687  03fa 5f            	clrw	x
2688  03fb 97            	ld	xl,a
2689  03fc 72fb01        	addw	x,(OFST-23,sp)
2690  03ff a688          	ld	a,#136
2691  0401 f7            	ld	(x),a
2692  0402               L1331:
2693                     ; 427 		bytesToCopy = currentLevelKnownBits / 8 + (currentLevelKnownBits % 8 ? 1 : 0); // The number of bytes needed to represent the known bits for this level.
2695  0402 7b0e          	ld	a,(OFST-10,sp)
2696  0404 5f            	clrw	x
2697  0405 4d            	tnz	a
2698  0406 2a01          	jrpl	L47
2699  0408 53            	cplw	x
2700  0409               L47:
2701  0409 97            	ld	xl,a
2702  040a a608          	ld	a,#8
2703  040c cd0000        	call	c_smodx
2705  040f a30000        	cpw	x,#0
2706  0412 2704          	jreq	L27
2707  0414 a601          	ld	a,#1
2708  0416 2001          	jra	L67
2709  0418               L27:
2710  0418 4f            	clr	a
2711  0419               L67:
2712  0419 6b02          	ld	(OFST-22,sp),a
2714  041b 7b0e          	ld	a,(OFST-10,sp)
2715  041d ae0008        	ldw	x,#8
2716  0420 51            	exgw	x,y
2717  0421 5f            	clrw	x
2718  0422 4d            	tnz	a
2719  0423 2a01          	jrpl	L001
2720  0425 5a            	decw	x
2721  0426               L001:
2722  0426 02            	rlwa	x,a
2723  0427 cd0000        	call	c_idiv
2725  042a 9f            	ld	a,xl
2726  042b 1b02          	add	a,(OFST-22,sp)
2727  042d 6b0b          	ld	(OFST-13,sp),a
2729                     ; 428 		if (bytesToCopy) {
2731  042f 0d0b          	tnz	(OFST-13,sp)
2732  0431 2748          	jreq	L3331
2733                     ; 429 			maxBytes = useCascadeTag ? 3 : 4; // Max 4 bytes in each Cascade Level. Only 3 left if we use the Cascade Tag
2735  0433 0d0d          	tnz	(OFST-11,sp)
2736  0435 2704          	jreq	L201
2737  0437 a603          	ld	a,#3
2738  0439 2002          	jra	L401
2739  043b               L201:
2740  043b a604          	ld	a,#4
2741  043d               L401:
2742  043d 6b06          	ld	(OFST-18,sp),a
2744                     ; 430 			if (bytesToCopy > maxBytes) {
2746  043f 7b0b          	ld	a,(OFST-13,sp)
2747  0441 1106          	cp	a,(OFST-18,sp)
2748  0443 2304          	jrule	L5331
2749                     ; 431 				bytesToCopy = maxBytes;
2751  0445 7b06          	ld	a,(OFST-18,sp)
2752  0447 6b0b          	ld	(OFST-13,sp),a
2754  0449               L5331:
2755                     ; 433 			for (count = 0; count < bytesToCopy; count++) {
2757  0449 0f0c          	clr	(OFST-12,sp)
2760  044b 2028          	jra	L3431
2761  044d               L7331:
2762                     ; 434 				buffer[index++] = uid0->uidByte[uidIndex + count];
2764  044d 96            	ldw	x,sp
2765  044e 1c000f        	addw	x,#OFST-9
2766  0451 1f01          	ldw	(OFST-23,sp),x
2768  0453 7b18          	ld	a,(OFST+0,sp)
2769  0455 97            	ld	xl,a
2770  0456 0c18          	inc	(OFST+0,sp)
2772  0458 9f            	ld	a,xl
2773  0459 5f            	clrw	x
2774  045a 97            	ld	xl,a
2775  045b 72fb01        	addw	x,(OFST-23,sp)
2776  045e 89            	pushw	x
2777  045f 7b1b          	ld	a,(OFST+3,sp)
2778  0461 97            	ld	xl,a
2779  0462 7b1c          	ld	a,(OFST+4,sp)
2780  0464 1b0e          	add	a,(OFST-10,sp)
2781  0466 2401          	jrnc	L601
2782  0468 5c            	incw	x
2783  0469               L601:
2784  0469 1b09          	add	a,(OFST-15,sp)
2785  046b 2401          	jrnc	L011
2786  046d 5c            	incw	x
2787  046e               L011:
2788  046e 02            	rlwa	x,a
2789  046f e601          	ld	a,(1,x)
2790  0471 85            	popw	x
2791  0472 f7            	ld	(x),a
2792                     ; 433 			for (count = 0; count < bytesToCopy; count++) {
2794  0473 0c0c          	inc	(OFST-12,sp)
2796  0475               L3431:
2799  0475 7b0c          	ld	a,(OFST-12,sp)
2800  0477 110b          	cp	a,(OFST-13,sp)
2801  0479 25d2          	jrult	L7331
2802  047b               L3331:
2803                     ; 438 		if (useCascadeTag) {
2805  047b 0d0d          	tnz	(OFST-11,sp)
2806  047d 2706          	jreq	L7431
2807                     ; 439 			currentLevelKnownBits += 8;
2809  047f 7b0e          	ld	a,(OFST-10,sp)
2810  0481 ab08          	add	a,#8
2811  0483 6b0e          	ld	(OFST-10,sp),a
2813  0485               L7431:
2814                     ; 443 		selectDone = 0;
2816  0485 0f06          	clr	(OFST-18,sp)
2818  0487               L1531:
2819                     ; 446 			if (currentLevelKnownBits >= 32) { // All UID bits in this Cascade Level are known. This is a SELECT.
2821  0487 9c            	rvf
2822  0488 7b0e          	ld	a,(OFST-10,sp)
2823  048a a120          	cp	a,#32
2824  048c 2f3c          	jrslt	L7531
2825                     ; 448 				buffer[1] = 0x70; // NVB - Number of Valid Bits: Seven whole bytes
2827  048e a670          	ld	a,#112
2828  0490 6b10          	ld	(OFST-8,sp),a
2830                     ; 450 				buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5];
2832  0492 7b11          	ld	a,(OFST-7,sp)
2833  0494 1812          	xor	a,(OFST-6,sp)
2834  0496 1813          	xor	a,(OFST-5,sp)
2835  0498 1814          	xor	a,(OFST-4,sp)
2836  049a 6b15          	ld	(OFST-3,sp),a
2838                     ; 452 				result = MFRC_CalculateCRC(buffer, 7, &buffer[7]);
2840  049c 96            	ldw	x,sp
2841  049d 1c0016        	addw	x,#OFST-2
2842  04a0 89            	pushw	x
2843  04a1 4b07          	push	#7
2844  04a3 96            	ldw	x,sp
2845  04a4 1c0012        	addw	x,#OFST-6
2846  04a7 cd01d4        	call	_MFRC_CalculateCRC
2848  04aa 5b03          	addw	sp,#3
2849  04ac 6b18          	ld	(OFST+0,sp),a
2851                     ; 453 				if (result != STATUS_OK) {
2853  04ae 0d18          	tnz	(OFST+0,sp)
2854  04b0 2706          	jreq	L1631
2855                     ; 454 					return result;
2857  04b2 7b18          	ld	a,(OFST+0,sp)
2859  04b4 ac900390      	jpf	L261
2860  04b8               L1631:
2861                     ; 456 				txLastBits		= 0; // 0 => All 8 bits are valid.
2863  04b8 0f0a          	clr	(OFST-14,sp)
2865                     ; 457 				bufferUsed		= 9;
2867  04ba a609          	ld	a,#9
2868  04bc 6b0b          	ld	(OFST-13,sp),a
2870                     ; 459 				responseBuffer	= &buffer[6];
2872  04be 96            	ldw	x,sp
2873  04bf 1c0015        	addw	x,#OFST-3
2874  04c2 1f08          	ldw	(OFST-16,sp),x
2876                     ; 460 				responseLength	= 3;
2878  04c4 a603          	ld	a,#3
2879  04c6 6b05          	ld	(OFST-19,sp),a
2882  04c8 2058          	jra	L3631
2883  04ca               L7531:
2884                     ; 464 				txLastBits		= currentLevelKnownBits % 8;
2886  04ca 7b0e          	ld	a,(OFST-10,sp)
2887  04cc ae0008        	ldw	x,#8
2888  04cf 51            	exgw	x,y
2889  04d0 5f            	clrw	x
2890  04d1 4d            	tnz	a
2891  04d2 2a01          	jrpl	L211
2892  04d4 5a            	decw	x
2893  04d5               L211:
2894  04d5 02            	rlwa	x,a
2895  04d6 cd0000        	call	c_idiv
2897  04d9 909f          	ld	a,yl
2898  04db 6b0a          	ld	(OFST-14,sp),a
2900                     ; 465 				count			= currentLevelKnownBits / 8;	// Number of whole bytes in the UID part.
2902  04dd 7b0e          	ld	a,(OFST-10,sp)
2903  04df ae0008        	ldw	x,#8
2904  04e2 51            	exgw	x,y
2905  04e3 5f            	clrw	x
2906  04e4 4d            	tnz	a
2907  04e5 2a01          	jrpl	L411
2908  04e7 5a            	decw	x
2909  04e8               L411:
2910  04e8 02            	rlwa	x,a
2911  04e9 cd0000        	call	c_idiv
2913  04ec 9f            	ld	a,xl
2914  04ed 6b0c          	ld	(OFST-12,sp),a
2916                     ; 466 				index			= 2 + count;					// Number of whole bytes: SEL + NVB + UIDs
2918  04ef 7b0c          	ld	a,(OFST-12,sp)
2919  04f1 ab02          	add	a,#2
2920  04f3 6b18          	ld	(OFST+0,sp),a
2922                     ; 467 				buffer[1]		= (index << 4) + txLastBits;	// NVB - Number of Valid Bits
2924  04f5 7b18          	ld	a,(OFST+0,sp)
2925  04f7 97            	ld	xl,a
2926  04f8 a610          	ld	a,#16
2927  04fa 42            	mul	x,a
2928  04fb 9f            	ld	a,xl
2929  04fc 1b0a          	add	a,(OFST-14,sp)
2930  04fe 6b10          	ld	(OFST-8,sp),a
2932                     ; 468 				bufferUsed		= index + (txLastBits ? 1 : 0);
2934  0500 0d0a          	tnz	(OFST-14,sp)
2935  0502 2704          	jreq	L611
2936  0504 a601          	ld	a,#1
2937  0506 2001          	jra	L021
2938  0508               L611:
2939  0508 4f            	clr	a
2940  0509               L021:
2941  0509 1b18          	add	a,(OFST+0,sp)
2942  050b 6b0b          	ld	(OFST-13,sp),a
2944                     ; 470 				responseBuffer	= &buffer[index];
2946  050d 96            	ldw	x,sp
2947  050e 1c000f        	addw	x,#OFST-9
2948  0511 9f            	ld	a,xl
2949  0512 5e            	swapw	x
2950  0513 1b18          	add	a,(OFST+0,sp)
2951  0515 2401          	jrnc	L221
2952  0517 5c            	incw	x
2953  0518               L221:
2954  0518 02            	rlwa	x,a
2955  0519 1f08          	ldw	(OFST-16,sp),x
2956  051b 01            	rrwa	x,a
2958                     ; 471 				responseLength	= sizeof(buffer) - index;
2960  051c a609          	ld	a,#9
2961  051e 1018          	sub	a,(OFST+0,sp)
2962  0520 6b05          	ld	(OFST-19,sp),a
2964  0522               L3631:
2965                     ; 475 			rxAlign = txLastBits;											// Having a separate variable is overkill. But it makes the next line easier to read.
2967  0522 7b0a          	ld	a,(OFST-14,sp)
2968  0524 6b0d          	ld	(OFST-11,sp),a
2970                     ; 476 			MFRC_WriteReg(BitFramingReg, (rxAlign << 4) + txLastBits);	// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
2972  0526 7b0d          	ld	a,(OFST-11,sp)
2973  0528 97            	ld	xl,a
2974  0529 a610          	ld	a,#16
2975  052b 42            	mul	x,a
2976  052c 9f            	ld	a,xl
2977  052d 1b0a          	add	a,(OFST-14,sp)
2978  052f ae1a00        	ldw	x,#6656
2979  0532 97            	ld	xl,a
2980  0533 cd0000        	call	_MFRC_WriteReg
2982                     ; 479 			result = MFRC_TransceiveData(buffer, bufferUsed, responseBuffer, &responseLength, &txLastBits, rxAlign, 0);
2984  0536 4b00          	push	#0
2985  0538 7b0e          	ld	a,(OFST-10,sp)
2986  053a 88            	push	a
2987  053b 96            	ldw	x,sp
2988  053c 1c000c        	addw	x,#OFST-12
2989  053f 89            	pushw	x
2990  0540 96            	ldw	x,sp
2991  0541 1c0009        	addw	x,#OFST-15
2992  0544 89            	pushw	x
2993  0545 1e0e          	ldw	x,(OFST-10,sp)
2994  0547 89            	pushw	x
2995  0548 7b13          	ld	a,(OFST-5,sp)
2996  054a 88            	push	a
2997  054b 96            	ldw	x,sp
2998  054c 1c0018        	addw	x,#OFST+0
2999  054f cd02d0        	call	_MFRC_TransceiveData
3001  0552 5b09          	addw	sp,#9
3002  0554 6b18          	ld	(OFST+0,sp),a
3004                     ; 480 			if (result == STATUS_COLLISION) { // More than one PICC in the field => collision.
3006  0556 7b18          	ld	a,(OFST+0,sp)
3007  0558 a102          	cp	a,#2
3008  055a 2703          	jreq	L461
3009  055c cc0607        	jp	L5631
3010  055f               L461:
3011                     ; 481 				valueOfCollReg = MFRC_ReadReg(CollReg); // CollReg[7..0] bits are: ValuesAfterColl reserved CollPosNotValid CollPos[4:0]
3013  055f a61c          	ld	a,#28
3014  0561 cd00b2        	call	_MFRC_ReadReg
3016  0564 6b0d          	ld	(OFST-11,sp),a
3018                     ; 482 				if (valueOfCollReg & 0x20) { // CollPosNotValid
3020  0566 7b0d          	ld	a,(OFST-11,sp)
3021  0568 a520          	bcp	a,#32
3022  056a 2706          	jreq	L7631
3023                     ; 483 					return STATUS_COLLISION; // Without a valid collision position we cannot continue
3025  056c a602          	ld	a,#2
3027  056e ac900390      	jpf	L261
3028  0572               L7631:
3029                     ; 485 				collisionPos = valueOfCollReg & 0x1F; // Values 0-31, 0 means bit 32.
3031  0572 7b0d          	ld	a,(OFST-11,sp)
3032  0574 a41f          	and	a,#31
3033  0576 6b18          	ld	(OFST+0,sp),a
3035                     ; 486 				if (collisionPos == 0) {
3037  0578 0d18          	tnz	(OFST+0,sp)
3038  057a 2604          	jrne	L1731
3039                     ; 487 					collisionPos = 32;
3041  057c a620          	ld	a,#32
3042  057e 6b18          	ld	(OFST+0,sp),a
3044  0580               L1731:
3045                     ; 489 				if (collisionPos <= currentLevelKnownBits) { // No progress - should not happen 
3047  0580 9c            	rvf
3048  0581 7b18          	ld	a,(OFST+0,sp)
3049  0583 5f            	clrw	x
3050  0584 97            	ld	xl,a
3051  0585 7b0e          	ld	a,(OFST-10,sp)
3052  0587 905f          	clrw	y
3053  0589 4d            	tnz	a
3054  058a 2a02          	jrpl	L421
3055  058c 9053          	cplw	y
3056  058e               L421:
3057  058e 9097          	ld	yl,a
3058  0590 90bf00        	ldw	c_y,y
3059  0593 b300          	cpw	x,c_y
3060  0595 2c06          	jrsgt	L3731
3061                     ; 490 					return STATUS_INTERNAL_ERROR;
3063  0597 a605          	ld	a,#5
3065  0599 ac900390      	jpf	L261
3066  059d               L3731:
3067                     ; 493 				currentLevelKnownBits	= collisionPos;
3069  059d 7b18          	ld	a,(OFST+0,sp)
3070  059f 6b0e          	ld	(OFST-10,sp),a
3072                     ; 494 				count			= currentLevelKnownBits % 8; // The bit to modify
3074  05a1 7b0e          	ld	a,(OFST-10,sp)
3075  05a3 ae0008        	ldw	x,#8
3076  05a6 51            	exgw	x,y
3077  05a7 5f            	clrw	x
3078  05a8 4d            	tnz	a
3079  05a9 2a01          	jrpl	L621
3080  05ab 5a            	decw	x
3081  05ac               L621:
3082  05ac 02            	rlwa	x,a
3083  05ad cd0000        	call	c_idiv
3085  05b0 909f          	ld	a,yl
3086  05b2 6b0c          	ld	(OFST-12,sp),a
3088                     ; 495 				checkBit		= (currentLevelKnownBits - 1) % 8;
3090  05b4 7b0e          	ld	a,(OFST-10,sp)
3091  05b6 5f            	clrw	x
3092  05b7 4d            	tnz	a
3093  05b8 2a01          	jrpl	L031
3094  05ba 53            	cplw	x
3095  05bb               L031:
3096  05bb 97            	ld	xl,a
3097  05bc 5a            	decw	x
3098  05bd a608          	ld	a,#8
3099  05bf cd0000        	call	c_smodx
3101  05c2 01            	rrwa	x,a
3102  05c3 6b0d          	ld	(OFST-11,sp),a
3103  05c5 02            	rlwa	x,a
3105                     ; 496 				index			= 1 + (currentLevelKnownBits / 8) + (count ? 1 : 0); // First byte is index 0.
3107  05c6 0d0c          	tnz	(OFST-12,sp)
3108  05c8 2704          	jreq	L231
3109  05ca a601          	ld	a,#1
3110  05cc 2001          	jra	L431
3111  05ce               L231:
3112  05ce 4f            	clr	a
3113  05cf               L431:
3114  05cf 6b02          	ld	(OFST-22,sp),a
3116  05d1 7b0e          	ld	a,(OFST-10,sp)
3117  05d3 ae0008        	ldw	x,#8
3118  05d6 51            	exgw	x,y
3119  05d7 5f            	clrw	x
3120  05d8 4d            	tnz	a
3121  05d9 2a01          	jrpl	L631
3122  05db 5a            	decw	x
3123  05dc               L631:
3124  05dc 02            	rlwa	x,a
3125  05dd cd0000        	call	c_idiv
3127  05e0 9f            	ld	a,xl
3128  05e1 1b02          	add	a,(OFST-22,sp)
3129  05e3 4c            	inc	a
3130  05e4 6b18          	ld	(OFST+0,sp),a
3132                     ; 497 				buffer[index]	|= (1 << checkBit);
3134  05e6 96            	ldw	x,sp
3135  05e7 1c000f        	addw	x,#OFST-9
3136  05ea 9f            	ld	a,xl
3137  05eb 5e            	swapw	x
3138  05ec 1b18          	add	a,(OFST+0,sp)
3139  05ee 2401          	jrnc	L041
3140  05f0 5c            	incw	x
3141  05f1               L041:
3142  05f1 02            	rlwa	x,a
3143  05f2 7b0d          	ld	a,(OFST-11,sp)
3144  05f4 905f          	clrw	y
3145  05f6 9097          	ld	yl,a
3146  05f8 a601          	ld	a,#1
3147  05fa 905d          	tnzw	y
3148  05fc 2705          	jreq	L241
3149  05fe               L441:
3150  05fe 48            	sll	a
3151  05ff 905a          	decw	y
3152  0601 26fb          	jrne	L441
3153  0603               L241:
3154  0603 fa            	or	a,(x)
3155  0604 f7            	ld	(x),a
3157  0605 201b          	jra	L5731
3158  0607               L5631:
3159                     ; 499 			else if (result != STATUS_OK) {
3161  0607 0d18          	tnz	(OFST+0,sp)
3162  0609 2706          	jreq	L7731
3163                     ; 500 				return result;
3165  060b 7b18          	ld	a,(OFST+0,sp)
3167  060d ac900390      	jpf	L261
3168  0611               L7731:
3169                     ; 503 				if (currentLevelKnownBits >= 32) { // This was a SELECT.
3171  0611 9c            	rvf
3172  0612 7b0e          	ld	a,(OFST-10,sp)
3173  0614 a120          	cp	a,#32
3174  0616 2f06          	jrslt	L3041
3175                     ; 504 					selectDone = 1; // No more anticollision 
3177  0618 a601          	ld	a,#1
3178  061a 6b06          	ld	(OFST-18,sp),a
3181  061c 2004          	jra	L5731
3182  061e               L3041:
3183                     ; 509 					currentLevelKnownBits = 32;
3185  061e a620          	ld	a,#32
3186  0620 6b0e          	ld	(OFST-10,sp),a
3188  0622               L5731:
3189                     ; 444 		while (!selectDone) {
3191  0622 0d06          	tnz	(OFST-18,sp)
3192  0624 2603          	jrne	L661
3193  0626 cc0487        	jp	L1531
3194  0629               L661:
3195                     ; 518 		index			= (buffer[2] == PICC_CMD_CT) ? 3 : 2; // source index in buffer[]
3197  0629 7b11          	ld	a,(OFST-7,sp)
3198  062b a188          	cp	a,#136
3199  062d 2604          	jrne	L641
3200  062f a603          	ld	a,#3
3201  0631 2002          	jra	L051
3202  0633               L641:
3203  0633 a602          	ld	a,#2
3204  0635               L051:
3205  0635 6b18          	ld	(OFST+0,sp),a
3207                     ; 519 		bytesToCopy		= (buffer[2] == PICC_CMD_CT) ? 3 : 4;
3209  0637 7b11          	ld	a,(OFST-7,sp)
3210  0639 a188          	cp	a,#136
3211  063b 2604          	jrne	L251
3212  063d a603          	ld	a,#3
3213  063f 2002          	jra	L451
3214  0641               L251:
3215  0641 a604          	ld	a,#4
3216  0643               L451:
3217  0643 6b0b          	ld	(OFST-13,sp),a
3219                     ; 520 		for (count = 0; count < bytesToCopy; count++) {
3221  0645 0f0c          	clr	(OFST-12,sp)
3224  0647 202d          	jra	L3141
3225  0649               L7041:
3226                     ; 521 			uid0->uidByte[uidIndex + count] = buffer[index++];
3228  0649 7b19          	ld	a,(OFST+1,sp)
3229  064b 97            	ld	xl,a
3230  064c 7b1a          	ld	a,(OFST+2,sp)
3231  064e 1b0c          	add	a,(OFST-12,sp)
3232  0650 2401          	jrnc	L651
3233  0652 5c            	incw	x
3234  0653               L651:
3235  0653 1b07          	add	a,(OFST-17,sp)
3236  0655 2401          	jrnc	L061
3237  0657 5c            	incw	x
3238  0658               L061:
3239  0658 02            	rlwa	x,a
3240  0659 9096          	ldw	y,sp
3241  065b 72a9000f      	addw	y,#OFST-9
3242  065f 1701          	ldw	(OFST-23,sp),y
3244  0661 7b18          	ld	a,(OFST+0,sp)
3245  0663 9097          	ld	yl,a
3246  0665 0c18          	inc	(OFST+0,sp)
3248  0667 909f          	ld	a,yl
3249  0669 905f          	clrw	y
3250  066b 9097          	ld	yl,a
3251  066d 72f901        	addw	y,(OFST-23,sp)
3252  0670 90f6          	ld	a,(y)
3253  0672 e701          	ld	(1,x),a
3254                     ; 520 		for (count = 0; count < bytesToCopy; count++) {
3256  0674 0c0c          	inc	(OFST-12,sp)
3258  0676               L3141:
3261  0676 7b0c          	ld	a,(OFST-12,sp)
3262  0678 110b          	cp	a,(OFST-13,sp)
3263  067a 25cd          	jrult	L7041
3264                     ; 525 		if (responseLength != 3 || txLastBits != 0) { // SAK must be exactly 24 bits (1 byte + CRC_A).
3266  067c 7b05          	ld	a,(OFST-19,sp)
3267  067e a103          	cp	a,#3
3268  0680 2604          	jrne	L1241
3270  0682 0d0a          	tnz	(OFST-14,sp)
3271  0684 2706          	jreq	L7141
3272  0686               L1241:
3273                     ; 526 			return STATUS_ERROR;
3275  0686 a601          	ld	a,#1
3277  0688 ac900390      	jpf	L261
3278  068c               L7141:
3279                     ; 529 		result = MFRC_CalculateCRC(responseBuffer, 1, &buffer[2]);
3281  068c 96            	ldw	x,sp
3282  068d 1c0011        	addw	x,#OFST-7
3283  0690 89            	pushw	x
3284  0691 4b01          	push	#1
3285  0693 1e0b          	ldw	x,(OFST-13,sp)
3286  0695 cd01d4        	call	_MFRC_CalculateCRC
3288  0698 5b03          	addw	sp,#3
3289  069a 6b18          	ld	(OFST+0,sp),a
3291                     ; 530 		if (result != STATUS_OK) {
3293  069c 0d18          	tnz	(OFST+0,sp)
3294  069e 2706          	jreq	L3241
3295                     ; 531 			return result;
3297  06a0 7b18          	ld	a,(OFST+0,sp)
3299  06a2 ac900390      	jpf	L261
3300  06a6               L3241:
3301                     ; 533 		if ((buffer[2] != responseBuffer[1]) || (buffer[3] != responseBuffer[2])) {
3303  06a6 1e08          	ldw	x,(OFST-16,sp)
3304  06a8 e601          	ld	a,(1,x)
3305  06aa 1111          	cp	a,(OFST-7,sp)
3306  06ac 2608          	jrne	L7241
3308  06ae 1e08          	ldw	x,(OFST-16,sp)
3309  06b0 e602          	ld	a,(2,x)
3310  06b2 1112          	cp	a,(OFST-6,sp)
3311  06b4 2706          	jreq	L5241
3312  06b6               L7241:
3313                     ; 534 			return STATUS_CRC_WRONG;
3315  06b6 a607          	ld	a,#7
3317  06b8 ac900390      	jpf	L261
3318  06bc               L5241:
3319                     ; 536 		if (responseBuffer[0] & 0x04) { // Cascade bit set - UID not complete yes
3321  06bc 1e08          	ldw	x,(OFST-16,sp)
3322  06be f6            	ld	a,(x)
3323  06bf a504          	bcp	a,#4
3324  06c1 2704          	jreq	L1341
3325                     ; 537 			cascadeLevel++;
3327  06c3 0c04          	inc	(OFST-20,sp)
3330  06c5 200b          	jra	L7131
3331  06c7               L1341:
3332                     ; 540 			uidComplete = 1;
3334  06c7 a601          	ld	a,#1
3335  06c9 6b03          	ld	(OFST-21,sp),a
3337                     ; 541 			uid0->sak = responseBuffer[0];
3339  06cb 1e08          	ldw	x,(OFST-16,sp)
3340  06cd f6            	ld	a,(x)
3341  06ce 1e19          	ldw	x,(OFST+1,sp)
3342  06d0 e70b          	ld	(11,x),a
3343  06d2               L7131:
3344                     ; 391 	while (!uidComplete) {
3346  06d2 0d03          	tnz	(OFST-21,sp)
3347  06d4 2603          	jrne	L071
3348  06d6 cc0383        	jp	L5131
3349  06d9               L071:
3350                     ; 546 	uid0->size = 3 * cascadeLevel + 1;
3352  06d9 7b04          	ld	a,(OFST-20,sp)
3353  06db 97            	ld	xl,a
3354  06dc a603          	ld	a,#3
3355  06de 42            	mul	x,a
3356  06df 9f            	ld	a,xl
3357  06e0 4c            	inc	a
3358  06e1 1e19          	ldw	x,(OFST+1,sp)
3359  06e3 f7            	ld	(x),a
3360                     ; 548 	return STATUS_OK;
3362  06e4 4f            	clr	a
3364  06e5 ac900390      	jpf	L261
3412                     ; 556 StatusCode PICC_HaltA() 
3412                     ; 557 {
3413                     	switch	.text
3414  06e9               _PICC_HaltA:
3416  06e9 5205          	subw	sp,#5
3417       00000005      OFST:	set	5
3420                     ; 562 	buffer[0] = PICC_CMD_HLTA;
3422  06eb a650          	ld	a,#80
3423  06ed 6b01          	ld	(OFST-4,sp),a
3425                     ; 563 	buffer[1] = 0;
3427  06ef 0f02          	clr	(OFST-3,sp)
3429                     ; 565 	result = MFRC_CalculateCRC(buffer, 2, &buffer[2]);
3431  06f1 96            	ldw	x,sp
3432  06f2 1c0003        	addw	x,#OFST-2
3433  06f5 89            	pushw	x
3434  06f6 4b02          	push	#2
3435  06f8 96            	ldw	x,sp
3436  06f9 1c0004        	addw	x,#OFST-1
3437  06fc cd01d4        	call	_MFRC_CalculateCRC
3439  06ff 5b03          	addw	sp,#3
3440  0701 6b05          	ld	(OFST+0,sp),a
3442                     ; 566 	if (result != STATUS_OK) {
3444  0703 0d05          	tnz	(OFST+0,sp)
3445  0705 2704          	jreq	L7541
3446                     ; 567 	return result;
3448  0707 7b05          	ld	a,(OFST+0,sp)
3450  0709 201e          	jra	L471
3451  070b               L7541:
3452                     ; 574 	result = MFRC_TransceiveData(buffer, sizeof(buffer), 0, 0, 0, 0, 0);
3454  070b 4b00          	push	#0
3455  070d 4b00          	push	#0
3456  070f 5f            	clrw	x
3457  0710 89            	pushw	x
3458  0711 5f            	clrw	x
3459  0712 89            	pushw	x
3460  0713 5f            	clrw	x
3461  0714 89            	pushw	x
3462  0715 4b04          	push	#4
3463  0717 96            	ldw	x,sp
3464  0718 1c000a        	addw	x,#OFST+5
3465  071b cd02d0        	call	_MFRC_TransceiveData
3467  071e 5b09          	addw	sp,#9
3468  0720 6b05          	ld	(OFST+0,sp),a
3470                     ; 575 	if (result == STATUS_TIMEOUT) {
3472  0722 7b05          	ld	a,(OFST+0,sp)
3473  0724 a103          	cp	a,#3
3474  0726 2604          	jrne	L1641
3475                     ; 576 		return STATUS_OK;
3477  0728 4f            	clr	a
3479  0729               L471:
3481  0729 5b05          	addw	sp,#5
3482  072b 81            	ret
3483  072c               L1641:
3484                     ; 578 	if (result == STATUS_OK) { // That is ironically NOT ok in this case ;-)
3486  072c 0d05          	tnz	(OFST+0,sp)
3487  072e 2604          	jrne	L3641
3488                     ; 579 		return STATUS_ERROR;
3490  0730 a601          	ld	a,#1
3492  0732 20f5          	jra	L471
3493  0734               L3641:
3494                     ; 581 	return result;
3496  0734 7b05          	ld	a,(OFST+0,sp)
3498  0736 20f1          	jra	L471
3612                     ; 596 StatusCode PCD_Authenticate(uint8_t command,		///< PICC_CMD_MF_AUTH_KEY_A or PICC_CMD_MF_AUTH_KEY_B
3612                     ; 597 														uint8_t blockAddr, 	///< The block number. See numbering in the comments in the .h file.
3612                     ; 598 														MIFARE_Key *key,	///< Pointer to the Crypteo1 key to use (6 bytes)
3612                     ; 599 														Uid_str *uid0			///< Pointer to Uid struct. The first 4 bytes of the UID is used.
3612                     ; 600 											) {
3613                     	switch	.text
3614  0738               _PCD_Authenticate:
3616  0738 89            	pushw	x
3617  0739 520e          	subw	sp,#14
3618       0000000e      OFST:	set	14
3621                     ; 601 	uint8_t waitIRq = 0x10;		// IdleIRq
3623  073b a610          	ld	a,#16
3624  073d 6b01          	ld	(OFST-13,sp),a
3626                     ; 606 	sendData[0] = command;
3628  073f 9e            	ld	a,xh
3629  0740 6b02          	ld	(OFST-12,sp),a
3631                     ; 607 	sendData[1] = blockAddr;
3633  0742 9f            	ld	a,xl
3634  0743 6b03          	ld	(OFST-11,sp),a
3636                     ; 608 	for (i = 0; i < MF_KEY_SIZE; i++) {	// 6 key bytes
3638  0745 0f0e          	clr	(OFST+0,sp)
3640  0747               L7451:
3641                     ; 609 		sendData[2+i] = key->keyByte[i];
3643  0747 96            	ldw	x,sp
3644  0748 1c0004        	addw	x,#OFST-10
3645  074b 9f            	ld	a,xl
3646  074c 5e            	swapw	x
3647  074d 1b0e          	add	a,(OFST+0,sp)
3648  074f 2401          	jrnc	L002
3649  0751 5c            	incw	x
3650  0752               L002:
3651  0752 02            	rlwa	x,a
3652  0753 89            	pushw	x
3653  0754 7b15          	ld	a,(OFST+7,sp)
3654  0756 97            	ld	xl,a
3655  0757 7b16          	ld	a,(OFST+8,sp)
3656  0759 1b10          	add	a,(OFST+2,sp)
3657  075b 2401          	jrnc	L202
3658  075d 5c            	incw	x
3659  075e               L202:
3660  075e 02            	rlwa	x,a
3661  075f f6            	ld	a,(x)
3662  0760 85            	popw	x
3663  0761 f7            	ld	(x),a
3664                     ; 608 	for (i = 0; i < MF_KEY_SIZE; i++) {	// 6 key bytes
3666  0762 0c0e          	inc	(OFST+0,sp)
3670  0764 7b0e          	ld	a,(OFST+0,sp)
3671  0766 a106          	cp	a,#6
3672  0768 25dd          	jrult	L7451
3673                     ; 615 	for (i = 0; i < 4; i++) {				// The last 4 bytes of the UID
3675  076a 0f0e          	clr	(OFST+0,sp)
3677  076c               L5551:
3678                     ; 616 		sendData[8+i] = uid0->uidByte[i+uid0->size-4];
3680  076c 96            	ldw	x,sp
3681  076d 1c000a        	addw	x,#OFST-4
3682  0770 9f            	ld	a,xl
3683  0771 5e            	swapw	x
3684  0772 1b0e          	add	a,(OFST+0,sp)
3685  0774 2401          	jrnc	L402
3686  0776 5c            	incw	x
3687  0777               L402:
3688  0777 02            	rlwa	x,a
3689  0778 89            	pushw	x
3690  0779 1e17          	ldw	x,(OFST+9,sp)
3691  077b f6            	ld	a,(x)
3692  077c 5f            	clrw	x
3693  077d 1b10          	add	a,(OFST+2,sp)
3694  077f 2401          	jrnc	L602
3695  0781 5c            	incw	x
3696  0782               L602:
3697  0782 02            	rlwa	x,a
3698  0783 1d0004        	subw	x,#4
3699  0786 72fb17        	addw	x,(OFST+9,sp)
3700  0789 e601          	ld	a,(1,x)
3701  078b 85            	popw	x
3702  078c f7            	ld	(x),a
3703                     ; 615 	for (i = 0; i < 4; i++) {				// The last 4 bytes of the UID
3705  078d 0c0e          	inc	(OFST+0,sp)
3709  078f 7b0e          	ld	a,(OFST+0,sp)
3710  0791 a104          	cp	a,#4
3711  0793 25d7          	jrult	L5551
3712                     ; 620 	return MFRC_CommunicateWithPICC(MFRC_MFAuthent, waitIRq, &sendData[0], sizeof(sendData), 0, 0, 0, 0, 0);
3714  0795 4b00          	push	#0
3715  0797 4b00          	push	#0
3716  0799 5f            	clrw	x
3717  079a 89            	pushw	x
3718  079b 5f            	clrw	x
3719  079c 89            	pushw	x
3720  079d 5f            	clrw	x
3721  079e 89            	pushw	x
3722  079f 4b0c          	push	#12
3723  07a1 96            	ldw	x,sp
3724  07a2 1c000b        	addw	x,#OFST-3
3725  07a5 89            	pushw	x
3726  07a6 7b0c          	ld	a,(OFST-2,sp)
3727  07a8 ae0e00        	ldw	x,#3584
3728  07ab 97            	ld	xl,a
3729  07ac ad0c          	call	_MFRC_CommunicateWithPICC
3731  07ae 5b0b          	addw	sp,#11
3734  07b0 5b10          	addw	sp,#16
3735  07b2 81            	ret
3759                     ; 627 void PCD_StopCrypto1() {
3760                     	switch	.text
3761  07b3               _PCD_StopCrypto1:
3765                     ; 629 	MFRC_ClearRegBitMask(Status2Reg, 0x08); // Status2Reg[7..0] bits are: TempSensClear I2CForceHS reserved reserved MFCrypto1On ModemState[2:0]
3767  07b3 ae1008        	ldw	x,#4104
3768  07b6 cd01bd        	call	_MFRC_ClearRegBitMask
3770                     ; 630 } // End PCD_StopCrypto1()
3773  07b9 81            	ret
3975                     ; 632 StatusCode MFRC_CommunicateWithPICC(uint8_t command,		///< The command to execute. One of the MFRC_Command enums.
3975                     ; 633 													uint8_t waitIRq,		///< The bits in the ComIrqReg register that signals successful completion of the command.
3975                     ; 634 													uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
3975                     ; 635 													uint8_t sendLen,		///< Number of uint8_ts to transfer to the FIFO.
3975                     ; 636 													uint8_t *backData,		///< nullptr or pointer to buffer if data should be read back after executing the command.
3975                     ; 637 													uint8_t *backLen,		///< In: Max number of uint8_ts to write to *backData. Out: The number of uint8_ts returned.
3975                     ; 638 													uint8_t *validBits,	///< In/Out: The number of valid bits in the last uint8_t. 0 for 8 valid bits.
3975                     ; 639 													uint8_t rxAlign,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
3975                     ; 640 													bool checkCRC		///< In: True => The last two uint8_ts of the response is assumed to be a CRC_A that must be validated.
3975                     ; 641 									 ) 
3975                     ; 642 {
3976                     	switch	.text
3977  07ba               _MFRC_CommunicateWithPICC:
3979  07ba 89            	pushw	x
3980  07bb 5206          	subw	sp,#6
3981       00000006      OFST:	set	6
3984                     ; 649 	uint8_t txLastBits = validBits ? *validBits : 0;
3986  07bd 1e12          	ldw	x,(OFST+12,sp)
3987  07bf 2705          	jreq	L412
3988  07c1 1e12          	ldw	x,(OFST+12,sp)
3989  07c3 f6            	ld	a,(x)
3990  07c4 2001          	jra	L612
3991  07c6               L412:
3992  07c6 4f            	clr	a
3993  07c7               L612:
3994  07c7 6b05          	ld	(OFST-1,sp),a
3996                     ; 650 	uint8_t bitFraming = (rxAlign << 4) + txLastBits;		// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
3998  07c9 7b14          	ld	a,(OFST+14,sp)
3999  07cb 97            	ld	xl,a
4000  07cc a610          	ld	a,#16
4001  07ce 42            	mul	x,a
4002  07cf 9f            	ld	a,xl
4003  07d0 1b05          	add	a,(OFST-1,sp)
4004  07d2 6b05          	ld	(OFST-1,sp),a
4006                     ; 653 	*backData = 0;
4008  07d4 1e0e          	ldw	x,(OFST+8,sp)
4009  07d6 7f            	clr	(x)
4010                     ; 654 	*backLen = 0;
4012  07d7 1e10          	ldw	x,(OFST+10,sp)
4013  07d9 7f            	clr	(x)
4014                     ; 655 	*validBits = 0;
4016  07da 1e12          	ldw	x,(OFST+12,sp)
4017  07dc 7f            	clr	(x)
4018                     ; 656 	rxAlign = 0;
4020  07dd 0f14          	clr	(OFST+14,sp)
4021                     ; 657 	checkCRC = 0;
4023  07df 0f15          	clr	(OFST+15,sp)
4024                     ; 659 	MFRC_WriteReg(CommandReg, MFRC_Idle);			// Stop any active command.
4026  07e1 ae0200        	ldw	x,#512
4027  07e4 cd0000        	call	_MFRC_WriteReg
4029                     ; 660 	MFRC_WriteReg(ComIrqReg, 0x7F);					// Clear all seven interrupt request bits
4031  07e7 ae087f        	ldw	x,#2175
4032  07ea cd0000        	call	_MFRC_WriteReg
4034                     ; 661 	MFRC_WriteReg(FIFOLevelReg, 0x80);				// FlushBuffer = 1, FIFO initialization
4036  07ed ae1480        	ldw	x,#5248
4037  07f0 cd0000        	call	_MFRC_WriteReg
4039                     ; 662 	MFRC_WriteRegs(FIFODataReg, sendData, sendLen);	// Write sendData to the FIFO
4041  07f3 7b0d          	ld	a,(OFST+7,sp)
4042  07f5 88            	push	a
4043  07f6 1e0c          	ldw	x,(OFST+6,sp)
4044  07f8 89            	pushw	x
4045  07f9 a612          	ld	a,#18
4046  07fb cd0050        	call	_MFRC_WriteRegs
4048  07fe 5b03          	addw	sp,#3
4049                     ; 663 	MFRC_WriteReg(BitFramingReg, bitFraming);		// Bit adjustments
4051  0800 7b05          	ld	a,(OFST-1,sp)
4052  0802 ae1a00        	ldw	x,#6656
4053  0805 97            	ld	xl,a
4054  0806 cd0000        	call	_MFRC_WriteReg
4056                     ; 664 	MFRC_WriteReg(CommandReg, command);				// Execute the command
4058  0809 7b07          	ld	a,(OFST+1,sp)
4059  080b ae0200        	ldw	x,#512
4060  080e 97            	ld	xl,a
4061  080f cd0000        	call	_MFRC_WriteReg
4063                     ; 665 	if (command == MFRC_Transceive) {
4065  0812 7b07          	ld	a,(OFST+1,sp)
4066  0814 a10c          	cp	a,#12
4067  0816 2606          	jrne	L5171
4068                     ; 666 		MFRC_SetRegBitMask(BitFramingReg, 0x80);	// StartSend=1, transmission of data starts
4070  0818 ae1a80        	ldw	x,#6784
4071  081b cd01a6        	call	_MFRC_SetRegBitMask
4073  081e               L5171:
4074                     ; 673 	for (i = 2000; i > 0; i--) {
4076  081e ae07d0        	ldw	x,#2000
4077  0821 1f03          	ldw	(OFST-3,sp),x
4079  0823               L7171:
4080                     ; 674 		n = MFRC_ReadReg(ComIrqReg);	// ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq LoAlertIRq ErrIRq TimerIRq
4082  0823 a608          	ld	a,#8
4083  0825 cd00b2        	call	_MFRC_ReadReg
4085  0828 6b06          	ld	(OFST+0,sp),a
4087                     ; 675 		if (n & waitIRq) {					// One of the interrupts that signal success has been set.
4089  082a 7b06          	ld	a,(OFST+0,sp)
4090  082c 1508          	bcp	a,(OFST+2,sp)
4091  082e 2615          	jrne	L3271
4092                     ; 676 			break;
4094                     ; 678 		if (n & 0x01) {						// Timer interrupt - nothing received in 25ms
4096  0830 7b06          	ld	a,(OFST+0,sp)
4097  0832 a501          	bcp	a,#1
4098  0834 2704          	jreq	L7271
4099                     ; 679 			return STATUS_TIMEOUT;
4101  0836 a603          	ld	a,#3
4103  0838 2011          	jra	L022
4104  083a               L7271:
4105                     ; 673 	for (i = 2000; i > 0; i--) {
4107  083a 1e03          	ldw	x,(OFST-3,sp)
4108  083c 1d0001        	subw	x,#1
4109  083f 1f03          	ldw	(OFST-3,sp),x
4113  0841 1e03          	ldw	x,(OFST-3,sp)
4114  0843 26de          	jrne	L7171
4115  0845               L3271:
4116                     ; 683 	if (i == 0) {
4118  0845 1e03          	ldw	x,(OFST-3,sp)
4119  0847 2605          	jrne	L1371
4120                     ; 684 		return STATUS_TIMEOUT;
4122  0849 a603          	ld	a,#3
4124  084b               L022:
4126  084b 5b08          	addw	sp,#8
4127  084d 81            	ret
4128  084e               L1371:
4129                     ; 688 	errorRegValue = MFRC_ReadReg(ErrorReg); // ErrorReg[7..0] bits are: WrErr TempErr reserved BufferOvfl CollErr CRCErr ParityErr ProtocolErr
4131  084e a60c          	ld	a,#12
4132  0850 cd00b2        	call	_MFRC_ReadReg
4134  0853 6b05          	ld	(OFST-1,sp),a
4136                     ; 689 	if (errorRegValue & 0x13) {	 // BufferOvfl ParityErr ProtocolErr
4138  0855 7b05          	ld	a,(OFST-1,sp)
4139  0857 a513          	bcp	a,#19
4140  0859 2704          	jreq	L3371
4141                     ; 690 		return STATUS_ERROR;
4143  085b a601          	ld	a,#1
4145  085d 20ec          	jra	L022
4146  085f               L3371:
4147                     ; 693 	_validBits = 0;
4149  085f 0f06          	clr	(OFST+0,sp)
4151                     ; 696 	if (backData && backLen) {
4153  0861 1e0e          	ldw	x,(OFST+8,sp)
4154  0863 273e          	jreq	L5371
4156  0865 1e10          	ldw	x,(OFST+10,sp)
4157  0867 273a          	jreq	L5371
4158                     ; 697 		uint8_t n = MFRC_ReadReg(FIFOLevelReg);	// Number of uint8_ts in the FIFO
4160  0869 a614          	ld	a,#20
4161  086b cd00b2        	call	_MFRC_ReadReg
4163  086e 6b06          	ld	(OFST+0,sp),a
4165                     ; 698 		if (n > *backLen) {
4167  0870 1e10          	ldw	x,(OFST+10,sp)
4168  0872 f6            	ld	a,(x)
4169  0873 1106          	cp	a,(OFST+0,sp)
4170  0875 2404          	jruge	L7371
4171                     ; 699 			return STATUS_NO_ROOM;
4173  0877 a604          	ld	a,#4
4175  0879 20d0          	jra	L022
4176  087b               L7371:
4177                     ; 701 		*backLen = n;											// Number of uint8_ts returned
4179  087b 7b06          	ld	a,(OFST+0,sp)
4180  087d 1e10          	ldw	x,(OFST+10,sp)
4181  087f f7            	ld	(x),a
4182                     ; 702 		MFRC_ReadRegs(FIFODataReg, n, backData, rxAlign);	// Get received data from FIFO
4184  0880 7b14          	ld	a,(OFST+14,sp)
4185  0882 88            	push	a
4186  0883 1e0f          	ldw	x,(OFST+9,sp)
4187  0885 89            	pushw	x
4188  0886 7b09          	ld	a,(OFST+3,sp)
4189  0888 ae1200        	ldw	x,#4608
4190  088b 97            	ld	xl,a
4191  088c cd00f6        	call	_MFRC_ReadRegs
4193  088f 5b03          	addw	sp,#3
4194                     ; 703 		_validBits = MFRC_ReadReg(ControlReg) & 0x07;		// RxLastBits[2:0] indicates the number of valid bits in the last received uint8_t. If this value is 000b, the whole uint8_t is valid.
4196  0891 a618          	ld	a,#24
4197  0893 cd00b2        	call	_MFRC_ReadReg
4199  0896 a407          	and	a,#7
4200  0898 6b06          	ld	(OFST+0,sp),a
4202                     ; 704 		if (validBits) {
4204  089a 1e12          	ldw	x,(OFST+12,sp)
4205  089c 2705          	jreq	L5371
4206                     ; 705 			*validBits = _validBits;
4208  089e 7b06          	ld	a,(OFST+0,sp)
4209  08a0 1e12          	ldw	x,(OFST+12,sp)
4210  08a2 f7            	ld	(x),a
4211  08a3               L5371:
4212                     ; 710 	if (errorRegValue & 0x08) {		// CollErr
4214  08a3 7b05          	ld	a,(OFST-1,sp)
4215  08a5 a508          	bcp	a,#8
4216  08a7 2704          	jreq	L3471
4217                     ; 711 		return STATUS_COLLISION;
4219  08a9 a602          	ld	a,#2
4221  08ab 209e          	jra	L022
4222  08ad               L3471:
4223                     ; 715 	if (backData && backLen && checkCRC) {
4225  08ad 1e0e          	ldw	x,(OFST+8,sp)
4226  08af 276b          	jreq	L5471
4228  08b1 1e10          	ldw	x,(OFST+10,sp)
4229  08b3 2767          	jreq	L5471
4231  08b5 0d15          	tnz	(OFST+15,sp)
4232  08b7 2763          	jreq	L5471
4233                     ; 717 		if (*backLen == 1 && _validBits == 4) {
4235  08b9 1e10          	ldw	x,(OFST+10,sp)
4236  08bb f6            	ld	a,(x)
4237  08bc a101          	cp	a,#1
4238  08be 260a          	jrne	L7471
4240  08c0 7b06          	ld	a,(OFST+0,sp)
4241  08c2 a104          	cp	a,#4
4242  08c4 2604          	jrne	L7471
4243                     ; 718 			return STATUS_MIFARE_NACK;
4245  08c6 a6ff          	ld	a,#255
4247  08c8 2081          	jpf	L022
4248  08ca               L7471:
4249                     ; 721 		if (*backLen < 2 || _validBits != 0) {
4251  08ca 1e10          	ldw	x,(OFST+10,sp)
4252  08cc f6            	ld	a,(x)
4253  08cd a102          	cp	a,#2
4254  08cf 2504          	jrult	L3571
4256  08d1 0d06          	tnz	(OFST+0,sp)
4257  08d3 2706          	jreq	L1571
4258  08d5               L3571:
4259                     ; 722 			return STATUS_CRC_WRONG;
4261  08d5 a607          	ld	a,#7
4263  08d7 ac4b084b      	jpf	L022
4264  08db               L1571:
4265                     ; 725 		status = MFRC_CalculateCRC(&backData[0], *backLen - 2, &controlBuffer[0]);
4267  08db 96            	ldw	x,sp
4268  08dc 1c0001        	addw	x,#OFST-5
4269  08df 89            	pushw	x
4270  08e0 1e12          	ldw	x,(OFST+12,sp)
4271  08e2 f6            	ld	a,(x)
4272  08e3 a002          	sub	a,#2
4273  08e5 88            	push	a
4274  08e6 1e11          	ldw	x,(OFST+11,sp)
4275  08e8 cd01d4        	call	_MFRC_CalculateCRC
4277  08eb 5b03          	addw	sp,#3
4278  08ed 6b05          	ld	(OFST-1,sp),a
4280                     ; 726 		if (status != STATUS_OK) {
4282  08ef 0d05          	tnz	(OFST-1,sp)
4283  08f1 2706          	jreq	L5571
4284                     ; 727 			return status;
4286  08f3 7b05          	ld	a,(OFST-1,sp)
4288  08f5 ac4b084b      	jpf	L022
4289  08f9               L5571:
4290                     ; 729 		if ((backData[*backLen - 2] != controlBuffer[0]) || (backData[*backLen - 1] != controlBuffer[1])) {
4292  08f9 1e10          	ldw	x,(OFST+10,sp)
4293  08fb f6            	ld	a,(x)
4294  08fc 5f            	clrw	x
4295  08fd 97            	ld	xl,a
4296  08fe 5a            	decw	x
4297  08ff 5a            	decw	x
4298  0900 72fb0e        	addw	x,(OFST+8,sp)
4299  0903 f6            	ld	a,(x)
4300  0904 1101          	cp	a,(OFST-5,sp)
4301  0906 260e          	jrne	L1671
4303  0908 1e10          	ldw	x,(OFST+10,sp)
4304  090a f6            	ld	a,(x)
4305  090b 5f            	clrw	x
4306  090c 97            	ld	xl,a
4307  090d 5a            	decw	x
4308  090e 72fb0e        	addw	x,(OFST+8,sp)
4309  0911 f6            	ld	a,(x)
4310  0912 1102          	cp	a,(OFST-4,sp)
4311  0914 2706          	jreq	L5471
4312  0916               L1671:
4313                     ; 730 			return STATUS_CRC_WRONG;
4315  0916 a607          	ld	a,#7
4317  0918 ac4b084b      	jpf	L022
4318  091c               L5471:
4319                     ; 733 	return STATUS_OK;
4321  091c 4f            	clr	a
4323  091d ac4b084b      	jpf	L022
4390                     ; 736 StatusCode MIFARE_Read(uint8_t blockAddr, 	///< MIFARE Classic: The block (0-0xff) number. MIFARE Ultralight: The first page to return data from.
4390                     ; 737 											uint8_t *buffer,		///< The buffer to store the data in
4390                     ; 738 											uint8_t *bufferSize	///< Buffer size, at least 18 uint8_ts. Also number of uint8_ts returned if STATUS_OK.
4390                     ; 739 										) 
4390                     ; 740 {
4391                     	switch	.text
4392  0921               _MIFARE_Read:
4394  0921 88            	push	a
4395  0922 88            	push	a
4396       00000001      OFST:	set	1
4399                     ; 744 	if ((buffer == 0) || (*bufferSize < 18)) {
4401  0923 1e05          	ldw	x,(OFST+4,sp)
4402  0925 2707          	jreq	L7102
4404  0927 1e07          	ldw	x,(OFST+6,sp)
4405  0929 f6            	ld	a,(x)
4406  092a a112          	cp	a,#18
4407  092c 2404          	jruge	L5102
4408  092e               L7102:
4409                     ; 745 		return STATUS_NO_ROOM;
4411  092e a604          	ld	a,#4
4413  0930 2021          	jra	L422
4414  0932               L5102:
4415                     ; 749 	buffer[0] = PICC_CMD_MF_READ;
4417  0932 1e05          	ldw	x,(OFST+4,sp)
4418  0934 a630          	ld	a,#48
4419  0936 f7            	ld	(x),a
4420                     ; 750 	buffer[1] = blockAddr;
4422  0937 7b02          	ld	a,(OFST+1,sp)
4423  0939 1e05          	ldw	x,(OFST+4,sp)
4424  093b e701          	ld	(1,x),a
4425                     ; 752 	result = MFRC_CalculateCRC(buffer, 2, &buffer[2]);
4427  093d 1e05          	ldw	x,(OFST+4,sp)
4428  093f 5c            	incw	x
4429  0940 5c            	incw	x
4430  0941 89            	pushw	x
4431  0942 4b02          	push	#2
4432  0944 1e08          	ldw	x,(OFST+7,sp)
4433  0946 cd01d4        	call	_MFRC_CalculateCRC
4435  0949 5b03          	addw	sp,#3
4436  094b 6b01          	ld	(OFST+0,sp),a
4438                     ; 753 	if (result != STATUS_OK) {
4440  094d 0d01          	tnz	(OFST+0,sp)
4441  094f 2704          	jreq	L1202
4442                     ; 754 		return result;
4444  0951 7b01          	ld	a,(OFST+0,sp)
4446  0953               L422:
4448  0953 85            	popw	x
4449  0954 81            	ret
4450  0955               L1202:
4451                     ; 758 	return MFRC_TransceiveData(buffer, 4, buffer, bufferSize, 0, 0, 1);
4453  0955 4b01          	push	#1
4454  0957 4b00          	push	#0
4455  0959 5f            	clrw	x
4456  095a 89            	pushw	x
4457  095b 1e0b          	ldw	x,(OFST+10,sp)
4458  095d 89            	pushw	x
4459  095e 1e0b          	ldw	x,(OFST+10,sp)
4460  0960 89            	pushw	x
4461  0961 4b04          	push	#4
4462  0963 1e0e          	ldw	x,(OFST+13,sp)
4463  0965 cd02d0        	call	_MFRC_TransceiveData
4465  0968 5b09          	addw	sp,#9
4467  096a 20e7          	jra	L422
4594                     ; 766 PICC_Type PICC_GetType(uint8_t sak		///< The SAK byte returned from PICC_Select().
4594                     ; 767 										) {
4595                     	switch	.text
4596  096c               L3_PICC_GetType:
4598  096c 88            	push	a
4599       00000000      OFST:	set	0
4602                     ; 772 	sak &= 0x7F;
4604  096d 7b01          	ld	a,(OFST+1,sp)
4605  096f a47f          	and	a,#127
4606  0971 6b01          	ld	(OFST+1,sp),a
4607                     ; 773 	switch (sak) {
4609  0973 7b01          	ld	a,(OFST+1,sp)
4611                     ; 784 		default:	return PICC_TYPE_UNKNOWN;
4612  0975 4d            	tnz	a
4613  0976 2739          	jreq	L3302
4614  0978 4a            	dec	a
4615  0979 2740          	jreq	L7302
4616  097b a003          	sub	a,#3
4617  097d 271e          	jreq	L3202
4618  097f a004          	sub	a,#4
4619  0981 2724          	jreq	L7202
4620  0983 4a            	dec	a
4621  0984 271c          	jreq	L5202
4622  0986 a007          	sub	a,#7
4623  0988 272c          	jreq	L5302
4624  098a 4a            	dec	a
4625  098b 2729          	jreq	L5302
4626  098d a007          	sub	a,#7
4627  098f 271b          	jreq	L1302
4628  0991 a008          	sub	a,#8
4629  0993 272b          	jreq	L1402
4630  0995 a020          	sub	a,#32
4631  0997 272c          	jreq	L3402
4632  0999               L5402:
4635  0999 4f            	clr	a
4638  099a 5b01          	addw	sp,#1
4639  099c 81            	ret
4640  099d               L3202:
4641                     ; 774 		case 0x04:	return PICC_TYPE_NOT_COMPLETE;	// UID not complete
4643  099d a6ff          	ld	a,#255
4646  099f 5b01          	addw	sp,#1
4647  09a1 81            	ret
4648  09a2               L5202:
4649                     ; 775 		case 0x09:	return PICC_TYPE_MIFARE_MINI;
4651  09a2 a603          	ld	a,#3
4654  09a4 5b01          	addw	sp,#1
4655  09a6 81            	ret
4656  09a7               L7202:
4657                     ; 776 		case 0x08:	return PICC_TYPE_MIFARE_1K;
4659  09a7 a604          	ld	a,#4
4662  09a9 5b01          	addw	sp,#1
4663  09ab 81            	ret
4664  09ac               L1302:
4665                     ; 777 		case 0x18:	return PICC_TYPE_MIFARE_4K;
4667  09ac a605          	ld	a,#5
4670  09ae 5b01          	addw	sp,#1
4671  09b0 81            	ret
4672  09b1               L3302:
4673                     ; 778 		case 0x00:	return PICC_TYPE_MIFARE_UL;
4675  09b1 a606          	ld	a,#6
4678  09b3 5b01          	addw	sp,#1
4679  09b5 81            	ret
4680  09b6               L5302:
4681                     ; 779 		case 0x10:
4681                     ; 780 		case 0x11:	return PICC_TYPE_MIFARE_PLUS;
4683  09b6 a607          	ld	a,#7
4686  09b8 5b01          	addw	sp,#1
4687  09ba 81            	ret
4688  09bb               L7302:
4689                     ; 781 		case 0x01:	return PICC_TYPE_TNP3XXX;
4691  09bb a609          	ld	a,#9
4694  09bd 5b01          	addw	sp,#1
4695  09bf 81            	ret
4696  09c0               L1402:
4697                     ; 782 		case 0x20:	return PICC_TYPE_ISO_14443_4;
4699  09c0 a601          	ld	a,#1
4702  09c2 5b01          	addw	sp,#1
4703  09c4 81            	ret
4704  09c5               L3402:
4705                     ; 783 		case 0x40:	return PICC_TYPE_ISO_18092;
4707  09c5 a602          	ld	a,#2
4710  09c7 5b01          	addw	sp,#1
4711  09c9 81            	ret
4747                     ; 792 void PCD_DumpVersionToSerial() {
4748                     	switch	.text
4749  09ca               _PCD_DumpVersionToSerial:
4751  09ca 88            	push	a
4752       00000001      OFST:	set	1
4755                     ; 794 	uint8_t v = MFRC_ReadReg(VersionReg);
4757  09cb a66e          	ld	a,#110
4758  09cd cd00b2        	call	_MFRC_ReadReg
4760                     ; 810 } // End PCD_DumpVersionToSerial()
4763  09d0 84            	pop	a
4764  09d1 81            	ret
4835                     ; 819 void PICC_DumpToSerial(Uid_str *uid0	///< Pointer to Uid struct returned from a successful PICC_Select().
4835                     ; 820 								) {
4836                     	switch	.text
4837  09d2               _PICC_DumpToSerial:
4839  09d2 89            	pushw	x
4840  09d3 5208          	subw	sp,#8
4841       00000008      OFST:	set	8
4844                     ; 826 	PICC_DumpDetailsToSerial(uid0);
4846  09d5 ad4c          	call	_PICC_DumpDetailsToSerial
4848                     ; 829 	piccType = PICC_GetType(uid0->sak);
4850  09d7 1e09          	ldw	x,(OFST+1,sp)
4851  09d9 e60b          	ld	a,(11,x)
4852  09db ad8f          	call	L3_PICC_GetType
4854  09dd 6b01          	ld	(OFST-7,sp),a
4856                     ; 830 	switch (piccType) {
4858  09df 7b01          	ld	a,(OFST-7,sp)
4860                     ; 853 		case PICC_TYPE_UNKNOWN:
4860                     ; 854 		case PICC_TYPE_NOT_COMPLETE:
4860                     ; 855 		default:
4860                     ; 856 			break; // No memory dump here
4861  09e1 a003          	sub	a,#3
4862  09e3 270b          	jreq	L1412
4863  09e5 4a            	dec	a
4864  09e6 2708          	jreq	L1412
4865  09e8 4a            	dec	a
4866  09e9 2705          	jreq	L1412
4867  09eb 4a            	dec	a
4868  09ec 272c          	jreq	L3412
4869  09ee 202d          	jra	L7022
4870  09f0               L1412:
4871                     ; 831 		case PICC_TYPE_MIFARE_MINI:
4871                     ; 832 		case PICC_TYPE_MIFARE_1K:
4871                     ; 833 		case PICC_TYPE_MIFARE_4K:
4871                     ; 834 			// All keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
4871                     ; 835 			for (i = 0; i < 6; i++) {
4873  09f0 0f08          	clr	(OFST+0,sp)
4875  09f2               L1122:
4876                     ; 836 				key.keyByte[i] = 0xFF;
4878  09f2 96            	ldw	x,sp
4879  09f3 1c0002        	addw	x,#OFST-6
4880  09f6 9f            	ld	a,xl
4881  09f7 5e            	swapw	x
4882  09f8 1b08          	add	a,(OFST+0,sp)
4883  09fa 2401          	jrnc	L432
4884  09fc 5c            	incw	x
4885  09fd               L432:
4886  09fd 02            	rlwa	x,a
4887  09fe a6ff          	ld	a,#255
4888  0a00 f7            	ld	(x),a
4889                     ; 835 			for (i = 0; i < 6; i++) {
4891  0a01 0c08          	inc	(OFST+0,sp)
4895  0a03 7b08          	ld	a,(OFST+0,sp)
4896  0a05 a106          	cp	a,#6
4897  0a07 25e9          	jrult	L1122
4898                     ; 838 			PICC_DumpMifareClassicToSerial(uid0, piccType, &key);
4900  0a09 96            	ldw	x,sp
4901  0a0a 1c0002        	addw	x,#OFST-6
4902  0a0d 89            	pushw	x
4903  0a0e 7b03          	ld	a,(OFST-5,sp)
4904  0a10 88            	push	a
4905  0a11 1e0c          	ldw	x,(OFST+4,sp)
4906  0a13 cd0000        	call	_PICC_DumpMifareClassicToSerial
4908  0a16 5b03          	addw	sp,#3
4909                     ; 839 			break;
4911  0a18 2003          	jra	L7022
4912  0a1a               L3412:
4913                     ; 841 		case PICC_TYPE_MIFARE_UL:
4913                     ; 842 			PICC_DumpMifareUltralightToSerial();
4915  0a1a cd0000        	call	_PICC_DumpMifareUltralightToSerial
4917                     ; 843 			break;
4919  0a1d               L5412:
4920                     ; 845 		case PICC_TYPE_ISO_14443_4:
4920                     ; 846 		case PICC_TYPE_MIFARE_DESFIRE:
4920                     ; 847 		case PICC_TYPE_ISO_18092:
4920                     ; 848 		case PICC_TYPE_MIFARE_PLUS:
4920                     ; 849 		case PICC_TYPE_TNP3XXX:
4920                     ; 850 			//Serial.println(F("Dumping memory contents not implemented for that PICC type."));
4920                     ; 851 			break;
4922  0a1d               L7412:
4923                     ; 853 		case PICC_TYPE_UNKNOWN:
4923                     ; 854 		case PICC_TYPE_NOT_COMPLETE:
4923                     ; 855 		default:
4923                     ; 856 			break; // No memory dump here
4925  0a1d               L7022:
4926                     ; 860 	PICC_HaltA(); // Already done if it was a MIFARE Classic PICC.
4928  0a1d cd06e9        	call	_PICC_HaltA
4930                     ; 861 } // End PICC_DumpToSerial()
4933  0a20 5b0a          	addw	sp,#10
4934  0a22 81            	ret
4992                     ; 868 void PICC_DumpDetailsToSerial(Uid_str *uid0	///< Pointer to Uid struct returned from a successful PICC_Select().
4992                     ; 869 									) {
4993                     	switch	.text
4994  0a23               _PICC_DumpDetailsToSerial:
4996  0a23 89            	pushw	x
4997  0a24 89            	pushw	x
4998       00000002      OFST:	set	2
5001                     ; 874 	for (i = 0; i < uid0->size; i++) {
5003  0a25 0f02          	clr	(OFST+0,sp)
5006  0a27 2011          	jra	L3522
5007  0a29               L7422:
5008                     ; 875 		if(uid0->uidByte[i] < 0x10)
5010  0a29 7b03          	ld	a,(OFST+1,sp)
5011  0a2b 97            	ld	xl,a
5012  0a2c 7b04          	ld	a,(OFST+2,sp)
5013  0a2e 1b02          	add	a,(OFST+0,sp)
5014  0a30 2401          	jrnc	L042
5015  0a32 5c            	incw	x
5016  0a33               L042:
5017  0a33 02            	rlwa	x,a
5018  0a34 e601          	ld	a,(1,x)
5019  0a36 a110          	cp	a,#16
5021  0a38               L1622:
5022                     ; 874 	for (i = 0; i < uid0->size; i++) {
5024  0a38 0c02          	inc	(OFST+0,sp)
5026  0a3a               L3522:
5029  0a3a 1e03          	ldw	x,(OFST+1,sp)
5030  0a3c f6            	ld	a,(x)
5031  0a3d 1102          	cp	a,(OFST+0,sp)
5032  0a3f 22e8          	jrugt	L7422
5033                     ; 886 	if(uid0->sak < 0x10)
5035  0a41 1e03          	ldw	x,(OFST+1,sp)
5036  0a43 e60b          	ld	a,(11,x)
5037  0a45 a110          	cp	a,#16
5038  0a47 2407          	jruge	L3622
5039                     ; 891 	piccType = PICC_GetType(uid0->sak);
5041  0a49 1e03          	ldw	x,(OFST+1,sp)
5042  0a4b e60b          	ld	a,(11,x)
5043  0a4d cd096c        	call	L3_PICC_GetType
5045  0a50               L3622:
5046                     ; 894 } // End PICC_DumpDetailsToSerial()
5049  0a50 5b04          	addw	sp,#4
5050  0a52 81            	ret
5108                     ; 906 bool PICC_IsNewCardPresent(void) 
5108                     ; 907 {
5109                     	switch	.text
5110  0a53               _PICC_IsNewCardPresent:
5112  0a53 5204          	subw	sp,#4
5113       00000004      OFST:	set	4
5116                     ; 909 	uint8_t bufferSize = sizeof(bufferATQA);
5118  0a55 a602          	ld	a,#2
5119  0a57 6b03          	ld	(OFST-1,sp),a
5121                     ; 913 	MFRC_WriteReg(TxModeReg, 0x00);
5123  0a59 ae2400        	ldw	x,#9216
5124  0a5c cd0000        	call	_MFRC_WriteReg
5126                     ; 914 	MFRC_WriteReg(RxModeReg, 0x00);
5128  0a5f ae2600        	ldw	x,#9728
5129  0a62 cd0000        	call	_MFRC_WriteReg
5131                     ; 916 	MFRC_WriteReg(ModWidthReg, 0x26);
5133  0a65 ae4826        	ldw	x,#18470
5134  0a68 cd0000        	call	_MFRC_WriteReg
5136                     ; 918 	result = PICC_RequestA(bufferATQA, &bufferSize);
5138  0a6b 96            	ldw	x,sp
5139  0a6c 1c0003        	addw	x,#OFST-1
5140  0a6f 89            	pushw	x
5141  0a70 96            	ldw	x,sp
5142  0a71 1c0003        	addw	x,#OFST-1
5143  0a74 cd02f7        	call	_PICC_RequestA
5145  0a77 85            	popw	x
5146  0a78 6b04          	ld	(OFST+0,sp),a
5148                     ; 919 	return ((result == STATUS_OK) || (result == STATUS_COLLISION));
5150  0a7a 0d04          	tnz	(OFST+0,sp)
5151  0a7c 2706          	jreq	L642
5152  0a7e 7b04          	ld	a,(OFST+0,sp)
5153  0a80 a102          	cp	a,#2
5154  0a82 2604          	jrne	L442
5155  0a84               L642:
5156  0a84 a601          	ld	a,#1
5157  0a86 2001          	jra	L052
5158  0a88               L442:
5159  0a88 4f            	clr	a
5160  0a89               L052:
5163  0a89 5b04          	addw	sp,#4
5164  0a8b 81            	ret
5203                     ; 930 bool PICC_ReadCardSerial(void) 
5203                     ; 931 {
5204                     	switch	.text
5205  0a8c               _PICC_ReadCardSerial:
5207  0a8c 88            	push	a
5208       00000001      OFST:	set	1
5211                     ; 933 	result = PICC_Select(&uid, 0);
5213  0a8d 4b00          	push	#0
5214  0a8f ae0002        	ldw	x,#_uid
5215  0a92 cd0366        	call	_PICC_Select
5217  0a95 5b01          	addw	sp,#1
5218  0a97 6b01          	ld	(OFST+0,sp),a
5220                     ; 934 	return (result == STATUS_OK);
5222  0a99 0d01          	tnz	(OFST+0,sp)
5223  0a9b 2604          	jrne	L452
5224  0a9d a601          	ld	a,#1
5225  0a9f 2001          	jra	L652
5226  0aa1               L452:
5227  0aa1 4f            	clr	a
5228  0aa2               L652:
5231  0aa2 5b01          	addw	sp,#1
5232  0aa4 81            	ret
5256                     	xdef	_count
5257                     	xdef	_PICC_ReadCardSerial
5258                     	xdef	_PICC_IsNewCardPresent
5259                     	xref	_PICC_DumpMifareUltralightToSerial
5260                     	xref	_PICC_DumpMifareClassicToSerial
5261                     	xdef	_PICC_DumpDetailsToSerial
5262                     	xdef	_PICC_DumpToSerial
5263                     	xdef	_PCD_DumpVersionToSerial
5264                     	xdef	_MIFARE_Read
5265                     	xdef	_PCD_StopCrypto1
5266                     	xdef	_PCD_Authenticate
5267                     	xdef	_PICC_HaltA
5268                     	xdef	_PICC_Select
5269                     	xdef	_PICC_REQA_or_WUPA
5270                     	xdef	_PICC_WakeupA
5271                     	xdef	_PICC_RequestA
5272                     	xdef	_MFRC_CommunicateWithPICC
5273                     	xdef	_MFRC_TransceiveData
5274                     	xdef	_MFRC_AntennaOn
5275                     	xdef	_MFRC_Reset
5276                     	xdef	_MFRC_Init
5277                     	xdef	_MFRC_CalculateCRC
5278                     	xdef	_MFRC_ClearRegBitMask
5279                     	xdef	_MFRC_SetRegBitMask
5280                     	xdef	_MFRC_ReadRegs
5281                     	xdef	_MFRC_ReadReg
5282                     	xdef	_MFRC_WriteRegs
5283                     	xdef	_MFRC_WriteReg
5284                     	switch	.ubsct
5285  0002               _uid:
5286  0002 000000000000  	ds.b	12
5287                     	xdef	_uid
5288                     	xref	_delay_us
5289                     	xref	_SPI_Transceive
5290                     	xref	_SPI_GetFlagStatus
5291                     	xref	_SPI_ReceiveData
5292                     	xref	_SPI_SendData
5293                     	xref	_GPIO_ResetBits
5294                     	xref	_GPIO_SetBits
5295                     	xref.b	c_x
5296                     	xref.b	c_y
5316                     	xref	c_smodx
5317                     	xref	c_idiv
5318                     	end
