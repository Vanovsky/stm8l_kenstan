   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  15                     	bsct
  16  0000               L3_counter1:
  17  0000 00            	dc.b	0
  18  0001               L5_T_TIMEOUT:
  19  0001 14            	dc.b	20
  58                     	bsct
  59  0002               _buz_count:
  60  0002 0000          	dc.w	0
  61  0004               _buz_period:
  62  0004 03e8          	dc.w	1000
  63  0006               _push_once:
  64  0006 01            	dc.b	1
 104                     ; 7 void delay_us(uint8_t del)
 104                     ; 8 {
 106                     	switch	.text
 107  0000               _delay_us:
 109  0000 88            	push	a
 110       00000000      OFST:	set	0
 113  0001               L55:
 114                     ; 9 	while(del--);
 116  0001 7b01          	ld	a,(OFST+1,sp)
 117  0003 0a01          	dec	(OFST+1,sp)
 118  0005 4d            	tnz	a
 119  0006 26f9          	jrne	L55
 120                     ; 10 }
 123  0008 84            	pop	a
 124  0009 81            	ret
 150                     ; 15 void EEPROM_config(void)
 150                     ; 16 {
 151                     	switch	.text
 152  000a               _EEPROM_config:
 156                     ; 17 	FLASH_DeInit();
 158  000a cd0000        	call	_FLASH_DeInit
 160                     ; 18 	FLASH_Unlock(FLASH_MemType_Data);
 162  000d a6f7          	ld	a,#247
 163  000f cd0000        	call	_FLASH_Unlock
 165                     ; 19 	FLASH_SetProgrammingTime(FLASH_ProgramTime_Standard);
 167  0012 4f            	clr	a
 168  0013 cd0000        	call	_FLASH_SetProgrammingTime
 170                     ; 20 }
 173  0016 81            	ret
 210                     ; 22 void EEPROM_writeRoutine(uint8_t w_data)
 210                     ; 23 {
 211                     	switch	.text
 212  0017               _EEPROM_writeRoutine:
 216                     ; 24 	FLASH_ProgramByte(0x0013FF, w_data);
 218  0017 88            	push	a
 219  0018 ae13ff        	ldw	x,#5119
 220  001b 89            	pushw	x
 221  001c ae0000        	ldw	x,#0
 222  001f 89            	pushw	x
 223  0020 cd0000        	call	_FLASH_ProgramByte
 225  0023 5b05          	addw	sp,#5
 227  0025               L111:
 228                     ; 25 	while(!(FLASH_GetFlagStatus(FLASH_FLAG_EOP)));
 230  0025 a604          	ld	a,#4
 231  0027 cd0000        	call	_FLASH_GetFlagStatus
 233  002a 4d            	tnz	a
 234  002b 27f8          	jreq	L111
 235                     ; 26 }
 238  002d 81            	ret
 273                     ; 28 uint8_t EEPROM_readRoutine(void)
 273                     ; 29 {
 274                     	switch	.text
 275  002e               _EEPROM_readRoutine:
 277  002e 88            	push	a
 278       00000001      OFST:	set	1
 281                     ; 31 	r_data = FLASH_ReadByte(0x0013FF);
 283  002f ae13ff        	ldw	x,#5119
 284  0032 89            	pushw	x
 285  0033 ae0000        	ldw	x,#0
 286  0036 89            	pushw	x
 287  0037 cd0000        	call	_FLASH_ReadByte
 289  003a 5b04          	addw	sp,#4
 290  003c 6b01          	ld	(OFST+0,sp),a
 292                     ; 32 	return r_data;
 294  003e 7b01          	ld	a,(OFST+0,sp)
 297  0040 5b01          	addw	sp,#1
 298  0042 81            	ret
 325                     ; 38 void CLK_config(void)
 325                     ; 39 {
 326                     	switch	.text
 327  0043               _CLK_config:
 331                     ; 40 	CLK_SYSCLKSourceSwitchCmd(ENABLE);
 333  0043 a601          	ld	a,#1
 334  0045 cd0000        	call	_CLK_SYSCLKSourceSwitchCmd
 336                     ; 41   CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
 338  0048 a601          	ld	a,#1
 339  004a cd0000        	call	_CLK_SYSCLKSourceConfig
 341                     ; 42   CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
 343  004d 4f            	clr	a
 344  004e cd0000        	call	_CLK_SYSCLKDivConfig
 347  0051               L541:
 348                     ; 43   while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}
 350  0051 cd0000        	call	_CLK_GetSYSCLKSource
 352  0054 a101          	cp	a,#1
 353  0056 26f9          	jrne	L541
 354                     ; 44 }
 357  0058 81            	ret
 391                     ; 46 void setSeg(int segNum)
 391                     ; 47 {
 392                     	switch	.text
 393  0059               _setSeg:
 397                     ; 48 	GPIOC->ODR &= ~(GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3);
 399  0059 c6500a        	ld	a,20490
 400  005c a4ec          	and	a,#236
 401  005e c7500a        	ld	20490,a
 402                     ; 49 	GPIOD->ODR &= ~GPIO_Pin_0;
 404  0061 7211500f      	bres	20495,#0
 405                     ; 51 	switch (segNum)
 408                     ; 64 			break;
 409  0065 5d            	tnzw	x
 410  0066 2710          	jreq	L151
 411  0068 5a            	decw	x
 412  0069 2713          	jreq	L351
 413  006b 5a            	decw	x
 414  006c 2716          	jreq	L551
 415  006e               L751:
 416                     ; 62 		default:
 416                     ; 63 			GPIOC->ODR &= ~(GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3);
 418  006e c6500a        	ld	a,20490
 419  0071 a4ec          	and	a,#236
 420  0073 c7500a        	ld	20490,a
 421                     ; 64 			break;
 423  0076 2010          	jra	L102
 424  0078               L151:
 425                     ; 53 		case 0:
 425                     ; 54 			GPIOC->ODR |= GPIO_Pin_SEG1;
 427  0078 7218500a      	bset	20490,#4
 428                     ; 55 			break;
 430  007c 200a          	jra	L102
 431  007e               L351:
 432                     ; 56 		case 1:
 432                     ; 57 			GPIOC->ODR |= GPIO_Pin_SEG2;
 434  007e 7212500a      	bset	20490,#1
 435                     ; 58 			break;
 437  0082 2004          	jra	L102
 438  0084               L551:
 439                     ; 59 		case 2:
 439                     ; 60 			GPIOC->ODR |= GPIO_Pin_SEG3;
 441  0084 7210500a      	bset	20490,#0
 442                     ; 61 			break;
 444  0088               L102:
 445                     ; 66 }
 448  0088 81            	ret
 493                     ; 68 void writeNumSym(uint8_t num, uint8_t seg)
 493                     ; 69 {
 494                     	switch	.text
 495  0089               _writeNumSym:
 497  0089 89            	pushw	x
 498       00000000      OFST:	set	0
 501                     ; 71 	setSeg(seg);
 503  008a 9f            	ld	a,xl
 504  008b 5f            	clrw	x
 505  008c 97            	ld	xl,a
 506  008d adca          	call	_setSeg
 508                     ; 74 	GPIO_SetBits(GPIOB, GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G | GPIO_Pin_DP);
 510  008f 4bff          	push	#255
 511  0091 ae5005        	ldw	x,#20485
 512  0094 cd0000        	call	_GPIO_SetBits
 514  0097 84            	pop	a
 515                     ; 75 	switch (num)
 517  0098 7b01          	ld	a,(OFST+1,sp)
 519                     ; 109 			break;
 520  009a 4d            	tnz	a
 521  009b 2725          	jreq	L302
 522  009d 4a            	dec	a
 523  009e 272c          	jreq	L502
 524  00a0 4a            	dec	a
 525  00a1 2733          	jreq	L702
 526  00a3 4a            	dec	a
 527  00a4 273a          	jreq	L112
 528  00a6 4a            	dec	a
 529  00a7 2741          	jreq	L312
 530  00a9 4a            	dec	a
 531  00aa 2748          	jreq	L512
 532  00ac 4a            	dec	a
 533  00ad 274f          	jreq	L712
 534  00af 4a            	dec	a
 535  00b0 2756          	jreq	L122
 536  00b2 4a            	dec	a
 537  00b3 275d          	jreq	L322
 538  00b5 4a            	dec	a
 539  00b6 2764          	jreq	L522
 540  00b8               L722:
 541                     ; 107 		default:
 541                     ; 108 			GPIOB->ODR |= (GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
 543  00b8 c65005        	ld	a,20485
 544  00bb aa7f          	or	a,#127
 545  00bd c75005        	ld	20485,a
 546                     ; 109 			break;
 548  00c0 2062          	jra	L552
 549  00c2               L302:
 550                     ; 77 		case 0: 
 550                     ; 78 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B |GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E |GPIO_Pin_F);	
 552  00c2 c65005        	ld	a,20485
 553  00c5 a4c0          	and	a,#192
 554  00c7 c75005        	ld	20485,a
 555                     ; 79 			break;
 557  00ca 2058          	jra	L552
 558  00cc               L502:
 559                     ; 80 		case 1: 
 559                     ; 81 			GPIOB->ODR &= ~(GPIO_Pin_B | GPIO_Pin_C);
 561  00cc c65005        	ld	a,20485
 562  00cf a4f9          	and	a,#249
 563  00d1 c75005        	ld	20485,a
 564                     ; 82 			break;
 566  00d4 204e          	jra	L552
 567  00d6               L702:
 568                     ; 83 		case 2: 
 568                     ; 84 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_G | GPIO_Pin_D | GPIO_Pin_E);
 570  00d6 c65005        	ld	a,20485
 571  00d9 a4a4          	and	a,#164
 572  00db c75005        	ld	20485,a
 573                     ; 85 			break;
 575  00de 2044          	jra	L552
 576  00e0               L112:
 577                     ; 86 		case 3: 
 577                     ; 87 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_G);
 579  00e0 c65005        	ld	a,20485
 580  00e3 a4b0          	and	a,#176
 581  00e5 c75005        	ld	20485,a
 582                     ; 88 			break;
 584  00e8 203a          	jra	L552
 585  00ea               L312:
 586                     ; 89 		case 4: 
 586                     ; 90 			GPIOB->ODR &= ~(GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_F | GPIO_Pin_G);
 588  00ea c65005        	ld	a,20485
 589  00ed a499          	and	a,#153
 590  00ef c75005        	ld	20485,a
 591                     ; 91 			break;
 593  00f2 2030          	jra	L552
 594  00f4               L512:
 595                     ; 92 		case 5: 
 595                     ; 93 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_G);
 597  00f4 c65005        	ld	a,20485
 598  00f7 a492          	and	a,#146
 599  00f9 c75005        	ld	20485,a
 600                     ; 94 			break;
 602  00fc 2026          	jra	L552
 603  00fe               L712:
 604                     ; 95 		case 6: 
 604                     ; 96 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_E | GPIO_Pin_G);
 606  00fe c65005        	ld	a,20485
 607  0101 a482          	and	a,#130
 608  0103 c75005        	ld	20485,a
 609                     ; 97 			break;
 611  0106 201c          	jra	L552
 612  0108               L122:
 613                     ; 98 		case 7: 
 613                     ; 99 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C);
 615  0108 c65005        	ld	a,20485
 616  010b a4f8          	and	a,#248
 617  010d c75005        	ld	20485,a
 618                     ; 100 			break;
 620  0110 2012          	jra	L552
 621  0112               L322:
 622                     ; 101 		case 8: 
 622                     ; 102 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
 624  0112 c65005        	ld	a,20485
 625  0115 a480          	and	a,#128
 626  0117 c75005        	ld	20485,a
 627                     ; 103 			break;
 629  011a 2008          	jra	L552
 630  011c               L522:
 631                     ; 104 		case 9: 
 631                     ; 105 			GPIOB->ODR &= ~(GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_F | GPIO_Pin_G);
 633  011c c65005        	ld	a,20485
 634  011f a490          	and	a,#144
 635  0121 c75005        	ld	20485,a
 636                     ; 106 			break;
 638  0124               L552:
 639                     ; 111 }
 642  0124 85            	popw	x
 643  0125 81            	ret
 698                     ; 113 void writeNumeric(uint8_t numeric)
 698                     ; 114 {
 699                     	switch	.text
 700  0126               _writeNumeric:
 702  0126 88            	push	a
 703  0127 5204          	subw	sp,#4
 704       00000004      OFST:	set	4
 707                     ; 118 	num[0] = numeric % 10;
 709  0129 5f            	clrw	x
 710  012a 97            	ld	xl,a
 711  012b a60a          	ld	a,#10
 712  012d 62            	div	x,a
 713  012e 5f            	clrw	x
 714  012f 97            	ld	xl,a
 715  0130 9f            	ld	a,xl
 716  0131 6b01          	ld	(OFST-3,sp),a
 718                     ; 119 	num[1] = (numeric / 10) % 10;
 720  0133 7b05          	ld	a,(OFST+1,sp)
 721  0135 5f            	clrw	x
 722  0136 97            	ld	xl,a
 723  0137 a60a          	ld	a,#10
 724  0139 cd0000        	call	c_sdivx
 726  013c a60a          	ld	a,#10
 727  013e cd0000        	call	c_smodx
 729  0141 01            	rrwa	x,a
 730  0142 6b02          	ld	(OFST-2,sp),a
 731  0144 02            	rlwa	x,a
 733                     ; 120 	num[2] = numeric / 100;
 735  0145 7b05          	ld	a,(OFST+1,sp)
 736  0147 5f            	clrw	x
 737  0148 97            	ld	xl,a
 738  0149 a664          	ld	a,#100
 739  014b 62            	div	x,a
 740  014c 9f            	ld	a,xl
 741  014d 6b03          	ld	(OFST-1,sp),a
 743                     ; 121 	for(i = 0; i < 3; i++) 
 745  014f 0f04          	clr	(OFST+0,sp)
 747  0151               L503:
 748                     ; 123 		writeNumSym(num[i], i);
 750  0151 7b04          	ld	a,(OFST+0,sp)
 751  0153 97            	ld	xl,a
 752  0154 89            	pushw	x
 753  0155 96            	ldw	x,sp
 754  0156 1c0003        	addw	x,#OFST-1
 755  0159 9f            	ld	a,xl
 756  015a 5e            	swapw	x
 757  015b 1b06          	add	a,(OFST+2,sp)
 758  015d 2401          	jrnc	L42
 759  015f 5c            	incw	x
 760  0160               L42:
 761  0160 02            	rlwa	x,a
 762  0161 f6            	ld	a,(x)
 763  0162 85            	popw	x
 764  0163 95            	ld	xh,a
 765  0164 cd0089        	call	_writeNumSym
 767                     ; 124 		delay_us(250);
 769  0167 a6fa          	ld	a,#250
 770  0169 cd0000        	call	_delay_us
 772                     ; 125 		delay_us(250);
 774  016c a6fa          	ld	a,#250
 775  016e cd0000        	call	_delay_us
 777                     ; 126 		delay_us(250);
 779  0171 a6fa          	ld	a,#250
 780  0173 cd0000        	call	_delay_us
 782                     ; 121 	for(i = 0; i < 3; i++) 
 784  0176 0c04          	inc	(OFST+0,sp)
 788  0178 7b04          	ld	a,(OFST+0,sp)
 789  017a a103          	cp	a,#3
 790  017c 25d3          	jrult	L503
 791                     ; 128 	writeNumSym(10, 3);
 793  017e ae0a03        	ldw	x,#2563
 794  0181 cd0089        	call	_writeNumSym
 796                     ; 129 }
 799  0184 5b05          	addw	sp,#5
 800  0186 81            	ret
 854                     ; 131 void writeNumericPos(uint8_t numeric, uint8_t pos)
 854                     ; 132 {
 855                     	switch	.text
 856  0187               _writeNumericPos:
 858  0187 89            	pushw	x
 859  0188 5203          	subw	sp,#3
 860       00000003      OFST:	set	3
 863                     ; 136 	num[0] = numeric % 10;
 865  018a 9e            	ld	a,xh
 866  018b 5f            	clrw	x
 867  018c 97            	ld	xl,a
 868  018d a60a          	ld	a,#10
 869  018f 62            	div	x,a
 870  0190 5f            	clrw	x
 871  0191 97            	ld	xl,a
 872  0192 9f            	ld	a,xl
 873  0193 6b01          	ld	(OFST-2,sp),a
 875                     ; 137 	num[1] = (numeric / 10) % 10;
 877  0195 7b04          	ld	a,(OFST+1,sp)
 878  0197 5f            	clrw	x
 879  0198 97            	ld	xl,a
 880  0199 a60a          	ld	a,#10
 881  019b cd0000        	call	c_sdivx
 883  019e a60a          	ld	a,#10
 884  01a0 cd0000        	call	c_smodx
 886  01a3 01            	rrwa	x,a
 887  01a4 6b02          	ld	(OFST-1,sp),a
 888  01a6 02            	rlwa	x,a
 890                     ; 138 	num[2] = numeric / 100;
 892  01a7 7b04          	ld	a,(OFST+1,sp)
 893  01a9 5f            	clrw	x
 894  01aa 97            	ld	xl,a
 895  01ab a664          	ld	a,#100
 896  01ad 62            	div	x,a
 897  01ae 9f            	ld	a,xl
 898  01af 6b03          	ld	(OFST+0,sp),a
 900                     ; 140 	writeNumSym(num[pos], pos);
 902  01b1 7b05          	ld	a,(OFST+2,sp)
 903  01b3 97            	ld	xl,a
 904  01b4 89            	pushw	x
 905  01b5 96            	ldw	x,sp
 906  01b6 1c0003        	addw	x,#OFST+0
 907  01b9 9f            	ld	a,xl
 908  01ba 5e            	swapw	x
 909  01bb 1b07          	add	a,(OFST+4,sp)
 910  01bd 2401          	jrnc	L03
 911  01bf 5c            	incw	x
 912  01c0               L03:
 913  01c0 02            	rlwa	x,a
 914  01c1 f6            	ld	a,(x)
 915  01c2 85            	popw	x
 916  01c3 95            	ld	xh,a
 917  01c4 cd0089        	call	_writeNumSym
 919                     ; 141 }
 922  01c7 5b05          	addw	sp,#5
 923  01c9 81            	ret
 947                     ; 143 void GPIO_config(void)
 947                     ; 144 {
 948                     	switch	.text
 949  01ca               _GPIO_config:
 953                     ; 177 	GPIO_Init(GPIOB, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6| GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast);
 955  01ca 4bf0          	push	#240
 956  01cc 4bff          	push	#255
 957  01ce ae5005        	ldw	x,#20485
 958  01d1 cd0000        	call	_GPIO_Init
 960  01d4 85            	popw	x
 961                     ; 178 	GPIO_Init(GPIOC, GPIO_Pin_SEG1 | GPIO_Pin_SEG2 | GPIO_Pin_SEG3 | GPIO_Pin_BUZZ, GPIO_Mode_Out_PP_High_Fast);
 963  01d5 4bf0          	push	#240
 964  01d7 4b53          	push	#83
 965  01d9 ae500a        	ldw	x,#20490
 966  01dc cd0000        	call	_GPIO_Init
 968  01df 85            	popw	x
 969                     ; 179 	GPIO_Init(GPIOD, GPIO_Pin_0,  GPIO_Mode_Out_PP_High_Fast);
 971  01e0 4bf0          	push	#240
 972  01e2 4b01          	push	#1
 973  01e4 ae500f        	ldw	x,#20495
 974  01e7 cd0000        	call	_GPIO_Init
 976  01ea 85            	popw	x
 977                     ; 180 	GPIO_Init(GPIOC, GPIO_Pin_2 | GPIO_Pin_3, GPIO_Mode_In_PU_No_IT);
 979  01eb 4b40          	push	#64
 980  01ed 4b0c          	push	#12
 981  01ef ae500a        	ldw	x,#20490
 982  01f2 cd0000        	call	_GPIO_Init
 984  01f5 85            	popw	x
 985                     ; 181 }
 988  01f6 81            	ret
1017                     ; 183 void TIM2_config(void)
1017                     ; 184 {
1018                     	switch	.text
1019  01f7               _TIM2_config:
1023                     ; 200 	TIM2_DeInit();	
1025  01f7 cd0000        	call	_TIM2_DeInit
1027                     ; 201 	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);	
1029  01fa ae0001        	ldw	x,#1
1030  01fd cd0000        	call	_CLK_PeripheralClockConfig
1032                     ; 202 	TIM2_TimeBaseInit(TIM2_Prescaler_128, TIM2_CounterMode_Up, 100);
1034  0200 ae0064        	ldw	x,#100
1035  0203 89            	pushw	x
1036  0204 ae0700        	ldw	x,#1792
1037  0207 cd0000        	call	_TIM2_TimeBaseInit
1039  020a 85            	popw	x
1040                     ; 203 	TIM2_ARRPreloadConfig(ENABLE);		
1042  020b a601          	ld	a,#1
1043  020d cd0000        	call	_TIM2_ARRPreloadConfig
1045                     ; 204 	TIM2_ITConfig(TIM2_IT_Update, ENABLE);
1047  0210 ae0101        	ldw	x,#257
1048  0213 cd0000        	call	_TIM2_ITConfig
1050                     ; 206 	TIM2_Cmd(ENABLE);
1052  0216 a601          	ld	a,#1
1053  0218 cd0000        	call	_TIM2_Cmd
1055                     ; 207 }
1058  021b 81            	ret
1087                     ; 209 void TIM3_config(void)
1087                     ; 210 {	
1088                     	switch	.text
1089  021c               _TIM3_config:
1093                     ; 211 	TIM3_DeInit();	
1095  021c cd0000        	call	_TIM3_DeInit
1097                     ; 212 	CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);	
1099  021f ae0101        	ldw	x,#257
1100  0222 cd0000        	call	_CLK_PeripheralClockConfig
1102                     ; 213 	TIM3_TimeBaseInit(TIM3_Prescaler_128, TIM3_CounterMode_Up, 12500);
1104  0225 ae30d4        	ldw	x,#12500
1105  0228 89            	pushw	x
1106  0229 ae0700        	ldw	x,#1792
1107  022c cd0000        	call	_TIM3_TimeBaseInit
1109  022f 85            	popw	x
1110                     ; 214 	TIM3_ARRPreloadConfig(ENABLE);		
1112  0230 a601          	ld	a,#1
1113  0232 cd0000        	call	_TIM3_ARRPreloadConfig
1115                     ; 215 	TIM3_ITConfig(TIM3_IT_Update, ENABLE);
1117  0235 ae0101        	ldw	x,#257
1118  0238 cd0000        	call	_TIM3_ITConfig
1120                     ; 217 	TIM3_Cmd(ENABLE);
1122  023b a601          	ld	a,#1
1123  023d cd0000        	call	_TIM3_Cmd
1125                     ; 218 }
1128  0240 81            	ret
1153                     ; 220 void EXTI_config(void)
1153                     ; 221 {
1154                     	switch	.text
1155  0241               _EXTI_config:
1159                     ; 222 	EXTI_DeInit();
1161  0241 cd0000        	call	_EXTI_DeInit
1163                     ; 223 	EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling);
1165  0244 ae0602        	ldw	x,#1538
1166  0247 cd0000        	call	_EXTI_SetPinSensitivity
1168                     ; 224 }
1171  024a 81            	ret
1198                     ; 226 void PWRCNTRL_config(void)
1198                     ; 227 {
1199                     	switch	.text
1200  024b               _PWRCNTRL_config:
1204                     ; 228 	PWR_DeInit();
1206  024b cd0000        	call	_PWR_DeInit
1208                     ; 229 	PWR_PVDLevelConfig(PWR_PVDLevel_2V05);
1210  024e a602          	ld	a,#2
1211  0250 cd0000        	call	_PWR_PVDLevelConfig
1213                     ; 230 	PWR_PVDITConfig(ENABLE);
1215  0253 a601          	ld	a,#1
1216  0255 cd0000        	call	_PWR_PVDITConfig
1218                     ; 231 	PWR_PVDCmd(ENABLE);
1220  0258 a601          	ld	a,#1
1221  025a cd0000        	call	_PWR_PVDCmd
1223                     ; 232 }
1226  025d 81            	ret
1249                     ; 234 void PWRCNTRL_routine(void)
1249                     ; 235 {
1250                     	switch	.text
1251  025e               _PWRCNTRL_routine:
1255                     ; 237 }
1258  025e 81            	ret
1261                     	bsct
1262  0007               _co:
1263  0007 19            	dc.b	25
1264  0008               _i:
1265  0008 00            	dc.b	0
1266  0009               _alarm_state:
1267  0009 00            	dc.b	0
1268  000a               _alarm_type:
1269  000a 00            	dc.b	0
1270  000b               _disp_state:
1271  000b 00            	dc.b	0
1272  000c               _count_state:
1273  000c 01            	dc.b	1
1312                     ; 247 int main()
1312                     ; 248 {
1313                     	switch	.text
1314  025f               _main:
1318                     ; 249 	GPIOB->ODR |= (GPIO_Pin_A | GPIO_Pin_B | GPIO_Pin_C | GPIO_Pin_D | GPIO_Pin_E | GPIO_Pin_F | GPIO_Pin_G);
1320  025f c65005        	ld	a,20485
1321  0262 aa7f          	or	a,#127
1322  0264 c75005        	ld	20485,a
1323                     ; 250 	GPIOC->ODR |= GPIO_Pin_BUZZ;
1325  0267 721c500a      	bset	20490,#6
1326                     ; 252 	enableInterrupts();
1329  026b 9a            rim
1331                     ; 253 	CLK_config();
1334  026c cd0043        	call	_CLK_config
1336                     ; 254 	TIM2_config();
1338  026f ad86          	call	_TIM2_config
1340                     ; 255 	TIM3_config();
1342  0271 ada9          	call	_TIM3_config
1344                     ; 256 	GPIO_config();
1346  0273 cd01ca        	call	_GPIO_config
1348                     ; 257 	EEPROM_config();
1350  0276 cd000a        	call	_EEPROM_config
1352                     ; 258 	PWRCNTRL_config();
1354  0279 add0          	call	_PWRCNTRL_config
1356                     ; 260 	counter1 = 0;
1358  027b 3f00          	clr	L3_counter1
1359                     ; 262 	T_TIMEOUT = EEPROM_readRoutine();
1361  027d cd002e        	call	_EEPROM_readRoutine
1363  0280 b701          	ld	L5_T_TIMEOUT,a
1364                     ; 263 	if (T_TIMEOUT == 0) T_TIMEOUT = 20;
1366  0282 3d01          	tnz	L5_T_TIMEOUT
1367  0284 2604          	jrne	L134
1370  0286 35140001      	mov	L5_T_TIMEOUT,#20
1371  028a               L134:
1372                     ; 264 	co = T_TIMEOUT;
1374  028a 450107        	mov	_co,L5_T_TIMEOUT
1375                     ; 265 	alarm_state = 0;
1377  028d 3f09          	clr	_alarm_state
1378  028f               L334:
1379                     ; 268 		save_pre_st = save_st;
1381  028f 450504        	mov	_save_pre_st,_save_st
1382                     ; 269 		save_st = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3);
1384  0292 4b08          	push	#8
1385  0294 ae500a        	ldw	x,#20490
1386  0297 cd0000        	call	_GPIO_ReadInputDataBit
1388  029a 5b01          	addw	sp,#1
1389  029c b705          	ld	_save_st,a
1390                     ; 271 		save_pre_st2 = save_st2;
1392  029e 450302        	mov	_save_pre_st2,_save_st2
1393                     ; 272 		save_st2 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2);
1395  02a1 4b04          	push	#4
1396  02a3 ae500a        	ldw	x,#20490
1397  02a6 cd0000        	call	_GPIO_ReadInputDataBit
1399  02a9 5b01          	addw	sp,#1
1400  02ab b703          	ld	_save_st2,a
1401                     ; 274 		if ((!save_st) && (save_st!= save_pre_st) && (disp_state == 1)) T_TIMEOUT++;
1403  02ad 3d05          	tnz	_save_st
1404  02af 260e          	jrne	L734
1406  02b1 b605          	ld	a,_save_st
1407  02b3 b104          	cp	a,_save_pre_st
1408  02b5 2708          	jreq	L734
1410  02b7 b60b          	ld	a,_disp_state
1411  02b9 a101          	cp	a,#1
1412  02bb 2602          	jrne	L734
1415  02bd 3c01          	inc	L5_T_TIMEOUT
1416  02bf               L734:
1417                     ; 275 		if ((!save_st2) && (save_st2!= save_pre_st2) && (disp_state == 1)) T_TIMEOUT--;
1419  02bf 3d03          	tnz	_save_st2
1420  02c1 260e          	jrne	L144
1422  02c3 b603          	ld	a,_save_st2
1423  02c5 b102          	cp	a,_save_pre_st2
1424  02c7 2708          	jreq	L144
1426  02c9 b60b          	ld	a,_disp_state
1427  02cb a101          	cp	a,#1
1428  02cd 2602          	jrne	L144
1431  02cf 3a01          	dec	L5_T_TIMEOUT
1432  02d1               L144:
1433                     ; 277 		if ((!save_st) && (disp_state != 1)) disp_state = 1;		
1435  02d1 3d05          	tnz	_save_st
1436  02d3 260a          	jrne	L344
1438  02d5 b60b          	ld	a,_disp_state
1439  02d7 a101          	cp	a,#1
1440  02d9 2704          	jreq	L344
1443  02db 3501000b      	mov	_disp_state,#1
1444  02df               L344:
1445                     ; 278 		if ((!save_st2) && (disp_state != 1)) disp_state = 1;
1447  02df 3d03          	tnz	_save_st2
1448  02e1 26ac          	jrne	L334
1450  02e3 b60b          	ld	a,_disp_state
1451  02e5 a101          	cp	a,#1
1452  02e7 27a6          	jreq	L334
1455  02e9 3501000b      	mov	_disp_state,#1
1456  02ed 20a0          	jra	L334
1480                     ; 282 INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
1480                     ; 283 {
1482                     	switch	.text
1483  02ef               f_EXTI2_IRQHandler:
1485  02ef 8a            	push	cc
1486  02f0 84            	pop	a
1487  02f1 a4bf          	and	a,#191
1488  02f3 88            	push	a
1489  02f4 86            	pop	cc
1490  02f5 3b0002        	push	c_x+2
1491  02f8 be00          	ldw	x,c_x
1492  02fa 89            	pushw	x
1493  02fb 3b0002        	push	c_y+2
1494  02fe be00          	ldw	x,c_y
1495  0300 89            	pushw	x
1498                     ; 284 	EXTI_ClearITPendingBit(EXTI_IT_Pin2);
1500  0301 ae0004        	ldw	x,#4
1501  0304 cd0000        	call	_EXTI_ClearITPendingBit
1503                     ; 285 }
1506  0307 85            	popw	x
1507  0308 bf00          	ldw	c_y,x
1508  030a 320002        	pop	c_y+2
1509  030d 85            	popw	x
1510  030e bf00          	ldw	c_x,x
1511  0310 320002        	pop	c_x+2
1512  0313 80            	iret
1535                     ; 287 INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
1535                     ; 288 {
1536                     	switch	.text
1537  0314               f_EXTI3_IRQHandler:
1539  0314 8a            	push	cc
1540  0315 84            	pop	a
1541  0316 a4bf          	and	a,#191
1542  0318 88            	push	a
1543  0319 86            	pop	cc
1544  031a 3b0002        	push	c_x+2
1545  031d be00          	ldw	x,c_x
1546  031f 89            	pushw	x
1547  0320 3b0002        	push	c_y+2
1548  0323 be00          	ldw	x,c_y
1549  0325 89            	pushw	x
1552                     ; 289 	EXTI_ClearITPendingBit(EXTI_IT_Pin3);
1554  0326 ae0008        	ldw	x,#8
1555  0329 cd0000        	call	_EXTI_ClearITPendingBit
1557                     ; 290 }
1560  032c 85            	popw	x
1561  032d bf00          	ldw	c_y,x
1562  032f 320002        	pop	c_y+2
1563  0332 85            	popw	x
1564  0333 bf00          	ldw	c_x,x
1565  0335 320002        	pop	c_x+2
1566  0338 80            	iret
1568                     	bsct
1569  000d               _disp_pos:
1570  000d 0000          	dc.w	0
1602                     ; 293 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
1602                     ; 294 {
1603                     	switch	.text
1604  0339               f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler:
1606  0339 8a            	push	cc
1607  033a 84            	pop	a
1608  033b a4bf          	and	a,#191
1609  033d 88            	push	a
1610  033e 86            	pop	cc
1611  033f 3b0002        	push	c_x+2
1612  0342 be00          	ldw	x,c_x
1613  0344 89            	pushw	x
1614  0345 3b0002        	push	c_y+2
1615  0348 be00          	ldw	x,c_y
1616  034a 89            	pushw	x
1619                     ; 295 	if (disp_state) 
1621  034b 3d0b          	tnz	_disp_state
1622  034d 271d          	jreq	L505
1623                     ; 298 		writeNumericPos(T_TIMEOUT, disp_pos);
1625  034f b60e          	ld	a,_disp_pos+1
1626  0351 97            	ld	xl,a
1627  0352 b601          	ld	a,L5_T_TIMEOUT
1628  0354 95            	ld	xh,a
1629  0355 cd0187        	call	_writeNumericPos
1631                     ; 299 		disp_pos++;
1633  0358 be0d          	ldw	x,_disp_pos
1634  035a 1c0001        	addw	x,#1
1635  035d bf0d          	ldw	_disp_pos,x
1636                     ; 300 		if (disp_pos > 2) disp_pos = 0;
1638  035f 9c            	rvf
1639  0360 be0d          	ldw	x,_disp_pos
1640  0362 a30003        	cpw	x,#3
1641  0365 2f0e          	jrslt	L115
1644  0367 5f            	clrw	x
1645  0368 bf0d          	ldw	_disp_pos,x
1646  036a 2009          	jra	L115
1647  036c               L505:
1648                     ; 303 	else writeNumericPos(T_TIMEOUT, 3);
1650  036c b601          	ld	a,L5_T_TIMEOUT
1651  036e ae0003        	ldw	x,#3
1652  0371 95            	ld	xh,a
1653  0372 cd0187        	call	_writeNumericPos
1655  0375               L115:
1656                     ; 305 	if (alarm_state)
1658  0375 3d09          	tnz	_alarm_state
1659  0377 2738          	jreq	L315
1660                     ; 307 		buz_count++;
1662  0379 be02          	ldw	x,_buz_count
1663  037b 1c0001        	addw	x,#1
1664  037e bf02          	ldw	_buz_count,x
1665                     ; 308 		switch (alarm_type)
1667  0380 b60a          	ld	a,_alarm_type
1669                     ; 316 			default: break;
1670  0382 4d            	tnz	a
1671  0383 2705          	jreq	L764
1672  0385 4a            	dec	a
1673  0386 2711          	jreq	L174
1674  0388 201e          	jra	L715
1675  038a               L764:
1676                     ; 310 			case 0:
1676                     ; 311 				if (buz_count < buz_period / 5) GPIOC->ODR ^= GPIO_Pin_BUZZ;
1678  038a be04          	ldw	x,_buz_period
1679  038c a605          	ld	a,#5
1680  038e 62            	div	x,a
1681  038f b302          	cpw	x,_buz_count
1682  0391 2315          	jrule	L715
1685  0393 901c500a      	bcpl	20490,#6
1686  0397 200f          	jra	L715
1687  0399               L174:
1688                     ; 313 			case 1:
1688                     ; 314 				if (buz_count < buz_period / 5 * 4) GPIOC->ODR ^= GPIO_Pin_BUZZ;
1690  0399 be04          	ldw	x,_buz_period
1691  039b a605          	ld	a,#5
1692  039d 62            	div	x,a
1693  039e 58            	sllw	x
1694  039f 58            	sllw	x
1695  03a0 b302          	cpw	x,_buz_count
1696  03a2 2304          	jrule	L715
1699  03a4 901c500a      	bcpl	20490,#6
1700  03a8               L374:
1701                     ; 316 			default: break;
1703  03a8               L715:
1704                     ; 318 		if (buz_count > buz_period) buz_count = 0;
1706  03a8 be02          	ldw	x,_buz_count
1707  03aa b304          	cpw	x,_buz_period
1708  03ac 2303          	jrule	L315
1711  03ae 5f            	clrw	x
1712  03af bf02          	ldw	_buz_count,x
1713  03b1               L315:
1714                     ; 320 	TIM2_ClearITPendingBit(TIM2_IT_Update);	/* ������� �������� ������� �����������!!!*/
1716  03b1 a601          	ld	a,#1
1717  03b3 cd0000        	call	_TIM2_ClearITPendingBit
1719                     ; 321 }
1722  03b6 85            	popw	x
1723  03b7 bf00          	ldw	c_y,x
1724  03b9 320002        	pop	c_y+2
1725  03bc 85            	popw	x
1726  03bd bf00          	ldw	c_x,x
1727  03bf 320002        	pop	c_x+2
1728  03c2 80            	iret
1730                     	bsct
1731  000f               _s_count:
1732  000f 00            	dc.b	0
1733  0010               _off_count:
1734  0010 00            	dc.b	0
1735  0011               _mul:
1736  0011 00            	dc.b	0
1737  0012               _mul_max:
1738  0012 0a            	dc.b	10
1775                     ; 325 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
1775                     ; 326 {
1776                     	switch	.text
1777  03c3               f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler:
1779  03c3 8a            	push	cc
1780  03c4 84            	pop	a
1781  03c5 a4bf          	and	a,#191
1782  03c7 88            	push	a
1783  03c8 86            	pop	cc
1784  03c9 3b0002        	push	c_x+2
1785  03cc be00          	ldw	x,c_x
1786  03ce 89            	pushw	x
1787  03cf 3b0002        	push	c_y+2
1788  03d2 be00          	ldw	x,c_y
1789  03d4 89            	pushw	x
1792                     ; 327 	s1 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3);
1794  03d5 4b08          	push	#8
1795  03d7 ae500a        	ldw	x,#20490
1796  03da cd0000        	call	_GPIO_ReadInputDataBit
1798  03dd 5b01          	addw	sp,#1
1799  03df b701          	ld	_s1,a
1800                     ; 328 	s2 = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2);
1802  03e1 4b04          	push	#4
1803  03e3 ae500a        	ldw	x,#20490
1804  03e6 cd0000        	call	_GPIO_ReadInputDataBit
1806  03e9 5b01          	addw	sp,#1
1807  03eb b700          	ld	_s2,a
1808                     ; 330 	if (count_state)
1810  03ed 3d0c          	tnz	_count_state
1811  03ef 2714          	jreq	L735
1812                     ; 332 		mul++;
1814  03f1 3c11          	inc	_mul
1815                     ; 333 		if (mul >= mul_max)
1817  03f3 b611          	ld	a,_mul
1818  03f5 b112          	cp	a,_mul_max
1819  03f7 250c          	jrult	L735
1820                     ; 335 			co--;
1822  03f9 3a07          	dec	_co
1823                     ; 336 			if (co <= 0) 
1825  03fb 3d07          	tnz	_co
1826  03fd 2604          	jrne	L345
1827                     ; 338 				alarm_state = 1;
1829  03ff 35010009      	mov	_alarm_state,#1
1830  0403               L345:
1831                     ; 340 			mul = 0;
1833  0403 3f11          	clr	_mul
1834  0405               L735:
1835                     ; 344 	if ((!(s1)) || (!(s2)))
1837  0405 3d01          	tnz	_s1
1838  0407 2704          	jreq	L745
1840  0409 3d00          	tnz	_s2
1841  040b 2644          	jrne	L545
1842  040d               L745:
1843                     ; 346 		off_count = 0;
1845  040d 3f10          	clr	_off_count
1846                     ; 347 		disp_state = 1;
1848  040f 3501000b      	mov	_disp_state,#1
1849                     ; 348 		count_state = 0;
1851  0413 3f0c          	clr	_count_state
1852                     ; 349 		alarm_state = 0;
1854  0415 3f09          	clr	_alarm_state
1855                     ; 350 		if (s_count <  36) s_count++;
1857  0417 b60f          	ld	a,_s_count
1858  0419 a124          	cp	a,#36
1859  041b 2402          	jruge	L155
1862  041d 3c0f          	inc	_s_count
1863  041f               L155:
1864                     ; 352 		if (!(s1))
1866  041f 3d01          	tnz	_s1
1867  0421 2608          	jrne	L355
1868                     ; 354 			if (s_count >= 6) T_TIMEOUT++;
1870  0423 b60f          	ld	a,_s_count
1871  0425 a106          	cp	a,#6
1872  0427 2502          	jrult	L355
1875  0429 3c01          	inc	L5_T_TIMEOUT
1876  042b               L355:
1877                     ; 356 		if (!(s2))
1879  042b 3d00          	tnz	_s2
1880  042d 2608          	jrne	L755
1881                     ; 358 			if (s_count >= 6) T_TIMEOUT--;
1883  042f b60f          	ld	a,_s_count
1884  0431 a106          	cp	a,#6
1885  0433 2502          	jrult	L755
1888  0435 3a01          	dec	L5_T_TIMEOUT
1889  0437               L755:
1890                     ; 361 		co = T_TIMEOUT;
1892  0437 450107        	mov	_co,L5_T_TIMEOUT
1893                     ; 362 		EEPROM_writeRoutine(T_TIMEOUT);
1895  043a b601          	ld	a,L5_T_TIMEOUT
1896  043c cd0017        	call	_EEPROM_writeRoutine
1899  043f               L365:
1900                     ; 375 	TIM3_ClearITPendingBit(TIM3_IT_Update);	/* ������� �������� ������� �����������!!!*/
1902  043f a601          	ld	a,#1
1903  0441 cd0000        	call	_TIM3_ClearITPendingBit
1905                     ; 376 }
1908  0444 85            	popw	x
1909  0445 bf00          	ldw	c_y,x
1910  0447 320002        	pop	c_y+2
1911  044a 85            	popw	x
1912  044b bf00          	ldw	c_x,x
1913  044d 320002        	pop	c_x+2
1914  0450 80            	iret
1915  0451               L545:
1916                     ; 366 		s_count = 0;
1918  0451 3f0f          	clr	_s_count
1919                     ; 367 		count_state = 0;
1921  0453 3f0c          	clr	_count_state
1922                     ; 368 		if (off_count <  30) off_count++;
1924  0455 b610          	ld	a,_off_count
1925  0457 a11e          	cp	a,#30
1926  0459 2402          	jruge	L565
1929  045b 3c10          	inc	_off_count
1930  045d               L565:
1931                     ; 369 		if (off_count >= 30) 
1933  045d b610          	ld	a,_off_count
1934  045f a11e          	cp	a,#30
1935  0461 25dc          	jrult	L365
1936                     ; 371 			disp_state = 0;
1938  0463 3f0b          	clr	_disp_state
1939                     ; 372 			count_state = 1;
1941  0465 3501000c      	mov	_count_state,#1
1942  0469 20d4          	jra	L365
1967                     ; 378 INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
1967                     ; 379 {
1968                     	switch	.text
1969  046b               f_EXTIE_F_PVD_IRQHandler:
1971  046b 8a            	push	cc
1972  046c 84            	pop	a
1973  046d a4bf          	and	a,#191
1974  046f 88            	push	a
1975  0470 86            	pop	cc
1976  0471 3b0002        	push	c_x+2
1977  0474 be00          	ldw	x,c_x
1978  0476 89            	pushw	x
1979  0477 3b0002        	push	c_y+2
1980  047a be00          	ldw	x,c_y
1981  047c 89            	pushw	x
1984                     ; 382 		alarm_type = 1;
1986  047d 3501000a      	mov	_alarm_type,#1
1987                     ; 384 	PWR_PVDClearITPendingBit();
1989  0481 cd0000        	call	_PWR_PVDClearITPendingBit
1991                     ; 385 }
1994  0484 85            	popw	x
1995  0485 bf00          	ldw	c_y,x
1996  0487 320002        	pop	c_y+2
1997  048a 85            	popw	x
1998  048b bf00          	ldw	c_x,x
1999  048d 320002        	pop	c_x+2
2000  0490 80            	iret
2194                     	xdef	_mul_max
2195                     	xdef	_mul
2196                     	xdef	_off_count
2197                     	xdef	_s_count
2198                     	switch	.ubsct
2199  0000               _s2:
2200  0000 00            	ds.b	1
2201                     	xdef	_s2
2202  0001               _s1:
2203  0001 00            	ds.b	1
2204                     	xdef	_s1
2205                     	xdef	_disp_pos
2206  0002               _save_pre_st2:
2207  0002 00            	ds.b	1
2208                     	xdef	_save_pre_st2
2209  0003               _save_st2:
2210  0003 00            	ds.b	1
2211                     	xdef	_save_st2
2212  0004               _save_pre_st:
2213  0004 00            	ds.b	1
2214                     	xdef	_save_pre_st
2215  0005               _save_st:
2216  0005 00            	ds.b	1
2217                     	xdef	_save_st
2218                     	xdef	_count_state
2219                     	xdef	_disp_state
2220                     	xdef	_alarm_type
2221                     	xdef	_alarm_state
2222                     	xdef	_i
2223                     	xdef	_co
2224                     	xdef	_setSeg
2225                     	xdef	_push_once
2226                     	xdef	_buz_period
2227                     	xdef	_buz_count
2228                     	xdef	_writeNumericPos
2229                     	xdef	_writeNumeric
2230                     	xdef	_writeNumSym
2231                     	xdef	_delay_us
2232                     	xdef	_main
2233                     	xdef	_PWRCNTRL_routine
2234                     	xdef	_PWRCNTRL_config
2235                     	xdef	_EXTI_config
2236                     	xdef	_TIM3_config
2237                     	xdef	_TIM2_config
2238                     	xdef	_GPIO_config
2239                     	xdef	_CLK_config
2240                     	xdef	_EEPROM_readRoutine
2241                     	xdef	_EEPROM_writeRoutine
2242                     	xdef	_EEPROM_config
2243                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler
2244                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler
2245                     	xdef	f_EXTI3_IRQHandler
2246                     	xdef	f_EXTI2_IRQHandler
2247                     	xdef	f_EXTIE_F_PVD_IRQHandler
2248                     	xref	_TIM3_ClearITPendingBit
2249                     	xref	_TIM3_ITConfig
2250                     	xref	_TIM3_Cmd
2251                     	xref	_TIM3_ARRPreloadConfig
2252                     	xref	_TIM3_TimeBaseInit
2253                     	xref	_TIM3_DeInit
2254                     	xref	_TIM2_ClearITPendingBit
2255                     	xref	_TIM2_ITConfig
2256                     	xref	_TIM2_Cmd
2257                     	xref	_TIM2_ARRPreloadConfig
2258                     	xref	_TIM2_TimeBaseInit
2259                     	xref	_TIM2_DeInit
2260                     	xref	_PWR_PVDClearITPendingBit
2261                     	xref	_PWR_PVDITConfig
2262                     	xref	_PWR_PVDCmd
2263                     	xref	_PWR_PVDLevelConfig
2264                     	xref	_PWR_DeInit
2265                     	xref	_GPIO_ReadInputDataBit
2266                     	xref	_GPIO_SetBits
2267                     	xref	_GPIO_Init
2268                     	xref	_FLASH_GetFlagStatus
2269                     	xref	_FLASH_ReadByte
2270                     	xref	_FLASH_ProgramByte
2271                     	xref	_FLASH_Unlock
2272                     	xref	_FLASH_DeInit
2273                     	xref	_FLASH_SetProgrammingTime
2274                     	xref	_EXTI_ClearITPendingBit
2275                     	xref	_EXTI_SetPinSensitivity
2276                     	xref	_EXTI_DeInit
2277                     	xref	_CLK_PeripheralClockConfig
2278                     	xref	_CLK_SYSCLKSourceSwitchCmd
2279                     	xref	_CLK_SYSCLKDivConfig
2280                     	xref	_CLK_GetSYSCLKSource
2281                     	xref	_CLK_SYSCLKSourceConfig
2282                     	xref.b	c_x
2283                     	xref.b	c_y
2303                     	xref	c_smodx
2304                     	xref	c_sdivx
2305                     	end
