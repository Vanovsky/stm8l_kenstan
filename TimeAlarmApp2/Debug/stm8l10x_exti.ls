   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  43                     ; 55 void EXTI_DeInit(void)
  43                     ; 56 {
  45                     	switch	.text
  46  0000               _EXTI_DeInit:
  50                     ; 57   EXTI->CR1 = EXTI_CR1_RESET_VALUE;
  52  0000 725f50a0      	clr	20640
  53                     ; 58   EXTI->CR2 = EXTI_CR2_RESET_VALUE;
  55  0004 725f50a1      	clr	20641
  56                     ; 59   EXTI->CR3 = EXTI_CR3_RESET_VALUE;
  58  0008 725f50a2      	clr	20642
  59                     ; 60   EXTI->SR1 = EXTI_SR1_RESET_VALUE;
  61  000c 725f50a3      	clr	20643
  62                     ; 61   EXTI->SR2 = EXTI_SR2_RESET_VALUE;
  64  0010 725f50a4      	clr	20644
  65                     ; 62   EXTI->CONF = EXTI_CONF_RESET_VALUE;
  67  0014 725f50a5      	clr	20645
  68                     ; 63 }
  71  0018 81            	ret
 176                     ; 84 void EXTI_SetPortSensitivity(EXTI_Port_TypeDef EXTI_Port,
 176                     ; 85                              EXTI_Trigger_TypeDef EXTI_TriggerValue)
 176                     ; 86 {
 177                     	switch	.text
 178  0019               _EXTI_SetPortSensitivity:
 180  0019 89            	pushw	x
 181       00000000      OFST:	set	0
 184                     ; 89   assert_param(IS_EXTI_PORT(EXTI_Port));
 186  001a 9e            	ld	a,xh
 187  001b 4d            	tnz	a
 188  001c 2705          	jreq	L21
 189  001e 9e            	ld	a,xh
 190  001f a102          	cp	a,#2
 191  0021 2603          	jrne	L01
 192  0023               L21:
 193  0023 4f            	clr	a
 194  0024 2010          	jra	L41
 195  0026               L01:
 196  0026 ae0059        	ldw	x,#89
 197  0029 89            	pushw	x
 198  002a ae0000        	ldw	x,#0
 199  002d 89            	pushw	x
 200  002e ae0000        	ldw	x,#L76
 201  0031 cd0000        	call	_assert_failed
 203  0034 5b04          	addw	sp,#4
 204  0036               L41:
 205                     ; 90   assert_param(IS_EXTI_Trigger_VALUE(EXTI_TriggerValue));
 207  0036 0d02          	tnz	(OFST+2,sp)
 208  0038 2712          	jreq	L02
 209  003a 7b02          	ld	a,(OFST+2,sp)
 210  003c a101          	cp	a,#1
 211  003e 270c          	jreq	L02
 212  0040 7b02          	ld	a,(OFST+2,sp)
 213  0042 a102          	cp	a,#2
 214  0044 2706          	jreq	L02
 215  0046 7b02          	ld	a,(OFST+2,sp)
 216  0048 a103          	cp	a,#3
 217  004a 2603          	jrne	L61
 218  004c               L02:
 219  004c 4f            	clr	a
 220  004d 2010          	jra	L22
 221  004f               L61:
 222  004f ae005a        	ldw	x,#90
 223  0052 89            	pushw	x
 224  0053 ae0000        	ldw	x,#0
 225  0056 89            	pushw	x
 226  0057 ae0000        	ldw	x,#L76
 227  005a cd0000        	call	_assert_failed
 229  005d 5b04          	addw	sp,#4
 230  005f               L22:
 231                     ; 93   if (EXTI_Port != EXTI_Port_B)
 233  005f 0d01          	tnz	(OFST+1,sp)
 234  0061 270a          	jreq	L17
 235                     ; 95     EXTI->CR3 &= (uint8_t)(~EXTI_CR3_PDIS);
 237  0063 c650a2        	ld	a,20642
 238  0066 a4f3          	and	a,#243
 239  0068 c750a2        	ld	20642,a
 241  006b 2008          	jra	L37
 242  006d               L17:
 243                     ; 99     EXTI->CR3 &= (uint8_t)(~EXTI_CR3_PBIS);
 245  006d c650a2        	ld	a,20642
 246  0070 a4fc          	and	a,#252
 247  0072 c750a2        	ld	20642,a
 248  0075               L37:
 249                     ; 103   EXTI->CR3 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << (uint8_t)EXTI_Port);
 251  0075 7b01          	ld	a,(OFST+1,sp)
 252  0077 5f            	clrw	x
 253  0078 97            	ld	xl,a
 254  0079 7b02          	ld	a,(OFST+2,sp)
 255  007b 5d            	tnzw	x
 256  007c 2704          	jreq	L42
 257  007e               L62:
 258  007e 48            	sll	a
 259  007f 5a            	decw	x
 260  0080 26fc          	jrne	L62
 261  0082               L42:
 262  0082 ca50a2        	or	a,20642
 263  0085 c750a2        	ld	20642,a
 264                     ; 105 }
 267  0088 85            	popw	x
 268  0089 81            	ret
 377                     ; 132 void EXTI_SetPinSensitivity(EXTI_Pin_TypeDef EXTI_PinNum,
 377                     ; 133                             EXTI_Trigger_TypeDef EXTI_TriggerValue)
 377                     ; 134 {
 378                     	switch	.text
 379  008a               _EXTI_SetPinSensitivity:
 381  008a 89            	pushw	x
 382       00000000      OFST:	set	0
 385                     ; 137   assert_param(IS_EXTI_PINNUM(EXTI_PinNum));
 387  008b 9e            	ld	a,xh
 388  008c 4d            	tnz	a
 389  008d 2723          	jreq	L43
 390  008f 9e            	ld	a,xh
 391  0090 a102          	cp	a,#2
 392  0092 271e          	jreq	L43
 393  0094 9e            	ld	a,xh
 394  0095 a104          	cp	a,#4
 395  0097 2719          	jreq	L43
 396  0099 9e            	ld	a,xh
 397  009a a106          	cp	a,#6
 398  009c 2714          	jreq	L43
 399  009e 9e            	ld	a,xh
 400  009f a110          	cp	a,#16
 401  00a1 270f          	jreq	L43
 402  00a3 9e            	ld	a,xh
 403  00a4 a112          	cp	a,#18
 404  00a6 270a          	jreq	L43
 405  00a8 9e            	ld	a,xh
 406  00a9 a114          	cp	a,#20
 407  00ab 2705          	jreq	L43
 408  00ad 9e            	ld	a,xh
 409  00ae a116          	cp	a,#22
 410  00b0 2603          	jrne	L23
 411  00b2               L43:
 412  00b2 4f            	clr	a
 413  00b3 2010          	jra	L63
 414  00b5               L23:
 415  00b5 ae0089        	ldw	x,#137
 416  00b8 89            	pushw	x
 417  00b9 ae0000        	ldw	x,#0
 418  00bc 89            	pushw	x
 419  00bd ae0000        	ldw	x,#L76
 420  00c0 cd0000        	call	_assert_failed
 422  00c3 5b04          	addw	sp,#4
 423  00c5               L63:
 424                     ; 138   assert_param(IS_EXTI_Trigger_VALUE(EXTI_TriggerValue));
 426  00c5 0d02          	tnz	(OFST+2,sp)
 427  00c7 2712          	jreq	L24
 428  00c9 7b02          	ld	a,(OFST+2,sp)
 429  00cb a101          	cp	a,#1
 430  00cd 270c          	jreq	L24
 431  00cf 7b02          	ld	a,(OFST+2,sp)
 432  00d1 a102          	cp	a,#2
 433  00d3 2706          	jreq	L24
 434  00d5 7b02          	ld	a,(OFST+2,sp)
 435  00d7 a103          	cp	a,#3
 436  00d9 2603          	jrne	L04
 437  00db               L24:
 438  00db 4f            	clr	a
 439  00dc 2010          	jra	L44
 440  00de               L04:
 441  00de ae008a        	ldw	x,#138
 442  00e1 89            	pushw	x
 443  00e2 ae0000        	ldw	x,#0
 444  00e5 89            	pushw	x
 445  00e6 ae0000        	ldw	x,#L76
 446  00e9 cd0000        	call	_assert_failed
 448  00ec 5b04          	addw	sp,#4
 449  00ee               L44:
 450                     ; 141   switch (EXTI_PinNum)
 452  00ee 7b01          	ld	a,(OFST+1,sp)
 454                     ; 175     default:
 454                     ; 176       break;
 455  00f0 4d            	tnz	a
 456  00f1 272f          	jreq	L57
 457  00f3 a002          	sub	a,#2
 458  00f5 274a          	jreq	L77
 459  00f7 a002          	sub	a,#2
 460  00f9 2765          	jreq	L101
 461  00fb a002          	sub	a,#2
 462  00fd 2603cc017f    	jreq	L301
 463  0102 a00a          	sub	a,#10
 464  0104 2603          	jrne	L601
 465  0106 cc019c        	jp	L501
 466  0109               L601:
 467  0109 a002          	sub	a,#2
 468  010b 2603          	jrne	L011
 469  010d cc01bb        	jp	L701
 470  0110               L011:
 471  0110 a002          	sub	a,#2
 472  0112 2603          	jrne	L211
 473  0114 cc01da        	jp	L111
 474  0117               L211:
 475  0117 a002          	sub	a,#2
 476  0119 2603          	jrne	L411
 477  011b cc01f9        	jp	L311
 478  011e               L411:
 479  011e ac160216      	jpf	L761
 480  0122               L57:
 481                     ; 143     case EXTI_Pin_0:
 481                     ; 144       EXTI->CR1 &= (uint8_t)(~EXTI_CR1_P0IS);
 483  0122 c650a0        	ld	a,20640
 484  0125 a4fc          	and	a,#252
 485  0127 c750a0        	ld	20640,a
 486                     ; 145       EXTI->CR1 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << (uint8_t)EXTI_PinNum);
 488  012a 7b01          	ld	a,(OFST+1,sp)
 489  012c 5f            	clrw	x
 490  012d 97            	ld	xl,a
 491  012e 7b02          	ld	a,(OFST+2,sp)
 492  0130 5d            	tnzw	x
 493  0131 2704          	jreq	L64
 494  0133               L05:
 495  0133 48            	sll	a
 496  0134 5a            	decw	x
 497  0135 26fc          	jrne	L05
 498  0137               L64:
 499  0137 ca50a0        	or	a,20640
 500  013a c750a0        	ld	20640,a
 501                     ; 146       break;
 503  013d ac160216      	jpf	L761
 504  0141               L77:
 505                     ; 147     case EXTI_Pin_1:
 505                     ; 148       EXTI->CR1 &= (uint8_t)(~EXTI_CR1_P1IS);
 507  0141 c650a0        	ld	a,20640
 508  0144 a4f3          	and	a,#243
 509  0146 c750a0        	ld	20640,a
 510                     ; 149       EXTI->CR1 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << (uint8_t)EXTI_PinNum);
 512  0149 7b01          	ld	a,(OFST+1,sp)
 513  014b 5f            	clrw	x
 514  014c 97            	ld	xl,a
 515  014d 7b02          	ld	a,(OFST+2,sp)
 516  014f 5d            	tnzw	x
 517  0150 2704          	jreq	L25
 518  0152               L45:
 519  0152 48            	sll	a
 520  0153 5a            	decw	x
 521  0154 26fc          	jrne	L45
 522  0156               L25:
 523  0156 ca50a0        	or	a,20640
 524  0159 c750a0        	ld	20640,a
 525                     ; 150       break;
 527  015c ac160216      	jpf	L761
 528  0160               L101:
 529                     ; 151     case EXTI_Pin_2:
 529                     ; 152       EXTI->CR1 &= (uint8_t)(~EXTI_CR1_P2IS);
 531  0160 c650a0        	ld	a,20640
 532  0163 a4cf          	and	a,#207
 533  0165 c750a0        	ld	20640,a
 534                     ; 153       EXTI->CR1 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << (uint8_t)EXTI_PinNum);
 536  0168 7b01          	ld	a,(OFST+1,sp)
 537  016a 5f            	clrw	x
 538  016b 97            	ld	xl,a
 539  016c 7b02          	ld	a,(OFST+2,sp)
 540  016e 5d            	tnzw	x
 541  016f 2704          	jreq	L65
 542  0171               L06:
 543  0171 48            	sll	a
 544  0172 5a            	decw	x
 545  0173 26fc          	jrne	L06
 546  0175               L65:
 547  0175 ca50a0        	or	a,20640
 548  0178 c750a0        	ld	20640,a
 549                     ; 154       break;
 551  017b ac160216      	jpf	L761
 552  017f               L301:
 553                     ; 155     case EXTI_Pin_3:
 553                     ; 156       EXTI->CR1 &= (uint8_t)(~EXTI_CR1_P3IS);
 555  017f c650a0        	ld	a,20640
 556  0182 a43f          	and	a,#63
 557  0184 c750a0        	ld	20640,a
 558                     ; 157       EXTI->CR1 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << (uint8_t)EXTI_PinNum);
 560  0187 7b01          	ld	a,(OFST+1,sp)
 561  0189 5f            	clrw	x
 562  018a 97            	ld	xl,a
 563  018b 7b02          	ld	a,(OFST+2,sp)
 564  018d 5d            	tnzw	x
 565  018e 2704          	jreq	L26
 566  0190               L46:
 567  0190 48            	sll	a
 568  0191 5a            	decw	x
 569  0192 26fc          	jrne	L46
 570  0194               L26:
 571  0194 ca50a0        	or	a,20640
 572  0197 c750a0        	ld	20640,a
 573                     ; 158       break;
 575  019a 207a          	jra	L761
 576  019c               L501:
 577                     ; 159     case EXTI_Pin_4:
 577                     ; 160       EXTI->CR2 &= (uint8_t)(~EXTI_CR2_P4IS);
 579  019c c650a1        	ld	a,20641
 580  019f a4fc          	and	a,#252
 581  01a1 c750a1        	ld	20641,a
 582                     ; 161       EXTI->CR2 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << ((uint8_t)EXTI_PinNum & (uint8_t)0xEF));
 584  01a4 7b01          	ld	a,(OFST+1,sp)
 585  01a6 a4ef          	and	a,#239
 586  01a8 5f            	clrw	x
 587  01a9 97            	ld	xl,a
 588  01aa 7b02          	ld	a,(OFST+2,sp)
 589  01ac 5d            	tnzw	x
 590  01ad 2704          	jreq	L66
 591  01af               L07:
 592  01af 48            	sll	a
 593  01b0 5a            	decw	x
 594  01b1 26fc          	jrne	L07
 595  01b3               L66:
 596  01b3 ca50a1        	or	a,20641
 597  01b6 c750a1        	ld	20641,a
 598                     ; 162       break;
 600  01b9 205b          	jra	L761
 601  01bb               L701:
 602                     ; 163     case EXTI_Pin_5:
 602                     ; 164       EXTI->CR2 &= (uint8_t)(~EXTI_CR2_P5IS);
 604  01bb c650a1        	ld	a,20641
 605  01be a4f3          	and	a,#243
 606  01c0 c750a1        	ld	20641,a
 607                     ; 165       EXTI->CR2 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << ((uint8_t)EXTI_PinNum & (uint8_t)0xEF));
 609  01c3 7b01          	ld	a,(OFST+1,sp)
 610  01c5 a4ef          	and	a,#239
 611  01c7 5f            	clrw	x
 612  01c8 97            	ld	xl,a
 613  01c9 7b02          	ld	a,(OFST+2,sp)
 614  01cb 5d            	tnzw	x
 615  01cc 2704          	jreq	L27
 616  01ce               L47:
 617  01ce 48            	sll	a
 618  01cf 5a            	decw	x
 619  01d0 26fc          	jrne	L47
 620  01d2               L27:
 621  01d2 ca50a1        	or	a,20641
 622  01d5 c750a1        	ld	20641,a
 623                     ; 166       break;
 625  01d8 203c          	jra	L761
 626  01da               L111:
 627                     ; 167     case EXTI_Pin_6:
 627                     ; 168       EXTI->CR2 &= (uint8_t)(~EXTI_CR2_P6IS);
 629  01da c650a1        	ld	a,20641
 630  01dd a4cf          	and	a,#207
 631  01df c750a1        	ld	20641,a
 632                     ; 169       EXTI->CR2 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << ((uint8_t)EXTI_PinNum & (uint8_t)0xEF));
 634  01e2 7b01          	ld	a,(OFST+1,sp)
 635  01e4 a4ef          	and	a,#239
 636  01e6 5f            	clrw	x
 637  01e7 97            	ld	xl,a
 638  01e8 7b02          	ld	a,(OFST+2,sp)
 639  01ea 5d            	tnzw	x
 640  01eb 2704          	jreq	L67
 641  01ed               L001:
 642  01ed 48            	sll	a
 643  01ee 5a            	decw	x
 644  01ef 26fc          	jrne	L001
 645  01f1               L67:
 646  01f1 ca50a1        	or	a,20641
 647  01f4 c750a1        	ld	20641,a
 648                     ; 170       break;
 650  01f7 201d          	jra	L761
 651  01f9               L311:
 652                     ; 171     case EXTI_Pin_7:
 652                     ; 172       EXTI->CR2 &= (uint8_t)(~EXTI_CR2_P7IS);
 654  01f9 c650a1        	ld	a,20641
 655  01fc a43f          	and	a,#63
 656  01fe c750a1        	ld	20641,a
 657                     ; 173       EXTI->CR2 |= (uint8_t)((uint8_t)(EXTI_TriggerValue) << ((uint8_t)EXTI_PinNum & (uint8_t)0xEF));
 659  0201 7b01          	ld	a,(OFST+1,sp)
 660  0203 a4ef          	and	a,#239
 661  0205 5f            	clrw	x
 662  0206 97            	ld	xl,a
 663  0207 7b02          	ld	a,(OFST+2,sp)
 664  0209 5d            	tnzw	x
 665  020a 2704          	jreq	L201
 666  020c               L401:
 667  020c 48            	sll	a
 668  020d 5a            	decw	x
 669  020e 26fc          	jrne	L401
 670  0210               L201:
 671  0210 ca50a1        	or	a,20641
 672  0213 c750a1        	ld	20641,a
 673                     ; 174       break;
 675  0216               L511:
 676                     ; 175     default:
 676                     ; 176       break;
 678  0216               L761:
 679                     ; 178 }
 682  0216 85            	popw	x
 683  0217 81            	ret
 788                     ; 193 void EXTI_SetHalfPortSelection(EXTI_HalfPort_TypeDef EXTI_HalfPort,
 788                     ; 194                                FunctionalState NewState)
 788                     ; 195 {
 789                     	switch	.text
 790  0218               _EXTI_SetHalfPortSelection:
 792  0218 89            	pushw	x
 793       00000000      OFST:	set	0
 796                     ; 197   assert_param(IS_EXTI_HALFPORT(EXTI_HalfPort));
 798  0219 9e            	ld	a,xh
 799  021a a101          	cp	a,#1
 800  021c 270f          	jreq	L221
 801  021e 9e            	ld	a,xh
 802  021f a102          	cp	a,#2
 803  0221 270a          	jreq	L221
 804  0223 9e            	ld	a,xh
 805  0224 a104          	cp	a,#4
 806  0226 2705          	jreq	L221
 807  0228 9e            	ld	a,xh
 808  0229 a108          	cp	a,#8
 809  022b 2603          	jrne	L021
 810  022d               L221:
 811  022d 4f            	clr	a
 812  022e 2010          	jra	L421
 813  0230               L021:
 814  0230 ae00c5        	ldw	x,#197
 815  0233 89            	pushw	x
 816  0234 ae0000        	ldw	x,#0
 817  0237 89            	pushw	x
 818  0238 ae0000        	ldw	x,#L76
 819  023b cd0000        	call	_assert_failed
 821  023e 5b04          	addw	sp,#4
 822  0240               L421:
 823                     ; 198   assert_param(IS_FUNCTIONAL_STATE(NewState));
 825  0240 0d02          	tnz	(OFST+2,sp)
 826  0242 2706          	jreq	L031
 827  0244 7b02          	ld	a,(OFST+2,sp)
 828  0246 a101          	cp	a,#1
 829  0248 2603          	jrne	L621
 830  024a               L031:
 831  024a 4f            	clr	a
 832  024b 2010          	jra	L231
 833  024d               L621:
 834  024d ae00c6        	ldw	x,#198
 835  0250 89            	pushw	x
 836  0251 ae0000        	ldw	x,#0
 837  0254 89            	pushw	x
 838  0255 ae0000        	ldw	x,#L76
 839  0258 cd0000        	call	_assert_failed
 841  025b 5b04          	addw	sp,#4
 842  025d               L231:
 843                     ; 200   if (NewState != DISABLE)
 845  025d 0d02          	tnz	(OFST+2,sp)
 846  025f 270a          	jreq	L732
 847                     ; 202     EXTI->CONF |= (uint8_t)EXTI_HalfPort; /* Enable port interrupt selector */
 849  0261 c650a5        	ld	a,20645
 850  0264 1a01          	or	a,(OFST+1,sp)
 851  0266 c750a5        	ld	20645,a
 853  0269 2009          	jra	L142
 854  026b               L732:
 855                     ; 206     EXTI->CONF &= (uint8_t)(~(uint8_t)EXTI_HalfPort); /* Disable port interrupt selector */
 857  026b 7b01          	ld	a,(OFST+1,sp)
 858  026d 43            	cpl	a
 859  026e c450a5        	and	a,20645
 860  0271 c750a5        	ld	20645,a
 861  0274               L142:
 862                     ; 208 }
 865  0274 85            	popw	x
 866  0275 81            	ret
 913                     ; 218 EXTI_Trigger_TypeDef EXTI_GetPortSensitivity(EXTI_Port_TypeDef EXTI_Port)
 913                     ; 219 {
 914                     	switch	.text
 915  0276               _EXTI_GetPortSensitivity:
 917  0276 88            	push	a
 918  0277 88            	push	a
 919       00000001      OFST:	set	1
 922                     ; 220   uint8_t value = 0;
 924                     ; 223   assert_param(IS_EXTI_PORT(EXTI_Port));
 926  0278 4d            	tnz	a
 927  0279 2704          	jreq	L041
 928  027b a102          	cp	a,#2
 929  027d 2603          	jrne	L631
 930  027f               L041:
 931  027f 4f            	clr	a
 932  0280 2010          	jra	L241
 933  0282               L631:
 934  0282 ae00df        	ldw	x,#223
 935  0285 89            	pushw	x
 936  0286 ae0000        	ldw	x,#0
 937  0289 89            	pushw	x
 938  028a ae0000        	ldw	x,#L76
 939  028d cd0000        	call	_assert_failed
 941  0290 5b04          	addw	sp,#4
 942  0292               L241:
 943                     ; 225   if (EXTI_Port != EXTI_Port_B)
 945  0292 0d02          	tnz	(OFST+1,sp)
 946  0294 270b          	jreq	L562
 947                     ; 227     value = (uint8_t)((EXTI->CR3 & EXTI_CR3_PDIS) >> 2);
 949  0296 c650a2        	ld	a,20642
 950  0299 44            	srl	a
 951  029a 44            	srl	a
 952  029b a403          	and	a,#3
 953  029d 6b01          	ld	(OFST+0,sp),a
 956  029f 2007          	jra	L762
 957  02a1               L562:
 958                     ; 231     value = (uint8_t)(EXTI->CR3 & EXTI_CR3_PBIS);
 960  02a1 c650a2        	ld	a,20642
 961  02a4 a403          	and	a,#3
 962  02a6 6b01          	ld	(OFST+0,sp),a
 964  02a8               L762:
 965                     ; 233   return((EXTI_Trigger_TypeDef)value);
 967  02a8 7b01          	ld	a,(OFST+0,sp)
 970  02aa 85            	popw	x
 971  02ab 81            	ret
1018                     ; 250 EXTI_Trigger_TypeDef EXTI_GetPinSensitivity(EXTI_Pin_TypeDef EXTI_PinNum)
1018                     ; 251 {
1019                     	switch	.text
1020  02ac               _EXTI_GetPinSensitivity:
1022  02ac 88            	push	a
1023  02ad 88            	push	a
1024       00000001      OFST:	set	1
1027                     ; 252   uint8_t value = 0;
1029  02ae 0f01          	clr	(OFST+0,sp)
1031                     ; 255   assert_param(IS_EXTI_PINNUM(EXTI_PinNum));
1033  02b0 4d            	tnz	a
1034  02b1 271c          	jreq	L051
1035  02b3 a102          	cp	a,#2
1036  02b5 2718          	jreq	L051
1037  02b7 a104          	cp	a,#4
1038  02b9 2714          	jreq	L051
1039  02bb a106          	cp	a,#6
1040  02bd 2710          	jreq	L051
1041  02bf a110          	cp	a,#16
1042  02c1 270c          	jreq	L051
1043  02c3 a112          	cp	a,#18
1044  02c5 2708          	jreq	L051
1045  02c7 a114          	cp	a,#20
1046  02c9 2704          	jreq	L051
1047  02cb a116          	cp	a,#22
1048  02cd 2603          	jrne	L641
1049  02cf               L051:
1050  02cf 4f            	clr	a
1051  02d0 2010          	jra	L251
1052  02d2               L641:
1053  02d2 ae00ff        	ldw	x,#255
1054  02d5 89            	pushw	x
1055  02d6 ae0000        	ldw	x,#0
1056  02d9 89            	pushw	x
1057  02da ae0000        	ldw	x,#L76
1058  02dd cd0000        	call	_assert_failed
1060  02e0 5b04          	addw	sp,#4
1061  02e2               L251:
1062                     ; 257   switch (EXTI_PinNum)
1064  02e2 7b02          	ld	a,(OFST+1,sp)
1066                     ; 283     default:
1066                     ; 284       break;
1067  02e4 4d            	tnz	a
1068  02e5 271e          	jreq	L172
1069  02e7 a002          	sub	a,#2
1070  02e9 2723          	jreq	L372
1071  02eb a002          	sub	a,#2
1072  02ed 272a          	jreq	L572
1073  02ef a002          	sub	a,#2
1074  02f1 2732          	jreq	L772
1075  02f3 a00a          	sub	a,#10
1076  02f5 273a          	jreq	L103
1077  02f7 a002          	sub	a,#2
1078  02f9 273f          	jreq	L303
1079  02fb a002          	sub	a,#2
1080  02fd 2746          	jreq	L503
1081  02ff a002          	sub	a,#2
1082  0301 274e          	jreq	L703
1083  0303 2056          	jra	L733
1084  0305               L172:
1085                     ; 259     case EXTI_Pin_0:
1085                     ; 260       value = (uint8_t)(EXTI->CR1 & EXTI_CR1_P0IS);
1087  0305 c650a0        	ld	a,20640
1088  0308 a403          	and	a,#3
1089  030a 6b01          	ld	(OFST+0,sp),a
1091                     ; 261       break;
1093  030c 204d          	jra	L733
1094  030e               L372:
1095                     ; 262     case EXTI_Pin_1:
1095                     ; 263       value = (uint8_t)((EXTI->CR1 & EXTI_CR1_P1IS) >> (uint8_t)EXTI_Pin_1);
1097  030e c650a0        	ld	a,20640
1098  0311 44            	srl	a
1099  0312 44            	srl	a
1100  0313 a403          	and	a,#3
1101  0315 6b01          	ld	(OFST+0,sp),a
1103                     ; 264       break;
1105  0317 2042          	jra	L733
1106  0319               L572:
1107                     ; 265     case EXTI_Pin_2:
1107                     ; 266       value = (uint8_t)((EXTI->CR1 & EXTI_CR1_P2IS) >> (uint8_t)EXTI_Pin_2);
1109  0319 c650a0        	ld	a,20640
1110  031c 4e            	swap	a
1111  031d a40f          	and	a,#15
1112  031f a403          	and	a,#3
1113  0321 6b01          	ld	(OFST+0,sp),a
1115                     ; 267       break;
1117  0323 2036          	jra	L733
1118  0325               L772:
1119                     ; 268     case EXTI_Pin_3:
1119                     ; 269       value = (uint8_t)((EXTI->CR1 & EXTI_CR1_P3IS) >> (uint8_t)EXTI_Pin_3);
1121  0325 c650a0        	ld	a,20640
1122  0328 4e            	swap	a
1123  0329 44            	srl	a
1124  032a 44            	srl	a
1125  032b a403          	and	a,#3
1126  032d 6b01          	ld	(OFST+0,sp),a
1128                     ; 270       break;
1130  032f 202a          	jra	L733
1131  0331               L103:
1132                     ; 271     case EXTI_Pin_4:
1132                     ; 272       value = (uint8_t)(EXTI->CR2 & EXTI_CR2_P4IS);
1134  0331 c650a1        	ld	a,20641
1135  0334 a403          	and	a,#3
1136  0336 6b01          	ld	(OFST+0,sp),a
1138                     ; 273       break;
1140  0338 2021          	jra	L733
1141  033a               L303:
1142                     ; 274     case EXTI_Pin_5:
1142                     ; 275       value = (uint8_t)((EXTI->CR2 & EXTI_CR2_P5IS) >> ((uint8_t)EXTI_Pin_5 & (uint8_t)0x0F));
1144  033a c650a1        	ld	a,20641
1145  033d 44            	srl	a
1146  033e 44            	srl	a
1147  033f a403          	and	a,#3
1148  0341 6b01          	ld	(OFST+0,sp),a
1150                     ; 276       break;
1152  0343 2016          	jra	L733
1153  0345               L503:
1154                     ; 277     case EXTI_Pin_6:
1154                     ; 278       value = (uint8_t)((EXTI->CR2 & EXTI_CR2_P6IS) >> ((uint8_t)EXTI_Pin_6 & (uint8_t)0x0F));
1156  0345 c650a1        	ld	a,20641
1157  0348 4e            	swap	a
1158  0349 a40f          	and	a,#15
1159  034b a403          	and	a,#3
1160  034d 6b01          	ld	(OFST+0,sp),a
1162                     ; 279       break;
1164  034f 200a          	jra	L733
1165  0351               L703:
1166                     ; 280     case EXTI_Pin_7:
1166                     ; 281       value = (uint8_t)((EXTI->CR2 & EXTI_CR2_P7IS) >> ((uint8_t)EXTI_Pin_7 & (uint8_t)0x0F));
1168  0351 c650a1        	ld	a,20641
1169  0354 4e            	swap	a
1170  0355 44            	srl	a
1171  0356 44            	srl	a
1172  0357 a403          	and	a,#3
1173  0359 6b01          	ld	(OFST+0,sp),a
1175                     ; 282       break;
1177  035b               L113:
1178                     ; 283     default:
1178                     ; 284       break;
1180  035b               L733:
1181                     ; 286   return((EXTI_Trigger_TypeDef)value);
1183  035b 7b01          	ld	a,(OFST+0,sp)
1186  035d 85            	popw	x
1187  035e 81            	ret
1235                     ; 299 FunctionalState EXTI_GetHalfPortSelection(EXTI_HalfPort_TypeDef EXTI_HalfPort)
1235                     ; 300 {
1236                     	switch	.text
1237  035f               _EXTI_GetHalfPortSelection:
1239  035f 88            	push	a
1240  0360 88            	push	a
1241       00000001      OFST:	set	1
1244                     ; 301   FunctionalState value = DISABLE;
1246                     ; 303   assert_param(IS_EXTI_HALFPORT(EXTI_HalfPort));
1248  0361 a101          	cp	a,#1
1249  0363 270c          	jreq	L061
1250  0365 a102          	cp	a,#2
1251  0367 2708          	jreq	L061
1252  0369 a104          	cp	a,#4
1253  036b 2704          	jreq	L061
1254  036d a108          	cp	a,#8
1255  036f 2603          	jrne	L651
1256  0371               L061:
1257  0371 4f            	clr	a
1258  0372 2010          	jra	L261
1259  0374               L651:
1260  0374 ae012f        	ldw	x,#303
1261  0377 89            	pushw	x
1262  0378 ae0000        	ldw	x,#0
1263  037b 89            	pushw	x
1264  037c ae0000        	ldw	x,#L76
1265  037f cd0000        	call	_assert_failed
1267  0382 5b04          	addw	sp,#4
1268  0384               L261:
1269                     ; 304   if ((EXTI->CONF & (uint8_t)EXTI_HalfPort) != (uint8_t)DISABLE)
1271  0384 c650a5        	ld	a,20645
1272  0387 1502          	bcp	a,(OFST+1,sp)
1273  0389 2706          	jreq	L363
1274                     ; 306     value = ENABLE;
1276  038b a601          	ld	a,#1
1277  038d 6b01          	ld	(OFST+0,sp),a
1280  038f 2002          	jra	L563
1281  0391               L363:
1282                     ; 310     value = DISABLE;
1284  0391 0f01          	clr	(OFST+0,sp)
1286  0393               L563:
1287                     ; 313   return(value);
1289  0393 7b01          	ld	a,(OFST+0,sp)
1292  0395 85            	popw	x
1293  0396 81            	ret
1436                     ; 332 ITStatus EXTI_GetITStatus(EXTI_IT_TypeDef EXTI_IT)
1436                     ; 333 {
1437                     	switch	.text
1438  0397               _EXTI_GetITStatus:
1440  0397 88            	push	a
1441  0398 88            	push	a
1442       00000001      OFST:	set	1
1445                     ; 334   ITStatus status = RESET;
1447                     ; 336   assert_param(IS_EXTI_ITPENDINGBIT(EXTI_IT));
1449  0399 a101          	cp	a,#1
1450  039b 2724          	jreq	L071
1451  039d a102          	cp	a,#2
1452  039f 2720          	jreq	L071
1453  03a1 a104          	cp	a,#4
1454  03a3 271c          	jreq	L071
1455  03a5 a108          	cp	a,#8
1456  03a7 2718          	jreq	L071
1457  03a9 a110          	cp	a,#16
1458  03ab 2714          	jreq	L071
1459  03ad a120          	cp	a,#32
1460  03af 2710          	jreq	L071
1461  03b1 a140          	cp	a,#64
1462  03b3 270c          	jreq	L071
1463  03b5 a180          	cp	a,#128
1464  03b7 2708          	jreq	L071
1465  03b9 a1f1          	cp	a,#241
1466  03bb 2704          	jreq	L071
1467  03bd a1f2          	cp	a,#242
1468  03bf 2603          	jrne	L661
1469  03c1               L071:
1470  03c1 4f            	clr	a
1471  03c2 2010          	jra	L271
1472  03c4               L661:
1473  03c4 ae0150        	ldw	x,#336
1474  03c7 89            	pushw	x
1475  03c8 ae0000        	ldw	x,#0
1476  03cb 89            	pushw	x
1477  03cc ae0000        	ldw	x,#L76
1478  03cf cd0000        	call	_assert_failed
1480  03d2 5b04          	addw	sp,#4
1481  03d4               L271:
1482                     ; 338   if (((uint8_t)EXTI_IT & (uint8_t)0xF0) == 0xF0)
1484  03d4 7b02          	ld	a,(OFST+1,sp)
1485  03d6 a4f0          	and	a,#240
1486  03d8 a1f0          	cp	a,#240
1487  03da 260b          	jrne	L154
1488                     ; 340     status = (ITStatus)(EXTI->SR2 & ((uint8_t)EXTI_IT & (uint8_t)0x0F));
1490  03dc 7b02          	ld	a,(OFST+1,sp)
1491  03de a40f          	and	a,#15
1492  03e0 c450a4        	and	a,20644
1493  03e3 6b01          	ld	(OFST+0,sp),a
1496  03e5 2007          	jra	L354
1497  03e7               L154:
1498                     ; 344     status = (ITStatus)(EXTI->SR1 & (uint8_t)EXTI_IT);
1500  03e7 c650a3        	ld	a,20643
1501  03ea 1402          	and	a,(OFST+1,sp)
1502  03ec 6b01          	ld	(OFST+0,sp),a
1504  03ee               L354:
1505                     ; 346   return((ITStatus)status);
1507  03ee 7b01          	ld	a,(OFST+0,sp)
1510  03f0 85            	popw	x
1511  03f1 81            	ret
1548                     ; 365 void EXTI_ClearITPendingBit(EXTI_IT_TypeDef EXTI_IT)
1548                     ; 366 {
1549                     	switch	.text
1550  03f2               _EXTI_ClearITPendingBit:
1552  03f2 88            	push	a
1553       00000000      OFST:	set	0
1556                     ; 368   assert_param(IS_EXTI_ITPENDINGBIT(EXTI_IT));
1558  03f3 a101          	cp	a,#1
1559  03f5 2724          	jreq	L002
1560  03f7 a102          	cp	a,#2
1561  03f9 2720          	jreq	L002
1562  03fb a104          	cp	a,#4
1563  03fd 271c          	jreq	L002
1564  03ff a108          	cp	a,#8
1565  0401 2718          	jreq	L002
1566  0403 a110          	cp	a,#16
1567  0405 2714          	jreq	L002
1568  0407 a120          	cp	a,#32
1569  0409 2710          	jreq	L002
1570  040b a140          	cp	a,#64
1571  040d 270c          	jreq	L002
1572  040f a180          	cp	a,#128
1573  0411 2708          	jreq	L002
1574  0413 a1f1          	cp	a,#241
1575  0415 2704          	jreq	L002
1576  0417 a1f2          	cp	a,#242
1577  0419 2603          	jrne	L671
1578  041b               L002:
1579  041b 4f            	clr	a
1580  041c 2010          	jra	L202
1581  041e               L671:
1582  041e ae0170        	ldw	x,#368
1583  0421 89            	pushw	x
1584  0422 ae0000        	ldw	x,#0
1585  0425 89            	pushw	x
1586  0426 ae0000        	ldw	x,#L76
1587  0429 cd0000        	call	_assert_failed
1589  042c 5b04          	addw	sp,#4
1590  042e               L202:
1591                     ; 370   if (((uint8_t)EXTI_IT & (uint8_t)0xF0) == 0xF0)
1593  042e 7b01          	ld	a,(OFST+1,sp)
1594  0430 a4f0          	and	a,#240
1595  0432 a1f0          	cp	a,#240
1596  0434 2609          	jrne	L374
1597                     ; 372     EXTI->SR2 = (uint8_t)((uint8_t)EXTI_IT & (uint8_t)0x0F);
1599  0436 7b01          	ld	a,(OFST+1,sp)
1600  0438 a40f          	and	a,#15
1601  043a c750a4        	ld	20644,a
1603  043d 2005          	jra	L574
1604  043f               L374:
1605                     ; 376     EXTI->SR1 = (uint8_t)EXTI_IT;
1607  043f 7b01          	ld	a,(OFST+1,sp)
1608  0441 c750a3        	ld	20643,a
1609  0444               L574:
1610                     ; 378 }
1613  0444 84            	pop	a
1614  0445 81            	ret
1627                     	xdef	_EXTI_ClearITPendingBit
1628                     	xdef	_EXTI_GetITStatus
1629                     	xdef	_EXTI_GetHalfPortSelection
1630                     	xdef	_EXTI_GetPinSensitivity
1631                     	xdef	_EXTI_GetPortSensitivity
1632                     	xdef	_EXTI_SetHalfPortSelection
1633                     	xdef	_EXTI_SetPinSensitivity
1634                     	xdef	_EXTI_SetPortSensitivity
1635                     	xdef	_EXTI_DeInit
1636                     	xref	_assert_failed
1637                     .const:	section	.text
1638  0000               L76:
1639  0000 5f6c69625c73  	dc.b	"_lib\src\stm8l10x_"
1640  0012 657874692e63  	dc.b	"exti.c",0
1660                     	end
