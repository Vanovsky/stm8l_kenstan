#include "main.h"

void CLK_config(void)
{
	/********************************************
	 * ��������� ��������� ������������ ������� *
	 ********************************************
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
  CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
  while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}*/
	
	CLK_MasterPrescalerConfig(CLK_MasterPrescaler_HSIDiv1);
}

void delay_us(uint32_t del_us)
{
	while(del_us--);
}

void GPIO_config(void)
{
	GPIOB->DDR = 0xFF;
}

void writeNumSym(uint8_t num, uint8_t seg)
{
	GPIOB->ODR |= (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
	GPIOC->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
	
	// Choosing segment position
	switch (seg)
	{
		case 0:
			GPIOC->ODR |= GPIO_Pin_0;
		break;
		case 1:
			GPIOC->ODR |= GPIO_Pin_1;	// 2seg pos
		break;
		case 2:
			GPIOC->ODR |= GPIO_Pin_2;	// 3seg pos
		break;
		default:
			GPIOC->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
		break;
	}
	
	// Choosing numeric to write
	switch (num)
	{
		case 0: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 |GPIO_Pin_2 |GPIO_Pin_3 |GPIO_Pin_4 |GPIO_Pin_5);	
			break;
		case 1: 
			GPIOB->ODR &= ~(GPIO_Pin_1 | GPIO_Pin_2);
			break;
		case 2: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_6);
			break;
		case 3: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6);
			break;
		case 4: 
			GPIOB->ODR &= ~(GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
		case 5: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
		case 6: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
		case 7: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
			break;
		case 8: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
		case 9: 
			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
		default:
			GPIOB->ODR |= (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
			break;
	}
}

void writeNumeric(uint16_t numeric)
{
	uint8_t* num, i;
	num[0] = numeric % 10;
	num[1] = (numeric / 10) % 10;
	num[2] = numeric / 100;
	for(i = 0; i < 3; i++) writeNumSym(num[i], i);
}

void TIM2_config(void)
{
	TIM2->CR1 |= (TIM_CR1_DIR | TIM_CR1_ARPE | TIM_CR1_CEN);
	TIM2->IER |= TIM_IER_CC1IE;
	
}
uint8_t counter1 = 0;
int main()
{
	int i = 0;
	uint8_t block;
	uint8_t len;
	uint8_t buffer1[18];
	uint8_t buffer2[18];
	
	enableInterrupts();
	CLK_config();
	TIM2_config();
	GPIO_config();
	//ITC_config();
	
	delay_us(1);

	
	while (1) 
	{
		counter1++;
		writeNumeric(counter1);
		delay_us(500);
	}
}


#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) *
  /* Infinite loop */
  while (1) {}
}
#endif