   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  15                     	bsct
  16  0000               _counter:
  17  0000 0000          	dc.w	0
  47                     ; 49 INTERRUPT_HANDLER(NonHandledInterrupt,0)
  47                     ; 50 {
  48                     	switch	.text
  49  0000               f_NonHandledInterrupt:
  53                     ; 54 }
  56  0000 80            	iret
  78                     ; 62 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  78                     ; 63 {
  79                     	switch	.text
  80  0001               f_TRAP_IRQHandler:
  84                     ; 67 }
  87  0001 80            	iret
 109                     ; 74 INTERRUPT_HANDLER(FLASH_IRQHandler,1)
 109                     ; 75 {
 110                     	switch	.text
 111  0002               f_FLASH_IRQHandler:
 115                     ; 79 }
 118  0002 80            	iret
 140                     ; 86 INTERRUPT_HANDLER(AWU_IRQHandler,4)
 140                     ; 87 {
 141                     	switch	.text
 142  0003               f_AWU_IRQHandler:
 146                     ; 91 }
 149  0003 80            	iret
 171                     ; 98 INTERRUPT_HANDLER(EXTIB_IRQHandler, 6)
 171                     ; 99 {
 172                     	switch	.text
 173  0004               f_EXTIB_IRQHandler:
 177                     ; 103 }
 180  0004 80            	iret
 202                     ; 110 INTERRUPT_HANDLER(EXTID_IRQHandler, 7)
 202                     ; 111 {
 203                     	switch	.text
 204  0005               f_EXTID_IRQHandler:
 208                     ; 115 }
 211  0005 80            	iret
 233                     ; 122 INTERRUPT_HANDLER(EXTI0_IRQHandler, 8)
 233                     ; 123 {
 234                     	switch	.text
 235  0006               f_EXTI0_IRQHandler:
 239                     ; 127 }
 242  0006 80            	iret
 264                     ; 134 INTERRUPT_HANDLER(EXTI1_IRQHandler, 9)
 264                     ; 135 {
 265                     	switch	.text
 266  0007               f_EXTI1_IRQHandler:
 270                     ; 139 }
 273  0007 80            	iret
 295                     ; 146 INTERRUPT_HANDLER(EXTI2_IRQHandler, 10)
 295                     ; 147 {
 296                     	switch	.text
 297  0008               f_EXTI2_IRQHandler:
 301                     ; 151 }
 304  0008 80            	iret
 326                     ; 158 INTERRUPT_HANDLER(EXTI3_IRQHandler, 11)
 326                     ; 159 {
 327                     	switch	.text
 328  0009               f_EXTI3_IRQHandler:
 332                     ; 163 }
 335  0009 80            	iret
 357                     ; 170 INTERRUPT_HANDLER(EXTI4_IRQHandler, 12)
 357                     ; 171 {
 358                     	switch	.text
 359  000a               f_EXTI4_IRQHandler:
 363                     ; 175 }
 366  000a 80            	iret
 388                     ; 182 INTERRUPT_HANDLER(EXTI5_IRQHandler, 13)
 388                     ; 183 {
 389                     	switch	.text
 390  000b               f_EXTI5_IRQHandler:
 394                     ; 187 }
 397  000b 80            	iret
 419                     ; 194 INTERRUPT_HANDLER(EXTI6_IRQHandler, 14)
 419                     ; 195 {
 420                     	switch	.text
 421  000c               f_EXTI6_IRQHandler:
 425                     ; 199 }
 428  000c 80            	iret
 450                     ; 206 INTERRUPT_HANDLER(EXTI7_IRQHandler, 15)
 450                     ; 207 {
 451                     	switch	.text
 452  000d               f_EXTI7_IRQHandler:
 456                     ; 211 }
 459  000d 80            	iret
 481                     ; 218 INTERRUPT_HANDLER(COMP_IRQHandler, 18)
 481                     ; 219 {
 482                     	switch	.text
 483  000e               f_COMP_IRQHandler:
 487                     ; 223 }
 490  000e 80            	iret
 513                     ; 230 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_IRQHandler, 19)
 513                     ; 231 {
 514                     	switch	.text
 515  000f               f_TIM2_UPD_OVF_TRG_BRK_IRQHandler:
 519                     ; 237 }
 522  000f 80            	iret
 545                     ; 244 INTERRUPT_HANDLER(TIM2_CAP_IRQHandler, 20)
 545                     ; 245 {
 546                     	switch	.text
 547  0010               f_TIM2_CAP_IRQHandler:
 551                     ; 249 }
 554  0010 80            	iret
 577                     ; 257 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_IRQHandler, 21)
 577                     ; 258 {
 578                     	switch	.text
 579  0011               f_TIM3_UPD_OVF_TRG_BRK_IRQHandler:
 583                     ; 262 }
 586  0011 80            	iret
 609                     ; 268 INTERRUPT_HANDLER(TIM3_CAP_IRQHandler, 22)
 609                     ; 269 {
 610                     	switch	.text
 611  0012               f_TIM3_CAP_IRQHandler:
 615                     ; 273 }
 618  0012 80            	iret
 641                     ; 279 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 25)
 641                     ; 280 {
 642                     	switch	.text
 643  0013               f_TIM4_UPD_OVF_IRQHandler:
 647                     ; 284 }
 650  0013 80            	iret
 672                     ; 291 INTERRUPT_HANDLER(SPI_IRQHandler, 26)
 672                     ; 292 {
 673                     	switch	.text
 674  0014               f_SPI_IRQHandler:
 678                     ; 296 }
 681  0014 80            	iret
 704                     ; 303 INTERRUPT_HANDLER(USART_TX_IRQHandler, 27)
 704                     ; 304 {
 705                     	switch	.text
 706  0015               f_USART_TX_IRQHandler:
 710                     ; 308 }
 713  0015 80            	iret
 736                     ; 315 INTERRUPT_HANDLER(USART_RX_IRQHandler, 28)
 736                     ; 316 {
 737                     	switch	.text
 738  0016               f_USART_RX_IRQHandler:
 742                     ; 320 }
 745  0016 80            	iret
 767                     ; 327 INTERRUPT_HANDLER(I2C_IRQHandler, 29)
 767                     ; 328 {
 768                     	switch	.text
 769  0017               f_I2C_IRQHandler:
 773                     ; 332 }
 776  0017 80            	iret
 799                     	xdef	_counter
 800                     	xdef	f_I2C_IRQHandler
 801                     	xdef	f_USART_RX_IRQHandler
 802                     	xdef	f_USART_TX_IRQHandler
 803                     	xdef	f_SPI_IRQHandler
 804                     	xdef	f_TIM4_UPD_OVF_IRQHandler
 805                     	xdef	f_TIM3_CAP_IRQHandler
 806                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_IRQHandler
 807                     	xdef	f_TIM2_CAP_IRQHandler
 808                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_IRQHandler
 809                     	xdef	f_COMP_IRQHandler
 810                     	xdef	f_EXTI7_IRQHandler
 811                     	xdef	f_EXTI6_IRQHandler
 812                     	xdef	f_EXTI5_IRQHandler
 813                     	xdef	f_EXTI4_IRQHandler
 814                     	xdef	f_EXTI3_IRQHandler
 815                     	xdef	f_EXTI2_IRQHandler
 816                     	xdef	f_EXTI1_IRQHandler
 817                     	xdef	f_EXTI0_IRQHandler
 818                     	xdef	f_EXTID_IRQHandler
 819                     	xdef	f_EXTIB_IRQHandler
 820                     	xdef	f_AWU_IRQHandler
 821                     	xdef	f_FLASH_IRQHandler
 822                     	xdef	f_TRAP_IRQHandler
 823                     	xdef	f_NonHandledInterrupt
 842                     	end
