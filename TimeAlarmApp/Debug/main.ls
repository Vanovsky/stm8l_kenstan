   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  44                     ; 3 void CLK_config(void)
  44                     ; 4 {
  46                     	switch	.text
  47  0000               _CLK_config:
  51                     ; 13 	CLK_MasterPrescalerConfig(CLK_MasterPrescaler_HSIDiv1);
  53  0000 4f            	clr	a
  54  0001 cd0000        	call	_CLK_MasterPrescalerConfig
  56                     ; 14 }
  59  0004 81            	ret
  93                     ; 16 void delay_us(uint32_t del_us)
  93                     ; 17 {
  94                     	switch	.text
  95  0005               _delay_us:
  97       00000000      OFST:	set	0
 100  0005               L14:
 101                     ; 18 	while(del_us--);
 103  0005 96            	ldw	x,sp
 104  0006 1c0003        	addw	x,#OFST+3
 105  0009 cd0000        	call	c_ltor
 107  000c 96            	ldw	x,sp
 108  000d 1c0003        	addw	x,#OFST+3
 109  0010 a601          	ld	a,#1
 110  0012 cd0000        	call	c_lgsbc
 112  0015 cd0000        	call	c_lrzmp
 114  0018 26eb          	jrne	L14
 115                     ; 19 }
 118  001a 81            	ret
 141                     ; 21 void GPIO_config(void)
 141                     ; 22 {
 142                     	switch	.text
 143  001b               _GPIO_config:
 147                     ; 23 	GPIOB->DDR = 0xFF;
 149  001b 35ff5007      	mov	20487,#255
 150                     ; 24 }
 153  001f 81            	ret
 196                     ; 26 void writeNumSym(uint8_t num, uint8_t seg)
 196                     ; 27 {
 197                     	switch	.text
 198  0020               _writeNumSym:
 200  0020 89            	pushw	x
 201       00000000      OFST:	set	0
 204                     ; 28 	GPIOB->ODR |= (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
 206  0021 c65005        	ld	a,20485
 207  0024 aa7f          	or	a,#127
 208  0026 c75005        	ld	20485,a
 209                     ; 29 	GPIOC->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
 211  0029 c6500a        	ld	a,20490
 212  002c a4f8          	and	a,#248
 213  002e c7500a        	ld	20490,a
 214                     ; 32 	switch (seg)
 216  0031 9f            	ld	a,xl
 218                     ; 45 		break;
 219  0032 4d            	tnz	a
 220  0033 2710          	jreq	L55
 221  0035 4a            	dec	a
 222  0036 2713          	jreq	L75
 223  0038 4a            	dec	a
 224  0039 2716          	jreq	L16
 225  003b               L36:
 226                     ; 43 		default:
 226                     ; 44 			GPIOC->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
 228  003b c6500a        	ld	a,20490
 229  003e a4f8          	and	a,#248
 230  0040 c7500a        	ld	20490,a
 231                     ; 45 		break;
 233  0043 2010          	jra	L731
 234  0045               L55:
 235                     ; 34 		case 0:
 235                     ; 35 			GPIOC->ODR |= GPIO_Pin_0;
 237  0045 7210500a      	bset	20490,#0
 238                     ; 36 		break;
 240  0049 200a          	jra	L731
 241  004b               L75:
 242                     ; 37 		case 1:
 242                     ; 38 			GPIOC->ODR |= GPIO_Pin_1;	// 2seg pos
 244  004b 7212500a      	bset	20490,#1
 245                     ; 39 		break;
 247  004f 2004          	jra	L731
 248  0051               L16:
 249                     ; 40 		case 2:
 249                     ; 41 			GPIOC->ODR |= GPIO_Pin_2;	// 3seg pos
 251  0051 7214500a      	bset	20490,#2
 252                     ; 42 		break;
 254  0055               L731:
 255                     ; 49 	switch (num)
 257  0055 7b01          	ld	a,(OFST+1,sp)
 259                     ; 83 			break;
 260  0057 4d            	tnz	a
 261  0058 2725          	jreq	L56
 262  005a 4a            	dec	a
 263  005b 272c          	jreq	L76
 264  005d 4a            	dec	a
 265  005e 2733          	jreq	L17
 266  0060 4a            	dec	a
 267  0061 273a          	jreq	L37
 268  0063 4a            	dec	a
 269  0064 2741          	jreq	L57
 270  0066 4a            	dec	a
 271  0067 2748          	jreq	L77
 272  0069 4a            	dec	a
 273  006a 274f          	jreq	L101
 274  006c 4a            	dec	a
 275  006d 2756          	jreq	L301
 276  006f 4a            	dec	a
 277  0070 275d          	jreq	L501
 278  0072 4a            	dec	a
 279  0073 2764          	jreq	L701
 280  0075               L111:
 281                     ; 81 		default:
 281                     ; 82 			GPIOB->ODR |= (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
 283  0075 c65005        	ld	a,20485
 284  0078 aa7f          	or	a,#127
 285  007a c75005        	ld	20485,a
 286                     ; 83 			break;
 288  007d 2062          	jra	L341
 289  007f               L56:
 290                     ; 51 		case 0: 
 290                     ; 52 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 |GPIO_Pin_2 |GPIO_Pin_3 |GPIO_Pin_4 |GPIO_Pin_5);	
 292  007f c65005        	ld	a,20485
 293  0082 a4c0          	and	a,#192
 294  0084 c75005        	ld	20485,a
 295                     ; 53 			break;
 297  0087 2058          	jra	L341
 298  0089               L76:
 299                     ; 54 		case 1: 
 299                     ; 55 			GPIOB->ODR &= ~(GPIO_Pin_1 | GPIO_Pin_2);
 301  0089 c65005        	ld	a,20485
 302  008c a4f9          	and	a,#249
 303  008e c75005        	ld	20485,a
 304                     ; 56 			break;
 306  0091 204e          	jra	L341
 307  0093               L17:
 308                     ; 57 		case 2: 
 308                     ; 58 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_6);
 310  0093 c65005        	ld	a,20485
 311  0096 a4a4          	and	a,#164
 312  0098 c75005        	ld	20485,a
 313                     ; 59 			break;
 315  009b 2044          	jra	L341
 316  009d               L37:
 317                     ; 60 		case 3: 
 317                     ; 61 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6);
 319  009d c65005        	ld	a,20485
 320  00a0 a4b0          	and	a,#176
 321  00a2 c75005        	ld	20485,a
 322                     ; 62 			break;
 324  00a5 203a          	jra	L341
 325  00a7               L57:
 326                     ; 63 		case 4: 
 326                     ; 64 			GPIOB->ODR &= ~(GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_5 | GPIO_Pin_6);
 328  00a7 c65005        	ld	a,20485
 329  00aa a499          	and	a,#153
 330  00ac c75005        	ld	20485,a
 331                     ; 65 			break;
 333  00af 2030          	jra	L341
 334  00b1               L77:
 335                     ; 66 		case 5: 
 335                     ; 67 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6);
 337  00b1 c65005        	ld	a,20485
 338  00b4 a492          	and	a,#146
 339  00b6 c75005        	ld	20485,a
 340                     ; 68 			break;
 342  00b9 2026          	jra	L341
 343  00bb               L101:
 344                     ; 69 		case 6: 
 344                     ; 70 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
 346  00bb c65005        	ld	a,20485
 347  00be a482          	and	a,#130
 348  00c0 c75005        	ld	20485,a
 349                     ; 71 			break;
 351  00c3 201c          	jra	L341
 352  00c5               L301:
 353                     ; 72 		case 7: 
 353                     ; 73 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
 355  00c5 c65005        	ld	a,20485
 356  00c8 a4f8          	and	a,#248
 357  00ca c75005        	ld	20485,a
 358                     ; 74 			break;
 360  00cd 2012          	jra	L341
 361  00cf               L501:
 362                     ; 75 		case 8: 
 362                     ; 76 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6);
 364  00cf c65005        	ld	a,20485
 365  00d2 a480          	and	a,#128
 366  00d4 c75005        	ld	20485,a
 367                     ; 77 			break;
 369  00d7 2008          	jra	L341
 370  00d9               L701:
 371                     ; 78 		case 9: 
 371                     ; 79 			GPIOB->ODR &= ~(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6);
 373  00d9 c65005        	ld	a,20485
 374  00dc a490          	and	a,#144
 375  00de c75005        	ld	20485,a
 376                     ; 80 			break;
 378  00e1               L341:
 379                     ; 85 }
 382  00e1 85            	popw	x
 383  00e2 81            	ret
 437                     ; 87 void writeNumeric(uint16_t numeric)
 437                     ; 88 {
 438                     	switch	.text
 439  00e3               _writeNumeric:
 441  00e3 89            	pushw	x
 442  00e4 5203          	subw	sp,#3
 443       00000003      OFST:	set	3
 446                     ; 90 	num[0] = numeric % 10;
 448  00e6 a60a          	ld	a,#10
 449  00e8 62            	div	x,a
 450  00e9 5f            	clrw	x
 451  00ea 97            	ld	xl,a
 452  00eb 1601          	ldw	y,(OFST-2,sp)
 453  00ed 01            	rrwa	x,a
 454  00ee 90f7          	ld	(y),a
 455  00f0 02            	rlwa	x,a
 456                     ; 91 	num[1] = (numeric / 10) % 10;
 458  00f1 1e04          	ldw	x,(OFST+1,sp)
 459  00f3 a60a          	ld	a,#10
 460  00f5 62            	div	x,a
 461  00f6 a60a          	ld	a,#10
 462  00f8 62            	div	x,a
 463  00f9 5f            	clrw	x
 464  00fa 97            	ld	xl,a
 465  00fb 1601          	ldw	y,(OFST-2,sp)
 466  00fd 01            	rrwa	x,a
 467  00fe 90e701        	ld	(1,y),a
 468  0101 02            	rlwa	x,a
 469                     ; 92 	num[2] = numeric / 100;
 471  0102 1e04          	ldw	x,(OFST+1,sp)
 472  0104 a664          	ld	a,#100
 473  0106 62            	div	x,a
 474  0107 1601          	ldw	y,(OFST-2,sp)
 475  0109 01            	rrwa	x,a
 476  010a 90e702        	ld	(2,y),a
 477  010d 02            	rlwa	x,a
 478                     ; 93 	for(i = 0; i < 3; i++) writeNumSym(num[i], i);
 480  010e 0f03          	clr	(OFST+0,sp)
 482  0110               L371:
 485  0110 7b03          	ld	a,(OFST+0,sp)
 486  0112 97            	ld	xl,a
 487  0113 89            	pushw	x
 488  0114 7b03          	ld	a,(OFST+0,sp)
 489  0116 97            	ld	xl,a
 490  0117 7b04          	ld	a,(OFST+1,sp)
 491  0119 1b05          	add	a,(OFST+2,sp)
 492  011b 2401          	jrnc	L61
 493  011d 5c            	incw	x
 494  011e               L61:
 495  011e 02            	rlwa	x,a
 496  011f f6            	ld	a,(x)
 497  0120 85            	popw	x
 498  0121 95            	ld	xh,a
 499  0122 cd0020        	call	_writeNumSym
 503  0125 0c03          	inc	(OFST+0,sp)
 507  0127 7b03          	ld	a,(OFST+0,sp)
 508  0129 a103          	cp	a,#3
 509  012b 25e3          	jrult	L371
 510                     ; 94 }
 513  012d 5b05          	addw	sp,#5
 514  012f 81            	ret
 537                     ; 96 void TIM2_config(void)
 537                     ; 97 {
 538                     	switch	.text
 539  0130               _TIM2_config:
 543                     ; 98 	TIM2->CR1 |= (TIM_CR1_DIR | TIM_CR1_ARPE | TIM_CR1_CEN);
 545  0130 c65250        	ld	a,21072
 546  0133 aa91          	or	a,#145
 547  0135 c75250        	ld	21072,a
 548                     ; 99 	TIM2->IER |= TIM_IER_CC1IE;
 550  0138 72125254      	bset	21076,#1
 551                     ; 101 }
 554  013c 81            	ret
 557                     	bsct
 558  0000               _counter1:
 559  0000 00            	dc.b	0
 598                     ; 103 int main()
 598                     ; 104 {
 599                     	switch	.text
 600  013d               _main:
 602  013d 89            	pushw	x
 603       00000002      OFST:	set	2
 606                     ; 105 	int i = 0;
 608                     ; 111 	enableInterrupts();
 611  013e 9a            rim
 613                     ; 112 	CLK_config();
 616  013f cd0000        	call	_CLK_config
 618                     ; 113 	TIM2_config();
 620  0142 adec          	call	_TIM2_config
 622                     ; 114 	GPIO_config();
 624  0144 cd001b        	call	_GPIO_config
 626                     ; 117 	delay_us(1);
 628  0147 ae0001        	ldw	x,#1
 629  014a 89            	pushw	x
 630  014b ae0000        	ldw	x,#0
 631  014e 89            	pushw	x
 632  014f cd0005        	call	_delay_us
 634  0152 5b04          	addw	sp,#4
 635  0154               L722:
 636                     ; 122 		counter1++;
 638  0154 3c00          	inc	_counter1
 639                     ; 123 		writeNumeric(counter1);
 641  0156 b600          	ld	a,_counter1
 642  0158 5f            	clrw	x
 643  0159 97            	ld	xl,a
 644  015a ad87          	call	_writeNumeric
 646                     ; 124 		delay_us(500);
 648  015c ae01f4        	ldw	x,#500
 649  015f 89            	pushw	x
 650  0160 ae0000        	ldw	x,#0
 651  0163 89            	pushw	x
 652  0164 cd0005        	call	_delay_us
 654  0167 5b04          	addw	sp,#4
 656  0169 20e9          	jra	L722
 691                     ; 137 void assert_failed(uint8_t* file, uint32_t line)
 691                     ; 138 { 
 692                     	switch	.text
 693  016b               _assert_failed:
 697  016b               L152:
 698  016b 20fe          	jra	L152
 722                     	xdef	_counter1
 723                     	xdef	_writeNumSym
 724                     	xdef	_writeNumeric
 725                     	xdef	_delay_us
 726                     	xdef	_main
 727                     	xdef	_TIM2_config
 728                     	xdef	_GPIO_config
 729                     	xdef	_CLK_config
 730                     	xdef	_assert_failed
 731                     	xref	_CLK_MasterPrescalerConfig
 750                     	xref	c_lrzmp
 751                     	xref	c_lgsbc
 752                     	xref	c_ltor
 753                     	end
