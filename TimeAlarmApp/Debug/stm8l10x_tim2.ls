   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  43                     ; 63 void TIM2_DeInit(void)
  43                     ; 64 {
  45                     	switch	.text
  46  0000               _TIM2_DeInit:
  50                     ; 65   TIM2->CR1 = TIM_CR1_RESET_VALUE;
  52  0000 725f5250      	clr	21072
  53                     ; 66   TIM2->CR2 = TIM_CR2_RESET_VALUE;
  55  0004 725f5251      	clr	21073
  56                     ; 67   TIM2->SMCR = TIM_SMCR_RESET_VALUE;
  58  0008 725f5252      	clr	21074
  59                     ; 68   TIM2->ETR = TIM_ETR_RESET_VALUE;
  61  000c 725f5253      	clr	21075
  62                     ; 69   TIM2->IER = TIM_IER_RESET_VALUE;
  64  0010 725f5254      	clr	21076
  65                     ; 70   TIM2->SR2 = TIM_SR2_RESET_VALUE;
  67  0014 725f5256      	clr	21078
  68                     ; 73   TIM2->CCER1 = TIM_CCER1_RESET_VALUE;
  70  0018 725f525a      	clr	21082
  71                     ; 75   TIM2->CCMR1 = 0x01;/*TIM2_ICxSource_TIxFPx */
  73  001c 35015258      	mov	21080,#1
  74                     ; 76   TIM2->CCMR2 = 0x01;/*TIM2_ICxSource_TIxFPx */
  76  0020 35015259      	mov	21081,#1
  77                     ; 79   TIM2->CCER1 = TIM_CCER1_RESET_VALUE;
  79  0024 725f525a      	clr	21082
  80                     ; 80   TIM2->CCMR1 = TIM_CCMR1_RESET_VALUE;
  82  0028 725f5258      	clr	21080
  83                     ; 81   TIM2->CCMR2 = TIM_CCMR2_RESET_VALUE;
  85  002c 725f5259      	clr	21081
  86                     ; 83   TIM2->CNTRH = TIM_CNTRH_RESET_VALUE;
  88  0030 725f525b      	clr	21083
  89                     ; 84   TIM2->CNTRL = TIM_CNTRL_RESET_VALUE;
  91  0034 725f525c      	clr	21084
  92                     ; 86   TIM2->PSCR = TIM_PSCR_RESET_VALUE;
  94  0038 725f525d      	clr	21085
  95                     ; 88   TIM2->ARRH = TIM_ARRH_RESET_VALUE;
  97  003c 35ff525e      	mov	21086,#255
  98                     ; 89   TIM2->ARRL = TIM_ARRL_RESET_VALUE;
 100  0040 35ff525f      	mov	21087,#255
 101                     ; 91   TIM2->CCR1H = TIM_CCR1H_RESET_VALUE;
 103  0044 725f5260      	clr	21088
 104                     ; 92   TIM2->CCR1L = TIM_CCR1L_RESET_VALUE;
 106  0048 725f5261      	clr	21089
 107                     ; 93   TIM2->CCR2H = TIM_CCR2H_RESET_VALUE;
 109  004c 725f5262      	clr	21090
 110                     ; 94   TIM2->CCR2L = TIM_CCR2L_RESET_VALUE;
 112  0050 725f5263      	clr	21091
 113                     ; 96   TIM2->OISR = TIM_OISR_RESET_VALUE;
 115  0054 725f5265      	clr	21093
 116                     ; 97   TIM2->EGR = 0x01;/*TIM_EGR_UG;*/
 118  0058 35015257      	mov	21079,#1
 119                     ; 98   TIM2->BKR = TIM_BKR_RESET_VALUE;
 121  005c 725f5264      	clr	21092
 122                     ; 99   TIM2->SR1 = TIM_SR1_RESET_VALUE;
 124  0060 725f5255      	clr	21077
 125                     ; 100 }
 128  0064 81            	ret
 291                     ; 124 void TIM2_TimeBaseInit(TIM2_Prescaler_TypeDef TIM2_Prescaler,
 291                     ; 125                        TIM2_CounterMode_TypeDef TIM2_CounterMode,
 291                     ; 126                        uint16_t TIM2_Period)
 291                     ; 127 {
 292                     	switch	.text
 293  0065               _TIM2_TimeBaseInit:
 295  0065 89            	pushw	x
 296       00000000      OFST:	set	0
 299                     ; 129   assert_param(IS_TIM2_PRESCALER(TIM2_Prescaler));
 301  0066 9e            	ld	a,xh
 302  0067 4d            	tnz	a
 303  0068 2723          	jreq	L21
 304  006a 9e            	ld	a,xh
 305  006b a101          	cp	a,#1
 306  006d 271e          	jreq	L21
 307  006f 9e            	ld	a,xh
 308  0070 a102          	cp	a,#2
 309  0072 2719          	jreq	L21
 310  0074 9e            	ld	a,xh
 311  0075 a103          	cp	a,#3
 312  0077 2714          	jreq	L21
 313  0079 9e            	ld	a,xh
 314  007a a104          	cp	a,#4
 315  007c 270f          	jreq	L21
 316  007e 9e            	ld	a,xh
 317  007f a105          	cp	a,#5
 318  0081 270a          	jreq	L21
 319  0083 9e            	ld	a,xh
 320  0084 a106          	cp	a,#6
 321  0086 2705          	jreq	L21
 322  0088 9e            	ld	a,xh
 323  0089 a107          	cp	a,#7
 324  008b 2603          	jrne	L01
 325  008d               L21:
 326  008d 4f            	clr	a
 327  008e 2010          	jra	L41
 328  0090               L01:
 329  0090 ae0081        	ldw	x,#129
 330  0093 89            	pushw	x
 331  0094 ae0000        	ldw	x,#0
 332  0097 89            	pushw	x
 333  0098 ae0000        	ldw	x,#L511
 334  009b cd0000        	call	_assert_failed
 336  009e 5b04          	addw	sp,#4
 337  00a0               L41:
 338                     ; 130   assert_param(IS_TIM2_COUNTER_MODE(TIM2_CounterMode));
 340  00a0 0d02          	tnz	(OFST+2,sp)
 341  00a2 2718          	jreq	L02
 342  00a4 7b02          	ld	a,(OFST+2,sp)
 343  00a6 a110          	cp	a,#16
 344  00a8 2712          	jreq	L02
 345  00aa 7b02          	ld	a,(OFST+2,sp)
 346  00ac a120          	cp	a,#32
 347  00ae 270c          	jreq	L02
 348  00b0 7b02          	ld	a,(OFST+2,sp)
 349  00b2 a140          	cp	a,#64
 350  00b4 2706          	jreq	L02
 351  00b6 7b02          	ld	a,(OFST+2,sp)
 352  00b8 a160          	cp	a,#96
 353  00ba 2603          	jrne	L61
 354  00bc               L02:
 355  00bc 4f            	clr	a
 356  00bd 2010          	jra	L22
 357  00bf               L61:
 358  00bf ae0082        	ldw	x,#130
 359  00c2 89            	pushw	x
 360  00c3 ae0000        	ldw	x,#0
 361  00c6 89            	pushw	x
 362  00c7 ae0000        	ldw	x,#L511
 363  00ca cd0000        	call	_assert_failed
 365  00cd 5b04          	addw	sp,#4
 366  00cf               L22:
 367                     ; 133   TIM2->ARRH = (uint8_t)(TIM2_Period >> 8) ;
 369  00cf 7b05          	ld	a,(OFST+5,sp)
 370  00d1 c7525e        	ld	21086,a
 371                     ; 134   TIM2->ARRL = (uint8_t)(TIM2_Period);
 373  00d4 7b06          	ld	a,(OFST+6,sp)
 374  00d6 c7525f        	ld	21087,a
 375                     ; 137   TIM2->PSCR = (uint8_t)(TIM2_Prescaler);
 377  00d9 7b01          	ld	a,(OFST+1,sp)
 378  00db c7525d        	ld	21085,a
 379                     ; 140   TIM2->CR1 &= (uint8_t)((uint8_t)(~TIM_CR1_CMS)) & ((uint8_t)(~TIM_CR1_DIR));
 381  00de c65250        	ld	a,21072
 382  00e1 a48f          	and	a,#143
 383  00e3 c75250        	ld	21072,a
 384                     ; 141   TIM2->CR1 |= (uint8_t)(TIM2_CounterMode);
 386  00e6 c65250        	ld	a,21072
 387  00e9 1a02          	or	a,(OFST+2,sp)
 388  00eb c75250        	ld	21072,a
 389                     ; 142 }
 392  00ee 85            	popw	x
 393  00ef 81            	ret
 592                     ; 169 void TIM2_OC1Init(TIM2_OCMode_TypeDef TIM2_OCMode,
 592                     ; 170                   TIM2_OutputState_TypeDef TIM2_OutputState,
 592                     ; 171                   uint16_t TIM2_Pulse,
 592                     ; 172                   TIM2_OCPolarity_TypeDef TIM2_OCPolarity,
 592                     ; 173                   TIM2_OCIdleState_TypeDef TIM2_OCIdleState)
 592                     ; 174 {
 593                     	switch	.text
 594  00f0               _TIM2_OC1Init:
 596  00f0 89            	pushw	x
 597  00f1 88            	push	a
 598       00000001      OFST:	set	1
 601                     ; 175   uint8_t tmpccmr1 = 0;
 603                     ; 178   assert_param(IS_TIM2_OC_MODE(TIM2_OCMode));
 605  00f2 9e            	ld	a,xh
 606  00f3 4d            	tnz	a
 607  00f4 2719          	jreq	L03
 608  00f6 9e            	ld	a,xh
 609  00f7 a110          	cp	a,#16
 610  00f9 2714          	jreq	L03
 611  00fb 9e            	ld	a,xh
 612  00fc a120          	cp	a,#32
 613  00fe 270f          	jreq	L03
 614  0100 9e            	ld	a,xh
 615  0101 a130          	cp	a,#48
 616  0103 270a          	jreq	L03
 617  0105 9e            	ld	a,xh
 618  0106 a160          	cp	a,#96
 619  0108 2705          	jreq	L03
 620  010a 9e            	ld	a,xh
 621  010b a170          	cp	a,#112
 622  010d 2603          	jrne	L62
 623  010f               L03:
 624  010f 4f            	clr	a
 625  0110 2010          	jra	L23
 626  0112               L62:
 627  0112 ae00b2        	ldw	x,#178
 628  0115 89            	pushw	x
 629  0116 ae0000        	ldw	x,#0
 630  0119 89            	pushw	x
 631  011a ae0000        	ldw	x,#L511
 632  011d cd0000        	call	_assert_failed
 634  0120 5b04          	addw	sp,#4
 635  0122               L23:
 636                     ; 179   assert_param(IS_TIM2_OUTPUT_STATE(TIM2_OutputState));
 638  0122 0d03          	tnz	(OFST+2,sp)
 639  0124 2706          	jreq	L63
 640  0126 7b03          	ld	a,(OFST+2,sp)
 641  0128 a101          	cp	a,#1
 642  012a 2603          	jrne	L43
 643  012c               L63:
 644  012c 4f            	clr	a
 645  012d 2010          	jra	L04
 646  012f               L43:
 647  012f ae00b3        	ldw	x,#179
 648  0132 89            	pushw	x
 649  0133 ae0000        	ldw	x,#0
 650  0136 89            	pushw	x
 651  0137 ae0000        	ldw	x,#L511
 652  013a cd0000        	call	_assert_failed
 654  013d 5b04          	addw	sp,#4
 655  013f               L04:
 656                     ; 180   assert_param(IS_TIM2_OC_POLARITY(TIM2_OCPolarity));
 658  013f 0d08          	tnz	(OFST+7,sp)
 659  0141 2706          	jreq	L44
 660  0143 7b08          	ld	a,(OFST+7,sp)
 661  0145 a101          	cp	a,#1
 662  0147 2603          	jrne	L24
 663  0149               L44:
 664  0149 4f            	clr	a
 665  014a 2010          	jra	L64
 666  014c               L24:
 667  014c ae00b4        	ldw	x,#180
 668  014f 89            	pushw	x
 669  0150 ae0000        	ldw	x,#0
 670  0153 89            	pushw	x
 671  0154 ae0000        	ldw	x,#L511
 672  0157 cd0000        	call	_assert_failed
 674  015a 5b04          	addw	sp,#4
 675  015c               L64:
 676                     ; 181   assert_param(IS_TIM2_OCIDLE_STATE(TIM2_OCIdleState));
 678  015c 7b09          	ld	a,(OFST+8,sp)
 679  015e a101          	cp	a,#1
 680  0160 2704          	jreq	L25
 681  0162 0d09          	tnz	(OFST+8,sp)
 682  0164 2603          	jrne	L05
 683  0166               L25:
 684  0166 4f            	clr	a
 685  0167 2010          	jra	L45
 686  0169               L05:
 687  0169 ae00b5        	ldw	x,#181
 688  016c 89            	pushw	x
 689  016d ae0000        	ldw	x,#0
 690  0170 89            	pushw	x
 691  0171 ae0000        	ldw	x,#L511
 692  0174 cd0000        	call	_assert_failed
 694  0177 5b04          	addw	sp,#4
 695  0179               L45:
 696                     ; 183   tmpccmr1 = TIM2->CCMR1;
 698  0179 c65258        	ld	a,21080
 699  017c 6b01          	ld	(OFST+0,sp),a
 701                     ; 186   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
 703  017e 7211525a      	bres	21082,#0
 704                     ; 188   tmpccmr1 &= (uint8_t)(~TIM_CCMR_OCM);
 706  0182 7b01          	ld	a,(OFST+0,sp)
 707  0184 a48f          	and	a,#143
 708  0186 6b01          	ld	(OFST+0,sp),a
 710                     ; 191   tmpccmr1 |= (uint8_t)TIM2_OCMode;
 712  0188 7b01          	ld	a,(OFST+0,sp)
 713  018a 1a02          	or	a,(OFST+1,sp)
 714  018c 6b01          	ld	(OFST+0,sp),a
 716                     ; 193   TIM2->CCMR1 = tmpccmr1;
 718  018e 7b01          	ld	a,(OFST+0,sp)
 719  0190 c75258        	ld	21080,a
 720                     ; 196   if (TIM2_OutputState == TIM2_OutputState_Enable)
 722  0193 7b03          	ld	a,(OFST+2,sp)
 723  0195 a101          	cp	a,#1
 724  0197 2606          	jrne	L132
 725                     ; 198     TIM2->CCER1 |= TIM_CCER1_CC1E;
 727  0199 7210525a      	bset	21082,#0
 729  019d 2004          	jra	L332
 730  019f               L132:
 731                     ; 202     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
 733  019f 7211525a      	bres	21082,#0
 734  01a3               L332:
 735                     ; 206   if (TIM2_OCPolarity == TIM2_OCPolarity_Low)
 737  01a3 7b08          	ld	a,(OFST+7,sp)
 738  01a5 a101          	cp	a,#1
 739  01a7 2606          	jrne	L532
 740                     ; 208     TIM2->CCER1 |= TIM_CCER1_CC1P;
 742  01a9 7212525a      	bset	21082,#1
 744  01ad 2004          	jra	L732
 745  01af               L532:
 746                     ; 212     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1P);
 748  01af 7213525a      	bres	21082,#1
 749  01b3               L732:
 750                     ; 216   if (TIM2_OCIdleState == TIM2_OCIdleState_Set)
 752  01b3 7b09          	ld	a,(OFST+8,sp)
 753  01b5 a101          	cp	a,#1
 754  01b7 2606          	jrne	L142
 755                     ; 218     TIM2->OISR |= TIM_OISR_OIS1;
 757  01b9 72105265      	bset	21093,#0
 759  01bd 2004          	jra	L342
 760  01bf               L142:
 761                     ; 222     TIM2->OISR &= (uint8_t)(~TIM_OISR_OIS1);
 763  01bf 72115265      	bres	21093,#0
 764  01c3               L342:
 765                     ; 226   TIM2->CCR1H = (uint8_t)(TIM2_Pulse >> 8);
 767  01c3 7b06          	ld	a,(OFST+5,sp)
 768  01c5 c75260        	ld	21088,a
 769                     ; 227   TIM2->CCR1L = (uint8_t)(TIM2_Pulse);
 771  01c8 7b07          	ld	a,(OFST+6,sp)
 772  01ca c75261        	ld	21089,a
 773                     ; 228 }
 776  01cd 5b03          	addw	sp,#3
 777  01cf 81            	ret
 861                     ; 255 void TIM2_OC2Init(TIM2_OCMode_TypeDef TIM2_OCMode,
 861                     ; 256                   TIM2_OutputState_TypeDef TIM2_OutputState,
 861                     ; 257                   uint16_t TIM2_Pulse,
 861                     ; 258                   TIM2_OCPolarity_TypeDef TIM2_OCPolarity,
 861                     ; 259                   TIM2_OCIdleState_TypeDef TIM2_OCIdleState)
 861                     ; 260 {
 862                     	switch	.text
 863  01d0               _TIM2_OC2Init:
 865  01d0 89            	pushw	x
 866  01d1 88            	push	a
 867       00000001      OFST:	set	1
 870                     ; 261   uint8_t tmpccmr2 = 0;
 872                     ; 264   assert_param(IS_TIM2_OC_MODE(TIM2_OCMode));
 874  01d2 9e            	ld	a,xh
 875  01d3 4d            	tnz	a
 876  01d4 2719          	jreq	L26
 877  01d6 9e            	ld	a,xh
 878  01d7 a110          	cp	a,#16
 879  01d9 2714          	jreq	L26
 880  01db 9e            	ld	a,xh
 881  01dc a120          	cp	a,#32
 882  01de 270f          	jreq	L26
 883  01e0 9e            	ld	a,xh
 884  01e1 a130          	cp	a,#48
 885  01e3 270a          	jreq	L26
 886  01e5 9e            	ld	a,xh
 887  01e6 a160          	cp	a,#96
 888  01e8 2705          	jreq	L26
 889  01ea 9e            	ld	a,xh
 890  01eb a170          	cp	a,#112
 891  01ed 2603          	jrne	L06
 892  01ef               L26:
 893  01ef 4f            	clr	a
 894  01f0 2010          	jra	L46
 895  01f2               L06:
 896  01f2 ae0108        	ldw	x,#264
 897  01f5 89            	pushw	x
 898  01f6 ae0000        	ldw	x,#0
 899  01f9 89            	pushw	x
 900  01fa ae0000        	ldw	x,#L511
 901  01fd cd0000        	call	_assert_failed
 903  0200 5b04          	addw	sp,#4
 904  0202               L46:
 905                     ; 265   assert_param(IS_TIM2_OUTPUT_STATE(TIM2_OutputState));
 907  0202 0d03          	tnz	(OFST+2,sp)
 908  0204 2706          	jreq	L07
 909  0206 7b03          	ld	a,(OFST+2,sp)
 910  0208 a101          	cp	a,#1
 911  020a 2603          	jrne	L66
 912  020c               L07:
 913  020c 4f            	clr	a
 914  020d 2010          	jra	L27
 915  020f               L66:
 916  020f ae0109        	ldw	x,#265
 917  0212 89            	pushw	x
 918  0213 ae0000        	ldw	x,#0
 919  0216 89            	pushw	x
 920  0217 ae0000        	ldw	x,#L511
 921  021a cd0000        	call	_assert_failed
 923  021d 5b04          	addw	sp,#4
 924  021f               L27:
 925                     ; 266   assert_param(IS_TIM2_OC_POLARITY(TIM2_OCPolarity));
 927  021f 0d08          	tnz	(OFST+7,sp)
 928  0221 2706          	jreq	L67
 929  0223 7b08          	ld	a,(OFST+7,sp)
 930  0225 a101          	cp	a,#1
 931  0227 2603          	jrne	L47
 932  0229               L67:
 933  0229 4f            	clr	a
 934  022a 2010          	jra	L001
 935  022c               L47:
 936  022c ae010a        	ldw	x,#266
 937  022f 89            	pushw	x
 938  0230 ae0000        	ldw	x,#0
 939  0233 89            	pushw	x
 940  0234 ae0000        	ldw	x,#L511
 941  0237 cd0000        	call	_assert_failed
 943  023a 5b04          	addw	sp,#4
 944  023c               L001:
 945                     ; 267   assert_param(IS_TIM2_OCIDLE_STATE(TIM2_OCIdleState));
 947  023c 7b09          	ld	a,(OFST+8,sp)
 948  023e a101          	cp	a,#1
 949  0240 2704          	jreq	L401
 950  0242 0d09          	tnz	(OFST+8,sp)
 951  0244 2603          	jrne	L201
 952  0246               L401:
 953  0246 4f            	clr	a
 954  0247 2010          	jra	L601
 955  0249               L201:
 956  0249 ae010b        	ldw	x,#267
 957  024c 89            	pushw	x
 958  024d ae0000        	ldw	x,#0
 959  0250 89            	pushw	x
 960  0251 ae0000        	ldw	x,#L511
 961  0254 cd0000        	call	_assert_failed
 963  0257 5b04          	addw	sp,#4
 964  0259               L601:
 965                     ; 269   tmpccmr2 = TIM2->CCMR2;
 967  0259 c65259        	ld	a,21081
 968  025c 6b01          	ld	(OFST+0,sp),a
 970                     ; 272   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2E);
 972  025e 7219525a      	bres	21082,#4
 973                     ; 275   tmpccmr2 &= (uint8_t)(~TIM_CCMR_OCM);
 975  0262 7b01          	ld	a,(OFST+0,sp)
 976  0264 a48f          	and	a,#143
 977  0266 6b01          	ld	(OFST+0,sp),a
 979                     ; 278   tmpccmr2 |= (uint8_t)TIM2_OCMode;
 981  0268 7b01          	ld	a,(OFST+0,sp)
 982  026a 1a02          	or	a,(OFST+1,sp)
 983  026c 6b01          	ld	(OFST+0,sp),a
 985                     ; 280   TIM2->CCMR2 = tmpccmr2;
 987  026e 7b01          	ld	a,(OFST+0,sp)
 988  0270 c75259        	ld	21081,a
 989                     ; 283   if (TIM2_OutputState == TIM2_OutputState_Enable)
 991  0273 7b03          	ld	a,(OFST+2,sp)
 992  0275 a101          	cp	a,#1
 993  0277 2606          	jrne	L703
 994                     ; 285     TIM2->CCER1 |= TIM_CCER1_CC2E;
 996  0279 7218525a      	bset	21082,#4
 998  027d 2004          	jra	L113
 999  027f               L703:
1000                     ; 289     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2E);
1002  027f 7219525a      	bres	21082,#4
1003  0283               L113:
1004                     ; 293   if (TIM2_OCPolarity == TIM2_OCPolarity_Low)
1006  0283 7b08          	ld	a,(OFST+7,sp)
1007  0285 a101          	cp	a,#1
1008  0287 2606          	jrne	L313
1009                     ; 295     TIM2->CCER1 |= TIM_CCER1_CC2P;
1011  0289 721a525a      	bset	21082,#5
1013  028d 2004          	jra	L513
1014  028f               L313:
1015                     ; 299     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2P);
1017  028f 721b525a      	bres	21082,#5
1018  0293               L513:
1019                     ; 304   if (TIM2_OCIdleState == TIM2_OCIdleState_Set)
1021  0293 7b09          	ld	a,(OFST+8,sp)
1022  0295 a101          	cp	a,#1
1023  0297 2606          	jrne	L713
1024                     ; 306     TIM2->OISR |= TIM_OISR_OIS2;
1026  0299 72145265      	bset	21093,#2
1028  029d 2004          	jra	L123
1029  029f               L713:
1030                     ; 310     TIM2->OISR &= (uint8_t)(~TIM_OISR_OIS2);
1032  029f 72155265      	bres	21093,#2
1033  02a3               L123:
1034                     ; 314   TIM2->CCR2H = (uint8_t)(TIM2_Pulse >> 8);
1036  02a3 7b06          	ld	a,(OFST+5,sp)
1037  02a5 c75262        	ld	21090,a
1038                     ; 315   TIM2->CCR2L = (uint8_t)(TIM2_Pulse);
1040  02a8 7b07          	ld	a,(OFST+6,sp)
1041  02aa c75263        	ld	21091,a
1042                     ; 316 }
1045  02ad 5b03          	addw	sp,#3
1046  02af 81            	ret
1245                     ; 344 void TIM2_BKRConfig(TIM2_OSSIState_TypeDef TIM2_OSSIState,
1245                     ; 345                     TIM2_LockLevel_TypeDef TIM2_LockLevel,
1245                     ; 346                     TIM2_BreakState_TypeDef TIM2_BreakState,
1245                     ; 347                     TIM2_BreakPolarity_TypeDef TIM2_BreakPolarity,
1245                     ; 348                     TIM2_AutomaticOutput_TypeDef TIM2_AutomaticOutput)
1245                     ; 349 
1245                     ; 350 {
1246                     	switch	.text
1247  02b0               _TIM2_BKRConfig:
1249  02b0 89            	pushw	x
1250       00000000      OFST:	set	0
1253                     ; 352   assert_param(IS_TIM2_OSSI_STATE(TIM2_OSSIState));
1255  02b1 9e            	ld	a,xh
1256  02b2 a104          	cp	a,#4
1257  02b4 2704          	jreq	L411
1258  02b6 9e            	ld	a,xh
1259  02b7 4d            	tnz	a
1260  02b8 2603          	jrne	L211
1261  02ba               L411:
1262  02ba 4f            	clr	a
1263  02bb 2010          	jra	L611
1264  02bd               L211:
1265  02bd ae0160        	ldw	x,#352
1266  02c0 89            	pushw	x
1267  02c1 ae0000        	ldw	x,#0
1268  02c4 89            	pushw	x
1269  02c5 ae0000        	ldw	x,#L511
1270  02c8 cd0000        	call	_assert_failed
1272  02cb 5b04          	addw	sp,#4
1273  02cd               L611:
1274                     ; 353   assert_param(IS_TIM2_LOCK_LEVEL(TIM2_LockLevel));
1276  02cd 0d02          	tnz	(OFST+2,sp)
1277  02cf 2712          	jreq	L221
1278  02d1 7b02          	ld	a,(OFST+2,sp)
1279  02d3 a101          	cp	a,#1
1280  02d5 270c          	jreq	L221
1281  02d7 7b02          	ld	a,(OFST+2,sp)
1282  02d9 a102          	cp	a,#2
1283  02db 2706          	jreq	L221
1284  02dd 7b02          	ld	a,(OFST+2,sp)
1285  02df a103          	cp	a,#3
1286  02e1 2603          	jrne	L021
1287  02e3               L221:
1288  02e3 4f            	clr	a
1289  02e4 2010          	jra	L421
1290  02e6               L021:
1291  02e6 ae0161        	ldw	x,#353
1292  02e9 89            	pushw	x
1293  02ea ae0000        	ldw	x,#0
1294  02ed 89            	pushw	x
1295  02ee ae0000        	ldw	x,#L511
1296  02f1 cd0000        	call	_assert_failed
1298  02f4 5b04          	addw	sp,#4
1299  02f6               L421:
1300                     ; 354   assert_param(IS_TIM2_BREAK_STATE(TIM2_BreakState));
1302  02f6 7b05          	ld	a,(OFST+5,sp)
1303  02f8 a110          	cp	a,#16
1304  02fa 2704          	jreq	L031
1305  02fc 0d05          	tnz	(OFST+5,sp)
1306  02fe 2603          	jrne	L621
1307  0300               L031:
1308  0300 4f            	clr	a
1309  0301 2010          	jra	L231
1310  0303               L621:
1311  0303 ae0162        	ldw	x,#354
1312  0306 89            	pushw	x
1313  0307 ae0000        	ldw	x,#0
1314  030a 89            	pushw	x
1315  030b ae0000        	ldw	x,#L511
1316  030e cd0000        	call	_assert_failed
1318  0311 5b04          	addw	sp,#4
1319  0313               L231:
1320                     ; 355   assert_param(IS_TIM2_BREAK_POLARITY(TIM2_BreakPolarity));
1322  0313 0d06          	tnz	(OFST+6,sp)
1323  0315 2706          	jreq	L631
1324  0317 7b06          	ld	a,(OFST+6,sp)
1325  0319 a120          	cp	a,#32
1326  031b 2603          	jrne	L431
1327  031d               L631:
1328  031d 4f            	clr	a
1329  031e 2010          	jra	L041
1330  0320               L431:
1331  0320 ae0163        	ldw	x,#355
1332  0323 89            	pushw	x
1333  0324 ae0000        	ldw	x,#0
1334  0327 89            	pushw	x
1335  0328 ae0000        	ldw	x,#L511
1336  032b cd0000        	call	_assert_failed
1338  032e 5b04          	addw	sp,#4
1339  0330               L041:
1340                     ; 356   assert_param(IS_TIM2_AUTOMATIC_OUTPUT_STATE(TIM2_AutomaticOutput));
1342  0330 7b07          	ld	a,(OFST+7,sp)
1343  0332 a140          	cp	a,#64
1344  0334 2704          	jreq	L441
1345  0336 0d07          	tnz	(OFST+7,sp)
1346  0338 2603          	jrne	L241
1347  033a               L441:
1348  033a 4f            	clr	a
1349  033b 2010          	jra	L641
1350  033d               L241:
1351  033d ae0164        	ldw	x,#356
1352  0340 89            	pushw	x
1353  0341 ae0000        	ldw	x,#0
1354  0344 89            	pushw	x
1355  0345 ae0000        	ldw	x,#L511
1356  0348 cd0000        	call	_assert_failed
1358  034b 5b04          	addw	sp,#4
1359  034d               L641:
1360                     ; 361   TIM2->BKR = (uint8_t)((uint8_t)TIM2_OSSIState | (uint8_t)TIM2_LockLevel | \
1360                     ; 362                         (uint8_t)TIM2_BreakState | (uint8_t)TIM2_BreakPolarity | \
1360                     ; 363                         (uint8_t)TIM2_AutomaticOutput);
1362  034d 7b01          	ld	a,(OFST+1,sp)
1363  034f 1a02          	or	a,(OFST+2,sp)
1364  0351 1a05          	or	a,(OFST+5,sp)
1365  0353 1a06          	or	a,(OFST+6,sp)
1366  0355 1a07          	or	a,(OFST+7,sp)
1367  0357 c75264        	ld	21092,a
1368                     ; 364 }
1371  035a 85            	popw	x
1372  035b 81            	ret
1557                     ; 390 void TIM2_ICInit(TIM2_Channel_TypeDef TIM2_Channel,
1557                     ; 391                  TIM2_ICPolarity_TypeDef TIM2_ICPolarity,
1557                     ; 392                  TIM2_ICSelection_TypeDef TIM2_ICSelection,
1557                     ; 393                  TIM2_ICPSC_TypeDef TIM2_ICPrescaler,
1557                     ; 394                  uint8_t TIM2_ICFilter)
1557                     ; 395 {
1558                     	switch	.text
1559  035c               _TIM2_ICInit:
1561  035c 89            	pushw	x
1562       00000000      OFST:	set	0
1565                     ; 397   assert_param(IS_TIM2_CHANNEL(TIM2_Channel));
1567  035d 9e            	ld	a,xh
1568  035e 4d            	tnz	a
1569  035f 2705          	jreq	L451
1570  0361 9e            	ld	a,xh
1571  0362 a101          	cp	a,#1
1572  0364 2603          	jrne	L251
1573  0366               L451:
1574  0366 4f            	clr	a
1575  0367 2010          	jra	L651
1576  0369               L251:
1577  0369 ae018d        	ldw	x,#397
1578  036c 89            	pushw	x
1579  036d ae0000        	ldw	x,#0
1580  0370 89            	pushw	x
1581  0371 ae0000        	ldw	x,#L511
1582  0374 cd0000        	call	_assert_failed
1584  0377 5b04          	addw	sp,#4
1585  0379               L651:
1586                     ; 399   if (TIM2_Channel == TIM2_Channel_1)
1588  0379 0d01          	tnz	(OFST+1,sp)
1589  037b 2614          	jrne	L145
1590                     ; 402     TI1_Config(TIM2_ICPolarity,
1590                     ; 403                TIM2_ICSelection,
1590                     ; 404                TIM2_ICFilter);
1592  037d 7b07          	ld	a,(OFST+7,sp)
1593  037f 88            	push	a
1594  0380 7b06          	ld	a,(OFST+6,sp)
1595  0382 97            	ld	xl,a
1596  0383 7b03          	ld	a,(OFST+3,sp)
1597  0385 95            	ld	xh,a
1598  0386 cd0de0        	call	L3_TI1_Config
1600  0389 84            	pop	a
1601                     ; 407     TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
1603  038a 7b06          	ld	a,(OFST+6,sp)
1604  038c cd0bf5        	call	_TIM2_SetIC1Prescaler
1607  038f 2012          	jra	L345
1608  0391               L145:
1609                     ; 412     TI2_Config(TIM2_ICPolarity,
1609                     ; 413                TIM2_ICSelection,
1609                     ; 414                TIM2_ICFilter);
1611  0391 7b07          	ld	a,(OFST+7,sp)
1612  0393 88            	push	a
1613  0394 7b06          	ld	a,(OFST+6,sp)
1614  0396 97            	ld	xl,a
1615  0397 7b03          	ld	a,(OFST+3,sp)
1616  0399 95            	ld	xh,a
1617  039a cd0e77        	call	L5_TI2_Config
1619  039d 84            	pop	a
1620                     ; 416     TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
1622  039e 7b06          	ld	a,(OFST+6,sp)
1623  03a0 cd0c31        	call	_TIM2_SetIC2Prescaler
1625  03a3               L345:
1626                     ; 418 }
1629  03a3 85            	popw	x
1630  03a4 81            	ret
1727                     ; 444 void TIM2_PWMIConfig(TIM2_Channel_TypeDef TIM2_Channel,
1727                     ; 445                      TIM2_ICPolarity_TypeDef TIM2_ICPolarity,
1727                     ; 446                      TIM2_ICSelection_TypeDef TIM2_ICSelection,
1727                     ; 447                      TIM2_ICPSC_TypeDef TIM2_ICPrescaler,
1727                     ; 448                      uint8_t TIM2_ICFilter)
1727                     ; 449 {
1728                     	switch	.text
1729  03a5               _TIM2_PWMIConfig:
1731  03a5 89            	pushw	x
1732  03a6 89            	pushw	x
1733       00000002      OFST:	set	2
1736                     ; 450   uint8_t icpolarity = (uint8_t)TIM2_ICPolarity_Rising;
1738                     ; 451   uint8_t icselection = (uint8_t)TIM2_ICSelection_DirectTI;
1740                     ; 454   assert_param(IS_TIM2_CHANNEL(TIM2_Channel));
1742  03a7 9e            	ld	a,xh
1743  03a8 4d            	tnz	a
1744  03a9 2705          	jreq	L461
1745  03ab 9e            	ld	a,xh
1746  03ac a101          	cp	a,#1
1747  03ae 2603          	jrne	L261
1748  03b0               L461:
1749  03b0 4f            	clr	a
1750  03b1 2010          	jra	L661
1751  03b3               L261:
1752  03b3 ae01c6        	ldw	x,#454
1753  03b6 89            	pushw	x
1754  03b7 ae0000        	ldw	x,#0
1755  03ba 89            	pushw	x
1756  03bb ae0000        	ldw	x,#L511
1757  03be cd0000        	call	_assert_failed
1759  03c1 5b04          	addw	sp,#4
1760  03c3               L661:
1761                     ; 457   if (TIM2_ICPolarity == TIM2_ICPolarity_Rising)
1763  03c3 0d04          	tnz	(OFST+2,sp)
1764  03c5 2606          	jrne	L316
1765                     ; 459     icpolarity = (uint8_t)TIM2_ICPolarity_Falling;
1767  03c7 a601          	ld	a,#1
1768  03c9 6b01          	ld	(OFST-1,sp),a
1771  03cb 2002          	jra	L516
1772  03cd               L316:
1773                     ; 463     icpolarity = (uint8_t)TIM2_ICPolarity_Rising;
1775  03cd 0f01          	clr	(OFST-1,sp)
1777  03cf               L516:
1778                     ; 467   if (TIM2_ICSelection == TIM2_ICSelection_DirectTI)
1780  03cf 7b07          	ld	a,(OFST+5,sp)
1781  03d1 a101          	cp	a,#1
1782  03d3 2606          	jrne	L716
1783                     ; 469     icselection = (uint8_t)TIM2_ICSelection_IndirectTI;
1785  03d5 a602          	ld	a,#2
1786  03d7 6b02          	ld	(OFST+0,sp),a
1789  03d9 2004          	jra	L126
1790  03db               L716:
1791                     ; 473     icselection = (uint8_t)TIM2_ICSelection_DirectTI;
1793  03db a601          	ld	a,#1
1794  03dd 6b02          	ld	(OFST+0,sp),a
1796  03df               L126:
1797                     ; 476   if (TIM2_Channel == TIM2_Channel_1)
1799  03df 0d03          	tnz	(OFST+1,sp)
1800  03e1 2626          	jrne	L326
1801                     ; 479     TI1_Config(TIM2_ICPolarity, TIM2_ICSelection,
1801                     ; 480                TIM2_ICFilter);
1803  03e3 7b09          	ld	a,(OFST+7,sp)
1804  03e5 88            	push	a
1805  03e6 7b08          	ld	a,(OFST+6,sp)
1806  03e8 97            	ld	xl,a
1807  03e9 7b05          	ld	a,(OFST+3,sp)
1808  03eb 95            	ld	xh,a
1809  03ec cd0de0        	call	L3_TI1_Config
1811  03ef 84            	pop	a
1812                     ; 483     TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
1814  03f0 7b08          	ld	a,(OFST+6,sp)
1815  03f2 cd0bf5        	call	_TIM2_SetIC1Prescaler
1817                     ; 486     TI2_Config((TIM2_ICPolarity_TypeDef)icpolarity, (TIM2_ICSelection_TypeDef)icselection, TIM2_ICFilter);
1819  03f5 7b09          	ld	a,(OFST+7,sp)
1820  03f7 88            	push	a
1821  03f8 7b03          	ld	a,(OFST+1,sp)
1822  03fa 97            	ld	xl,a
1823  03fb 7b02          	ld	a,(OFST+0,sp)
1824  03fd 95            	ld	xh,a
1825  03fe cd0e77        	call	L5_TI2_Config
1827  0401 84            	pop	a
1828                     ; 489     TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
1830  0402 7b08          	ld	a,(OFST+6,sp)
1831  0404 cd0c31        	call	_TIM2_SetIC2Prescaler
1834  0407 2024          	jra	L526
1835  0409               L326:
1836                     ; 494     TI2_Config(TIM2_ICPolarity, TIM2_ICSelection,
1836                     ; 495                TIM2_ICFilter);
1838  0409 7b09          	ld	a,(OFST+7,sp)
1839  040b 88            	push	a
1840  040c 7b08          	ld	a,(OFST+6,sp)
1841  040e 97            	ld	xl,a
1842  040f 7b05          	ld	a,(OFST+3,sp)
1843  0411 95            	ld	xh,a
1844  0412 cd0e77        	call	L5_TI2_Config
1846  0415 84            	pop	a
1847                     ; 498     TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
1849  0416 7b08          	ld	a,(OFST+6,sp)
1850  0418 cd0c31        	call	_TIM2_SetIC2Prescaler
1852                     ; 501     TI1_Config((TIM2_ICPolarity_TypeDef)icpolarity, (TIM2_ICSelection_TypeDef)icselection, TIM2_ICFilter);
1854  041b 7b09          	ld	a,(OFST+7,sp)
1855  041d 88            	push	a
1856  041e 7b03          	ld	a,(OFST+1,sp)
1857  0420 97            	ld	xl,a
1858  0421 7b02          	ld	a,(OFST+0,sp)
1859  0423 95            	ld	xh,a
1860  0424 cd0de0        	call	L3_TI1_Config
1862  0427 84            	pop	a
1863                     ; 504     TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
1865  0428 7b08          	ld	a,(OFST+6,sp)
1866  042a cd0bf5        	call	_TIM2_SetIC1Prescaler
1868  042d               L526:
1869                     ; 506 }
1872  042d 5b04          	addw	sp,#4
1873  042f 81            	ret
1929                     ; 514 void TIM2_Cmd(FunctionalState NewState)
1929                     ; 515 {
1930                     	switch	.text
1931  0430               _TIM2_Cmd:
1933  0430 88            	push	a
1934       00000000      OFST:	set	0
1937                     ; 517   assert_param(IS_FUNCTIONAL_STATE(NewState));
1939  0431 4d            	tnz	a
1940  0432 2704          	jreq	L471
1941  0434 a101          	cp	a,#1
1942  0436 2603          	jrne	L271
1943  0438               L471:
1944  0438 4f            	clr	a
1945  0439 2010          	jra	L671
1946  043b               L271:
1947  043b ae0205        	ldw	x,#517
1948  043e 89            	pushw	x
1949  043f ae0000        	ldw	x,#0
1950  0442 89            	pushw	x
1951  0443 ae0000        	ldw	x,#L511
1952  0446 cd0000        	call	_assert_failed
1954  0449 5b04          	addw	sp,#4
1955  044b               L671:
1956                     ; 520   if (NewState != DISABLE)
1958  044b 0d01          	tnz	(OFST+1,sp)
1959  044d 2706          	jreq	L556
1960                     ; 522     TIM2->CR1 |= TIM_CR1_CEN;
1962  044f 72105250      	bset	21072,#0
1964  0453 2004          	jra	L756
1965  0455               L556:
1966                     ; 526     TIM2->CR1 &= (uint8_t)(~TIM_CR1_CEN);
1968  0455 72115250      	bres	21072,#0
1969  0459               L756:
1970                     ; 528 }
1973  0459 84            	pop	a
1974  045a 81            	ret
2011                     ; 536 void TIM2_CtrlPWMOutputs(FunctionalState NewState)
2011                     ; 537 {
2012                     	switch	.text
2013  045b               _TIM2_CtrlPWMOutputs:
2015  045b 88            	push	a
2016       00000000      OFST:	set	0
2019                     ; 539   assert_param(IS_FUNCTIONAL_STATE(NewState));
2021  045c 4d            	tnz	a
2022  045d 2704          	jreq	L402
2023  045f a101          	cp	a,#1
2024  0461 2603          	jrne	L202
2025  0463               L402:
2026  0463 4f            	clr	a
2027  0464 2010          	jra	L602
2028  0466               L202:
2029  0466 ae021b        	ldw	x,#539
2030  0469 89            	pushw	x
2031  046a ae0000        	ldw	x,#0
2032  046d 89            	pushw	x
2033  046e ae0000        	ldw	x,#L511
2034  0471 cd0000        	call	_assert_failed
2036  0474 5b04          	addw	sp,#4
2037  0476               L602:
2038                     ; 543   if (NewState != DISABLE)
2040  0476 0d01          	tnz	(OFST+1,sp)
2041  0478 2706          	jreq	L776
2042                     ; 545     TIM2->BKR |= TIM_BKR_MOE ;
2044  047a 721e5264      	bset	21092,#7
2046  047e 2004          	jra	L107
2047  0480               L776:
2048                     ; 549     TIM2->BKR &= (uint8_t)(~TIM_BKR_MOE) ;
2050  0480 721f5264      	bres	21092,#7
2051  0484               L107:
2052                     ; 551 }
2055  0484 84            	pop	a
2056  0485 81            	ret
2143                     ; 566 void TIM2_ITConfig(TIM2_IT_TypeDef TIM2_IT, FunctionalState NewState)
2143                     ; 567 {
2144                     	switch	.text
2145  0486               _TIM2_ITConfig:
2147  0486 89            	pushw	x
2148       00000000      OFST:	set	0
2151                     ; 569   assert_param(IS_TIM2_IT(TIM2_IT));
2153  0487 9e            	ld	a,xh
2154  0488 4d            	tnz	a
2155  0489 2703          	jreq	L212
2156  048b 4f            	clr	a
2157  048c 2010          	jra	L412
2158  048e               L212:
2159  048e ae0239        	ldw	x,#569
2160  0491 89            	pushw	x
2161  0492 ae0000        	ldw	x,#0
2162  0495 89            	pushw	x
2163  0496 ae0000        	ldw	x,#L511
2164  0499 cd0000        	call	_assert_failed
2166  049c 5b04          	addw	sp,#4
2167  049e               L412:
2168                     ; 570   assert_param(IS_FUNCTIONAL_STATE(NewState));
2170  049e 0d02          	tnz	(OFST+2,sp)
2171  04a0 2706          	jreq	L022
2172  04a2 7b02          	ld	a,(OFST+2,sp)
2173  04a4 a101          	cp	a,#1
2174  04a6 2603          	jrne	L612
2175  04a8               L022:
2176  04a8 4f            	clr	a
2177  04a9 2010          	jra	L222
2178  04ab               L612:
2179  04ab ae023a        	ldw	x,#570
2180  04ae 89            	pushw	x
2181  04af ae0000        	ldw	x,#0
2182  04b2 89            	pushw	x
2183  04b3 ae0000        	ldw	x,#L511
2184  04b6 cd0000        	call	_assert_failed
2186  04b9 5b04          	addw	sp,#4
2187  04bb               L222:
2188                     ; 572   if (NewState != DISABLE)
2190  04bb 0d02          	tnz	(OFST+2,sp)
2191  04bd 270a          	jreq	L347
2192                     ; 575     TIM2->IER |= (uint8_t)TIM2_IT;
2194  04bf c65254        	ld	a,21076
2195  04c2 1a01          	or	a,(OFST+1,sp)
2196  04c4 c75254        	ld	21076,a
2198  04c7 2009          	jra	L547
2199  04c9               L347:
2200                     ; 580     TIM2->IER &= (uint8_t)(~(uint8_t)TIM2_IT);
2202  04c9 7b01          	ld	a,(OFST+1,sp)
2203  04cb 43            	cpl	a
2204  04cc c45254        	and	a,21076
2205  04cf c75254        	ld	21076,a
2206  04d2               L547:
2207                     ; 582 }
2210  04d2 85            	popw	x
2211  04d3 81            	ret
2235                     ; 589 void TIM2_InternalClockConfig(void)
2235                     ; 590 {
2236                     	switch	.text
2237  04d4               _TIM2_InternalClockConfig:
2241                     ; 592   TIM2->SMCR &= (uint8_t)(~TIM_SMCR_SMS);
2243  04d4 c65252        	ld	a,21074
2244  04d7 a4f8          	and	a,#248
2245  04d9 c75252        	ld	21074,a
2246                     ; 593 }
2249  04dc 81            	ret
2366                     ; 611 void TIM2_ETRClockMode1Config(TIM2_ExtTRGPSC_TypeDef TIM2_ExtTRGPrescaler,
2366                     ; 612                               TIM2_ExtTRGPolarity_TypeDef TIM2_ExtTRGPolarity,
2366                     ; 613                               uint8_t TIM2_ExtTRGFilter)
2366                     ; 614 {
2367                     	switch	.text
2368  04dd               _TIM2_ETRClockMode1Config:
2370  04dd 89            	pushw	x
2371       00000000      OFST:	set	0
2374                     ; 616   TIM2_ETRConfig(TIM2_ExtTRGPrescaler, TIM2_ExtTRGPolarity, TIM2_ExtTRGFilter);
2376  04de 7b05          	ld	a,(OFST+5,sp)
2377  04e0 88            	push	a
2378  04e1 9f            	ld	a,xl
2379  04e2 97            	ld	xl,a
2380  04e3 7b02          	ld	a,(OFST+2,sp)
2381  04e5 95            	ld	xh,a
2382  04e6 ad35          	call	_TIM2_ETRConfig
2384  04e8 84            	pop	a
2385                     ; 619   TIM2->SMCR &= (uint8_t)(~TIM_SMCR_SMS);
2387  04e9 c65252        	ld	a,21074
2388  04ec a4f8          	and	a,#248
2389  04ee c75252        	ld	21074,a
2390                     ; 620   TIM2->SMCR |= (uint8_t)(TIM2_SlaveMode_External1);
2392  04f1 c65252        	ld	a,21074
2393  04f4 aa07          	or	a,#7
2394  04f6 c75252        	ld	21074,a
2395                     ; 623   TIM2->SMCR &= (uint8_t)(~TIM_SMCR_TS);
2397  04f9 c65252        	ld	a,21074
2398  04fc a48f          	and	a,#143
2399  04fe c75252        	ld	21074,a
2400                     ; 624   TIM2->SMCR |= (uint8_t)((TIM2_TRGSelection_TypeDef)TIM2_TRGSelection_ETRF);
2402  0501 c65252        	ld	a,21074
2403  0504 aa70          	or	a,#112
2404  0506 c75252        	ld	21074,a
2405                     ; 625 }
2408  0509 85            	popw	x
2409  050a 81            	ret
2467                     ; 634 void TIM2_ETRClockMode2Config(TIM2_ExtTRGPSC_TypeDef TIM2_ExtTRGPrescaler,
2467                     ; 635                               TIM2_ExtTRGPolarity_TypeDef TIM2_ExtTRGPolarity,
2467                     ; 636                               uint8_t TIM2_ExtTRGFilter)
2467                     ; 637 {
2468                     	switch	.text
2469  050b               _TIM2_ETRClockMode2Config:
2471  050b 89            	pushw	x
2472       00000000      OFST:	set	0
2475                     ; 639   TIM2_ETRConfig(TIM2_ExtTRGPrescaler, TIM2_ExtTRGPolarity, TIM2_ExtTRGFilter);
2477  050c 7b05          	ld	a,(OFST+5,sp)
2478  050e 88            	push	a
2479  050f 9f            	ld	a,xl
2480  0510 97            	ld	xl,a
2481  0511 7b02          	ld	a,(OFST+2,sp)
2482  0513 95            	ld	xh,a
2483  0514 ad07          	call	_TIM2_ETRConfig
2485  0516 84            	pop	a
2486                     ; 642   TIM2->ETR |= TIM_ETR_ECE ;
2488  0517 721c5253      	bset	21075,#6
2489                     ; 643 }
2492  051b 85            	popw	x
2493  051c 81            	ret
2550                     ; 660 void TIM2_ETRConfig(TIM2_ExtTRGPSC_TypeDef TIM2_ExtTRGPrescaler,
2550                     ; 661                     TIM2_ExtTRGPolarity_TypeDef TIM2_ExtTRGPolarity,
2550                     ; 662                     uint8_t TIM2_ExtTRGFilter)
2550                     ; 663 {
2551                     	switch	.text
2552  051d               _TIM2_ETRConfig:
2554  051d 89            	pushw	x
2555       00000000      OFST:	set	0
2558                     ; 665   assert_param(IS_TIM2_EXT_PRESCALER(TIM2_ExtTRGPrescaler));
2560  051e 9e            	ld	a,xh
2561  051f 4d            	tnz	a
2562  0520 270f          	jreq	L632
2563  0522 9e            	ld	a,xh
2564  0523 a110          	cp	a,#16
2565  0525 270a          	jreq	L632
2566  0527 9e            	ld	a,xh
2567  0528 a120          	cp	a,#32
2568  052a 2705          	jreq	L632
2569  052c 9e            	ld	a,xh
2570  052d a130          	cp	a,#48
2571  052f 2603          	jrne	L432
2572  0531               L632:
2573  0531 4f            	clr	a
2574  0532 2010          	jra	L042
2575  0534               L432:
2576  0534 ae0299        	ldw	x,#665
2577  0537 89            	pushw	x
2578  0538 ae0000        	ldw	x,#0
2579  053b 89            	pushw	x
2580  053c ae0000        	ldw	x,#L511
2581  053f cd0000        	call	_assert_failed
2583  0542 5b04          	addw	sp,#4
2584  0544               L042:
2585                     ; 666   assert_param(IS_TIM2_EXT_POLARITY(TIM2_ExtTRGPolarity));
2587  0544 7b02          	ld	a,(OFST+2,sp)
2588  0546 a180          	cp	a,#128
2589  0548 2704          	jreq	L442
2590  054a 0d02          	tnz	(OFST+2,sp)
2591  054c 2603          	jrne	L242
2592  054e               L442:
2593  054e 4f            	clr	a
2594  054f 2010          	jra	L642
2595  0551               L242:
2596  0551 ae029a        	ldw	x,#666
2597  0554 89            	pushw	x
2598  0555 ae0000        	ldw	x,#0
2599  0558 89            	pushw	x
2600  0559 ae0000        	ldw	x,#L511
2601  055c cd0000        	call	_assert_failed
2603  055f 5b04          	addw	sp,#4
2604  0561               L642:
2605                     ; 667   assert_param(IS_TIM2_EXT_FILTER(TIM2_ExtTRGFilter));
2607  0561 7b05          	ld	a,(OFST+5,sp)
2608  0563 a110          	cp	a,#16
2609  0565 2403          	jruge	L052
2610  0567 4f            	clr	a
2611  0568 2010          	jra	L252
2612  056a               L052:
2613  056a ae029b        	ldw	x,#667
2614  056d 89            	pushw	x
2615  056e ae0000        	ldw	x,#0
2616  0571 89            	pushw	x
2617  0572 ae0000        	ldw	x,#L511
2618  0575 cd0000        	call	_assert_failed
2620  0578 5b04          	addw	sp,#4
2621  057a               L252:
2622                     ; 669   TIM2->ETR |= (uint8_t)((uint8_t)TIM2_ExtTRGPrescaler | (uint8_t)TIM2_ExtTRGPolarity | (uint8_t)TIM2_ExtTRGFilter);
2624  057a 7b01          	ld	a,(OFST+1,sp)
2625  057c 1a02          	or	a,(OFST+2,sp)
2626  057e 1a05          	or	a,(OFST+5,sp)
2627  0580 ca5253        	or	a,21075
2628  0583 c75253        	ld	21075,a
2629                     ; 670 }
2632  0586 85            	popw	x
2633  0587 81            	ret
2723                     ; 687 void TIM2_TIxExternalClockConfig(TIM2_TIxExternalCLK1Source_TypeDef TIM2_TIxExternalCLKSource,
2723                     ; 688                                  TIM2_ICPolarity_TypeDef TIM2_ICPolarity,
2723                     ; 689                                  uint8_t TIM2_ICFilter)
2723                     ; 690 {
2724                     	switch	.text
2725  0588               _TIM2_TIxExternalClockConfig:
2727  0588 89            	pushw	x
2728       00000000      OFST:	set	0
2731                     ; 692   assert_param(IS_TIM2_TIXCLK_SOURCE(TIM2_TIxExternalCLKSource));
2733  0589 9e            	ld	a,xh
2734  058a a140          	cp	a,#64
2735  058c 270a          	jreq	L062
2736  058e 9e            	ld	a,xh
2737  058f a160          	cp	a,#96
2738  0591 2705          	jreq	L062
2739  0593 9e            	ld	a,xh
2740  0594 a150          	cp	a,#80
2741  0596 2603          	jrne	L652
2742  0598               L062:
2743  0598 4f            	clr	a
2744  0599 2010          	jra	L262
2745  059b               L652:
2746  059b ae02b4        	ldw	x,#692
2747  059e 89            	pushw	x
2748  059f ae0000        	ldw	x,#0
2749  05a2 89            	pushw	x
2750  05a3 ae0000        	ldw	x,#L511
2751  05a6 cd0000        	call	_assert_failed
2753  05a9 5b04          	addw	sp,#4
2754  05ab               L262:
2755                     ; 693   assert_param(IS_TIM2_IC_POLARITY(TIM2_ICPolarity));
2757  05ab 0d02          	tnz	(OFST+2,sp)
2758  05ad 2706          	jreq	L662
2759  05af 7b02          	ld	a,(OFST+2,sp)
2760  05b1 a101          	cp	a,#1
2761  05b3 2603          	jrne	L462
2762  05b5               L662:
2763  05b5 4f            	clr	a
2764  05b6 2010          	jra	L072
2765  05b8               L462:
2766  05b8 ae02b5        	ldw	x,#693
2767  05bb 89            	pushw	x
2768  05bc ae0000        	ldw	x,#0
2769  05bf 89            	pushw	x
2770  05c0 ae0000        	ldw	x,#L511
2771  05c3 cd0000        	call	_assert_failed
2773  05c6 5b04          	addw	sp,#4
2774  05c8               L072:
2775                     ; 694   assert_param(IS_TIM2_IC_FILTER(TIM2_ICFilter));
2777  05c8 7b05          	ld	a,(OFST+5,sp)
2778  05ca a110          	cp	a,#16
2779  05cc 2403          	jruge	L272
2780  05ce 4f            	clr	a
2781  05cf 2010          	jra	L472
2782  05d1               L272:
2783  05d1 ae02b6        	ldw	x,#694
2784  05d4 89            	pushw	x
2785  05d5 ae0000        	ldw	x,#0
2786  05d8 89            	pushw	x
2787  05d9 ae0000        	ldw	x,#L511
2788  05dc cd0000        	call	_assert_failed
2790  05df 5b04          	addw	sp,#4
2791  05e1               L472:
2792                     ; 697   if (TIM2_TIxExternalCLKSource == TIM2_TIxExternalCLK1Source_TI2)
2794  05e1 7b01          	ld	a,(OFST+1,sp)
2795  05e3 a160          	cp	a,#96
2796  05e5 260f          	jrne	L5411
2797                     ; 699     TI2_Config(TIM2_ICPolarity, TIM2_ICSelection_DirectTI, TIM2_ICFilter);
2799  05e7 7b05          	ld	a,(OFST+5,sp)
2800  05e9 88            	push	a
2801  05ea 7b03          	ld	a,(OFST+3,sp)
2802  05ec ae0001        	ldw	x,#1
2803  05ef 95            	ld	xh,a
2804  05f0 cd0e77        	call	L5_TI2_Config
2806  05f3 84            	pop	a
2808  05f4 200d          	jra	L7411
2809  05f6               L5411:
2810                     ; 703     TI1_Config(TIM2_ICPolarity, TIM2_ICSelection_DirectTI, TIM2_ICFilter);
2812  05f6 7b05          	ld	a,(OFST+5,sp)
2813  05f8 88            	push	a
2814  05f9 7b03          	ld	a,(OFST+3,sp)
2815  05fb ae0001        	ldw	x,#1
2816  05fe 95            	ld	xh,a
2817  05ff cd0de0        	call	L3_TI1_Config
2819  0602 84            	pop	a
2820  0603               L7411:
2821                     ; 707   TIM2_SelectInputTrigger((TIM2_TRGSelection_TypeDef)TIM2_TIxExternalCLKSource);
2823  0603 7b01          	ld	a,(OFST+1,sp)
2824  0605 ad0a          	call	_TIM2_SelectInputTrigger
2826                     ; 710   TIM2->SMCR |= (uint8_t)(TIM2_SlaveMode_External1);
2828  0607 c65252        	ld	a,21074
2829  060a aa07          	or	a,#7
2830  060c c75252        	ld	21074,a
2831                     ; 711 }
2834  060f 85            	popw	x
2835  0610 81            	ret
2936                     ; 727 void TIM2_SelectInputTrigger(TIM2_TRGSelection_TypeDef TIM2_InputTriggerSource)
2936                     ; 728 {
2937                     	switch	.text
2938  0611               _TIM2_SelectInputTrigger:
2940  0611 88            	push	a
2941  0612 88            	push	a
2942       00000001      OFST:	set	1
2945                     ; 729   uint8_t tmpsmcr = 0;
2947                     ; 732   assert_param(IS_TIM2_TRIGGER_SELECTION(TIM2_InputTriggerSource));
2949  0613 a120          	cp	a,#32
2950  0615 2713          	jreq	L203
2951  0617 4d            	tnz	a
2952  0618 2710          	jreq	L203
2953  061a a140          	cp	a,#64
2954  061c 270c          	jreq	L203
2955  061e a150          	cp	a,#80
2956  0620 2708          	jreq	L203
2957  0622 a160          	cp	a,#96
2958  0624 2704          	jreq	L203
2959  0626 a170          	cp	a,#112
2960  0628 2603          	jrne	L003
2961  062a               L203:
2962  062a 4f            	clr	a
2963  062b 2010          	jra	L403
2964  062d               L003:
2965  062d ae02dc        	ldw	x,#732
2966  0630 89            	pushw	x
2967  0631 ae0000        	ldw	x,#0
2968  0634 89            	pushw	x
2969  0635 ae0000        	ldw	x,#L511
2970  0638 cd0000        	call	_assert_failed
2972  063b 5b04          	addw	sp,#4
2973  063d               L403:
2974                     ; 734   tmpsmcr = TIM2->SMCR;
2976  063d c65252        	ld	a,21074
2977  0640 6b01          	ld	(OFST+0,sp),a
2979                     ; 737   tmpsmcr &= (uint8_t)(~TIM_SMCR_TS);
2981  0642 7b01          	ld	a,(OFST+0,sp)
2982  0644 a48f          	and	a,#143
2983  0646 6b01          	ld	(OFST+0,sp),a
2985                     ; 738   tmpsmcr |= (uint8_t)TIM2_InputTriggerSource;
2987  0648 7b01          	ld	a,(OFST+0,sp)
2988  064a 1a02          	or	a,(OFST+1,sp)
2989  064c 6b01          	ld	(OFST+0,sp),a
2991                     ; 740   TIM2->SMCR = (uint8_t)tmpsmcr;
2993  064e 7b01          	ld	a,(OFST+0,sp)
2994  0650 c75252        	ld	21074,a
2995                     ; 741 }
2998  0653 85            	popw	x
2999  0654 81            	ret
3036                     ; 749 void TIM2_UpdateDisableConfig(FunctionalState NewState)
3036                     ; 750 {
3037                     	switch	.text
3038  0655               _TIM2_UpdateDisableConfig:
3040  0655 88            	push	a
3041       00000000      OFST:	set	0
3044                     ; 752   assert_param(IS_FUNCTIONAL_STATE(NewState));
3046  0656 4d            	tnz	a
3047  0657 2704          	jreq	L213
3048  0659 a101          	cp	a,#1
3049  065b 2603          	jrne	L013
3050  065d               L213:
3051  065d 4f            	clr	a
3052  065e 2010          	jra	L413
3053  0660               L013:
3054  0660 ae02f0        	ldw	x,#752
3055  0663 89            	pushw	x
3056  0664 ae0000        	ldw	x,#0
3057  0667 89            	pushw	x
3058  0668 ae0000        	ldw	x,#L511
3059  066b cd0000        	call	_assert_failed
3061  066e 5b04          	addw	sp,#4
3062  0670               L413:
3063                     ; 755   if (NewState != DISABLE)
3065  0670 0d01          	tnz	(OFST+1,sp)
3066  0672 2706          	jreq	L1321
3067                     ; 757     TIM2->CR1 |= TIM_CR1_UDIS;
3069  0674 72125250      	bset	21072,#1
3071  0678 2004          	jra	L3321
3072  067a               L1321:
3073                     ; 761     TIM2->CR1 &= (uint8_t)(~TIM_CR1_UDIS);
3075  067a 72135250      	bres	21072,#1
3076  067e               L3321:
3077                     ; 763 }
3080  067e 84            	pop	a
3081  067f 81            	ret
3140                     ; 773 void TIM2_UpdateRequestConfig(TIM2_UpdateSource_TypeDef TIM2_UpdateSource)
3140                     ; 774 {
3141                     	switch	.text
3142  0680               _TIM2_UpdateRequestConfig:
3144  0680 88            	push	a
3145       00000000      OFST:	set	0
3148                     ; 776   assert_param(IS_TIM2_UPDATE_SOURCE(TIM2_UpdateSource));
3150  0681 4d            	tnz	a
3151  0682 2704          	jreq	L223
3152  0684 a101          	cp	a,#1
3153  0686 2603          	jrne	L023
3154  0688               L223:
3155  0688 4f            	clr	a
3156  0689 2010          	jra	L423
3157  068b               L023:
3158  068b ae0308        	ldw	x,#776
3159  068e 89            	pushw	x
3160  068f ae0000        	ldw	x,#0
3161  0692 89            	pushw	x
3162  0693 ae0000        	ldw	x,#L511
3163  0696 cd0000        	call	_assert_failed
3165  0699 5b04          	addw	sp,#4
3166  069b               L423:
3167                     ; 779   if (TIM2_UpdateSource == TIM2_UpdateSource_Regular)
3169  069b 7b01          	ld	a,(OFST+1,sp)
3170  069d a101          	cp	a,#1
3171  069f 2606          	jrne	L3621
3172                     ; 781     TIM2->CR1 |= TIM_CR1_URS ;
3174  06a1 72145250      	bset	21072,#2
3176  06a5 2004          	jra	L5621
3177  06a7               L3621:
3178                     ; 785     TIM2->CR1 &= (uint8_t)(~TIM_CR1_URS);
3180  06a7 72155250      	bres	21072,#2
3181  06ab               L5621:
3182                     ; 787 }
3185  06ab 84            	pop	a
3186  06ac 81            	ret
3223                     ; 795 void TIM2_SelectHallSensor(FunctionalState NewState)
3223                     ; 796 {
3224                     	switch	.text
3225  06ad               _TIM2_SelectHallSensor:
3227  06ad 88            	push	a
3228       00000000      OFST:	set	0
3231                     ; 798   assert_param(IS_FUNCTIONAL_STATE(NewState));
3233  06ae 4d            	tnz	a
3234  06af 2704          	jreq	L233
3235  06b1 a101          	cp	a,#1
3236  06b3 2603          	jrne	L033
3237  06b5               L233:
3238  06b5 4f            	clr	a
3239  06b6 2010          	jra	L433
3240  06b8               L033:
3241  06b8 ae031e        	ldw	x,#798
3242  06bb 89            	pushw	x
3243  06bc ae0000        	ldw	x,#0
3244  06bf 89            	pushw	x
3245  06c0 ae0000        	ldw	x,#L511
3246  06c3 cd0000        	call	_assert_failed
3248  06c6 5b04          	addw	sp,#4
3249  06c8               L433:
3250                     ; 801   if (NewState != DISABLE)
3252  06c8 0d01          	tnz	(OFST+1,sp)
3253  06ca 2706          	jreq	L5031
3254                     ; 803     TIM2->CR2 |= TIM_CR2_TI1S;
3256  06cc 721e5251      	bset	21073,#7
3258  06d0 2004          	jra	L7031
3259  06d2               L5031:
3260                     ; 807     TIM2->CR2 &= (uint8_t)(~TIM_CR2_TI1S);
3262  06d2 721f5251      	bres	21073,#7
3263  06d6               L7031:
3264                     ; 809 }
3267  06d6 84            	pop	a
3268  06d7 81            	ret
3326                     ; 819 void TIM2_SelectOnePulseMode(TIM2_OPMode_TypeDef TIM2_OPMode)
3326                     ; 820 {
3327                     	switch	.text
3328  06d8               _TIM2_SelectOnePulseMode:
3330  06d8 88            	push	a
3331       00000000      OFST:	set	0
3334                     ; 822   assert_param(IS_TIM2_OPM_MODE(TIM2_OPMode));
3336  06d9 a101          	cp	a,#1
3337  06db 2703          	jreq	L243
3338  06dd 4d            	tnz	a
3339  06de 2603          	jrne	L043
3340  06e0               L243:
3341  06e0 4f            	clr	a
3342  06e1 2010          	jra	L443
3343  06e3               L043:
3344  06e3 ae0336        	ldw	x,#822
3345  06e6 89            	pushw	x
3346  06e7 ae0000        	ldw	x,#0
3347  06ea 89            	pushw	x
3348  06eb ae0000        	ldw	x,#L511
3349  06ee cd0000        	call	_assert_failed
3351  06f1 5b04          	addw	sp,#4
3352  06f3               L443:
3353                     ; 825   if (TIM2_OPMode == TIM2_OPMode_Single)
3355  06f3 7b01          	ld	a,(OFST+1,sp)
3356  06f5 a101          	cp	a,#1
3357  06f7 2606          	jrne	L7331
3358                     ; 827     TIM2->CR1 |= TIM_CR1_OPM ;
3360  06f9 72165250      	bset	21072,#3
3362  06fd 2004          	jra	L1431
3363  06ff               L7331:
3364                     ; 831     TIM2->CR1 &= (uint8_t)(~TIM_CR1_OPM);
3366  06ff 72175250      	bres	21072,#3
3367  0703               L1431:
3368                     ; 833 }
3371  0703 84            	pop	a
3372  0704 81            	ret
3472                     ; 847 void TIM2_SelectOutputTrigger(TIM2_TRGOSource_TypeDef TIM2_TRGOSource)
3472                     ; 848 {
3473                     	switch	.text
3474  0705               _TIM2_SelectOutputTrigger:
3476  0705 88            	push	a
3477  0706 88            	push	a
3478       00000001      OFST:	set	1
3481                     ; 849   uint8_t tmpcr2 = 0;
3483                     ; 852   assert_param(IS_TIM2_TRGO_SOURCE(TIM2_TRGOSource));
3485  0707 4d            	tnz	a
3486  0708 2714          	jreq	L253
3487  070a a110          	cp	a,#16
3488  070c 2710          	jreq	L253
3489  070e a120          	cp	a,#32
3490  0710 270c          	jreq	L253
3491  0712 a130          	cp	a,#48
3492  0714 2708          	jreq	L253
3493  0716 a140          	cp	a,#64
3494  0718 2704          	jreq	L253
3495  071a a150          	cp	a,#80
3496  071c 2603          	jrne	L053
3497  071e               L253:
3498  071e 4f            	clr	a
3499  071f 2010          	jra	L453
3500  0721               L053:
3501  0721 ae0354        	ldw	x,#852
3502  0724 89            	pushw	x
3503  0725 ae0000        	ldw	x,#0
3504  0728 89            	pushw	x
3505  0729 ae0000        	ldw	x,#L511
3506  072c cd0000        	call	_assert_failed
3508  072f 5b04          	addw	sp,#4
3509  0731               L453:
3510                     ; 854   tmpcr2 = TIM2->CR2;
3512  0731 c65251        	ld	a,21073
3513  0734 6b01          	ld	(OFST+0,sp),a
3515                     ; 857   tmpcr2 &= (uint8_t)(~TIM_CR2_MMS);
3517  0736 7b01          	ld	a,(OFST+0,sp)
3518  0738 a48f          	and	a,#143
3519  073a 6b01          	ld	(OFST+0,sp),a
3521                     ; 860   tmpcr2 |= (uint8_t)TIM2_TRGOSource;
3523  073c 7b01          	ld	a,(OFST+0,sp)
3524  073e 1a02          	or	a,(OFST+1,sp)
3525  0740 6b01          	ld	(OFST+0,sp),a
3527                     ; 862   TIM2->CR2 = tmpcr2;
3529  0742 7b01          	ld	a,(OFST+0,sp)
3530  0744 c75251        	ld	21073,a
3531                     ; 863 }
3534  0747 85            	popw	x
3535  0748 81            	ret
3619                     ; 875 void TIM2_SelectSlaveMode(TIM2_SlaveMode_TypeDef TIM2_SlaveMode)
3619                     ; 876 {
3620                     	switch	.text
3621  0749               _TIM2_SelectSlaveMode:
3623  0749 88            	push	a
3624  074a 88            	push	a
3625       00000001      OFST:	set	1
3628                     ; 877   uint8_t tmpsmcr = 0;
3630                     ; 880   assert_param(IS_TIM2_SLAVE_MODE(TIM2_SlaveMode));
3632  074b a104          	cp	a,#4
3633  074d 270c          	jreq	L263
3634  074f a105          	cp	a,#5
3635  0751 2708          	jreq	L263
3636  0753 a106          	cp	a,#6
3637  0755 2704          	jreq	L263
3638  0757 a107          	cp	a,#7
3639  0759 2603          	jrne	L063
3640  075b               L263:
3641  075b 4f            	clr	a
3642  075c 2010          	jra	L463
3643  075e               L063:
3644  075e ae0370        	ldw	x,#880
3645  0761 89            	pushw	x
3646  0762 ae0000        	ldw	x,#0
3647  0765 89            	pushw	x
3648  0766 ae0000        	ldw	x,#L511
3649  0769 cd0000        	call	_assert_failed
3651  076c 5b04          	addw	sp,#4
3652  076e               L463:
3653                     ; 882   tmpsmcr = TIM2->SMCR;
3655  076e c65252        	ld	a,21074
3656  0771 6b01          	ld	(OFST+0,sp),a
3658                     ; 885   tmpsmcr &= (uint8_t)(~TIM_SMCR_SMS);
3660  0773 7b01          	ld	a,(OFST+0,sp)
3661  0775 a4f8          	and	a,#248
3662  0777 6b01          	ld	(OFST+0,sp),a
3664                     ; 888   tmpsmcr |= (uint8_t)TIM2_SlaveMode;
3666  0779 7b01          	ld	a,(OFST+0,sp)
3667  077b 1a02          	or	a,(OFST+1,sp)
3668  077d 6b01          	ld	(OFST+0,sp),a
3670                     ; 890   TIM2->SMCR = tmpsmcr;
3672  077f 7b01          	ld	a,(OFST+0,sp)
3673  0781 c75252        	ld	21074,a
3674                     ; 891 }
3677  0784 85            	popw	x
3678  0785 81            	ret
3715                     ; 899 void TIM2_SelectMasterSlaveMode(FunctionalState NewState)
3715                     ; 900 {
3716                     	switch	.text
3717  0786               _TIM2_SelectMasterSlaveMode:
3719  0786 88            	push	a
3720       00000000      OFST:	set	0
3723                     ; 902   assert_param(IS_FUNCTIONAL_STATE(NewState));
3725  0787 4d            	tnz	a
3726  0788 2704          	jreq	L273
3727  078a a101          	cp	a,#1
3728  078c 2603          	jrne	L073
3729  078e               L273:
3730  078e 4f            	clr	a
3731  078f 2010          	jra	L473
3732  0791               L073:
3733  0791 ae0386        	ldw	x,#902
3734  0794 89            	pushw	x
3735  0795 ae0000        	ldw	x,#0
3736  0798 89            	pushw	x
3737  0799 ae0000        	ldw	x,#L511
3738  079c cd0000        	call	_assert_failed
3740  079f 5b04          	addw	sp,#4
3741  07a1               L473:
3742                     ; 905   if (NewState != DISABLE)
3744  07a1 0d01          	tnz	(OFST+1,sp)
3745  07a3 2706          	jreq	L1641
3746                     ; 907     TIM2->SMCR |= TIM_SMCR_MSM;
3748  07a5 721e5252      	bset	21074,#7
3750  07a9 2004          	jra	L3641
3751  07ab               L1641:
3752                     ; 911     TIM2->SMCR &= (uint8_t)(~TIM_SMCR_MSM);
3754  07ab 721f5252      	bres	21074,#7
3755  07af               L3641:
3756                     ; 913 }
3759  07af 84            	pop	a
3760  07b0 81            	ret
3874                     ; 932 void TIM2_EncoderInterfaceConfig(TIM2_EncoderMode_TypeDef TIM2_EncoderMode,
3874                     ; 933                                  TIM2_ICPolarity_TypeDef TIM2_IC1Polarity,
3874                     ; 934                                  TIM2_ICPolarity_TypeDef TIM2_IC2Polarity)
3874                     ; 935 {
3875                     	switch	.text
3876  07b1               _TIM2_EncoderInterfaceConfig:
3878  07b1 89            	pushw	x
3879  07b2 5203          	subw	sp,#3
3880       00000003      OFST:	set	3
3883                     ; 936   uint8_t tmpsmcr = 0;
3885                     ; 937   uint8_t tmpccmr1 = 0;
3887                     ; 938   uint8_t tmpccmr2 = 0;
3889                     ; 941   assert_param(IS_TIM2_ENCODER_MODE(TIM2_EncoderMode));
3891  07b4 9e            	ld	a,xh
3892  07b5 a101          	cp	a,#1
3893  07b7 270a          	jreq	L204
3894  07b9 9e            	ld	a,xh
3895  07ba a102          	cp	a,#2
3896  07bc 2705          	jreq	L204
3897  07be 9e            	ld	a,xh
3898  07bf a103          	cp	a,#3
3899  07c1 2603          	jrne	L004
3900  07c3               L204:
3901  07c3 4f            	clr	a
3902  07c4 2010          	jra	L404
3903  07c6               L004:
3904  07c6 ae03ad        	ldw	x,#941
3905  07c9 89            	pushw	x
3906  07ca ae0000        	ldw	x,#0
3907  07cd 89            	pushw	x
3908  07ce ae0000        	ldw	x,#L511
3909  07d1 cd0000        	call	_assert_failed
3911  07d4 5b04          	addw	sp,#4
3912  07d6               L404:
3913                     ; 942   assert_param(IS_TIM2_IC_POLARITY(TIM2_IC1Polarity));
3915  07d6 0d05          	tnz	(OFST+2,sp)
3916  07d8 2706          	jreq	L014
3917  07da 7b05          	ld	a,(OFST+2,sp)
3918  07dc a101          	cp	a,#1
3919  07de 2603          	jrne	L604
3920  07e0               L014:
3921  07e0 4f            	clr	a
3922  07e1 2010          	jra	L214
3923  07e3               L604:
3924  07e3 ae03ae        	ldw	x,#942
3925  07e6 89            	pushw	x
3926  07e7 ae0000        	ldw	x,#0
3927  07ea 89            	pushw	x
3928  07eb ae0000        	ldw	x,#L511
3929  07ee cd0000        	call	_assert_failed
3931  07f1 5b04          	addw	sp,#4
3932  07f3               L214:
3933                     ; 943   assert_param(IS_TIM2_IC_POLARITY(TIM2_IC2Polarity));
3935  07f3 0d08          	tnz	(OFST+5,sp)
3936  07f5 2706          	jreq	L614
3937  07f7 7b08          	ld	a,(OFST+5,sp)
3938  07f9 a101          	cp	a,#1
3939  07fb 2603          	jrne	L414
3940  07fd               L614:
3941  07fd 4f            	clr	a
3942  07fe 2010          	jra	L024
3943  0800               L414:
3944  0800 ae03af        	ldw	x,#943
3945  0803 89            	pushw	x
3946  0804 ae0000        	ldw	x,#0
3947  0807 89            	pushw	x
3948  0808 ae0000        	ldw	x,#L511
3949  080b cd0000        	call	_assert_failed
3951  080e 5b04          	addw	sp,#4
3952  0810               L024:
3953                     ; 945   tmpsmcr = TIM2->SMCR;
3955  0810 c65252        	ld	a,21074
3956  0813 6b01          	ld	(OFST-2,sp),a
3958                     ; 946   tmpccmr1 = TIM2->CCMR1;
3960  0815 c65258        	ld	a,21080
3961  0818 6b02          	ld	(OFST-1,sp),a
3963                     ; 947   tmpccmr2 = TIM2->CCMR2;
3965  081a c65259        	ld	a,21081
3966  081d 6b03          	ld	(OFST+0,sp),a
3968                     ; 950   tmpsmcr &= (uint8_t)(TIM_SMCR_MSM | TIM_SMCR_TS)  ;
3970  081f 7b01          	ld	a,(OFST-2,sp)
3971  0821 a4f0          	and	a,#240
3972  0823 6b01          	ld	(OFST-2,sp),a
3974                     ; 951   tmpsmcr |= (uint8_t)TIM2_EncoderMode;
3976  0825 7b01          	ld	a,(OFST-2,sp)
3977  0827 1a04          	or	a,(OFST+1,sp)
3978  0829 6b01          	ld	(OFST-2,sp),a
3980                     ; 954   tmpccmr1 &= (uint8_t)(~TIM_CCMR_CCxS);
3982  082b 7b02          	ld	a,(OFST-1,sp)
3983  082d a4fc          	and	a,#252
3984  082f 6b02          	ld	(OFST-1,sp),a
3986                     ; 955   tmpccmr2 &= (uint8_t)(~TIM_CCMR_CCxS);
3988  0831 7b03          	ld	a,(OFST+0,sp)
3989  0833 a4fc          	and	a,#252
3990  0835 6b03          	ld	(OFST+0,sp),a
3992                     ; 956   tmpccmr1 |= TIM_CCMR_TIxDirect_Set;
3994  0837 7b02          	ld	a,(OFST-1,sp)
3995  0839 aa01          	or	a,#1
3996  083b 6b02          	ld	(OFST-1,sp),a
3998                     ; 957   tmpccmr2 |= TIM_CCMR_TIxDirect_Set;
4000  083d 7b03          	ld	a,(OFST+0,sp)
4001  083f aa01          	or	a,#1
4002  0841 6b03          	ld	(OFST+0,sp),a
4004                     ; 960   if (TIM2_IC1Polarity == TIM2_ICPolarity_Falling)
4006  0843 7b05          	ld	a,(OFST+2,sp)
4007  0845 a101          	cp	a,#1
4008  0847 2606          	jrne	L1451
4009                     ; 962     TIM2->CCER1 |= TIM_CCER1_CC1P ;
4011  0849 7212525a      	bset	21082,#1
4013  084d 2004          	jra	L3451
4014  084f               L1451:
4015                     ; 966     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1P) ;
4017  084f 7213525a      	bres	21082,#1
4018  0853               L3451:
4019                     ; 969   if (TIM2_IC2Polarity == TIM2_ICPolarity_Falling)
4021  0853 7b08          	ld	a,(OFST+5,sp)
4022  0855 a101          	cp	a,#1
4023  0857 2606          	jrne	L5451
4024                     ; 971     TIM2->CCER1 |= TIM_CCER1_CC2P ;
4026  0859 721a525a      	bset	21082,#5
4028  085d 2004          	jra	L7451
4029  085f               L5451:
4030                     ; 975     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2P) ;
4032  085f 721b525a      	bres	21082,#5
4033  0863               L7451:
4034                     ; 978   TIM2->SMCR = tmpsmcr;
4036  0863 7b01          	ld	a,(OFST-2,sp)
4037  0865 c75252        	ld	21074,a
4038                     ; 979   TIM2->CCMR1 = tmpccmr1;
4040  0868 7b02          	ld	a,(OFST-1,sp)
4041  086a c75258        	ld	21080,a
4042                     ; 980   TIM2->CCMR2 = tmpccmr2;
4044  086d 7b03          	ld	a,(OFST+0,sp)
4045  086f c75259        	ld	21081,a
4046                     ; 981 }
4049  0872 5b05          	addw	sp,#5
4050  0874 81            	ret
4119                     ; 1001 void TIM2_PrescalerConfig(TIM2_Prescaler_TypeDef TIM2_Prescaler,
4119                     ; 1002                           TIM2_PSCReloadMode_TypeDef TIM2_PSCReloadMode)
4119                     ; 1003 {
4120                     	switch	.text
4121  0875               _TIM2_PrescalerConfig:
4123  0875 89            	pushw	x
4124       00000000      OFST:	set	0
4127                     ; 1005   assert_param(IS_TIM2_PRESCALER(TIM2_Prescaler));
4129  0876 9e            	ld	a,xh
4130  0877 4d            	tnz	a
4131  0878 2723          	jreq	L624
4132  087a 9e            	ld	a,xh
4133  087b a101          	cp	a,#1
4134  087d 271e          	jreq	L624
4135  087f 9e            	ld	a,xh
4136  0880 a102          	cp	a,#2
4137  0882 2719          	jreq	L624
4138  0884 9e            	ld	a,xh
4139  0885 a103          	cp	a,#3
4140  0887 2714          	jreq	L624
4141  0889 9e            	ld	a,xh
4142  088a a104          	cp	a,#4
4143  088c 270f          	jreq	L624
4144  088e 9e            	ld	a,xh
4145  088f a105          	cp	a,#5
4146  0891 270a          	jreq	L624
4147  0893 9e            	ld	a,xh
4148  0894 a106          	cp	a,#6
4149  0896 2705          	jreq	L624
4150  0898 9e            	ld	a,xh
4151  0899 a107          	cp	a,#7
4152  089b 2603          	jrne	L424
4153  089d               L624:
4154  089d 4f            	clr	a
4155  089e 2010          	jra	L034
4156  08a0               L424:
4157  08a0 ae03ed        	ldw	x,#1005
4158  08a3 89            	pushw	x
4159  08a4 ae0000        	ldw	x,#0
4160  08a7 89            	pushw	x
4161  08a8 ae0000        	ldw	x,#L511
4162  08ab cd0000        	call	_assert_failed
4164  08ae 5b04          	addw	sp,#4
4165  08b0               L034:
4166                     ; 1006   assert_param(IS_TIM2_PRESCALER_RELOAD(TIM2_PSCReloadMode));
4168  08b0 0d02          	tnz	(OFST+2,sp)
4169  08b2 2706          	jreq	L434
4170  08b4 7b02          	ld	a,(OFST+2,sp)
4171  08b6 a101          	cp	a,#1
4172  08b8 2603          	jrne	L234
4173  08ba               L434:
4174  08ba 4f            	clr	a
4175  08bb 2010          	jra	L634
4176  08bd               L234:
4177  08bd ae03ee        	ldw	x,#1006
4178  08c0 89            	pushw	x
4179  08c1 ae0000        	ldw	x,#0
4180  08c4 89            	pushw	x
4181  08c5 ae0000        	ldw	x,#L511
4182  08c8 cd0000        	call	_assert_failed
4184  08cb 5b04          	addw	sp,#4
4185  08cd               L634:
4186                     ; 1009   TIM2->PSCR = (uint8_t)(TIM2_Prescaler);
4188  08cd 7b01          	ld	a,(OFST+1,sp)
4189  08cf c7525d        	ld	21085,a
4190                     ; 1012   if (TIM2_PSCReloadMode == TIM2_PSCReloadMode_Immediate)
4192  08d2 7b02          	ld	a,(OFST+2,sp)
4193  08d4 a101          	cp	a,#1
4194  08d6 2606          	jrne	L3061
4195                     ; 1014     TIM2->EGR |= TIM_EGR_UG ;
4197  08d8 72105257      	bset	21079,#0
4199  08dc 2004          	jra	L5061
4200  08de               L3061:
4201                     ; 1018     TIM2->EGR &= (uint8_t)(~TIM_EGR_UG) ;
4203  08de 72115257      	bres	21079,#0
4204  08e2               L5061:
4205                     ; 1020 }
4208  08e2 85            	popw	x
4209  08e3 81            	ret
4255                     ; 1033 void TIM2_CounterModeConfig(TIM2_CounterMode_TypeDef TIM2_CounterMode)
4255                     ; 1034 {
4256                     	switch	.text
4257  08e4               _TIM2_CounterModeConfig:
4259  08e4 88            	push	a
4260  08e5 88            	push	a
4261       00000001      OFST:	set	1
4264                     ; 1035   uint8_t tmpcr1 = 0;
4266                     ; 1038   assert_param(IS_TIM2_COUNTER_MODE(TIM2_CounterMode));
4268  08e6 4d            	tnz	a
4269  08e7 2710          	jreq	L444
4270  08e9 a110          	cp	a,#16
4271  08eb 270c          	jreq	L444
4272  08ed a120          	cp	a,#32
4273  08ef 2708          	jreq	L444
4274  08f1 a140          	cp	a,#64
4275  08f3 2704          	jreq	L444
4276  08f5 a160          	cp	a,#96
4277  08f7 2603          	jrne	L244
4278  08f9               L444:
4279  08f9 4f            	clr	a
4280  08fa 2010          	jra	L644
4281  08fc               L244:
4282  08fc ae040e        	ldw	x,#1038
4283  08ff 89            	pushw	x
4284  0900 ae0000        	ldw	x,#0
4285  0903 89            	pushw	x
4286  0904 ae0000        	ldw	x,#L511
4287  0907 cd0000        	call	_assert_failed
4289  090a 5b04          	addw	sp,#4
4290  090c               L644:
4291                     ; 1040   tmpcr1 = TIM2->CR1;
4293  090c c65250        	ld	a,21072
4294  090f 6b01          	ld	(OFST+0,sp),a
4296                     ; 1043   tmpcr1 &= (uint8_t)((uint8_t)(~TIM_CR1_CMS) & (uint8_t)(~TIM_CR1_DIR));
4298  0911 7b01          	ld	a,(OFST+0,sp)
4299  0913 a48f          	and	a,#143
4300  0915 6b01          	ld	(OFST+0,sp),a
4302                     ; 1046   tmpcr1 |= (uint8_t)TIM2_CounterMode;
4304  0917 7b01          	ld	a,(OFST+0,sp)
4305  0919 1a02          	or	a,(OFST+1,sp)
4306  091b 6b01          	ld	(OFST+0,sp),a
4308                     ; 1048   TIM2->CR1 = tmpcr1;
4310  091d 7b01          	ld	a,(OFST+0,sp)
4311  091f c75250        	ld	21072,a
4312                     ; 1049 }
4315  0922 85            	popw	x
4316  0923 81            	ret
4384                     ; 1059 void TIM2_ForcedOC1Config(TIM2_ForcedAction_TypeDef TIM2_ForcedAction)
4384                     ; 1060 {
4385                     	switch	.text
4386  0924               _TIM2_ForcedOC1Config:
4388  0924 88            	push	a
4389  0925 88            	push	a
4390       00000001      OFST:	set	1
4393                     ; 1061   uint8_t tmpccmr1 = 0;
4395                     ; 1064   assert_param(IS_TIM2_FORCED_ACTION(TIM2_ForcedAction));
4397  0926 a150          	cp	a,#80
4398  0928 2704          	jreq	L454
4399  092a a140          	cp	a,#64
4400  092c 2603          	jrne	L254
4401  092e               L454:
4402  092e 4f            	clr	a
4403  092f 2010          	jra	L654
4404  0931               L254:
4405  0931 ae0428        	ldw	x,#1064
4406  0934 89            	pushw	x
4407  0935 ae0000        	ldw	x,#0
4408  0938 89            	pushw	x
4409  0939 ae0000        	ldw	x,#L511
4410  093c cd0000        	call	_assert_failed
4412  093f 5b04          	addw	sp,#4
4413  0941               L654:
4414                     ; 1066   tmpccmr1 = TIM2->CCMR1;
4416  0941 c65258        	ld	a,21080
4417  0944 6b01          	ld	(OFST+0,sp),a
4419                     ; 1069   tmpccmr1 &= (uint8_t)(~TIM_CCMR_OCM);
4421  0946 7b01          	ld	a,(OFST+0,sp)
4422  0948 a48f          	and	a,#143
4423  094a 6b01          	ld	(OFST+0,sp),a
4425                     ; 1072   tmpccmr1 |= (uint8_t)TIM2_ForcedAction;
4427  094c 7b01          	ld	a,(OFST+0,sp)
4428  094e 1a02          	or	a,(OFST+1,sp)
4429  0950 6b01          	ld	(OFST+0,sp),a
4431                     ; 1074   TIM2->CCMR1 = tmpccmr1;
4433  0952 7b01          	ld	a,(OFST+0,sp)
4434  0954 c75258        	ld	21080,a
4435                     ; 1075 }
4438  0957 85            	popw	x
4439  0958 81            	ret
4485                     ; 1085 void TIM2_ForcedOC2Config(TIM2_ForcedAction_TypeDef TIM2_ForcedAction)
4485                     ; 1086 {
4486                     	switch	.text
4487  0959               _TIM2_ForcedOC2Config:
4489  0959 88            	push	a
4490  095a 88            	push	a
4491       00000001      OFST:	set	1
4494                     ; 1087   uint8_t tmpccmr2 = 0;
4496                     ; 1090   assert_param(IS_TIM2_FORCED_ACTION(TIM2_ForcedAction));
4498  095b a150          	cp	a,#80
4499  095d 2704          	jreq	L464
4500  095f a140          	cp	a,#64
4501  0961 2603          	jrne	L264
4502  0963               L464:
4503  0963 4f            	clr	a
4504  0964 2010          	jra	L664
4505  0966               L264:
4506  0966 ae0442        	ldw	x,#1090
4507  0969 89            	pushw	x
4508  096a ae0000        	ldw	x,#0
4509  096d 89            	pushw	x
4510  096e ae0000        	ldw	x,#L511
4511  0971 cd0000        	call	_assert_failed
4513  0974 5b04          	addw	sp,#4
4514  0976               L664:
4515                     ; 1092   tmpccmr2 = TIM2->CCMR2;
4517  0976 c65259        	ld	a,21081
4518  0979 6b01          	ld	(OFST+0,sp),a
4520                     ; 1095   tmpccmr2 &= (uint8_t)(~TIM_CCMR_OCM);
4522  097b 7b01          	ld	a,(OFST+0,sp)
4523  097d a48f          	and	a,#143
4524  097f 6b01          	ld	(OFST+0,sp),a
4526                     ; 1098   tmpccmr2 |= (uint8_t)TIM2_ForcedAction;
4528  0981 7b01          	ld	a,(OFST+0,sp)
4529  0983 1a02          	or	a,(OFST+1,sp)
4530  0985 6b01          	ld	(OFST+0,sp),a
4532                     ; 1100   TIM2->CCMR2 = tmpccmr2;
4534  0987 7b01          	ld	a,(OFST+0,sp)
4535  0989 c75259        	ld	21081,a
4536                     ; 1101 }
4539  098c 85            	popw	x
4540  098d 81            	ret
4577                     ; 1109 void TIM2_ARRPreloadConfig(FunctionalState NewState)
4577                     ; 1110 {
4578                     	switch	.text
4579  098e               _TIM2_ARRPreloadConfig:
4581  098e 88            	push	a
4582       00000000      OFST:	set	0
4585                     ; 1112   assert_param(IS_FUNCTIONAL_STATE(NewState));
4587  098f 4d            	tnz	a
4588  0990 2704          	jreq	L474
4589  0992 a101          	cp	a,#1
4590  0994 2603          	jrne	L274
4591  0996               L474:
4592  0996 4f            	clr	a
4593  0997 2010          	jra	L674
4594  0999               L274:
4595  0999 ae0458        	ldw	x,#1112
4596  099c 89            	pushw	x
4597  099d ae0000        	ldw	x,#0
4598  09a0 89            	pushw	x
4599  09a1 ae0000        	ldw	x,#L511
4600  09a4 cd0000        	call	_assert_failed
4602  09a7 5b04          	addw	sp,#4
4603  09a9               L674:
4604                     ; 1115   if (NewState != DISABLE)
4606  09a9 0d01          	tnz	(OFST+1,sp)
4607  09ab 2706          	jreq	L3271
4608                     ; 1117     TIM2->CR1 |= TIM_CR1_ARPE;
4610  09ad 721e5250      	bset	21072,#7
4612  09b1 2004          	jra	L5271
4613  09b3               L3271:
4614                     ; 1121     TIM2->CR1 &= (uint8_t)(~TIM_CR1_ARPE);
4616  09b3 721f5250      	bres	21072,#7
4617  09b7               L5271:
4618                     ; 1123 }
4621  09b7 84            	pop	a
4622  09b8 81            	ret
4659                     ; 1131 void TIM2_OC1PreloadConfig(FunctionalState NewState)
4659                     ; 1132 {
4660                     	switch	.text
4661  09b9               _TIM2_OC1PreloadConfig:
4663  09b9 88            	push	a
4664       00000000      OFST:	set	0
4667                     ; 1134   assert_param(IS_FUNCTIONAL_STATE(NewState));
4669  09ba 4d            	tnz	a
4670  09bb 2704          	jreq	L405
4671  09bd a101          	cp	a,#1
4672  09bf 2603          	jrne	L205
4673  09c1               L405:
4674  09c1 4f            	clr	a
4675  09c2 2010          	jra	L605
4676  09c4               L205:
4677  09c4 ae046e        	ldw	x,#1134
4678  09c7 89            	pushw	x
4679  09c8 ae0000        	ldw	x,#0
4680  09cb 89            	pushw	x
4681  09cc ae0000        	ldw	x,#L511
4682  09cf cd0000        	call	_assert_failed
4684  09d2 5b04          	addw	sp,#4
4685  09d4               L605:
4686                     ; 1137   if (NewState != DISABLE)
4688  09d4 0d01          	tnz	(OFST+1,sp)
4689  09d6 2706          	jreq	L5471
4690                     ; 1139     TIM2->CCMR1 |= TIM_CCMR_OCxPE ;
4692  09d8 72165258      	bset	21080,#3
4694  09dc 2004          	jra	L7471
4695  09de               L5471:
4696                     ; 1143     TIM2->CCMR1 &= (uint8_t)(~TIM_CCMR_OCxPE) ;
4698  09de 72175258      	bres	21080,#3
4699  09e2               L7471:
4700                     ; 1145 }
4703  09e2 84            	pop	a
4704  09e3 81            	ret
4741                     ; 1153 void TIM2_OC2PreloadConfig(FunctionalState NewState)
4741                     ; 1154 {
4742                     	switch	.text
4743  09e4               _TIM2_OC2PreloadConfig:
4745  09e4 88            	push	a
4746       00000000      OFST:	set	0
4749                     ; 1156   assert_param(IS_FUNCTIONAL_STATE(NewState));
4751  09e5 4d            	tnz	a
4752  09e6 2704          	jreq	L415
4753  09e8 a101          	cp	a,#1
4754  09ea 2603          	jrne	L215
4755  09ec               L415:
4756  09ec 4f            	clr	a
4757  09ed 2010          	jra	L615
4758  09ef               L215:
4759  09ef ae0484        	ldw	x,#1156
4760  09f2 89            	pushw	x
4761  09f3 ae0000        	ldw	x,#0
4762  09f6 89            	pushw	x
4763  09f7 ae0000        	ldw	x,#L511
4764  09fa cd0000        	call	_assert_failed
4766  09fd 5b04          	addw	sp,#4
4767  09ff               L615:
4768                     ; 1159   if (NewState != DISABLE)
4770  09ff 0d01          	tnz	(OFST+1,sp)
4771  0a01 2706          	jreq	L7671
4772                     ; 1161     TIM2->CCMR2 |= TIM_CCMR_OCxPE ;
4774  0a03 72165259      	bset	21081,#3
4776  0a07 2004          	jra	L1771
4777  0a09               L7671:
4778                     ; 1165     TIM2->CCMR2 &= (uint8_t)(~TIM_CCMR_OCxPE) ;
4780  0a09 72175259      	bres	21081,#3
4781  0a0d               L1771:
4782                     ; 1167 }
4785  0a0d 84            	pop	a
4786  0a0e 81            	ret
4822                     ; 1174 void TIM2_OC1FastCmd(FunctionalState NewState)
4822                     ; 1175 {
4823                     	switch	.text
4824  0a0f               _TIM2_OC1FastCmd:
4826  0a0f 88            	push	a
4827       00000000      OFST:	set	0
4830                     ; 1177   assert_param(IS_FUNCTIONAL_STATE(NewState));
4832  0a10 4d            	tnz	a
4833  0a11 2704          	jreq	L425
4834  0a13 a101          	cp	a,#1
4835  0a15 2603          	jrne	L225
4836  0a17               L425:
4837  0a17 4f            	clr	a
4838  0a18 2010          	jra	L625
4839  0a1a               L225:
4840  0a1a ae0499        	ldw	x,#1177
4841  0a1d 89            	pushw	x
4842  0a1e ae0000        	ldw	x,#0
4843  0a21 89            	pushw	x
4844  0a22 ae0000        	ldw	x,#L511
4845  0a25 cd0000        	call	_assert_failed
4847  0a28 5b04          	addw	sp,#4
4848  0a2a               L625:
4849                     ; 1180   if (NewState != DISABLE)
4851  0a2a 0d01          	tnz	(OFST+1,sp)
4852  0a2c 2706          	jreq	L1102
4853                     ; 1182     TIM2->CCMR1 |= TIM_CCMR_OCxFE ;
4855  0a2e 72145258      	bset	21080,#2
4857  0a32 2004          	jra	L3102
4858  0a34               L1102:
4859                     ; 1186     TIM2->CCMR1 &= (uint8_t)(~TIM_CCMR_OCxFE) ;
4861  0a34 72155258      	bres	21080,#2
4862  0a38               L3102:
4863                     ; 1188 }
4866  0a38 84            	pop	a
4867  0a39 81            	ret
4903                     ; 1195 void TIM2_OC2FastCmd(FunctionalState NewState)
4903                     ; 1196 {
4904                     	switch	.text
4905  0a3a               _TIM2_OC2FastCmd:
4907  0a3a 88            	push	a
4908       00000000      OFST:	set	0
4911                     ; 1198   assert_param(IS_FUNCTIONAL_STATE(NewState));
4913  0a3b 4d            	tnz	a
4914  0a3c 2704          	jreq	L435
4915  0a3e a101          	cp	a,#1
4916  0a40 2603          	jrne	L235
4917  0a42               L435:
4918  0a42 4f            	clr	a
4919  0a43 2010          	jra	L635
4920  0a45               L235:
4921  0a45 ae04ae        	ldw	x,#1198
4922  0a48 89            	pushw	x
4923  0a49 ae0000        	ldw	x,#0
4924  0a4c 89            	pushw	x
4925  0a4d ae0000        	ldw	x,#L511
4926  0a50 cd0000        	call	_assert_failed
4928  0a53 5b04          	addw	sp,#4
4929  0a55               L635:
4930                     ; 1201   if (NewState != DISABLE)
4932  0a55 0d01          	tnz	(OFST+1,sp)
4933  0a57 2706          	jreq	L3302
4934                     ; 1203     TIM2->CCMR2 |= TIM_CCMR_OCxFE ;
4936  0a59 72145259      	bset	21081,#2
4938  0a5d 2004          	jra	L5302
4939  0a5f               L3302:
4940                     ; 1207     TIM2->CCMR2 &= (uint8_t)(~TIM_CCMR_OCxFE) ;
4942  0a5f 72155259      	bres	21081,#2
4943  0a63               L5302:
4944                     ; 1209 }
4947  0a63 84            	pop	a
4948  0a64 81            	ret
5030                     ; 1222 void TIM2_GenerateEvent(TIM2_EventSource_TypeDef TIM2_EventSource)
5030                     ; 1223 {
5031                     	switch	.text
5032  0a65               _TIM2_GenerateEvent:
5034  0a65 88            	push	a
5035       00000000      OFST:	set	0
5038                     ; 1225   assert_param(IS_TIM2_EVENT_SOURCE((uint8_t)TIM2_EventSource));
5040  0a66 a518          	bcp	a,#24
5041  0a68 2607          	jrne	L245
5042  0a6a 0d01          	tnz	(OFST+1,sp)
5043  0a6c 2703          	jreq	L245
5044  0a6e 4f            	clr	a
5045  0a6f 2010          	jra	L445
5046  0a71               L245:
5047  0a71 ae04c9        	ldw	x,#1225
5048  0a74 89            	pushw	x
5049  0a75 ae0000        	ldw	x,#0
5050  0a78 89            	pushw	x
5051  0a79 ae0000        	ldw	x,#L511
5052  0a7c cd0000        	call	_assert_failed
5054  0a7f 5b04          	addw	sp,#4
5055  0a81               L445:
5056                     ; 1228   TIM2->EGR |= (uint8_t)TIM2_EventSource;
5058  0a81 c65257        	ld	a,21079
5059  0a84 1a01          	or	a,(OFST+1,sp)
5060  0a86 c75257        	ld	21079,a
5061                     ; 1229 }
5064  0a89 84            	pop	a
5065  0a8a 81            	ret
5102                     ; 1239 void TIM2_OC1PolarityConfig(TIM2_OCPolarity_TypeDef TIM2_OCPolarity)
5102                     ; 1240 {
5103                     	switch	.text
5104  0a8b               _TIM2_OC1PolarityConfig:
5106  0a8b 88            	push	a
5107       00000000      OFST:	set	0
5110                     ; 1242   assert_param(IS_TIM2_OC_POLARITY(TIM2_OCPolarity));
5112  0a8c 4d            	tnz	a
5113  0a8d 2704          	jreq	L255
5114  0a8f a101          	cp	a,#1
5115  0a91 2603          	jrne	L055
5116  0a93               L255:
5117  0a93 4f            	clr	a
5118  0a94 2010          	jra	L455
5119  0a96               L055:
5120  0a96 ae04da        	ldw	x,#1242
5121  0a99 89            	pushw	x
5122  0a9a ae0000        	ldw	x,#0
5123  0a9d 89            	pushw	x
5124  0a9e ae0000        	ldw	x,#L511
5125  0aa1 cd0000        	call	_assert_failed
5127  0aa4 5b04          	addw	sp,#4
5128  0aa6               L455:
5129                     ; 1245   if (TIM2_OCPolarity == TIM2_OCPolarity_Low)
5131  0aa6 7b01          	ld	a,(OFST+1,sp)
5132  0aa8 a101          	cp	a,#1
5133  0aaa 2606          	jrne	L1112
5134                     ; 1247     TIM2->CCER1 |= TIM_CCER1_CC1P ;
5136  0aac 7212525a      	bset	21082,#1
5138  0ab0 2004          	jra	L3112
5139  0ab2               L1112:
5140                     ; 1251     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1P) ;
5142  0ab2 7213525a      	bres	21082,#1
5143  0ab6               L3112:
5144                     ; 1253 }
5147  0ab6 84            	pop	a
5148  0ab7 81            	ret
5185                     ; 1263 void TIM2_OC2PolarityConfig(TIM2_OCPolarity_TypeDef TIM2_OCPolarity)
5185                     ; 1264 {
5186                     	switch	.text
5187  0ab8               _TIM2_OC2PolarityConfig:
5189  0ab8 88            	push	a
5190       00000000      OFST:	set	0
5193                     ; 1266   assert_param(IS_TIM2_OC_POLARITY(TIM2_OCPolarity));
5195  0ab9 4d            	tnz	a
5196  0aba 2704          	jreq	L265
5197  0abc a101          	cp	a,#1
5198  0abe 2603          	jrne	L065
5199  0ac0               L265:
5200  0ac0 4f            	clr	a
5201  0ac1 2010          	jra	L465
5202  0ac3               L065:
5203  0ac3 ae04f2        	ldw	x,#1266
5204  0ac6 89            	pushw	x
5205  0ac7 ae0000        	ldw	x,#0
5206  0aca 89            	pushw	x
5207  0acb ae0000        	ldw	x,#L511
5208  0ace cd0000        	call	_assert_failed
5210  0ad1 5b04          	addw	sp,#4
5211  0ad3               L465:
5212                     ; 1269   if (TIM2_OCPolarity == TIM2_OCPolarity_Low)
5214  0ad3 7b01          	ld	a,(OFST+1,sp)
5215  0ad5 a101          	cp	a,#1
5216  0ad7 2606          	jrne	L3312
5217                     ; 1271     TIM2->CCER1 |= TIM_CCER1_CC2P ;
5219  0ad9 721a525a      	bset	21082,#5
5221  0add 2004          	jra	L5312
5222  0adf               L3312:
5223                     ; 1275     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2P) ;
5225  0adf 721b525a      	bres	21082,#5
5226  0ae3               L5312:
5227                     ; 1277 }
5230  0ae3 84            	pop	a
5231  0ae4 81            	ret
5277                     ; 1289 void TIM2_CCxCmd(TIM2_Channel_TypeDef TIM2_Channel,
5277                     ; 1290                  FunctionalState NewState)
5277                     ; 1291 {
5278                     	switch	.text
5279  0ae5               _TIM2_CCxCmd:
5281  0ae5 89            	pushw	x
5282       00000000      OFST:	set	0
5285                     ; 1293   assert_param(IS_TIM2_CHANNEL(TIM2_Channel));
5287  0ae6 9e            	ld	a,xh
5288  0ae7 4d            	tnz	a
5289  0ae8 2705          	jreq	L275
5290  0aea 9e            	ld	a,xh
5291  0aeb a101          	cp	a,#1
5292  0aed 2603          	jrne	L075
5293  0aef               L275:
5294  0aef 4f            	clr	a
5295  0af0 2010          	jra	L475
5296  0af2               L075:
5297  0af2 ae050d        	ldw	x,#1293
5298  0af5 89            	pushw	x
5299  0af6 ae0000        	ldw	x,#0
5300  0af9 89            	pushw	x
5301  0afa ae0000        	ldw	x,#L511
5302  0afd cd0000        	call	_assert_failed
5304  0b00 5b04          	addw	sp,#4
5305  0b02               L475:
5306                     ; 1294   assert_param(IS_FUNCTIONAL_STATE(NewState));
5308  0b02 0d02          	tnz	(OFST+2,sp)
5309  0b04 2706          	jreq	L006
5310  0b06 7b02          	ld	a,(OFST+2,sp)
5311  0b08 a101          	cp	a,#1
5312  0b0a 2603          	jrne	L675
5313  0b0c               L006:
5314  0b0c 4f            	clr	a
5315  0b0d 2010          	jra	L206
5316  0b0f               L675:
5317  0b0f ae050e        	ldw	x,#1294
5318  0b12 89            	pushw	x
5319  0b13 ae0000        	ldw	x,#0
5320  0b16 89            	pushw	x
5321  0b17 ae0000        	ldw	x,#L511
5322  0b1a cd0000        	call	_assert_failed
5324  0b1d 5b04          	addw	sp,#4
5325  0b1f               L206:
5326                     ; 1296   if (TIM2_Channel == TIM2_Channel_1)
5328  0b1f 0d01          	tnz	(OFST+1,sp)
5329  0b21 2610          	jrne	L1612
5330                     ; 1299     if (NewState != DISABLE)
5332  0b23 0d02          	tnz	(OFST+2,sp)
5333  0b25 2706          	jreq	L3612
5334                     ; 1301       TIM2->CCER1 |= TIM_CCER1_CC1E ;
5336  0b27 7210525a      	bset	21082,#0
5338  0b2b 2014          	jra	L7612
5339  0b2d               L3612:
5340                     ; 1305       TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E) ;
5342  0b2d 7211525a      	bres	21082,#0
5343  0b31 200e          	jra	L7612
5344  0b33               L1612:
5345                     ; 1312     if (NewState != DISABLE)
5347  0b33 0d02          	tnz	(OFST+2,sp)
5348  0b35 2706          	jreq	L1712
5349                     ; 1314       TIM2->CCER1 |= TIM_CCER1_CC2E;
5351  0b37 7218525a      	bset	21082,#4
5353  0b3b 2004          	jra	L7612
5354  0b3d               L1712:
5355                     ; 1318       TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2E) ;
5357  0b3d 7219525a      	bres	21082,#4
5358  0b41               L7612:
5359                     ; 1322 }
5362  0b41 85            	popw	x
5363  0b42 81            	ret
5409                     ; 1342 void TIM2_SelectOCxM(TIM2_Channel_TypeDef TIM2_Channel,
5409                     ; 1343                      TIM2_OCMode_TypeDef TIM2_OCMode)
5409                     ; 1344 {
5410                     	switch	.text
5411  0b43               _TIM2_SelectOCxM:
5413  0b43 89            	pushw	x
5414       00000000      OFST:	set	0
5417                     ; 1346   assert_param(IS_TIM2_CHANNEL(TIM2_Channel));
5419  0b44 9e            	ld	a,xh
5420  0b45 4d            	tnz	a
5421  0b46 2705          	jreq	L016
5422  0b48 9e            	ld	a,xh
5423  0b49 a101          	cp	a,#1
5424  0b4b 2603          	jrne	L606
5425  0b4d               L016:
5426  0b4d 4f            	clr	a
5427  0b4e 2010          	jra	L216
5428  0b50               L606:
5429  0b50 ae0542        	ldw	x,#1346
5430  0b53 89            	pushw	x
5431  0b54 ae0000        	ldw	x,#0
5432  0b57 89            	pushw	x
5433  0b58 ae0000        	ldw	x,#L511
5434  0b5b cd0000        	call	_assert_failed
5436  0b5e 5b04          	addw	sp,#4
5437  0b60               L216:
5438                     ; 1347   assert_param(IS_TIM2_OCM(TIM2_OCMode));
5440  0b60 0d02          	tnz	(OFST+2,sp)
5441  0b62 272a          	jreq	L616
5442  0b64 7b02          	ld	a,(OFST+2,sp)
5443  0b66 a110          	cp	a,#16
5444  0b68 2724          	jreq	L616
5445  0b6a 7b02          	ld	a,(OFST+2,sp)
5446  0b6c a120          	cp	a,#32
5447  0b6e 271e          	jreq	L616
5448  0b70 7b02          	ld	a,(OFST+2,sp)
5449  0b72 a130          	cp	a,#48
5450  0b74 2718          	jreq	L616
5451  0b76 7b02          	ld	a,(OFST+2,sp)
5452  0b78 a160          	cp	a,#96
5453  0b7a 2712          	jreq	L616
5454  0b7c 7b02          	ld	a,(OFST+2,sp)
5455  0b7e a170          	cp	a,#112
5456  0b80 270c          	jreq	L616
5457  0b82 7b02          	ld	a,(OFST+2,sp)
5458  0b84 a150          	cp	a,#80
5459  0b86 2706          	jreq	L616
5460  0b88 7b02          	ld	a,(OFST+2,sp)
5461  0b8a a140          	cp	a,#64
5462  0b8c 2603          	jrne	L416
5463  0b8e               L616:
5464  0b8e 4f            	clr	a
5465  0b8f 2010          	jra	L026
5466  0b91               L416:
5467  0b91 ae0543        	ldw	x,#1347
5468  0b94 89            	pushw	x
5469  0b95 ae0000        	ldw	x,#0
5470  0b98 89            	pushw	x
5471  0b99 ae0000        	ldw	x,#L511
5472  0b9c cd0000        	call	_assert_failed
5474  0b9f 5b04          	addw	sp,#4
5475  0ba1               L026:
5476                     ; 1349   if (TIM2_Channel == TIM2_Channel_1)
5478  0ba1 0d01          	tnz	(OFST+1,sp)
5479  0ba3 2616          	jrne	L7122
5480                     ; 1352     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
5482  0ba5 7211525a      	bres	21082,#0
5483                     ; 1355     TIM2->CCMR1 &= (uint8_t)(~TIM_CCMR_OCM);
5485  0ba9 c65258        	ld	a,21080
5486  0bac a48f          	and	a,#143
5487  0bae c75258        	ld	21080,a
5488                     ; 1358     TIM2->CCMR1 |= (uint8_t)TIM2_OCMode;
5490  0bb1 c65258        	ld	a,21080
5491  0bb4 1a02          	or	a,(OFST+2,sp)
5492  0bb6 c75258        	ld	21080,a
5494  0bb9 2014          	jra	L1222
5495  0bbb               L7122:
5496                     ; 1363     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2E);
5498  0bbb 7219525a      	bres	21082,#4
5499                     ; 1366     TIM2->CCMR2 &= (uint8_t)(~TIM_CCMR_OCM);
5501  0bbf c65259        	ld	a,21081
5502  0bc2 a48f          	and	a,#143
5503  0bc4 c75259        	ld	21081,a
5504                     ; 1369     TIM2->CCMR2 |= (uint8_t)TIM2_OCMode;
5506  0bc7 c65259        	ld	a,21081
5507  0bca 1a02          	or	a,(OFST+2,sp)
5508  0bcc c75259        	ld	21081,a
5509  0bcf               L1222:
5510                     ; 1371 }
5513  0bcf 85            	popw	x
5514  0bd0 81            	ret
5548                     ; 1379 void TIM2_SetCounter(uint16_t TIM2_Counter)
5548                     ; 1380 {
5549                     	switch	.text
5550  0bd1               _TIM2_SetCounter:
5554                     ; 1383   TIM2->CNTRH = (uint8_t)(TIM2_Counter >> 8);
5556  0bd1 9e            	ld	a,xh
5557  0bd2 c7525b        	ld	21083,a
5558                     ; 1384   TIM2->CNTRL = (uint8_t)(TIM2_Counter);
5560  0bd5 9f            	ld	a,xl
5561  0bd6 c7525c        	ld	21084,a
5562                     ; 1385 }
5565  0bd9 81            	ret
5599                     ; 1393 void TIM2_SetAutoreload(uint16_t TIM2_Autoreload)
5599                     ; 1394 {
5600                     	switch	.text
5601  0bda               _TIM2_SetAutoreload:
5605                     ; 1396   TIM2->ARRH = (uint8_t)(TIM2_Autoreload >> 8);
5607  0bda 9e            	ld	a,xh
5608  0bdb c7525e        	ld	21086,a
5609                     ; 1397   TIM2->ARRL = (uint8_t)(TIM2_Autoreload);
5611  0bde 9f            	ld	a,xl
5612  0bdf c7525f        	ld	21087,a
5613                     ; 1398 }
5616  0be2 81            	ret
5650                     ; 1406 void TIM2_SetCompare1(uint16_t TIM2_Compare)
5650                     ; 1407 {
5651                     	switch	.text
5652  0be3               _TIM2_SetCompare1:
5656                     ; 1409   TIM2->CCR1H = (uint8_t)(TIM2_Compare >> 8);
5658  0be3 9e            	ld	a,xh
5659  0be4 c75260        	ld	21088,a
5660                     ; 1410   TIM2->CCR1L = (uint8_t)(TIM2_Compare);
5662  0be7 9f            	ld	a,xl
5663  0be8 c75261        	ld	21089,a
5664                     ; 1411 }
5667  0beb 81            	ret
5701                     ; 1419 void TIM2_SetCompare2(uint16_t TIM2_Compare)
5701                     ; 1420 {
5702                     	switch	.text
5703  0bec               _TIM2_SetCompare2:
5707                     ; 1422   TIM2->CCR2H = (uint8_t)(TIM2_Compare >> 8);
5709  0bec 9e            	ld	a,xh
5710  0bed c75262        	ld	21090,a
5711                     ; 1423   TIM2->CCR2L = (uint8_t)(TIM2_Compare);
5713  0bf0 9f            	ld	a,xl
5714  0bf1 c75263        	ld	21091,a
5715                     ; 1424 }
5718  0bf4 81            	ret
5764                     ; 1436 void TIM2_SetIC1Prescaler(TIM2_ICPSC_TypeDef TIM2_IC1Prescaler)
5764                     ; 1437 {
5765                     	switch	.text
5766  0bf5               _TIM2_SetIC1Prescaler:
5768  0bf5 88            	push	a
5769  0bf6 88            	push	a
5770       00000001      OFST:	set	1
5773                     ; 1438   uint8_t tmpccmr1 = 0;
5775                     ; 1441   assert_param(IS_TIM2_IC_PRESCALER(TIM2_IC1Prescaler));
5777  0bf7 4d            	tnz	a
5778  0bf8 270c          	jreq	L636
5779  0bfa a104          	cp	a,#4
5780  0bfc 2708          	jreq	L636
5781  0bfe a108          	cp	a,#8
5782  0c00 2704          	jreq	L636
5783  0c02 a10c          	cp	a,#12
5784  0c04 2603          	jrne	L436
5785  0c06               L636:
5786  0c06 4f            	clr	a
5787  0c07 2010          	jra	L046
5788  0c09               L436:
5789  0c09 ae05a1        	ldw	x,#1441
5790  0c0c 89            	pushw	x
5791  0c0d ae0000        	ldw	x,#0
5792  0c10 89            	pushw	x
5793  0c11 ae0000        	ldw	x,#L511
5794  0c14 cd0000        	call	_assert_failed
5796  0c17 5b04          	addw	sp,#4
5797  0c19               L046:
5798                     ; 1443   tmpccmr1 = TIM2->CCMR1;
5800  0c19 c65258        	ld	a,21080
5801  0c1c 6b01          	ld	(OFST+0,sp),a
5803                     ; 1446   tmpccmr1 &= (uint8_t)(~TIM_CCMR_ICxPSC);
5805  0c1e 7b01          	ld	a,(OFST+0,sp)
5806  0c20 a4f3          	and	a,#243
5807  0c22 6b01          	ld	(OFST+0,sp),a
5809                     ; 1449   tmpccmr1 |= (uint8_t)TIM2_IC1Prescaler;
5811  0c24 7b01          	ld	a,(OFST+0,sp)
5812  0c26 1a02          	or	a,(OFST+1,sp)
5813  0c28 6b01          	ld	(OFST+0,sp),a
5815                     ; 1451   TIM2->CCMR1 = tmpccmr1;
5817  0c2a 7b01          	ld	a,(OFST+0,sp)
5818  0c2c c75258        	ld	21080,a
5819                     ; 1452 }
5822  0c2f 85            	popw	x
5823  0c30 81            	ret
5869                     ; 1464 void TIM2_SetIC2Prescaler(TIM2_ICPSC_TypeDef TIM2_IC2Prescaler)
5869                     ; 1465 {
5870                     	switch	.text
5871  0c31               _TIM2_SetIC2Prescaler:
5873  0c31 88            	push	a
5874  0c32 88            	push	a
5875       00000001      OFST:	set	1
5878                     ; 1466   uint8_t tmpccmr2 = 0;
5880                     ; 1469   assert_param(IS_TIM2_IC_PRESCALER(TIM2_IC2Prescaler));
5882  0c33 4d            	tnz	a
5883  0c34 270c          	jreq	L646
5884  0c36 a104          	cp	a,#4
5885  0c38 2708          	jreq	L646
5886  0c3a a108          	cp	a,#8
5887  0c3c 2704          	jreq	L646
5888  0c3e a10c          	cp	a,#12
5889  0c40 2603          	jrne	L446
5890  0c42               L646:
5891  0c42 4f            	clr	a
5892  0c43 2010          	jra	L056
5893  0c45               L446:
5894  0c45 ae05bd        	ldw	x,#1469
5895  0c48 89            	pushw	x
5896  0c49 ae0000        	ldw	x,#0
5897  0c4c 89            	pushw	x
5898  0c4d ae0000        	ldw	x,#L511
5899  0c50 cd0000        	call	_assert_failed
5901  0c53 5b04          	addw	sp,#4
5902  0c55               L056:
5903                     ; 1471   tmpccmr2 = TIM2->CCMR2;
5905  0c55 c65259        	ld	a,21081
5906  0c58 6b01          	ld	(OFST+0,sp),a
5908                     ; 1474   tmpccmr2 &= (uint8_t)(~TIM_CCMR_ICxPSC);
5910  0c5a 7b01          	ld	a,(OFST+0,sp)
5911  0c5c a4f3          	and	a,#243
5912  0c5e 6b01          	ld	(OFST+0,sp),a
5914                     ; 1477   tmpccmr2 |= (uint8_t)TIM2_IC2Prescaler;
5916  0c60 7b01          	ld	a,(OFST+0,sp)
5917  0c62 1a02          	or	a,(OFST+1,sp)
5918  0c64 6b01          	ld	(OFST+0,sp),a
5920                     ; 1479   TIM2->CCMR2 = tmpccmr2;
5922  0c66 7b01          	ld	a,(OFST+0,sp)
5923  0c68 c75259        	ld	21081,a
5924                     ; 1480 }
5927  0c6b 85            	popw	x
5928  0c6c 81            	ret
5980                     ; 1487 uint16_t TIM2_GetCapture1(void)
5980                     ; 1488 {
5981                     	switch	.text
5982  0c6d               _TIM2_GetCapture1:
5984  0c6d 5204          	subw	sp,#4
5985       00000004      OFST:	set	4
5988                     ; 1489   uint16_t tmpccr1 = 0;
5990                     ; 1492   tmpccr1h = TIM2->CCR1H;
5992  0c6f c65260        	ld	a,21088
5993  0c72 6b02          	ld	(OFST-2,sp),a
5995                     ; 1493   tmpccr1l = TIM2->CCR1L;
5997  0c74 c65261        	ld	a,21089
5998  0c77 6b01          	ld	(OFST-3,sp),a
6000                     ; 1495   tmpccr1 = (uint16_t)(tmpccr1l);
6002  0c79 7b01          	ld	a,(OFST-3,sp)
6003  0c7b 5f            	clrw	x
6004  0c7c 97            	ld	xl,a
6005  0c7d 1f03          	ldw	(OFST-1,sp),x
6007                     ; 1496   tmpccr1 |= (uint16_t)((uint16_t)tmpccr1h << 8);
6009  0c7f 7b02          	ld	a,(OFST-2,sp)
6010  0c81 5f            	clrw	x
6011  0c82 97            	ld	xl,a
6012  0c83 4f            	clr	a
6013  0c84 02            	rlwa	x,a
6014  0c85 01            	rrwa	x,a
6015  0c86 1a04          	or	a,(OFST+0,sp)
6016  0c88 01            	rrwa	x,a
6017  0c89 1a03          	or	a,(OFST-1,sp)
6018  0c8b 01            	rrwa	x,a
6019  0c8c 1f03          	ldw	(OFST-1,sp),x
6021                     ; 1498   return ((uint16_t)tmpccr1);
6023  0c8e 1e03          	ldw	x,(OFST-1,sp)
6026  0c90 5b04          	addw	sp,#4
6027  0c92 81            	ret
6079                     ; 1506 uint16_t TIM2_GetCapture2(void)
6079                     ; 1507 {
6080                     	switch	.text
6081  0c93               _TIM2_GetCapture2:
6083  0c93 5204          	subw	sp,#4
6084       00000004      OFST:	set	4
6087                     ; 1508   uint16_t tmpccr2 = 0;
6089                     ; 1511   tmpccr2h = TIM2->CCR2H;
6091  0c95 c65262        	ld	a,21090
6092  0c98 6b02          	ld	(OFST-2,sp),a
6094                     ; 1512   tmpccr2l = TIM2->CCR2L;
6096  0c9a c65263        	ld	a,21091
6097  0c9d 6b01          	ld	(OFST-3,sp),a
6099                     ; 1514   tmpccr2 = (uint16_t)(tmpccr2l);
6101  0c9f 7b01          	ld	a,(OFST-3,sp)
6102  0ca1 5f            	clrw	x
6103  0ca2 97            	ld	xl,a
6104  0ca3 1f03          	ldw	(OFST-1,sp),x
6106                     ; 1515   tmpccr2 |= (uint16_t)((uint16_t)tmpccr2h << 8);
6108  0ca5 7b02          	ld	a,(OFST-2,sp)
6109  0ca7 5f            	clrw	x
6110  0ca8 97            	ld	xl,a
6111  0ca9 4f            	clr	a
6112  0caa 02            	rlwa	x,a
6113  0cab 01            	rrwa	x,a
6114  0cac 1a04          	or	a,(OFST+0,sp)
6115  0cae 01            	rrwa	x,a
6116  0caf 1a03          	or	a,(OFST-1,sp)
6117  0cb1 01            	rrwa	x,a
6118  0cb2 1f03          	ldw	(OFST-1,sp),x
6120                     ; 1517   return ((uint16_t)tmpccr2);
6122  0cb4 1e03          	ldw	x,(OFST-1,sp)
6125  0cb6 5b04          	addw	sp,#4
6126  0cb8 81            	ret
6178                     ; 1525 uint16_t TIM2_GetCounter(void)
6178                     ; 1526 {
6179                     	switch	.text
6180  0cb9               _TIM2_GetCounter:
6182  0cb9 5204          	subw	sp,#4
6183       00000004      OFST:	set	4
6186                     ; 1527   uint16_t tmpcnt = 0;
6188                     ; 1530   tmpcntrh = TIM2->CNTRH;
6190  0cbb c6525b        	ld	a,21083
6191  0cbe 6b02          	ld	(OFST-2,sp),a
6193                     ; 1531   tmpcntrl = TIM2->CNTRL;
6195  0cc0 c6525c        	ld	a,21084
6196  0cc3 6b01          	ld	(OFST-3,sp),a
6198                     ; 1533   tmpcnt = (uint16_t)(tmpcntrl);
6200  0cc5 7b01          	ld	a,(OFST-3,sp)
6201  0cc7 5f            	clrw	x
6202  0cc8 97            	ld	xl,a
6203  0cc9 1f03          	ldw	(OFST-1,sp),x
6205                     ; 1534   tmpcnt |= (uint16_t)((uint16_t)tmpcntrh << 8);
6207  0ccb 7b02          	ld	a,(OFST-2,sp)
6208  0ccd 5f            	clrw	x
6209  0cce 97            	ld	xl,a
6210  0ccf 4f            	clr	a
6211  0cd0 02            	rlwa	x,a
6212  0cd1 01            	rrwa	x,a
6213  0cd2 1a04          	or	a,(OFST+0,sp)
6214  0cd4 01            	rrwa	x,a
6215  0cd5 1a03          	or	a,(OFST-1,sp)
6216  0cd7 01            	rrwa	x,a
6217  0cd8 1f03          	ldw	(OFST-1,sp),x
6219                     ; 1536   return ((uint16_t)tmpcnt);
6221  0cda 1e03          	ldw	x,(OFST-1,sp)
6224  0cdc 5b04          	addw	sp,#4
6225  0cde 81            	ret
6249                     ; 1544 TIM2_Prescaler_TypeDef TIM2_GetPrescaler(void)
6249                     ; 1545 {
6250                     	switch	.text
6251  0cdf               _TIM2_GetPrescaler:
6255                     ; 1547   return ((TIM2_Prescaler_TypeDef)TIM2->PSCR);
6257  0cdf c6525d        	ld	a,21085
6260  0ce2 81            	ret
6400                     ; 1563 FlagStatus TIM2_GetFlagStatus(TIM2_FLAG_TypeDef TIM2_FLAG)
6400                     ; 1564 {
6401                     	switch	.text
6402  0ce3               _TIM2_GetFlagStatus:
6404  0ce3 89            	pushw	x
6405  0ce4 89            	pushw	x
6406       00000002      OFST:	set	2
6409                     ; 1565   FlagStatus bitstatus = RESET;
6411                     ; 1569   assert_param(IS_TIM2_GET_FLAG(TIM2_FLAG));
6413  0ce5 a30001        	cpw	x,#1
6414  0ce8 271e          	jreq	L666
6415  0cea a30002        	cpw	x,#2
6416  0ced 2719          	jreq	L666
6417  0cef a30004        	cpw	x,#4
6418  0cf2 2714          	jreq	L666
6419  0cf4 a30040        	cpw	x,#64
6420  0cf7 270f          	jreq	L666
6421  0cf9 a30080        	cpw	x,#128
6422  0cfc 270a          	jreq	L666
6423  0cfe a30200        	cpw	x,#512
6424  0d01 2705          	jreq	L666
6425  0d03 a30400        	cpw	x,#1024
6426  0d06 2603          	jrne	L466
6427  0d08               L666:
6428  0d08 4f            	clr	a
6429  0d09 2010          	jra	L076
6430  0d0b               L466:
6431  0d0b ae0621        	ldw	x,#1569
6432  0d0e 89            	pushw	x
6433  0d0f ae0000        	ldw	x,#0
6434  0d12 89            	pushw	x
6435  0d13 ae0000        	ldw	x,#L511
6436  0d16 cd0000        	call	_assert_failed
6438  0d19 5b04          	addw	sp,#4
6439  0d1b               L076:
6440                     ; 1571   tim2_flag_l = (uint8_t)(TIM2->SR1 & (uint8_t)(TIM2_FLAG));
6442  0d1b c65255        	ld	a,21077
6443  0d1e 1404          	and	a,(OFST+2,sp)
6444  0d20 6b01          	ld	(OFST-1,sp),a
6446                     ; 1572   tim2_flag_h = (uint8_t)(TIM2->SR2 & (uint8_t)((uint16_t)TIM2_FLAG >> 8));
6448  0d22 c65256        	ld	a,21078
6449  0d25 1403          	and	a,(OFST+1,sp)
6450  0d27 6b02          	ld	(OFST+0,sp),a
6452                     ; 1574   if ((uint8_t)(tim2_flag_l | tim2_flag_h) != 0)
6454  0d29 7b01          	ld	a,(OFST-1,sp)
6455  0d2b 1a02          	or	a,(OFST+0,sp)
6456  0d2d 2706          	jreq	L5552
6457                     ; 1576     bitstatus = SET;
6459  0d2f a601          	ld	a,#1
6460  0d31 6b02          	ld	(OFST+0,sp),a
6463  0d33 2002          	jra	L7552
6464  0d35               L5552:
6465                     ; 1580     bitstatus = RESET;
6467  0d35 0f02          	clr	(OFST+0,sp)
6469  0d37               L7552:
6470                     ; 1582   return ((FlagStatus)bitstatus);
6472  0d37 7b02          	ld	a,(OFST+0,sp)
6475  0d39 5b04          	addw	sp,#4
6476  0d3b 81            	ret
6512                     ; 1596 void TIM2_ClearFlag(TIM2_FLAG_TypeDef TIM2_FLAG)
6512                     ; 1597 {
6513                     	switch	.text
6514  0d3c               _TIM2_ClearFlag:
6516  0d3c 89            	pushw	x
6517       00000000      OFST:	set	0
6520                     ; 1599   assert_param(IS_TIM2_CLEAR_FLAG((uint16_t)TIM2_FLAG));
6522  0d3d 01            	rrwa	x,a
6523  0d3e a438          	and	a,#56
6524  0d40 01            	rrwa	x,a
6525  0d41 a4f9          	and	a,#249
6526  0d43 01            	rrwa	x,a
6527  0d44 a30000        	cpw	x,#0
6528  0d47 2607          	jrne	L476
6529  0d49 1e01          	ldw	x,(OFST+1,sp)
6530  0d4b 2703          	jreq	L476
6531  0d4d 4f            	clr	a
6532  0d4e 2010          	jra	L676
6533  0d50               L476:
6534  0d50 ae063f        	ldw	x,#1599
6535  0d53 89            	pushw	x
6536  0d54 ae0000        	ldw	x,#0
6537  0d57 89            	pushw	x
6538  0d58 ae0000        	ldw	x,#L511
6539  0d5b cd0000        	call	_assert_failed
6541  0d5e 5b04          	addw	sp,#4
6542  0d60               L676:
6543                     ; 1601   TIM2->SR1 = (uint8_t)(~(uint8_t)(TIM2_FLAG));
6545  0d60 7b02          	ld	a,(OFST+2,sp)
6546  0d62 43            	cpl	a
6547  0d63 c75255        	ld	21077,a
6548                     ; 1602   TIM2->SR2 = (uint8_t)(~(uint8_t)((uint16_t)TIM2_FLAG >> 8));
6550  0d66 7b01          	ld	a,(OFST+1,sp)
6551  0d68 43            	cpl	a
6552  0d69 c75256        	ld	21078,a
6553                     ; 1603 }
6556  0d6c 85            	popw	x
6557  0d6d 81            	ret
6622                     ; 1616 ITStatus TIM2_GetITStatus(TIM2_IT_TypeDef TIM2_IT)
6622                     ; 1617 {
6623                     	switch	.text
6624  0d6e               _TIM2_GetITStatus:
6626  0d6e 88            	push	a
6627  0d6f 5203          	subw	sp,#3
6628       00000003      OFST:	set	3
6631                     ; 1618   __IO ITStatus bitstatus = RESET;
6633  0d71 0f03          	clr	(OFST+0,sp)
6635                     ; 1620   __IO uint8_t TIM2_itStatus = 0x0, TIM2_itEnable = 0x0;
6637  0d73 0f01          	clr	(OFST-2,sp)
6641  0d75 0f02          	clr	(OFST-1,sp)
6643                     ; 1623   assert_param(IS_TIM2_GET_IT(TIM2_IT));
6645  0d77 a101          	cp	a,#1
6646  0d79 2710          	jreq	L407
6647  0d7b a102          	cp	a,#2
6648  0d7d 270c          	jreq	L407
6649  0d7f a104          	cp	a,#4
6650  0d81 2708          	jreq	L407
6651  0d83 a140          	cp	a,#64
6652  0d85 2704          	jreq	L407
6653  0d87 a180          	cp	a,#128
6654  0d89 2603          	jrne	L207
6655  0d8b               L407:
6656  0d8b 4f            	clr	a
6657  0d8c 2010          	jra	L607
6658  0d8e               L207:
6659  0d8e ae0657        	ldw	x,#1623
6660  0d91 89            	pushw	x
6661  0d92 ae0000        	ldw	x,#0
6662  0d95 89            	pushw	x
6663  0d96 ae0000        	ldw	x,#L511
6664  0d99 cd0000        	call	_assert_failed
6666  0d9c 5b04          	addw	sp,#4
6667  0d9e               L607:
6668                     ; 1625   TIM2_itStatus = (uint8_t)(TIM2->SR1 & (uint8_t)TIM2_IT);
6670  0d9e c65255        	ld	a,21077
6671  0da1 1404          	and	a,(OFST+1,sp)
6672  0da3 6b01          	ld	(OFST-2,sp),a
6674                     ; 1627   TIM2_itEnable = (uint8_t)(TIM2->IER & (uint8_t)TIM2_IT);
6676  0da5 c65254        	ld	a,21076
6677  0da8 1404          	and	a,(OFST+1,sp)
6678  0daa 6b02          	ld	(OFST-1,sp),a
6680                     ; 1629   if ((TIM2_itStatus != (uint8_t)RESET) && (TIM2_itEnable != (uint8_t)RESET))
6682  0dac 0d01          	tnz	(OFST-2,sp)
6683  0dae 270a          	jreq	L1362
6685  0db0 0d02          	tnz	(OFST-1,sp)
6686  0db2 2706          	jreq	L1362
6687                     ; 1631     bitstatus = (ITStatus)SET;
6689  0db4 a601          	ld	a,#1
6690  0db6 6b03          	ld	(OFST+0,sp),a
6693  0db8 2002          	jra	L3362
6694  0dba               L1362:
6695                     ; 1635     bitstatus = (ITStatus)RESET;
6697  0dba 0f03          	clr	(OFST+0,sp)
6699  0dbc               L3362:
6700                     ; 1637   return ((ITStatus)bitstatus);
6702  0dbc 7b03          	ld	a,(OFST+0,sp)
6705  0dbe 5b04          	addw	sp,#4
6706  0dc0 81            	ret
6743                     ; 1651 void TIM2_ClearITPendingBit(TIM2_IT_TypeDef TIM2_IT)
6743                     ; 1652 {
6744                     	switch	.text
6745  0dc1               _TIM2_ClearITPendingBit:
6747  0dc1 88            	push	a
6748       00000000      OFST:	set	0
6751                     ; 1654   assert_param(IS_TIM2_IT(TIM2_IT));
6753  0dc2 4d            	tnz	a
6754  0dc3 2703          	jreq	L217
6755  0dc5 4f            	clr	a
6756  0dc6 2010          	jra	L417
6757  0dc8               L217:
6758  0dc8 ae0676        	ldw	x,#1654
6759  0dcb 89            	pushw	x
6760  0dcc ae0000        	ldw	x,#0
6761  0dcf 89            	pushw	x
6762  0dd0 ae0000        	ldw	x,#L511
6763  0dd3 cd0000        	call	_assert_failed
6765  0dd6 5b04          	addw	sp,#4
6766  0dd8               L417:
6767                     ; 1657   TIM2->SR1 = (uint8_t)(~(uint8_t)TIM2_IT);
6769  0dd8 7b01          	ld	a,(OFST+1,sp)
6770  0dda 43            	cpl	a
6771  0ddb c75255        	ld	21077,a
6772                     ; 1658 }
6775  0dde 84            	pop	a
6776  0ddf 81            	ret
6849                     ; 1675 static void TI1_Config(TIM2_ICPolarity_TypeDef TIM2_ICPolarity, \
6849                     ; 1676                        TIM2_ICSelection_TypeDef TIM2_ICSelection, \
6849                     ; 1677                        uint8_t TIM2_ICFilter)
6849                     ; 1678 {
6850                     	switch	.text
6851  0de0               L3_TI1_Config:
6853  0de0 89            	pushw	x
6854  0de1 89            	pushw	x
6855       00000002      OFST:	set	2
6858                     ; 1679   uint8_t tmpccmr1 = 0;
6860                     ; 1680   uint8_t tmpicpolarity = (uint8_t)TIM2_ICPolarity;
6862  0de2 9e            	ld	a,xh
6863  0de3 6b01          	ld	(OFST-1,sp),a
6865                     ; 1681   tmpccmr1 = TIM2->CCMR1;
6867  0de5 c65258        	ld	a,21080
6868  0de8 6b02          	ld	(OFST+0,sp),a
6870                     ; 1684   assert_param(IS_TIM2_IC_POLARITY(TIM2_ICPolarity));
6872  0dea 9e            	ld	a,xh
6873  0deb 4d            	tnz	a
6874  0dec 2705          	jreq	L227
6875  0dee 9e            	ld	a,xh
6876  0def a101          	cp	a,#1
6877  0df1 2603          	jrne	L027
6878  0df3               L227:
6879  0df3 4f            	clr	a
6880  0df4 2010          	jra	L427
6881  0df6               L027:
6882  0df6 ae0694        	ldw	x,#1684
6883  0df9 89            	pushw	x
6884  0dfa ae0000        	ldw	x,#0
6885  0dfd 89            	pushw	x
6886  0dfe ae0000        	ldw	x,#L511
6887  0e01 cd0000        	call	_assert_failed
6889  0e04 5b04          	addw	sp,#4
6890  0e06               L427:
6891                     ; 1685   assert_param(IS_TIM2_IC_SELECTION(TIM2_ICSelection));
6893  0e06 7b04          	ld	a,(OFST+2,sp)
6894  0e08 a101          	cp	a,#1
6895  0e0a 270c          	jreq	L037
6896  0e0c 7b04          	ld	a,(OFST+2,sp)
6897  0e0e a102          	cp	a,#2
6898  0e10 2706          	jreq	L037
6899  0e12 7b04          	ld	a,(OFST+2,sp)
6900  0e14 a103          	cp	a,#3
6901  0e16 2603          	jrne	L627
6902  0e18               L037:
6903  0e18 4f            	clr	a
6904  0e19 2010          	jra	L237
6905  0e1b               L627:
6906  0e1b ae0695        	ldw	x,#1685
6907  0e1e 89            	pushw	x
6908  0e1f ae0000        	ldw	x,#0
6909  0e22 89            	pushw	x
6910  0e23 ae0000        	ldw	x,#L511
6911  0e26 cd0000        	call	_assert_failed
6913  0e29 5b04          	addw	sp,#4
6914  0e2b               L237:
6915                     ; 1686   assert_param(IS_TIM2_IC_FILTER(TIM2_ICFilter));
6917  0e2b 7b07          	ld	a,(OFST+5,sp)
6918  0e2d a110          	cp	a,#16
6919  0e2f 2403          	jruge	L437
6920  0e31 4f            	clr	a
6921  0e32 2010          	jra	L637
6922  0e34               L437:
6923  0e34 ae0696        	ldw	x,#1686
6924  0e37 89            	pushw	x
6925  0e38 ae0000        	ldw	x,#0
6926  0e3b 89            	pushw	x
6927  0e3c ae0000        	ldw	x,#L511
6928  0e3f cd0000        	call	_assert_failed
6930  0e42 5b04          	addw	sp,#4
6931  0e44               L637:
6932                     ; 1689   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
6934  0e44 7211525a      	bres	21082,#0
6935                     ; 1692   tmpccmr1 &= (uint8_t)(~TIM_CCMR_CCxS) & (uint8_t)(~TIM_CCMR_ICxF);
6937  0e48 7b02          	ld	a,(OFST+0,sp)
6938  0e4a a40c          	and	a,#12
6939  0e4c 6b02          	ld	(OFST+0,sp),a
6941                     ; 1693   tmpccmr1 |= (uint8_t)(((uint8_t)(TIM2_ICSelection)) | ((uint8_t)(TIM2_ICFilter << 4)));
6943  0e4e 7b07          	ld	a,(OFST+5,sp)
6944  0e50 97            	ld	xl,a
6945  0e51 a610          	ld	a,#16
6946  0e53 42            	mul	x,a
6947  0e54 9f            	ld	a,xl
6948  0e55 1a04          	or	a,(OFST+2,sp)
6949  0e57 1a02          	or	a,(OFST+0,sp)
6950  0e59 6b02          	ld	(OFST+0,sp),a
6952                     ; 1695   TIM2->CCMR1 = tmpccmr1;
6954  0e5b 7b02          	ld	a,(OFST+0,sp)
6955  0e5d c75258        	ld	21080,a
6956                     ; 1698   if (tmpicpolarity == (uint8_t)(TIM2_ICPolarity_Falling))
6958  0e60 7b01          	ld	a,(OFST-1,sp)
6959  0e62 a101          	cp	a,#1
6960  0e64 2606          	jrne	L1172
6961                     ; 1700     TIM2->CCER1 |= TIM_CCER1_CC1P;
6963  0e66 7212525a      	bset	21082,#1
6965  0e6a 2004          	jra	L3172
6966  0e6c               L1172:
6967                     ; 1704     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1P);
6969  0e6c 7213525a      	bres	21082,#1
6970  0e70               L3172:
6971                     ; 1708   TIM2->CCER1 |=  TIM_CCER1_CC1E;
6973  0e70 7210525a      	bset	21082,#0
6974                     ; 1709 }
6977  0e74 5b04          	addw	sp,#4
6978  0e76 81            	ret
7051                     ; 1726 static void TI2_Config(TIM2_ICPolarity_TypeDef TIM2_ICPolarity,
7051                     ; 1727                        TIM2_ICSelection_TypeDef TIM2_ICSelection,
7051                     ; 1728                        uint8_t TIM2_ICFilter)
7051                     ; 1729 {
7052                     	switch	.text
7053  0e77               L5_TI2_Config:
7055  0e77 89            	pushw	x
7056  0e78 89            	pushw	x
7057       00000002      OFST:	set	2
7060                     ; 1730   uint8_t tmpccmr2 = 0;
7062                     ; 1731   uint8_t tmpicpolarity = (uint8_t)TIM2_ICPolarity;
7064  0e79 9e            	ld	a,xh
7065  0e7a 6b01          	ld	(OFST-1,sp),a
7067                     ; 1734   assert_param(IS_TIM2_IC_POLARITY(TIM2_ICPolarity));
7069  0e7c 9e            	ld	a,xh
7070  0e7d 4d            	tnz	a
7071  0e7e 2705          	jreq	L447
7072  0e80 9e            	ld	a,xh
7073  0e81 a101          	cp	a,#1
7074  0e83 2603          	jrne	L247
7075  0e85               L447:
7076  0e85 4f            	clr	a
7077  0e86 2010          	jra	L647
7078  0e88               L247:
7079  0e88 ae06c6        	ldw	x,#1734
7080  0e8b 89            	pushw	x
7081  0e8c ae0000        	ldw	x,#0
7082  0e8f 89            	pushw	x
7083  0e90 ae0000        	ldw	x,#L511
7084  0e93 cd0000        	call	_assert_failed
7086  0e96 5b04          	addw	sp,#4
7087  0e98               L647:
7088                     ; 1735   assert_param(IS_TIM2_IC_SELECTION(TIM2_ICSelection));
7090  0e98 7b04          	ld	a,(OFST+2,sp)
7091  0e9a a101          	cp	a,#1
7092  0e9c 270c          	jreq	L257
7093  0e9e 7b04          	ld	a,(OFST+2,sp)
7094  0ea0 a102          	cp	a,#2
7095  0ea2 2706          	jreq	L257
7096  0ea4 7b04          	ld	a,(OFST+2,sp)
7097  0ea6 a103          	cp	a,#3
7098  0ea8 2603          	jrne	L057
7099  0eaa               L257:
7100  0eaa 4f            	clr	a
7101  0eab 2010          	jra	L457
7102  0ead               L057:
7103  0ead ae06c7        	ldw	x,#1735
7104  0eb0 89            	pushw	x
7105  0eb1 ae0000        	ldw	x,#0
7106  0eb4 89            	pushw	x
7107  0eb5 ae0000        	ldw	x,#L511
7108  0eb8 cd0000        	call	_assert_failed
7110  0ebb 5b04          	addw	sp,#4
7111  0ebd               L457:
7112                     ; 1736   assert_param(IS_TIM2_IC_FILTER(TIM2_ICFilter));
7114  0ebd 7b07          	ld	a,(OFST+5,sp)
7115  0ebf a110          	cp	a,#16
7116  0ec1 2403          	jruge	L657
7117  0ec3 4f            	clr	a
7118  0ec4 2010          	jra	L067
7119  0ec6               L657:
7120  0ec6 ae06c8        	ldw	x,#1736
7121  0ec9 89            	pushw	x
7122  0eca ae0000        	ldw	x,#0
7123  0ecd 89            	pushw	x
7124  0ece ae0000        	ldw	x,#L511
7125  0ed1 cd0000        	call	_assert_failed
7127  0ed4 5b04          	addw	sp,#4
7128  0ed6               L067:
7129                     ; 1738   tmpccmr2 = TIM2->CCMR2;
7131  0ed6 c65259        	ld	a,21081
7132  0ed9 6b02          	ld	(OFST+0,sp),a
7134                     ; 1741   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2E);
7136  0edb 7219525a      	bres	21082,#4
7137                     ; 1744   tmpccmr2 &= (uint8_t)(~TIM_CCMR_CCxS) & (uint8_t)(~TIM_CCMR_ICxF);
7139  0edf 7b02          	ld	a,(OFST+0,sp)
7140  0ee1 a40c          	and	a,#12
7141  0ee3 6b02          	ld	(OFST+0,sp),a
7143                     ; 1745   tmpccmr2 |= (uint8_t)(((uint8_t)(TIM2_ICSelection)) | ((uint8_t)(TIM2_ICFilter << 4)));
7145  0ee5 7b07          	ld	a,(OFST+5,sp)
7146  0ee7 97            	ld	xl,a
7147  0ee8 a610          	ld	a,#16
7148  0eea 42            	mul	x,a
7149  0eeb 9f            	ld	a,xl
7150  0eec 1a04          	or	a,(OFST+2,sp)
7151  0eee 1a02          	or	a,(OFST+0,sp)
7152  0ef0 6b02          	ld	(OFST+0,sp),a
7154                     ; 1747   TIM2->CCMR2 = tmpccmr2;
7156  0ef2 7b02          	ld	a,(OFST+0,sp)
7157  0ef4 c75259        	ld	21081,a
7158                     ; 1750   if (tmpicpolarity == (uint8_t)TIM2_ICPolarity_Falling)
7160  0ef7 7b01          	ld	a,(OFST-1,sp)
7161  0ef9 a101          	cp	a,#1
7162  0efb 2606          	jrne	L3572
7163                     ; 1752     TIM2->CCER1 |= TIM_CCER1_CC2P ;
7165  0efd 721a525a      	bset	21082,#5
7167  0f01 2004          	jra	L5572
7168  0f03               L3572:
7169                     ; 1756     TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC2P) ;
7171  0f03 721b525a      	bres	21082,#5
7172  0f07               L5572:
7173                     ; 1760   TIM2->CCER1 |=  TIM_CCER1_CC2E;
7175  0f07 7218525a      	bset	21082,#4
7176                     ; 1761 }
7179  0f0b 5b04          	addw	sp,#4
7180  0f0d 81            	ret
7248                     ; 1771 uint32_t TIM2_ComputeLsiClockFreq(uint32_t TIM2_TimerClockFreq)
7248                     ; 1772 {
7249                     	switch	.text
7250  0f0e               _TIM2_ComputeLsiClockFreq:
7252  0f0e 520c          	subw	sp,#12
7253       0000000c      OFST:	set	12
7256                     ; 1777   TIM2_ICInit(TIM2_Channel_1, TIM2_ICPolarity_Rising, TIM2_ICSelection_DirectTI, TIM2_ICPSC_Div8, 0x0);
7258  0f10 4b00          	push	#0
7259  0f12 4b0c          	push	#12
7260  0f14 4b01          	push	#1
7261  0f16 5f            	clrw	x
7262  0f17 cd035c        	call	_TIM2_ICInit
7264  0f1a 5b03          	addw	sp,#3
7265                     ; 1780   TIM2_ITConfig(TIM2_IT_CC1, ENABLE);
7267  0f1c ae0201        	ldw	x,#513
7268  0f1f cd0486        	call	_TIM2_ITConfig
7270                     ; 1783   TIM2_Cmd(ENABLE);
7272  0f22 a601          	ld	a,#1
7273  0f24 cd0430        	call	_TIM2_Cmd
7275                     ; 1785   TIM2->SR1 = 0x00;
7277  0f27 725f5255      	clr	21077
7278                     ; 1786   TIM2->SR2 = 0x00;
7280  0f2b 725f5256      	clr	21078
7281                     ; 1789   TIM2_ClearFlag(TIM2_FLAG_CC1);
7283  0f2f ae0002        	ldw	x,#2
7284  0f32 cd0d3c        	call	_TIM2_ClearFlag
7287  0f35               L3103:
7288                     ; 1792   while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != (uint8_t)TIM2_FLAG_CC1)
7290  0f35 c65255        	ld	a,21077
7291  0f38 a402          	and	a,#2
7292  0f3a a102          	cp	a,#2
7293  0f3c 26f7          	jrne	L3103
7294                     ; 1795   ICValue1 = TIM2_GetCapture1();
7296  0f3e cd0c6d        	call	_TIM2_GetCapture1
7298  0f41 1f09          	ldw	(OFST-3,sp),x
7300                     ; 1796   TIM2_ClearFlag(TIM2_FLAG_CC1);
7302  0f43 ae0002        	ldw	x,#2
7303  0f46 cd0d3c        	call	_TIM2_ClearFlag
7306  0f49               L1203:
7307                     ; 1799   while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != (uint8_t)TIM2_FLAG_CC1)
7309  0f49 c65255        	ld	a,21077
7310  0f4c a402          	and	a,#2
7311  0f4e a102          	cp	a,#2
7312  0f50 26f7          	jrne	L1203
7313                     ; 1802   ICValue2 = TIM2_GetCapture1();
7315  0f52 cd0c6d        	call	_TIM2_GetCapture1
7317  0f55 1f0b          	ldw	(OFST-1,sp),x
7319                     ; 1803   TIM2_ClearFlag(TIM2_FLAG_CC1);
7321  0f57 ae0002        	ldw	x,#2
7322  0f5a cd0d3c        	call	_TIM2_ClearFlag
7324                     ; 1806   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
7326  0f5d 7211525a      	bres	21082,#0
7327                     ; 1808   TIM2->CCMR1 = 0x00;
7329  0f61 725f5258      	clr	21080
7330                     ; 1810   TIM2_Cmd(DISABLE);
7332  0f65 4f            	clr	a
7333  0f66 cd0430        	call	_TIM2_Cmd
7335                     ; 1813   LSIClockFreq = (8 * TIM2_TimerClockFreq) / (ICValue2 - ICValue1);
7337  0f69 1e0b          	ldw	x,(OFST-1,sp)
7338  0f6b 72f009        	subw	x,(OFST-3,sp)
7339  0f6e cd0000        	call	c_uitolx
7341  0f71 96            	ldw	x,sp
7342  0f72 1c0001        	addw	x,#OFST-11
7343  0f75 cd0000        	call	c_rtol
7346  0f78 96            	ldw	x,sp
7347  0f79 1c000f        	addw	x,#OFST+3
7348  0f7c cd0000        	call	c_ltor
7350  0f7f a603          	ld	a,#3
7351  0f81 cd0000        	call	c_llsh
7353  0f84 96            	ldw	x,sp
7354  0f85 1c0001        	addw	x,#OFST-11
7355  0f88 cd0000        	call	c_ludv
7357  0f8b 96            	ldw	x,sp
7358  0f8c 1c0005        	addw	x,#OFST-7
7359  0f8f cd0000        	call	c_rtol
7362                     ; 1814   return LSIClockFreq;
7364  0f92 96            	ldw	x,sp
7365  0f93 1c0005        	addw	x,#OFST-7
7366  0f96 cd0000        	call	c_ltor
7370  0f99 5b0c          	addw	sp,#12
7371  0f9b 81            	ret
7395                     ; 1822 FunctionalState TIM2_GetStatus(void)
7395                     ; 1823 {
7396                     	switch	.text
7397  0f9c               _TIM2_GetStatus:
7401                     ; 1824   return ((FunctionalState)(TIM2->CR1 & TIM_CR1_CEN));
7403  0f9c c65250        	ld	a,21072
7404  0f9f a401          	and	a,#1
7407  0fa1 81            	ret
7420                     	xdef	_TIM2_GetStatus
7421                     	xdef	_TIM2_ComputeLsiClockFreq
7422                     	xdef	_TIM2_ClearITPendingBit
7423                     	xdef	_TIM2_GetITStatus
7424                     	xdef	_TIM2_ClearFlag
7425                     	xdef	_TIM2_GetFlagStatus
7426                     	xdef	_TIM2_GetPrescaler
7427                     	xdef	_TIM2_GetCounter
7428                     	xdef	_TIM2_GetCapture2
7429                     	xdef	_TIM2_GetCapture1
7430                     	xdef	_TIM2_SetIC2Prescaler
7431                     	xdef	_TIM2_SetIC1Prescaler
7432                     	xdef	_TIM2_SetCompare2
7433                     	xdef	_TIM2_SetCompare1
7434                     	xdef	_TIM2_SetAutoreload
7435                     	xdef	_TIM2_SetCounter
7436                     	xdef	_TIM2_SelectOCxM
7437                     	xdef	_TIM2_CCxCmd
7438                     	xdef	_TIM2_OC2PolarityConfig
7439                     	xdef	_TIM2_OC1PolarityConfig
7440                     	xdef	_TIM2_GenerateEvent
7441                     	xdef	_TIM2_OC2FastCmd
7442                     	xdef	_TIM2_OC1FastCmd
7443                     	xdef	_TIM2_OC2PreloadConfig
7444                     	xdef	_TIM2_OC1PreloadConfig
7445                     	xdef	_TIM2_ARRPreloadConfig
7446                     	xdef	_TIM2_ForcedOC2Config
7447                     	xdef	_TIM2_ForcedOC1Config
7448                     	xdef	_TIM2_CounterModeConfig
7449                     	xdef	_TIM2_PrescalerConfig
7450                     	xdef	_TIM2_EncoderInterfaceConfig
7451                     	xdef	_TIM2_SelectMasterSlaveMode
7452                     	xdef	_TIM2_SelectSlaveMode
7453                     	xdef	_TIM2_SelectOutputTrigger
7454                     	xdef	_TIM2_SelectOnePulseMode
7455                     	xdef	_TIM2_SelectHallSensor
7456                     	xdef	_TIM2_UpdateRequestConfig
7457                     	xdef	_TIM2_UpdateDisableConfig
7458                     	xdef	_TIM2_SelectInputTrigger
7459                     	xdef	_TIM2_TIxExternalClockConfig
7460                     	xdef	_TIM2_ETRConfig
7461                     	xdef	_TIM2_ETRClockMode2Config
7462                     	xdef	_TIM2_ETRClockMode1Config
7463                     	xdef	_TIM2_InternalClockConfig
7464                     	xdef	_TIM2_ITConfig
7465                     	xdef	_TIM2_CtrlPWMOutputs
7466                     	xdef	_TIM2_Cmd
7467                     	xdef	_TIM2_PWMIConfig
7468                     	xdef	_TIM2_ICInit
7469                     	xdef	_TIM2_BKRConfig
7470                     	xdef	_TIM2_OC2Init
7471                     	xdef	_TIM2_OC1Init
7472                     	xdef	_TIM2_TimeBaseInit
7473                     	xdef	_TIM2_DeInit
7474                     	xref	_assert_failed
7475                     .const:	section	.text
7476  0000               L511:
7477  0000 5f6c69625c73  	dc.b	"_lib\src\stm8l10x_"
7478  0012 74696d322e63  	dc.b	"tim2.c",0
7479                     	xref.b	c_x
7499                     	xref	c_ludv
7500                     	xref	c_rtol
7501                     	xref	c_uitolx
7502                     	xref	c_llsh
7503                     	xref	c_ltor
7504                     	end
