   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  44                     ; 3 void GPIO_SPI_INIT(void)
  44                     ; 4 {
  46                     	switch	.text
  47  0000               _GPIO_SPI_INIT:
  51                     ; 5 	GPIO_Init(SPI_NSS_Port, SPI_NSS_Pin, GPIO_Mode_Out_PP_Low_Fast);
  53  0000 4be0          	push	#224
  54  0002 4b10          	push	#16
  55  0004 ae5005        	ldw	x,#20485
  56  0007 cd0000        	call	_GPIO_Init
  58  000a 85            	popw	x
  59                     ; 7 }
  62  000b 81            	ret
  75                     	xdef	_GPIO_SPI_INIT
  76                     	xref	_GPIO_Init
  95                     	end
