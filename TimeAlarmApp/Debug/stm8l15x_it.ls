   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  15                     	bsct
  16  0000               _counter:
  17  0000 00            	dc.b	0
  47                     ; 54 INTERRUPT_HANDLER(NonHandledInterrupt,0)
  47                     ; 55 {
  48                     	switch	.text
  49  0000               f_NonHandledInterrupt:
  53                     ; 59 }
  56  0000 80            	iret
  78                     ; 69 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  78                     ; 70 {
  79                     	switch	.text
  80  0001               f_TRAP_IRQHandler:
  84                     ; 74 }
  87  0001 80            	iret
 109                     ; 80 INTERRUPT_HANDLER(FLASH_IRQHandler,1)
 109                     ; 81 {
 110                     	switch	.text
 111  0002               f_FLASH_IRQHandler:
 115                     ; 85 }
 118  0002 80            	iret
 141                     ; 91 INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler,2)
 141                     ; 92 {
 142                     	switch	.text
 143  0003               f_DMA1_CHANNEL0_1_IRQHandler:
 147                     ; 96 }
 150  0003 80            	iret
 173                     ; 102 INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler,3)
 173                     ; 103 {
 174                     	switch	.text
 175  0004               f_DMA1_CHANNEL2_3_IRQHandler:
 179                     ; 107 }
 182  0004 80            	iret
 205                     ; 113 INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler,4)
 205                     ; 114 {
 206                     	switch	.text
 207  0005               f_RTC_CSSLSE_IRQHandler:
 211                     ; 118 }
 214  0005 80            	iret
 237                     ; 124 INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
 237                     ; 125 {
 238                     	switch	.text
 239  0006               f_EXTIE_F_PVD_IRQHandler:
 243                     ; 129 }
 246  0006 80            	iret
 268                     ; 136 INTERRUPT_HANDLER(EXTIB_G_IRQHandler,6)
 268                     ; 137 {
 269                     	switch	.text
 270  0007               f_EXTIB_G_IRQHandler:
 274                     ; 141 }
 277  0007 80            	iret
 299                     ; 148 INTERRUPT_HANDLER(EXTID_H_IRQHandler,7)
 299                     ; 149 {
 300                     	switch	.text
 301  0008               f_EXTID_H_IRQHandler:
 305                     ; 153 }
 308  0008 80            	iret
 330                     ; 160 INTERRUPT_HANDLER(EXTI0_IRQHandler,8)
 330                     ; 161 {
 331                     	switch	.text
 332  0009               f_EXTI0_IRQHandler:
 336                     ; 167 }
 339  0009 80            	iret
 363                     ; 174 INTERRUPT_HANDLER(EXTI1_IRQHandler,9)
 363                     ; 175 {
 364                     	switch	.text
 365  000a               f_EXTI1_IRQHandler:
 367  000a 8a            	push	cc
 368  000b 84            	pop	a
 369  000c a4bf          	and	a,#191
 370  000e 88            	push	a
 371  000f 86            	pop	cc
 372  0010 3b0002        	push	c_x+2
 373  0013 be00          	ldw	x,c_x
 374  0015 89            	pushw	x
 375  0016 3b0002        	push	c_y+2
 376  0019 be00          	ldw	x,c_y
 377  001b 89            	pushw	x
 380                     ; 179 	GPIO_SetBits(GPIOC, GPIO_Pin_7);
 382  001c 4b80          	push	#128
 383  001e ae500a        	ldw	x,#20490
 384  0021 cd0000        	call	_GPIO_SetBits
 386  0024 84            	pop	a
 387                     ; 180 	EXTI_ClearITPendingBit(EXTI_IT_Pin1); /* ������� �������� ������� �����������!!!*/
 389  0025 ae0002        	ldw	x,#2
 390  0028 cd0000        	call	_EXTI_ClearITPendingBit
 392                     ; 181 }
 395  002b 85            	popw	x
 396  002c bf00          	ldw	c_y,x
 397  002e 320002        	pop	c_y+2
 398  0031 85            	popw	x
 399  0032 bf00          	ldw	c_x,x
 400  0034 320002        	pop	c_x+2
 401  0037 80            	iret
 423                     ; 188 INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
 423                     ; 189 {
 424                     	switch	.text
 425  0038               f_EXTI2_IRQHandler:
 429                     ; 193 }
 432  0038 80            	iret
 454                     ; 200 INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
 454                     ; 201 {
 455                     	switch	.text
 456  0039               f_EXTI3_IRQHandler:
 460                     ; 205 }
 463  0039 80            	iret
 485                     ; 212 INTERRUPT_HANDLER(EXTI4_IRQHandler,12)
 485                     ; 213 {
 486                     	switch	.text
 487  003a               f_EXTI4_IRQHandler:
 491                     ; 217 }
 494  003a 80            	iret
 516                     ; 224 INTERRUPT_HANDLER(EXTI5_IRQHandler,13)
 516                     ; 225 {
 517                     	switch	.text
 518  003b               f_EXTI5_IRQHandler:
 522                     ; 229 }
 525  003b 80            	iret
 547                     ; 236 INTERRUPT_HANDLER(EXTI6_IRQHandler,14)
 547                     ; 237 {
 548                     	switch	.text
 549  003c               f_EXTI6_IRQHandler:
 553                     ; 241 }
 556  003c 80            	iret
 578                     ; 248 INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
 578                     ; 249 {
 579                     	switch	.text
 580  003d               f_EXTI7_IRQHandler:
 584                     ; 253 }
 587  003d 80            	iret
 609                     ; 259 INTERRUPT_HANDLER(LCD_AES_IRQHandler,16)
 609                     ; 260 {
 610                     	switch	.text
 611  003e               f_LCD_AES_IRQHandler:
 615                     ; 264 }
 618  003e 80            	iret
 641                     ; 270 INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler,17)
 641                     ; 271 {
 642                     	switch	.text
 643  003f               f_SWITCH_CSS_BREAK_DAC_IRQHandler:
 647                     ; 275 }
 650  003f 80            	iret
 673                     ; 282 INTERRUPT_HANDLER(ADC1_COMP_IRQHandler,18)
 673                     ; 283 {
 674                     	switch	.text
 675  0040               f_ADC1_COMP_IRQHandler:
 679                     ; 287 }
 682  0040 80            	iret
 684                     	bsct
 685  0001               _tgl:
 686  0001 01            	dc.b	1
 712                     ; 295 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
 712                     ; 296 {
 713                     	switch	.text
 714  0041               f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler:
 716  0041 8a            	push	cc
 717  0042 84            	pop	a
 718  0043 a4bf          	and	a,#191
 719  0045 88            	push	a
 720  0046 86            	pop	cc
 721  0047 3b0002        	push	c_x+2
 722  004a be00          	ldw	x,c_x
 723  004c 89            	pushw	x
 724  004d 3b0002        	push	c_y+2
 725  0050 be00          	ldw	x,c_y
 726  0052 89            	pushw	x
 729                     ; 300 	counter++;
 731  0053 3c00          	inc	_counter
 732                     ; 301 	writeNumeric(counter);
 734  0055 b600          	ld	a,_counter
 735  0057 5f            	clrw	x
 736  0058 97            	ld	xl,a
 737  0059 cd0000        	call	_writeNumeric
 739                     ; 302 	TIM2_ClearITPendingBit(TIM2_IT_Update);	/* ������� �������� ������� �����������!!!*/
 741  005c a601          	ld	a,#1
 742  005e cd0000        	call	_TIM2_ClearITPendingBit
 744                     ; 303 }
 747  0061 85            	popw	x
 748  0062 bf00          	ldw	c_y,x
 749  0064 320002        	pop	c_y+2
 750  0067 85            	popw	x
 751  0068 bf00          	ldw	c_x,x
 752  006a 320002        	pop	c_x+2
 753  006d 80            	iret
 776                     ; 310 INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler,20)
 776                     ; 311 {
 777                     	switch	.text
 778  006e               f_TIM2_CC_USART2_RX_IRQHandler:
 782                     ; 315 }
 785  006e 80            	iret
 809                     ; 323 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
 809                     ; 324 {
 810                     	switch	.text
 811  006f               f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler:
 815                     ; 328 }
 818  006f 80            	iret
 841                     ; 334 INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler,22)
 841                     ; 335 {
 842                     	switch	.text
 843  0070               f_TIM3_CC_USART3_RX_IRQHandler:
 847                     ; 339 }
 850  0070 80            	iret
 873                     ; 345 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler,23)
 873                     ; 346 {
 874                     	switch	.text
 875  0071               f_TIM1_UPD_OVF_TRG_COM_IRQHandler:
 879                     ; 350 }
 882  0071 80            	iret
 904                     ; 356 INTERRUPT_HANDLER(TIM1_CC_IRQHandler,24)
 904                     ; 357 {
 905                     	switch	.text
 906  0072               f_TIM1_CC_IRQHandler:
 910                     ; 361 }
 913  0072 80            	iret
 936                     ; 368 INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler,25)
 936                     ; 369 {
 937                     	switch	.text
 938  0073               f_TIM4_UPD_OVF_TRG_IRQHandler:
 942                     ; 373 }
 945  0073 80            	iret
 967                     ; 379 INTERRUPT_HANDLER(SPI1_IRQHandler,26)
 967                     ; 380 {
 968                     	switch	.text
 969  0074               f_SPI1_IRQHandler:
 973                     ; 385 }
 976  0074 80            	iret
1000                     ; 392 INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler,27)
1000                     ; 393 {
1001                     	switch	.text
1002  0075               f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler:
1006                     ; 397 }
1009  0075 80            	iret
1032                     ; 404 INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
1032                     ; 405 {
1033                     	switch	.text
1034  0076               f_USART1_RX_TIM5_CC_IRQHandler:
1038                     ; 409 }
1041  0076 80            	iret
1064                     ; 416 INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler,29)
1064                     ; 417 {
1065                     	switch	.text
1066  0077               f_I2C1_SPI2_IRQHandler:
1070                     ; 421 }
1073  0077 80            	iret
1105                     	xdef	_tgl
1106                     	xdef	_counter
1107                     	xref	_writeNumeric
1108                     	xdef	f_I2C1_SPI2_IRQHandler
1109                     	xdef	f_USART1_RX_TIM5_CC_IRQHandler
1110                     	xdef	f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler
1111                     	xdef	f_SPI1_IRQHandler
1112                     	xdef	f_TIM4_UPD_OVF_TRG_IRQHandler
1113                     	xdef	f_TIM1_CC_IRQHandler
1114                     	xdef	f_TIM1_UPD_OVF_TRG_COM_IRQHandler
1115                     	xdef	f_TIM3_CC_USART3_RX_IRQHandler
1116                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler
1117                     	xdef	f_TIM2_CC_USART2_RX_IRQHandler
1118                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler
1119                     	xdef	f_ADC1_COMP_IRQHandler
1120                     	xdef	f_SWITCH_CSS_BREAK_DAC_IRQHandler
1121                     	xdef	f_LCD_AES_IRQHandler
1122                     	xdef	f_EXTI7_IRQHandler
1123                     	xdef	f_EXTI6_IRQHandler
1124                     	xdef	f_EXTI5_IRQHandler
1125                     	xdef	f_EXTI4_IRQHandler
1126                     	xdef	f_EXTI3_IRQHandler
1127                     	xdef	f_EXTI2_IRQHandler
1128                     	xdef	f_EXTI1_IRQHandler
1129                     	xdef	f_EXTI0_IRQHandler
1130                     	xdef	f_EXTID_H_IRQHandler
1131                     	xdef	f_EXTIB_G_IRQHandler
1132                     	xdef	f_EXTIE_F_PVD_IRQHandler
1133                     	xdef	f_RTC_CSSLSE_IRQHandler
1134                     	xdef	f_DMA1_CHANNEL2_3_IRQHandler
1135                     	xdef	f_DMA1_CHANNEL0_1_IRQHandler
1136                     	xdef	f_FLASH_IRQHandler
1137                     	xdef	f_TRAP_IRQHandler
1138                     	xdef	f_NonHandledInterrupt
1139                     	xref	_TIM2_ClearITPendingBit
1140                     	xref	_GPIO_SetBits
1141                     	xref	_EXTI_ClearITPendingBit
1142                     	xref.b	c_x
1143                     	xref.b	c_y
1162                     	end
