   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
 109                     ; 57 void GPIO_DeInit(GPIO_TypeDef* GPIOx)
 109                     ; 58 {
 111                     	switch	.text
 112  0000               _GPIO_DeInit:
 116                     ; 59   GPIOx->ODR = GPIO_ODR_RESET_VALUE; /* Reset Output Data Register */
 118  0000 7f            	clr	(x)
 119                     ; 60   GPIOx->DDR = GPIO_DDR_RESET_VALUE; /* Reset Data Direction Register */
 121  0001 6f02          	clr	(2,x)
 122                     ; 61   GPIOx->CR1 = GPIO_CR1_RESET_VALUE; /* Reset Control Register 1 */
 124  0003 6f03          	clr	(3,x)
 125                     ; 62   GPIOx->CR2 = GPIO_CR2_RESET_VALUE; /* Reset Control Register 2 */
 127  0005 6f04          	clr	(4,x)
 128                     ; 63 }
 131  0007 81            	ret
 288                     ; 98 void GPIO_Init(GPIO_TypeDef* GPIOx,
 288                     ; 99                uint8_t GPIO_Pin,
 288                     ; 100                GPIO_Mode_TypeDef GPIO_Mode)
 288                     ; 101 {
 289                     	switch	.text
 290  0008               _GPIO_Init:
 292  0008 89            	pushw	x
 293       00000000      OFST:	set	0
 296                     ; 106   assert_param(IS_GPIO_MODE(GPIO_Mode));
 298  0009 0d06          	tnz	(OFST+6,sp)
 299  000b 2742          	jreq	L21
 300  000d 7b06          	ld	a,(OFST+6,sp)
 301  000f a140          	cp	a,#64
 302  0011 273c          	jreq	L21
 303  0013 7b06          	ld	a,(OFST+6,sp)
 304  0015 a120          	cp	a,#32
 305  0017 2736          	jreq	L21
 306  0019 7b06          	ld	a,(OFST+6,sp)
 307  001b a160          	cp	a,#96
 308  001d 2730          	jreq	L21
 309  001f 7b06          	ld	a,(OFST+6,sp)
 310  0021 a1a0          	cp	a,#160
 311  0023 272a          	jreq	L21
 312  0025 7b06          	ld	a,(OFST+6,sp)
 313  0027 a1e0          	cp	a,#224
 314  0029 2724          	jreq	L21
 315  002b 7b06          	ld	a,(OFST+6,sp)
 316  002d a180          	cp	a,#128
 317  002f 271e          	jreq	L21
 318  0031 7b06          	ld	a,(OFST+6,sp)
 319  0033 a1c0          	cp	a,#192
 320  0035 2718          	jreq	L21
 321  0037 7b06          	ld	a,(OFST+6,sp)
 322  0039 a1b0          	cp	a,#176
 323  003b 2712          	jreq	L21
 324  003d 7b06          	ld	a,(OFST+6,sp)
 325  003f a1f0          	cp	a,#240
 326  0041 270c          	jreq	L21
 327  0043 7b06          	ld	a,(OFST+6,sp)
 328  0045 a190          	cp	a,#144
 329  0047 2706          	jreq	L21
 330  0049 7b06          	ld	a,(OFST+6,sp)
 331  004b a1d0          	cp	a,#208
 332  004d 2603          	jrne	L01
 333  004f               L21:
 334  004f 4f            	clr	a
 335  0050 2010          	jra	L41
 336  0052               L01:
 337  0052 ae006a        	ldw	x,#106
 338  0055 89            	pushw	x
 339  0056 ae0000        	ldw	x,#0
 340  0059 89            	pushw	x
 341  005a ae0000        	ldw	x,#L541
 342  005d cd0000        	call	_assert_failed
 344  0060 5b04          	addw	sp,#4
 345  0062               L41:
 346                     ; 107   assert_param(IS_GPIO_PIN(GPIO_Pin));
 348  0062 0d05          	tnz	(OFST+5,sp)
 349  0064 2703          	jreq	L61
 350  0066 4f            	clr	a
 351  0067 2010          	jra	L02
 352  0069               L61:
 353  0069 ae006b        	ldw	x,#107
 354  006c 89            	pushw	x
 355  006d ae0000        	ldw	x,#0
 356  0070 89            	pushw	x
 357  0071 ae0000        	ldw	x,#L541
 358  0074 cd0000        	call	_assert_failed
 360  0077 5b04          	addw	sp,#4
 361  0079               L02:
 362                     ; 113   if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x80) != (uint8_t)0x00) /* Output mode */
 364  0079 7b06          	ld	a,(OFST+6,sp)
 365  007b a580          	bcp	a,#128
 366  007d 271f          	jreq	L741
 367                     ; 115     if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x10) != (uint8_t)0x00) /* High level */
 369  007f 7b06          	ld	a,(OFST+6,sp)
 370  0081 a510          	bcp	a,#16
 371  0083 2708          	jreq	L151
 372                     ; 117       GPIOx->ODR |= GPIO_Pin;
 374  0085 1e01          	ldw	x,(OFST+1,sp)
 375  0087 f6            	ld	a,(x)
 376  0088 1a05          	or	a,(OFST+5,sp)
 377  008a f7            	ld	(x),a
 379  008b 2007          	jra	L351
 380  008d               L151:
 381                     ; 120       GPIOx->ODR &= (uint8_t)(~(GPIO_Pin));
 383  008d 1e01          	ldw	x,(OFST+1,sp)
 384  008f 7b05          	ld	a,(OFST+5,sp)
 385  0091 43            	cpl	a
 386  0092 f4            	and	a,(x)
 387  0093 f7            	ld	(x),a
 388  0094               L351:
 389                     ; 123     GPIOx->DDR |= GPIO_Pin;
 391  0094 1e01          	ldw	x,(OFST+1,sp)
 392  0096 e602          	ld	a,(2,x)
 393  0098 1a05          	or	a,(OFST+5,sp)
 394  009a e702          	ld	(2,x),a
 396  009c 2009          	jra	L551
 397  009e               L741:
 398                     ; 127     GPIOx->DDR &= (uint8_t)(~(GPIO_Pin));
 400  009e 1e01          	ldw	x,(OFST+1,sp)
 401  00a0 7b05          	ld	a,(OFST+5,sp)
 402  00a2 43            	cpl	a
 403  00a3 e402          	and	a,(2,x)
 404  00a5 e702          	ld	(2,x),a
 405  00a7               L551:
 406                     ; 134   if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x40) != (uint8_t)0x00) /* Pull-Up or Push-Pull */
 408  00a7 7b06          	ld	a,(OFST+6,sp)
 409  00a9 a540          	bcp	a,#64
 410  00ab 270a          	jreq	L751
 411                     ; 136     GPIOx->CR1 |= GPIO_Pin;
 413  00ad 1e01          	ldw	x,(OFST+1,sp)
 414  00af e603          	ld	a,(3,x)
 415  00b1 1a05          	or	a,(OFST+5,sp)
 416  00b3 e703          	ld	(3,x),a
 418  00b5 2009          	jra	L161
 419  00b7               L751:
 420                     ; 139     GPIOx->CR1 &= (uint8_t)(~(GPIO_Pin));
 422  00b7 1e01          	ldw	x,(OFST+1,sp)
 423  00b9 7b05          	ld	a,(OFST+5,sp)
 424  00bb 43            	cpl	a
 425  00bc e403          	and	a,(3,x)
 426  00be e703          	ld	(3,x),a
 427  00c0               L161:
 428                     ; 146   if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x20) != (uint8_t)0x00) /* Interrupt or Slow slope */
 430  00c0 7b06          	ld	a,(OFST+6,sp)
 431  00c2 a520          	bcp	a,#32
 432  00c4 270a          	jreq	L361
 433                     ; 148     GPIOx->CR2 |= GPIO_Pin;
 435  00c6 1e01          	ldw	x,(OFST+1,sp)
 436  00c8 e604          	ld	a,(4,x)
 437  00ca 1a05          	or	a,(OFST+5,sp)
 438  00cc e704          	ld	(4,x),a
 440  00ce 2009          	jra	L561
 441  00d0               L361:
 442                     ; 151     GPIOx->CR2 &= (uint8_t)(~(GPIO_Pin));
 444  00d0 1e01          	ldw	x,(OFST+1,sp)
 445  00d2 7b05          	ld	a,(OFST+5,sp)
 446  00d4 43            	cpl	a
 447  00d5 e404          	and	a,(4,x)
 448  00d7 e704          	ld	(4,x),a
 449  00d9               L561:
 450                     ; 154 }
 453  00d9 85            	popw	x
 454  00da 81            	ret
 500                     ; 163 void GPIO_Write(GPIO_TypeDef* GPIOx, uint8_t GPIO_PortVal)
 500                     ; 164 {
 501                     	switch	.text
 502  00db               _GPIO_Write:
 504  00db 89            	pushw	x
 505       00000000      OFST:	set	0
 508                     ; 165   GPIOx->ODR = GPIO_PortVal;
 510  00dc 7b05          	ld	a,(OFST+5,sp)
 511  00de 1e01          	ldw	x,(OFST+1,sp)
 512  00e0 f7            	ld	(x),a
 513                     ; 166 }
 516  00e1 85            	popw	x
 517  00e2 81            	ret
 678                     ; 185 void GPIO_WriteBit(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin, BitAction GPIO_BitVal)
 678                     ; 186 {
 679                     	switch	.text
 680  00e3               _GPIO_WriteBit:
 682  00e3 89            	pushw	x
 683       00000000      OFST:	set	0
 686                     ; 188   assert_param(IS_GPIO_PIN(GPIO_Pin));
 688  00e4 0d05          	tnz	(OFST+5,sp)
 689  00e6 2703          	jreq	L62
 690  00e8 4f            	clr	a
 691  00e9 2010          	jra	L03
 692  00eb               L62:
 693  00eb ae00bc        	ldw	x,#188
 694  00ee 89            	pushw	x
 695  00ef ae0000        	ldw	x,#0
 696  00f2 89            	pushw	x
 697  00f3 ae0000        	ldw	x,#L541
 698  00f6 cd0000        	call	_assert_failed
 700  00f9 5b04          	addw	sp,#4
 701  00fb               L03:
 702                     ; 189   assert_param(IS_STATE_VALUE(GPIO_BitVal));
 704  00fb 7b06          	ld	a,(OFST+6,sp)
 705  00fd a101          	cp	a,#1
 706  00ff 2704          	jreq	L43
 707  0101 0d06          	tnz	(OFST+6,sp)
 708  0103 2603          	jrne	L23
 709  0105               L43:
 710  0105 4f            	clr	a
 711  0106 2010          	jra	L63
 712  0108               L23:
 713  0108 ae00bd        	ldw	x,#189
 714  010b 89            	pushw	x
 715  010c ae0000        	ldw	x,#0
 716  010f 89            	pushw	x
 717  0110 ae0000        	ldw	x,#L541
 718  0113 cd0000        	call	_assert_failed
 720  0116 5b04          	addw	sp,#4
 721  0118               L63:
 722                     ; 191   if (GPIO_BitVal != RESET)
 724  0118 0d06          	tnz	(OFST+6,sp)
 725  011a 2717          	jreq	L503
 726                     ; 193     SetBit(GPIOx->ODR, GPIO_Pin);
 728  011c 1e01          	ldw	x,(OFST+1,sp)
 729  011e 7b05          	ld	a,(OFST+5,sp)
 730  0120 905f          	clrw	y
 731  0122 9097          	ld	yl,a
 732  0124 a601          	ld	a,#1
 733  0126 905d          	tnzw	y
 734  0128 2705          	jreq	L04
 735  012a               L24:
 736  012a 48            	sll	a
 737  012b 905a          	decw	y
 738  012d 26fb          	jrne	L24
 739  012f               L04:
 740  012f fa            	or	a,(x)
 741  0130 f7            	ld	(x),a
 743  0131 2017          	jra	L703
 744  0133               L503:
 745                     ; 198     ClrBit(GPIOx->ODR, GPIO_Pin);
 747  0133 1e01          	ldw	x,(OFST+1,sp)
 748  0135 7b05          	ld	a,(OFST+5,sp)
 749  0137 905f          	clrw	y
 750  0139 9097          	ld	yl,a
 751  013b a601          	ld	a,#1
 752  013d 905d          	tnzw	y
 753  013f 2705          	jreq	L44
 754  0141               L64:
 755  0141 48            	sll	a
 756  0142 905a          	decw	y
 757  0144 26fb          	jrne	L64
 758  0146               L44:
 759  0146 a8ff          	xor	a,#255
 760  0148 f4            	and	a,(x)
 761  0149 f7            	ld	(x),a
 762  014a               L703:
 763                     ; 200 }
 766  014a 85            	popw	x
 767  014b 81            	ret
 813                     ; 218 void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint8_t GPIO_Pin)
 813                     ; 219 {
 814                     	switch	.text
 815  014c               _GPIO_SetBits:
 817  014c 89            	pushw	x
 818       00000000      OFST:	set	0
 821                     ; 220   GPIOx->ODR |= GPIO_Pin;
 823  014d f6            	ld	a,(x)
 824  014e 1a05          	or	a,(OFST+5,sp)
 825  0150 f7            	ld	(x),a
 826                     ; 221 }
 829  0151 85            	popw	x
 830  0152 81            	ret
 876                     ; 239 void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint8_t GPIO_Pin)
 876                     ; 240 {
 877                     	switch	.text
 878  0153               _GPIO_ResetBits:
 880  0153 89            	pushw	x
 881       00000000      OFST:	set	0
 884                     ; 241   GPIOx->ODR &= (uint8_t)(~GPIO_Pin);
 886  0154 7b05          	ld	a,(OFST+5,sp)
 887  0156 43            	cpl	a
 888  0157 f4            	and	a,(x)
 889  0158 f7            	ld	(x),a
 890                     ; 242 }
 893  0159 85            	popw	x
 894  015a 81            	ret
 940                     ; 251 void GPIO_ToggleBits(GPIO_TypeDef* GPIOx, uint8_t GPIO_Pin)
 940                     ; 252 {
 941                     	switch	.text
 942  015b               _GPIO_ToggleBits:
 944  015b 89            	pushw	x
 945       00000000      OFST:	set	0
 948                     ; 253   GPIOx->ODR ^= GPIO_Pin;
 950  015c f6            	ld	a,(x)
 951  015d 1805          	xor	a,	(OFST+5,sp)
 952  015f f7            	ld	(x),a
 953                     ; 254 }
 956  0160 85            	popw	x
 957  0161 81            	ret
 994                     ; 262 uint8_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx)
 994                     ; 263 {
 995                     	switch	.text
 996  0162               _GPIO_ReadInputData:
1000                     ; 264   return ((uint8_t)GPIOx->IDR);
1002  0162 e601          	ld	a,(1,x)
1005  0164 81            	ret
1043                     ; 273 uint8_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx)
1043                     ; 274 {
1044                     	switch	.text
1045  0165               _GPIO_ReadOutputData:
1049                     ; 275   return ((uint8_t)GPIOx->ODR);
1051  0165 f6            	ld	a,(x)
1054  0166 81            	ret
1103                     ; 294 BitStatus GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)
1103                     ; 295 {
1104                     	switch	.text
1105  0167               _GPIO_ReadInputDataBit:
1107  0167 89            	pushw	x
1108       00000000      OFST:	set	0
1111                     ; 296   return ((BitStatus)(GPIOx->IDR & (uint8_t)GPIO_Pin));
1113  0168 e601          	ld	a,(1,x)
1114  016a 1405          	and	a,(OFST+5,sp)
1117  016c 85            	popw	x
1118  016d 81            	ret
1167                     ; 315 BitStatus GPIO_ReadOutputDataBit(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)
1167                     ; 316 {
1168                     	switch	.text
1169  016e               _GPIO_ReadOutputDataBit:
1171  016e 89            	pushw	x
1172       00000000      OFST:	set	0
1175                     ; 317   return ((BitStatus)(GPIOx->ODR & (uint8_t)GPIO_Pin));
1177  016f f6            	ld	a,(x)
1178  0170 1405          	and	a,(OFST+5,sp)
1181  0172 85            	popw	x
1182  0173 81            	ret
1260                     ; 337 void GPIO_ExternalPullUpConfig(GPIO_TypeDef* GPIOx, uint8_t GPIO_Pin, FunctionalState NewState)
1260                     ; 338 {
1261                     	switch	.text
1262  0174               _GPIO_ExternalPullUpConfig:
1264  0174 89            	pushw	x
1265       00000000      OFST:	set	0
1268                     ; 340   assert_param(IS_GPIO_PIN(GPIO_Pin));
1270  0175 0d05          	tnz	(OFST+5,sp)
1271  0177 2703          	jreq	L07
1272  0179 4f            	clr	a
1273  017a 2010          	jra	L27
1274  017c               L07:
1275  017c ae0154        	ldw	x,#340
1276  017f 89            	pushw	x
1277  0180 ae0000        	ldw	x,#0
1278  0183 89            	pushw	x
1279  0184 ae0000        	ldw	x,#L541
1280  0187 cd0000        	call	_assert_failed
1282  018a 5b04          	addw	sp,#4
1283  018c               L27:
1284                     ; 341   assert_param(IS_FUNCTIONAL_STATE(NewState));
1286  018c 0d06          	tnz	(OFST+6,sp)
1287  018e 2706          	jreq	L67
1288  0190 7b06          	ld	a,(OFST+6,sp)
1289  0192 a101          	cp	a,#1
1290  0194 2603          	jrne	L47
1291  0196               L67:
1292  0196 4f            	clr	a
1293  0197 2010          	jra	L001
1294  0199               L47:
1295  0199 ae0155        	ldw	x,#341
1296  019c 89            	pushw	x
1297  019d ae0000        	ldw	x,#0
1298  01a0 89            	pushw	x
1299  01a1 ae0000        	ldw	x,#L541
1300  01a4 cd0000        	call	_assert_failed
1302  01a7 5b04          	addw	sp,#4
1303  01a9               L001:
1304                     ; 343   if (NewState != DISABLE) /* External Pull-Up Set*/
1306  01a9 0d06          	tnz	(OFST+6,sp)
1307  01ab 270a          	jreq	L555
1308                     ; 345     GPIOx->CR1 |= GPIO_Pin;
1310  01ad 1e01          	ldw	x,(OFST+1,sp)
1311  01af e603          	ld	a,(3,x)
1312  01b1 1a05          	or	a,(OFST+5,sp)
1313  01b3 e703          	ld	(3,x),a
1315  01b5 2009          	jra	L755
1316  01b7               L555:
1317                     ; 348     GPIOx->CR1 &= (uint8_t)(~(GPIO_Pin));
1319  01b7 1e01          	ldw	x,(OFST+1,sp)
1320  01b9 7b05          	ld	a,(OFST+5,sp)
1321  01bb 43            	cpl	a
1322  01bc e403          	and	a,(3,x)
1323  01be e703          	ld	(3,x),a
1324  01c0               L755:
1325                     ; 350 }
1328  01c0 85            	popw	x
1329  01c1 81            	ret
1342                     	xdef	_GPIO_ExternalPullUpConfig
1343                     	xdef	_GPIO_ReadOutputDataBit
1344                     	xdef	_GPIO_ReadInputDataBit
1345                     	xdef	_GPIO_ReadOutputData
1346                     	xdef	_GPIO_ReadInputData
1347                     	xdef	_GPIO_ToggleBits
1348                     	xdef	_GPIO_ResetBits
1349                     	xdef	_GPIO_SetBits
1350                     	xdef	_GPIO_WriteBit
1351                     	xdef	_GPIO_Write
1352                     	xdef	_GPIO_Init
1353                     	xdef	_GPIO_DeInit
1354                     	xref	_assert_failed
1355                     .const:	section	.text
1356  0000               L541:
1357  0000 5f6c69625c73  	dc.b	"_lib\src\stm8l10x_"
1358  0012 6770696f2e63  	dc.b	"gpio.c",0
1378                     	end
