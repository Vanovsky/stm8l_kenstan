   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
 154                     ; 3 void SPI_INIT(SPI_TypeDef* SPIx, SPI_NSS_type SPI_NSS, SPI_MODE_type SPI_MODE)
 154                     ; 4 {
 156                     	switch	.text
 157  0000               _SPI_INIT:
 159  0000 89            	pushw	x
 160       00000000      OFST:	set	0
 163                     ; 6 	SPIx->CR1 &= ~SPI_CR1_SPE;
 165  0001 f6            	ld	a,(x)
 166  0002 a4bf          	and	a,#191
 167  0004 f7            	ld	(x),a
 168                     ; 9 	CLK->PCKENR1 |= CLK_PCKENR1_SPI1;
 170  0005 721850c3      	bset	20675,#4
 171                     ; 11 	SPIx->CR1 |= SPI_CR1_BR;
 173  0009 f6            	ld	a,(x)
 174  000a aa38          	or	a,#56
 175  000c f7            	ld	(x),a
 176                     ; 14 	SPIx->CR1 &= ~SPI_CR1_CPOL;
 178  000d f6            	ld	a,(x)
 179  000e a4fd          	and	a,#253
 180  0010 f7            	ld	(x),a
 181                     ; 15 	SPIx->CR1 &= ~SPI_CR1_CPHA;
 183  0011 f6            	ld	a,(x)
 184  0012 a4fe          	and	a,#254
 185  0014 f7            	ld	(x),a
 186                     ; 17 	SPIx->CR1 &= ~SPI_CR1_LSBFIRST;
 188  0015 f6            	ld	a,(x)
 189  0016 a47f          	and	a,#127
 190  0018 f7            	ld	(x),a
 191                     ; 20 	if (SPI_NSS == SPI_NSS_Software)
 193  0019 0d05          	tnz	(OFST+5,sp)
 194  001b 260e          	jrne	L501
 195                     ; 23 		SPIx->CR2 |= SPI_CR2_SSM;
 197  001d e601          	ld	a,(1,x)
 198  001f aa02          	or	a,#2
 199  0021 e701          	ld	(1,x),a
 200                     ; 24 		SPIx->CR2 |= SPI_CR2_SSI;
 202  0023 e601          	ld	a,(1,x)
 203  0025 aa01          	or	a,#1
 204  0027 e701          	ld	(1,x),a
 206  0029 2010          	jra	L701
 207  002b               L501:
 208                     ; 29 		SPIx->CR2 &= ~SPI_CR2_SSM;
 210  002b 1e01          	ldw	x,(OFST+1,sp)
 211  002d e601          	ld	a,(1,x)
 212  002f a4fd          	and	a,#253
 213  0031 e701          	ld	(1,x),a
 214                     ; 30 		SPIx->CR2 |= SPI_CR2_SSI;
 216  0033 1e01          	ldw	x,(OFST+1,sp)
 217  0035 e601          	ld	a,(1,x)
 218  0037 aa01          	or	a,#1
 219  0039 e701          	ld	(1,x),a
 220  003b               L701:
 221                     ; 33 	if (SPI_MODE != SPI_MODE_Master)
 223  003b 7b06          	ld	a,(OFST+6,sp)
 224  003d a101          	cp	a,#1
 225  003f 2708          	jreq	L111
 226                     ; 34 		SPIx->CR1 &= ~SPI_CR1_MSTR;	/* Slave Mode */
 228  0041 1e01          	ldw	x,(OFST+1,sp)
 229  0043 f6            	ld	a,(x)
 230  0044 a4fb          	and	a,#251
 231  0046 f7            	ld	(x),a
 233  0047 2006          	jra	L311
 234  0049               L111:
 235                     ; 35 		else SPIx->CR1 |= SPI_CR1_MSTR;	/* Master Mode */
 237  0049 1e01          	ldw	x,(OFST+1,sp)
 238  004b f6            	ld	a,(x)
 239  004c aa04          	or	a,#4
 240  004e f7            	ld	(x),a
 241  004f               L311:
 242                     ; 38 	SPIx->CR1 |= SPI_CR1_SPE;
 244  004f 1e01          	ldw	x,(OFST+1,sp)
 245  0051 f6            	ld	a,(x)
 246  0052 aa40          	or	a,#64
 247  0054 f7            	ld	(x),a
 248                     ; 39 }
 251  0055 85            	popw	x
 252  0056 81            	ret
 309                     ; 41 uint8_t SPI_Transfer(SPI_TypeDef* SPIx, uint8_t w_data)
 309                     ; 42 {
 310                     	switch	.text
 311  0057               _SPI_Transfer:
 313  0057 89            	pushw	x
 314  0058 88            	push	a
 315       00000001      OFST:	set	1
 318  0059               L741:
 319                     ; 45 	while((SPIx->SR & SPI_SR_TXE) == 0 ) {} // Waiting for completed transfer (while Tx is not empty)
 321  0059 1e02          	ldw	x,(OFST+1,sp)
 322  005b e603          	ld	a,(3,x)
 323  005d a502          	bcp	a,#2
 324  005f 27f8          	jreq	L741
 325                     ; 46 	GPIO_ResetBits(SPI_NSS_Port, SPI_NSS_Pin);
 327  0061 4b10          	push	#16
 328  0063 ae5005        	ldw	x,#20485
 329  0066 cd0000        	call	_GPIO_ResetBits
 331  0069 84            	pop	a
 332                     ; 47 	SPI1->DR = w_data;
 334  006a 7b06          	ld	a,(OFST+5,sp)
 335  006c c75204        	ld	20996,a
 337  006f               L751:
 338                     ; 48 	while((SPIx->SR & SPI_SR_BSY) != 0 ) {}
 340  006f 1e02          	ldw	x,(OFST+1,sp)
 341  0071 e603          	ld	a,(3,x)
 342  0073 a580          	bcp	a,#128
 343  0075 26f8          	jrne	L751
 344                     ; 49 	GPIO_SetBits(SPI_NSS_Port, SPI_NSS_Pin);
 346  0077 4b10          	push	#16
 347  0079 ae5005        	ldw	x,#20485
 348  007c cd0000        	call	_GPIO_SetBits
 350  007f 84            	pop	a
 351                     ; 50 	r_data = SPI1->DR;
 353  0080 c65204        	ld	a,20996
 354  0083 6b01          	ld	(OFST+0,sp),a
 356                     ; 52 	return(r_data);
 358  0085 7b01          	ld	a,(OFST+0,sp)
 361  0087 5b03          	addw	sp,#3
 362  0089 81            	ret
 375                     	xdef	_SPI_Transfer
 376                     	xdef	_SPI_INIT
 377                     	xref	_GPIO_ResetBits
 378                     	xref	_GPIO_SetBits
 397                     	end
