#ifndef MAIN_H
#define MAIN_H

#include "stm8l10x.h"
#include "stm8l10x_clk.h"
#include "stm8l10x_tim2.h"
#include "stm8l10x_it.h"

void CLK_config(void);
void GPIO_config(void);
void TIM2_config(void);

int main();
void delay_us(uint32_t del_us);
void writeNumeric(uint16_t numeric);
void writeNumSym(uint8_t num, uint8_t seg);

#endif
