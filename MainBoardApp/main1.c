#include "main.h"

void CLK_config(void)
{
	/********************************************
	 * ��������� ��������� ������������ ������� *
	 ********************************************/
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
  CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
  while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}
}

void delay_us(uint32_t del_us)
{
	//del_us;
	while(del_us--);
}

void GPIO_config(void)
{
	/************************ 
	 * ������������� ������ *
	 ************************/
	/* SPI Pins */
	GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast );	// SCK
	GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast );	// MOSI
	GPIO_Init(GPIOD, MFRC_CS_Pin, GPIO_Mode_Out_PP_High_Fast );	// CSN
	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
	
	/* GPIO Pins */
	GPIO_Init(GPIOD, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast );	// Soft Reset Pin
	GPIO_Init(GPIOE, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast );	// Green LED
	GPIO_Init(GPIOC, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast );	//  Blue LED
	
	GPIO_ResetBits(GPIOE, GPIO_Pin_7);
	GPIO_ResetBits(GPIOC, GPIO_Pin_7);
	
	/* ���� �������� */
	//GPIO_Init(GPIOC, GPIO_Pin_1, GPIO_Mode_In_PU_IT);
}

void SoftResetRFID(void)
{
	GPIO_ResetBits(GPIOD, GPIO_Pin_5);
	GPIO_SetBits(GPIOD, GPIO_Pin_5);
}

void EXTI_config(void)
{
	EXTI_DeInit();
	/************************************
	 * ������������� ������� ���������� *
	 ************************************/
	//EXTI_SetPinSensitivity(EXTI_Pin_1, EXTI_Trigger_Rising);
	//EXTI->CR1 |= 0b01; //������������� 0b10 � P0IS
}

void ITC_config(void)
{
	//ITC_SetSoftwarePriority(EXTI7_IRQn, ITC_PriorityLevel_1);
	//ITC_SetSoftwarePriority(SPI1_IRQn, ITC_PriorityLevel_2);
	ITC_SetSoftwarePriority(TIM2_UPD_OVF_TRG_BRK_IRQn, ITC_PriorityLevel_3);
}

void SPI_config(void)
{
	SPI_DeInit(SPI1);	
	/*********************
	 * ������������� SPI *
	 *********************/
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);	
	
	SPI_Init(SPI1, 
						SPI_FirstBit_MSB,
						SPI_BaudRatePrescaler_32,
						SPI_Mode_Master,
						//SPI_Mode_Slave,
						SPI_CPOL_Low,
						SPI_CPHA_1Edge, 
						SPI_Direction_2Lines_FullDuplex,
						SPI_NSS_Soft, 
						0);
	//SPI_ITConfig(SPI1, SPI_IT_RXNE, ENABLE);
	SPI_Cmd(SPI1, ENABLE);
}

void I2C_config(void)
{
	I2C_DeInit(I2C1);
	/*********************
	 * ������������� I2C *
	 *********************/
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
	
	/* system_clock / 2 */
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_2);
	
	/* Initialize I2C peripheral */
  I2C_Init(I2C1, 
					 I2C_SPEED, 
					 0xA0,
           I2C_Mode_I2C, 
					 I2C_DutyCycle_2,
           I2C_Ack_Enable, 
					 I2C_AcknowledgedAddress_7bit);
	
	/* Enable Buffer and Event Interrupt*/
  //I2C_ITConfig(I2C1, (I2C_IT_TypeDef)(I2C_IT_EVT | I2C_IT_BUF) , ENABLE);
	
	I2C_Cmd(I2C1, ENABLE);
}

void SPI_Send(uint8_t* arrData, uint8_t arrCount)
{
	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
	while(arrCount--)
	{
		while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
		SPI_SendData(SPI1, *arrData++);
	}
	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
}

uint8_t SPI_Transceive(uint8_t TransmitData)
{
	uint8_t ReceiveData;
	
	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
	SPI_SendData(SPI1, TransmitData);
	//while(SPI_GetFlagStatus(SPI1, SPI_IT_RXNE));
	ReceiveData = SPI_ReceiveData(SPI1);
	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
	
	return ReceiveData;
}

void SPI_SimpleSend(uint8_t arrData)
{
	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
	SPI_SendData(SPI1, arrData);
	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
}

void TIM2_config(void)
{
	TIM2_DeInit();	
	/**********************************
	 * ������������� �������-�������� *
	 **********************************/
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);	
	TIM2_TimeBaseInit(TIM2_Prescaler_128, TIM2_CounterMode_Up, 31250);
	TIM2_ARRPreloadConfig(ENABLE);		
	TIM2_ITConfig(TIM2_IT_Update, ENABLE);
	
	TIM2_Cmd(ENABLE);
}

//uint8_t blockAddr = 4;
//uint8_t buffer[18];
//uint8_t size = sizeof(buffer);

int main()
{
	int i = 0;
	uint8_t block;
	uint8_t len;
	StatusCode status;
	uint8_t buffer1[18];
	uint8_t buffer2[18];
	
	enableInterrupts();
	CLK_config();
	TIM2_config();
	GPIO_config();
	//ITC_config();
	//EXTI_config();
	SPI_config();
	//I2C_config();
	
	MFRC_Init();
	delay_us(1);

	
	while (1) 
	{
					// Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
		MIFARE_Key key;
		for (i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
		
		/*some variables we need
		uint8_t block;
		uint8_t len;
		StatusCode status;*/
		
		//-------------------------------------------
		
		// Look for new cards
		if ( !  PICC_IsNewCardPresent()) {
			return;
		}
		
		// Select one of the cards
		if ( !  PICC_ReadCardSerial()) {
			return;
		}
		
		//Serial.println(F("**Card Detected:**"));
		GPIO_SetBits(GPIOC, GPIO_Pin_7);	// BLUE LED
		
		//-------------------------------------------
		
		 PICC_DumpDetailsToSerial(&( uid)); //dump some details about the card
		
		// PICC_DumpToSerial(&( uid));      //uncomment this to see all blocks in hex
		
		//-------------------------------------------
		
		//Serial.print(F("Name: "));
		
		//uint8_t buffer1[18];
		
		block = 4;
		len = 18;
		
		//------------------------------------------- GET FIRST NAME
		status =  PCD_Authenticate( PICC_CMD_MF_AUTH_KEY_A, 4, &key, &( uid)); //line 834 of  cpp file
		if (status !=  STATUS_OK) {
			//Serial.print(F("Authentication failed: "));
			//Serial.println( GetStatusCodeName(status));
			return;
		}
		
		status =  MIFARE_Read(block, buffer1, &len);
		if (status !=  STATUS_OK) {
			//Serial.print(F("Reading failed: "));
			//Serial.println( GetStatusCodeName(status));
			return;
		}
		
		//PRINT FIRST NAME
		for (i = 0; i < 16; i++)
		{
			if (buffer1[i] != 32)
			{
				//Serial.write(buffer1[i]);
			}
		}
		//Serial.print(" ");
		
		//---------------------------------------- GET LAST NAME
		
		//byte buffer2[18];
		block = 1;
		
		status =  PCD_Authenticate( PICC_CMD_MF_AUTH_KEY_A, 1, &key, &( uid)); //line 834
		if (status !=  STATUS_OK) {
			//Serial.print(F("Authentication failed: "));
			//Serial.println( GetStatusCodeName(status));
			return;
		}
		
		status =  MIFARE_Read(block, buffer2, &len);
		if (status !=  STATUS_OK) {
			//Serial.print(F("Reading failed: "));
			//Serial.println( GetStatusCodeName(status));
			return;
		}
		
		/*PRINT LAST NAME
		for (uint8_t i = 0; i < 16; i++) {
			Serial.write(buffer2[i] );
		}*/
		
		
		//----------------------------------------
		
		//Serial.println(F("\n**End Reading**\n"));
		GPIO_ResetBits(GPIOC, GPIO_Pin_7);	// BLUE LED
		
		delay_us(20); //change value if you want to read cards faster
		
		 PICC_HaltA();
		 PCD_StopCrypto1();
	}
}


#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) *
  /* Infinite loop */
  while (1) {}
}
#endif