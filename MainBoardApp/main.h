#ifndef MAIN_H
#define MAIN_H

#include "stdlib.h"
#include "stdio.h"
#include "stm8l15x.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_dma.h"
#include "stm8l15x_gpio.h"
#include "stm8l15x_it.h"
#include "stm8l15x_itc.h"
#include "stm8l15x_i2c.h"
#include "stm8l15x_spi.h"
#include "mfrc_spi1.h"

#define I2C_SPEED 100000
#define SLAVE_ADDRESS  0x30
#define RFID_BUF_LEN 25

#define MFRC_CS_Pin 	GPIO_Pin_4
#define MFRC_RST_Pin 	GPIO_Pin_5

void 		CLK_config(void);
void 		GPIO_config(void);
void 		SoftResetRFID(void);
void 		EXTI_config(void);
void 		ITC_config(void);
void 		SPI_config(void);
void 		I2C_config(void);
void 		TIM2_config(void);
void 		SPI_Send(uint8_t* arrData, uint8_t arrCount);
void 		SPI_SimpleSend(uint8_t arrData);
uint8_t SPI_Transceive(uint8_t TransmitData);
void 		RFID_readBuffer(void);
void 		RFID_test(void);

int main();
void delay_us(uint32_t del_us);

#endif
