   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
 424                     ; 11 void MFRC_WriteReg(MfrcRegTypedef MfrcReg, uint8_t value)
 424                     ; 12 {
 426                     	switch	.text
 427  0000               _MFRC_WriteReg:
 429  0000 89            	pushw	x
 430       00000000      OFST:	set	0
 433                     ; 13 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 435  0001 4b10          	push	#16
 436  0003 ae500f        	ldw	x,#20495
 437  0006 cd0000        	call	_GPIO_ResetBits
 439  0009 84            	pop	a
 441  000a               L702:
 442                     ; 15 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 444  000a 4b02          	push	#2
 445  000c ae5200        	ldw	x,#20992
 446  000f cd0000        	call	_SPI_GetFlagStatus
 448  0012 5b01          	addw	sp,#1
 449  0014 4d            	tnz	a
 450  0015 27f3          	jreq	L702
 451                     ; 16 	SPI_SendData(SPI1, MfrcReg);
 453  0017 7b01          	ld	a,(OFST+1,sp)
 454  0019 88            	push	a
 455  001a ae5200        	ldw	x,#20992
 456  001d cd0000        	call	_SPI_SendData
 458  0020 84            	pop	a
 460  0021               L512:
 461                     ; 17 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 463  0021 4b02          	push	#2
 464  0023 ae5200        	ldw	x,#20992
 465  0026 cd0000        	call	_SPI_GetFlagStatus
 467  0029 5b01          	addw	sp,#1
 468  002b 4d            	tnz	a
 469  002c 27f3          	jreq	L512
 470                     ; 18 	SPI_SendData(SPI1, value);
 472  002e 7b02          	ld	a,(OFST+2,sp)
 473  0030 88            	push	a
 474  0031 ae5200        	ldw	x,#20992
 475  0034 cd0000        	call	_SPI_SendData
 477  0037 84            	pop	a
 479  0038               L322:
 480                     ; 20 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 482  0038 4b80          	push	#128
 483  003a ae5200        	ldw	x,#20992
 484  003d cd0000        	call	_SPI_GetFlagStatus
 486  0040 5b01          	addw	sp,#1
 487  0042 4d            	tnz	a
 488  0043 26f3          	jrne	L322
 489                     ; 21 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 491  0045 4b10          	push	#16
 492  0047 ae500f        	ldw	x,#20495
 493  004a cd0000        	call	_GPIO_SetBits
 495  004d 84            	pop	a
 496                     ; 22 }
 499  004e 85            	popw	x
 500  004f 81            	ret
 558                     ; 28 void MFRC_WriteRegs(MfrcRegTypedef MfrcReg, uint8_t *values, uint8_t count)
 558                     ; 29 {
 559                     	switch	.text
 560  0050               _MFRC_WriteRegs:
 562  0050 88            	push	a
 563       00000000      OFST:	set	0
 566                     ; 30 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 568  0051 4b10          	push	#16
 569  0053 ae500f        	ldw	x,#20495
 570  0056 cd0000        	call	_GPIO_ResetBits
 572  0059 84            	pop	a
 574  005a               L752:
 575                     ; 31 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 577  005a 4b02          	push	#2
 578  005c ae5200        	ldw	x,#20992
 579  005f cd0000        	call	_SPI_GetFlagStatus
 581  0062 5b01          	addw	sp,#1
 582  0064 4d            	tnz	a
 583  0065 27f3          	jreq	L752
 584                     ; 32 	SPI_SendData(SPI1, MfrcReg);
 586  0067 7b01          	ld	a,(OFST+1,sp)
 587  0069 88            	push	a
 588  006a ae5200        	ldw	x,#20992
 589  006d cd0000        	call	_SPI_SendData
 591  0070 84            	pop	a
 593  0071 2020          	jra	L562
 594  0073               L372:
 595                     ; 35 		while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 597  0073 4b02          	push	#2
 598  0075 ae5200        	ldw	x,#20992
 599  0078 cd0000        	call	_SPI_GetFlagStatus
 601  007b 5b01          	addw	sp,#1
 602  007d 4d            	tnz	a
 603  007e 27f3          	jreq	L372
 604                     ; 36 		SPI_SendData(SPI1, *values++);
 606  0080 1e04          	ldw	x,(OFST+4,sp)
 607  0082 1c0001        	addw	x,#1
 608  0085 1f04          	ldw	(OFST+4,sp),x
 609  0087 1d0001        	subw	x,#1
 610  008a f6            	ld	a,(x)
 611  008b 88            	push	a
 612  008c ae5200        	ldw	x,#20992
 613  008f cd0000        	call	_SPI_SendData
 615  0092 84            	pop	a
 616  0093               L562:
 617                     ; 33 	while(count--)
 619  0093 7b06          	ld	a,(OFST+6,sp)
 620  0095 0a06          	dec	(OFST+6,sp)
 621  0097 4d            	tnz	a
 622  0098 26d9          	jrne	L372
 624  009a               L103:
 625                     ; 38 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 627  009a 4b80          	push	#128
 628  009c ae5200        	ldw	x,#20992
 629  009f cd0000        	call	_SPI_GetFlagStatus
 631  00a2 5b01          	addw	sp,#1
 632  00a4 4d            	tnz	a
 633  00a5 26f3          	jrne	L103
 634                     ; 39 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 636  00a7 4b10          	push	#16
 637  00a9 ae500f        	ldw	x,#20495
 638  00ac cd0000        	call	_GPIO_SetBits
 640  00af 84            	pop	a
 641                     ; 40 }
 644  00b0 84            	pop	a
 645  00b1 81            	ret
 694                     ; 46 uint8_t MFRC_ReadReg(MfrcRegTypedef MfrcReg)
 694                     ; 47 {
 695                     	switch	.text
 696  00b2               _MFRC_ReadReg:
 698  00b2 88            	push	a
 699  00b3 88            	push	a
 700       00000001      OFST:	set	1
 703                     ; 49 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 705  00b4 4b10          	push	#16
 706  00b6 ae500f        	ldw	x,#20495
 707  00b9 cd0000        	call	_GPIO_ResetBits
 709  00bc 84            	pop	a
 711  00bd               L133:
 712                     ; 50 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 714  00bd 4b02          	push	#2
 715  00bf ae5200        	ldw	x,#20992
 716  00c2 cd0000        	call	_SPI_GetFlagStatus
 718  00c5 5b01          	addw	sp,#1
 719  00c7 4d            	tnz	a
 720  00c8 27f3          	jreq	L133
 721                     ; 51 	SPI_SendData(SPI1, MfrcReg);
 723  00ca 7b02          	ld	a,(OFST+1,sp)
 724  00cc 88            	push	a
 725  00cd ae5200        	ldw	x,#20992
 726  00d0 cd0000        	call	_SPI_SendData
 728  00d3 84            	pop	a
 729                     ; 52 	data = SPI_ReceiveData(SPI1);
 731  00d4 ae5200        	ldw	x,#20992
 732  00d7 cd0000        	call	_SPI_ReceiveData
 734  00da 6b01          	ld	(OFST+0,sp),a
 737  00dc               L143:
 738                     ; 53 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 740  00dc 4b80          	push	#128
 741  00de ae5200        	ldw	x,#20992
 742  00e1 cd0000        	call	_SPI_GetFlagStatus
 744  00e4 5b01          	addw	sp,#1
 745  00e6 4d            	tnz	a
 746  00e7 26f3          	jrne	L143
 747                     ; 54 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 749  00e9 4b10          	push	#16
 750  00eb ae500f        	ldw	x,#20495
 751  00ee cd0000        	call	_GPIO_SetBits
 753  00f1 84            	pop	a
 754                     ; 55 	return data;
 756  00f2 7b01          	ld	a,(OFST+0,sp)
 759  00f4 85            	popw	x
 760  00f5 81            	ret
 763                     	bsct
 764  0000               L543_address:
 765  0000 00            	dc.b	0
 766  0001               L743_index:
 767  0001 00            	dc.b	0
 768                     	switch	.ubsct
 769  0000               L153_mask:
 770  0000 00            	ds.b	1
 771  0001               L353_value:
 772  0001 00            	ds.b	1
 874                     ; 62 void MFRC_ReadRegs(MfrcRegTypedef MfrcReg, uint8_t count, uint8_t* values, uint8_t rxAlign)
 874                     ; 63 {
 875                     	switch	.text
 876  00f6               _MFRC_ReadRegs:
 878  00f6 89            	pushw	x
 879  00f7 88            	push	a
 880       00000001      OFST:	set	1
 883                     ; 68 	rxAlign = 0;
 885  00f8 0f08          	clr	(OFST+7,sp)
 886                     ; 70 	if (count == 0) {
 888  00fa 9f            	ld	a,xl
 889  00fb 4d            	tnz	a
 890  00fc 2603          	jrne	L62
 891  00fe cc01a3        	jp	L42
 892  0101               L62:
 893                     ; 71 		return;
 895                     ; 74 	address = 0x80 | MfrcReg;				// MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
 897  0101 7b02          	ld	a,(OFST+1,sp)
 898  0103 aa80          	or	a,#128
 899  0105 b700          	ld	L543_address,a
 900                     ; 75 	index = 0;							// Index in values array.
 902  0107 3f01          	clr	L743_index
 903                     ; 77 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin); //digitalWrite(_chipSelectPin, LOW);		// Select slave
 905  0109 4b10          	push	#16
 906  010b ae500f        	ldw	x,#20495
 907  010e cd0000        	call	_GPIO_ResetBits
 909  0111 84            	pop	a
 910                     ; 78 	count--;								// One read is performed outside of the loop
 912  0112 0a03          	dec	(OFST+2,sp)
 914  0114               L334:
 915                     ; 80 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 917  0114 4b02          	push	#2
 918  0116 ae5200        	ldw	x,#20992
 919  0119 cd0000        	call	_SPI_GetFlagStatus
 921  011c 5b01          	addw	sp,#1
 922  011e 4d            	tnz	a
 923  011f 27f3          	jreq	L334
 924                     ; 81 	SPI_SendData(SPI1, address);
 926  0121 3b0000        	push	L543_address
 927  0124 ae5200        	ldw	x,#20992
 928  0127 cd0000        	call	_SPI_SendData
 930  012a 84            	pop	a
 931                     ; 83 	if (rxAlign) {		// Only update bit positions rxAlign..7 in values[0]
 933  012b 0d08          	tnz	(OFST+7,sp)
 934  012d 2753          	jreq	L344
 935                     ; 85 		mask = (0xFF << rxAlign) & 0xFF;
 937  012f 7b08          	ld	a,(OFST+7,sp)
 938  0131 5f            	clrw	x
 939  0132 97            	ld	xl,a
 940  0133 a6ff          	ld	a,#255
 941  0135 5d            	tnzw	x
 942  0136 2704          	jreq	L41
 943  0138               L61:
 944  0138 48            	sll	a
 945  0139 5a            	decw	x
 946  013a 26fc          	jrne	L61
 947  013c               L41:
 948  013c a4ff          	and	a,#255
 949  013e b700          	ld	L153_mask,a
 950                     ; 87 		value = SPI_Transceive(address);
 952  0140 b600          	ld	a,L543_address
 953  0142 cd0000        	call	_SPI_Transceive
 955  0145 b701          	ld	L353_value,a
 956                     ; 89 		values[0] = (values[0] & ~mask) | (value & mask);
 958  0147 b601          	ld	a,L353_value
 959  0149 b400          	and	a,L153_mask
 960  014b 6b01          	ld	(OFST+0,sp),a
 962  014d b600          	ld	a,L153_mask
 963  014f 43            	cpl	a
 964  0150 1e06          	ldw	x,(OFST+5,sp)
 965  0152 f4            	and	a,(x)
 966  0153 1a01          	or	a,(OFST+0,sp)
 967  0155 1e06          	ldw	x,(OFST+5,sp)
 968  0157 f7            	ld	(x),a
 969                     ; 90 		values[0] = (values[0] & ~mask) | (value & mask);
 971  0158 b601          	ld	a,L353_value
 972  015a b400          	and	a,L153_mask
 973  015c 6b01          	ld	(OFST+0,sp),a
 975  015e b600          	ld	a,L153_mask
 976  0160 43            	cpl	a
 977  0161 1e06          	ldw	x,(OFST+5,sp)
 978  0163 f4            	and	a,(x)
 979  0164 1a01          	or	a,(OFST+0,sp)
 980  0166 1e06          	ldw	x,(OFST+5,sp)
 981  0168 f7            	ld	(x),a
 982                     ; 91 		index++;
 984  0169 3c01          	inc	L743_index
 985  016b 2015          	jra	L344
 986  016d               L144:
 987                     ; 94 		values[index] = SPI_Transceive(address);	// Read value and tell that we want to read the same address again.
 989  016d 7b06          	ld	a,(OFST+5,sp)
 990  016f 97            	ld	xl,a
 991  0170 7b07          	ld	a,(OFST+6,sp)
 992  0172 bb01          	add	a,L743_index
 993  0174 2401          	jrnc	L02
 994  0176 5c            	incw	x
 995  0177               L02:
 996  0177 02            	rlwa	x,a
 997  0178 89            	pushw	x
 998  0179 b600          	ld	a,L543_address
 999  017b cd0000        	call	_SPI_Transceive
1001  017e 85            	popw	x
1002  017f f7            	ld	(x),a
1003                     ; 95 		index++;
1005  0180 3c01          	inc	L743_index
1006  0182               L344:
1007                     ; 93 	while (index < count) {
1009  0182 b601          	ld	a,L743_index
1010  0184 1103          	cp	a,(OFST+2,sp)
1011  0186 25e5          	jrult	L144
1012                     ; 97 	values[index] = SPI_Transceive(0);			// Read the final uint8_t. Send 0 to stop reading.
1014  0188 7b06          	ld	a,(OFST+5,sp)
1015  018a 97            	ld	xl,a
1016  018b 7b07          	ld	a,(OFST+6,sp)
1017  018d bb01          	add	a,L743_index
1018  018f 2401          	jrnc	L22
1019  0191 5c            	incw	x
1020  0192               L22:
1021  0192 02            	rlwa	x,a
1022  0193 89            	pushw	x
1023  0194 4f            	clr	a
1024  0195 cd0000        	call	_SPI_Transceive
1026  0198 85            	popw	x
1027  0199 f7            	ld	(x),a
1028                     ; 98 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
1030  019a 4b10          	push	#16
1031  019c ae500f        	ldw	x,#20495
1032  019f cd0000        	call	_GPIO_SetBits
1034  01a2 84            	pop	a
1035                     ; 100 }
1036  01a3               L42:
1039  01a3 5b03          	addw	sp,#3
1040  01a5 81            	ret
1095                     ; 105 void MFRC_SetRegBitMask(MfrcRegTypedef MfrcReg, uint8_t mask)
1095                     ; 106 {
1096                     	switch	.text
1097  01a6               _MFRC_SetRegBitMask:
1099  01a6 89            	pushw	x
1100  01a7 88            	push	a
1101       00000001      OFST:	set	1
1104                     ; 108 	tmp = MFRC_ReadReg(MfrcReg);
1106  01a8 9e            	ld	a,xh
1107  01a9 cd00b2        	call	_MFRC_ReadReg
1109  01ac 6b01          	ld	(OFST+0,sp),a
1111                     ; 109 	MFRC_WriteReg(MfrcReg, tmp & (~mask));		// clear bit mask
1113  01ae 7b03          	ld	a,(OFST+2,sp)
1114  01b0 43            	cpl	a
1115  01b1 1401          	and	a,(OFST+0,sp)
1116  01b3 97            	ld	xl,a
1117  01b4 7b02          	ld	a,(OFST+1,sp)
1118  01b6 95            	ld	xh,a
1119  01b7 cd0000        	call	_MFRC_WriteReg
1121                     ; 110 }
1124  01ba 5b03          	addw	sp,#3
1125  01bc 81            	ret
1181                     ; 115 void MFRC_ClearRegBitMask(MfrcRegTypedef MfrcReg, uint8_t mask)
1181                     ; 116 {
1182                     	switch	.text
1183  01bd               _MFRC_ClearRegBitMask:
1185  01bd 89            	pushw	x
1186  01be 88            	push	a
1187       00000001      OFST:	set	1
1190                     ; 118 	tmp = MFRC_ReadReg(MfrcReg);
1192  01bf 9e            	ld	a,xh
1193  01c0 cd00b2        	call	_MFRC_ReadReg
1195  01c3 6b01          	ld	(OFST+0,sp),a
1197                     ; 119 	MFRC_WriteReg(MfrcReg, tmp & (~mask));		// clear bit mask
1199  01c5 7b03          	ld	a,(OFST+2,sp)
1200  01c7 43            	cpl	a
1201  01c8 1401          	and	a,(OFST+0,sp)
1202  01ca 97            	ld	xl,a
1203  01cb 7b02          	ld	a,(OFST+1,sp)
1204  01cd 95            	ld	xh,a
1205  01ce cd0000        	call	_MFRC_WriteReg
1207                     ; 120 }
1210  01d1 5b03          	addw	sp,#3
1211  01d3 81            	ret
1357                     ; 127 StatusCode MFRC_CalculateCRC(uint8_t *data, uint8_t length, uint8_t* result)
1357                     ; 128 {
1358                     	switch	.text
1359  01d4               _MFRC_CalculateCRC:
1361  01d4 89            	pushw	x
1362  01d5 5203          	subw	sp,#3
1363       00000003      OFST:	set	3
1366                     ; 130 	MFRC_WriteReg(CommandReg, MFRC_Idle);		// Stop any active command.
1368  01d7 ae0200        	ldw	x,#512
1369  01da cd0000        	call	_MFRC_WriteReg
1371                     ; 131 	MFRC_WriteReg(DivIrqReg, 0x04);				// Clear the CRCIRq interrupt request bit
1373  01dd ae0a04        	ldw	x,#2564
1374  01e0 cd0000        	call	_MFRC_WriteReg
1376                     ; 132 	MFRC_WriteReg(FIFOLevelReg, 0x80);			// FlushBuffer = 1, FIFO initialization
1378  01e3 ae1480        	ldw	x,#5248
1379  01e6 cd0000        	call	_MFRC_WriteReg
1381                     ; 133 	MFRC_WriteRegs(FIFODataReg, data, length);	// Write data to the FIFO
1383  01e9 7b08          	ld	a,(OFST+5,sp)
1384  01eb 88            	push	a
1385  01ec 1e05          	ldw	x,(OFST+2,sp)
1386  01ee 89            	pushw	x
1387  01ef a612          	ld	a,#18
1388  01f1 cd0050        	call	_MFRC_WriteRegs
1390  01f4 5b03          	addw	sp,#3
1391                     ; 134 	MFRC_WriteReg(CommandReg, MFRC_CalcCRC);		// Start the calculation
1393  01f6 ae0203        	ldw	x,#515
1394  01f9 cd0000        	call	_MFRC_WriteReg
1396                     ; 140 	for (i = 5000; i > 0; i--) {
1398  01fc ae1388        	ldw	x,#5000
1399  01ff 1f02          	ldw	(OFST-1,sp),x
1401  0201               L706:
1402                     ; 142 		uint8_t n = MFRC_ReadReg(DivIrqReg);
1404  0201 a60a          	ld	a,#10
1405  0203 cd00b2        	call	_MFRC_ReadReg
1407  0206 6b01          	ld	(OFST-2,sp),a
1409                     ; 143 		if (n & 0x04) {									// CRCIRq bit set - calculation done
1411  0208 7b01          	ld	a,(OFST-2,sp)
1412  020a a504          	bcp	a,#4
1413  020c 271a          	jreq	L516
1414                     ; 144 			MFRC_WriteReg(CommandReg, MFRC_Idle);	// Stop calculating CRC for new content in the FIFO.
1416  020e ae0200        	ldw	x,#512
1417  0211 cd0000        	call	_MFRC_WriteReg
1419                     ; 146 			result[0] = MFRC_ReadReg(CRCResultRegL);
1421  0214 a644          	ld	a,#68
1422  0216 cd00b2        	call	_MFRC_ReadReg
1424  0219 1e09          	ldw	x,(OFST+6,sp)
1425  021b f7            	ld	(x),a
1426                     ; 147 			result[1] = MFRC_ReadReg(CRCResultRegH);
1428  021c a642          	ld	a,#66
1429  021e cd00b2        	call	_MFRC_ReadReg
1431  0221 1e09          	ldw	x,(OFST+6,sp)
1432  0223 e701          	ld	(1,x),a
1433                     ; 148 			return STATUS_OK;
1435  0225 4f            	clr	a
1437  0226 200d          	jra	L63
1438  0228               L516:
1439                     ; 140 	for (i = 5000; i > 0; i--) {
1441  0228 1e02          	ldw	x,(OFST-1,sp)
1442  022a 1d0001        	subw	x,#1
1443  022d 1f02          	ldw	(OFST-1,sp),x
1447  022f 1e02          	ldw	x,(OFST-1,sp)
1448  0231 26ce          	jrne	L706
1449                     ; 152 	return STATUS_TIMEOUT;
1451  0233 a603          	ld	a,#3
1453  0235               L63:
1455  0235 5b05          	addw	sp,#5
1456  0237 81            	ret
1517                     ; 162 void MFRC_Init(void) 
1517                     ; 163 {
1518                     	switch	.text
1519  0238               _MFRC_Init:
1521  0238 88            	push	a
1522       00000001      OFST:	set	1
1525                     ; 165 	hardReset = 0;
1527                     ; 168 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
1529  0239 4b10          	push	#16
1530  023b ae500f        	ldw	x,#20495
1531  023e cd0000        	call	_GPIO_SetBits
1533  0241 84            	pop	a
1534                     ; 176 			GPIO_ResetBits(GPIOD, MFRC_RST_Pin);		// Make shure we have a clean LOW state.
1536  0242 4b20          	push	#32
1537  0244 ae500f        	ldw	x,#20495
1538  0247 cd0000        	call	_GPIO_ResetBits
1540  024a 84            	pop	a
1541                     ; 178 			GPIO_SetBits(GPIOD, MFRC_RST_Pin);		// Exit power down mode. This triggers a hard reset.
1543  024b 4b20          	push	#32
1544  024d ae500f        	ldw	x,#20495
1545  0250 cd0000        	call	_GPIO_SetBits
1547  0253 84            	pop	a
1548                     ; 180 			delay_us(1); //delay(50);
1550  0254 ae0001        	ldw	x,#1
1551  0257 89            	pushw	x
1552  0258 ae0000        	ldw	x,#0
1553  025b 89            	pushw	x
1554  025c cd0000        	call	_delay_us
1556  025f 5b04          	addw	sp,#4
1557                     ; 181 			hardReset = 1;
1559                     ; 190 	MFRC_WriteReg(TxModeReg, 0x00);
1561  0261 ae2400        	ldw	x,#9216
1562  0264 cd0000        	call	_MFRC_WriteReg
1564                     ; 191 	MFRC_WriteReg(RxModeReg, 0x00);
1566  0267 ae2600        	ldw	x,#9728
1567  026a cd0000        	call	_MFRC_WriteReg
1569                     ; 193 	MFRC_WriteReg(ModWidthReg, 0x26);
1571  026d ae4826        	ldw	x,#18470
1572  0270 cd0000        	call	_MFRC_WriteReg
1574                     ; 198 	MFRC_WriteReg(TModeReg, 0x80);			// TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
1576  0273 ae5480        	ldw	x,#21632
1577  0276 cd0000        	call	_MFRC_WriteReg
1579                     ; 199 	MFRC_WriteReg(TPrescalerReg, 0xA9);		// TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25?s.
1581  0279 ae56a9        	ldw	x,#22185
1582  027c cd0000        	call	_MFRC_WriteReg
1584                     ; 200 	MFRC_WriteReg(TReloadRegH, 0x03);		// Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
1586  027f ae5803        	ldw	x,#22531
1587  0282 cd0000        	call	_MFRC_WriteReg
1589                     ; 201 	MFRC_WriteReg(TReloadRegL, 0xE8);
1591  0285 ae5ae8        	ldw	x,#23272
1592  0288 cd0000        	call	_MFRC_WriteReg
1594                     ; 203 	MFRC_WriteReg(TxASKReg, 0x40);		// Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
1596  028b ae2a40        	ldw	x,#10816
1597  028e cd0000        	call	_MFRC_WriteReg
1599                     ; 204 	MFRC_WriteReg(ModeReg, 0x3D);		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
1601  0291 ae223d        	ldw	x,#8765
1602  0294 cd0000        	call	_MFRC_WriteReg
1604                     ; 205 	MFRC_AntennaOn();						// Enable the antenna driver pins TX1 and TX2 (they were disabled by the reset)
1606  0297 ad1a          	call	_MFRC_AntennaOn
1608                     ; 206 } // End MFRC_Init()
1611  0299 84            	pop	a
1612  029a 81            	ret
1615                     	bsct
1616  0002               _count:
1617  0002 00            	dc.b	0
1641                     ; 213 void MFRC_Reset(void) 
1641                     ; 214 {
1642                     	switch	.text
1643  029b               _MFRC_Reset:
1647                     ; 215 	MFRC_WriteReg(CommandReg, MFRC_SoftReset);	// Issue the SoftReset command.
1649  029b ae020f        	ldw	x,#527
1650  029e cd0000        	call	_MFRC_WriteReg
1652  02a1               L756:
1653                     ; 223 	} while ((MFRC_ReadReg(CommandReg) & (1 << 4)) && (++count) < 3);
1655  02a1 a602          	ld	a,#2
1656  02a3 cd00b2        	call	_MFRC_ReadReg
1658  02a6 a510          	bcp	a,#16
1659  02a8 2708          	jreq	L366
1661  02aa 3c02          	inc	_count
1662  02ac b602          	ld	a,_count
1663  02ae a103          	cp	a,#3
1664  02b0 25ef          	jrult	L756
1665  02b2               L366:
1666                     ; 224 } // End MFRC_Reset()
1669  02b2 81            	ret
1705                     ; 230 void MFRC_AntennaOn() 
1705                     ; 231 {
1706                     	switch	.text
1707  02b3               _MFRC_AntennaOn:
1709  02b3 88            	push	a
1710       00000001      OFST:	set	1
1713                     ; 232 	uint8_t value = MFRC_ReadReg(TxControlReg);
1715  02b4 a628          	ld	a,#40
1716  02b6 cd00b2        	call	_MFRC_ReadReg
1718  02b9 6b01          	ld	(OFST+0,sp),a
1720                     ; 233 	if ((value & 0x03) != 0x03) {
1722  02bb 7b01          	ld	a,(OFST+0,sp)
1723  02bd a403          	and	a,#3
1724  02bf a103          	cp	a,#3
1725  02c1 270b          	jreq	L307
1726                     ; 234 		MFRC_WriteReg(TxControlReg, value | 0x03);
1728  02c3 7b01          	ld	a,(OFST+0,sp)
1729  02c5 aa03          	or	a,#3
1730  02c7 ae2800        	ldw	x,#10240
1731  02ca 97            	ld	xl,a
1732  02cb cd0000        	call	_MFRC_WriteReg
1734  02ce               L307:
1735                     ; 236 } // End MFRC_AntennaOn()
1738  02ce 84            	pop	a
1739  02cf 81            	ret
1844                     ; 244 StatusCode MFRC_TransceiveData(uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
1844                     ; 245 													uint8_t sendLen,		///< Number of uint8_ts to transfer to the FIFO.
1844                     ; 246 													uint8_t *backData,		///< nullptr or pointer to buffer if data should be read back after executing the command.
1844                     ; 247 													uint8_t *backLen,		///< In: Max number of uint8_ts to write to *backData. Out: The number of uint8_ts returned.
1844                     ; 248 													uint8_t *validBits,	///< In/Out: The number of valid bits in the last uint8_t. 0 for 8 valid bits. Default nullptr.
1844                     ; 249 													uint8_t rxAlign,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
1844                     ; 250 													bool checkCRC		///< In: True => The last two uint8_ts of the response is assumed to be a CRC_A that must be validated.
1844                     ; 251 								 ) 
1844                     ; 252 {
1845                     	switch	.text
1846  02d0               _MFRC_TransceiveData:
1848  02d0 89            	pushw	x
1849  02d1 88            	push	a
1850       00000001      OFST:	set	1
1853                     ; 254 	rxAlign = 0, 
1853                     ; 255 	checkCRC = 0;
1855  02d2 0f0d          	clr	(OFST+12,sp)
1856  02d4 0f0e          	clr	(OFST+13,sp)
1857                     ; 256 	*validBits = 0;
1859  02d6 1e0b          	ldw	x,(OFST+10,sp)
1860  02d8 7f            	clr	(x)
1861                     ; 258 	waitIRq = 0x30;		// RxIRq and IdleIRq
1863                     ; 259 	return MFRC_CommunicateWithPICC(MFRC_Transceive, waitIRq, sendData, sendLen, backData, backLen, validBits, rxAlign, checkCRC);
1865  02d9 4b00          	push	#0
1866  02db 4b00          	push	#0
1867  02dd 1e0d          	ldw	x,(OFST+12,sp)
1868  02df 89            	pushw	x
1869  02e0 1e0d          	ldw	x,(OFST+12,sp)
1870  02e2 89            	pushw	x
1871  02e3 1e0d          	ldw	x,(OFST+12,sp)
1872  02e5 89            	pushw	x
1873  02e6 7b0e          	ld	a,(OFST+13,sp)
1874  02e8 88            	push	a
1875  02e9 1e0b          	ldw	x,(OFST+10,sp)
1876  02eb 89            	pushw	x
1877  02ec ae0c30        	ldw	x,#3120
1878  02ef cd07ba        	call	_MFRC_CommunicateWithPICC
1880  02f2 5b0b          	addw	sp,#11
1883  02f4 5b03          	addw	sp,#3
1884  02f6 81            	ret
1931                     ; 268 StatusCode PICC_RequestA(uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
1931                     ; 269 												uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
1931                     ; 270 										) 
1931                     ; 271 {
1932                     	switch	.text
1933  02f7               _PICC_RequestA:
1935  02f7 89            	pushw	x
1936       00000000      OFST:	set	0
1939                     ; 272 	return PICC_REQA_or_WUPA(PICC_CMD_REQA, bufferATQA, bufferSize);
1941  02f8 1e05          	ldw	x,(OFST+5,sp)
1942  02fa 89            	pushw	x
1943  02fb 1e03          	ldw	x,(OFST+3,sp)
1944  02fd 89            	pushw	x
1945  02fe a626          	ld	a,#38
1946  0300 ad13          	call	_PICC_REQA_or_WUPA
1948  0302 5b04          	addw	sp,#4
1951  0304 85            	popw	x
1952  0305 81            	ret
1999                     ; 281 StatusCode PICC_WakeupA(	uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
1999                     ; 282 							uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
1999                     ; 283 										) 
1999                     ; 284 {
2000                     	switch	.text
2001  0306               _PICC_WakeupA:
2003  0306 89            	pushw	x
2004       00000000      OFST:	set	0
2007                     ; 285 	return PICC_REQA_or_WUPA(PICC_CMD_WUPA, bufferATQA, bufferSize);
2009  0307 1e05          	ldw	x,(OFST+5,sp)
2010  0309 89            	pushw	x
2011  030a 1e03          	ldw	x,(OFST+3,sp)
2012  030c 89            	pushw	x
2013  030d a652          	ld	a,#82
2014  030f ad04          	call	_PICC_REQA_or_WUPA
2016  0311 5b04          	addw	sp,#4
2019  0313 85            	popw	x
2020  0314 81            	ret
2096                     ; 294 StatusCode PICC_REQA_or_WUPA(	uint8_t command, 		///< The command to send - PICC_CMD_REQA or PICC_CMD_WUPA
2096                     ; 295 															uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
2096                     ; 296 															uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
2096                     ; 297 											) 
2096                     ; 298 {
2097                     	switch	.text
2098  0315               _PICC_REQA_or_WUPA:
2100  0315 88            	push	a
2101  0316 89            	pushw	x
2102       00000002      OFST:	set	2
2105                     ; 302 	if ((bufferATQA == 0) || (*bufferSize < 2)) {	// The ATQA response is 2 bytes long.
2107  0317 1e06          	ldw	x,(OFST+4,sp)
2108  0319 2707          	jreq	L3601
2110  031b 1e08          	ldw	x,(OFST+6,sp)
2111  031d f6            	ld	a,(x)
2112  031e a102          	cp	a,#2
2113  0320 2404          	jruge	L1601
2114  0322               L3601:
2115                     ; 303 		return STATUS_NO_ROOM;
2117  0322 a604          	ld	a,#4
2119  0324 202b          	jra	L65
2120  0326               L1601:
2121                     ; 305 	MFRC_ClearRegBitMask(CollReg, 0x80);		// ValuesAfterColl=1 => Bits received after collision are cleared.
2123  0326 ae1c80        	ldw	x,#7296
2124  0329 cd01bd        	call	_MFRC_ClearRegBitMask
2126                     ; 306 	validBits = 7;									// For REQA and WUPA we need the short frame format - transmit only 7 bits of the last (and only) byte. TxLastBits = BitFramingReg[2..0]
2128  032c a607          	ld	a,#7
2129  032e 6b01          	ld	(OFST-1,sp),a
2131                     ; 307 	status = MFRC_TransceiveData(&command, 1, bufferATQA, bufferSize, &validBits, 0, 0);
2133  0330 4b00          	push	#0
2134  0332 4b00          	push	#0
2135  0334 96            	ldw	x,sp
2136  0335 1c0003        	addw	x,#OFST+1
2137  0338 89            	pushw	x
2138  0339 1e0c          	ldw	x,(OFST+10,sp)
2139  033b 89            	pushw	x
2140  033c 1e0c          	ldw	x,(OFST+10,sp)
2141  033e 89            	pushw	x
2142  033f 4b01          	push	#1
2143  0341 96            	ldw	x,sp
2144  0342 1c000c        	addw	x,#OFST+10
2145  0345 ad89          	call	_MFRC_TransceiveData
2147  0347 5b09          	addw	sp,#9
2148  0349 6b02          	ld	(OFST+0,sp),a
2150                     ; 308 	if (status != STATUS_OK) {
2152  034b 0d02          	tnz	(OFST+0,sp)
2153  034d 2705          	jreq	L5601
2154                     ; 309 		return status;
2156  034f 7b02          	ld	a,(OFST+0,sp)
2158  0351               L65:
2160  0351 5b03          	addw	sp,#3
2161  0353 81            	ret
2162  0354               L5601:
2163                     ; 311 	if ((*bufferSize != 2) || (validBits != 0)) {		// ATQA must be exactly 16 bits.
2165  0354 1e08          	ldw	x,(OFST+6,sp)
2166  0356 f6            	ld	a,(x)
2167  0357 a102          	cp	a,#2
2168  0359 2604          	jrne	L1701
2170  035b 0d01          	tnz	(OFST-1,sp)
2171  035d 2704          	jreq	L7601
2172  035f               L1701:
2173                     ; 312 		return STATUS_ERROR;
2175  035f a601          	ld	a,#1
2177  0361 20ee          	jra	L65
2178  0363               L7601:
2179                     ; 314 	return STATUS_OK;
2181  0363 4f            	clr	a
2183  0364 20eb          	jra	L65
2453                     ; 334 StatusCode PICC_Select(	Uid_str *uid0,			///< Pointer to Uid struct. Normally output, but can also be used to supply a known UID.
2453                     ; 335 												uint8_t validBits		///< The number of known UID bits supplied in *uid. Normally 0. If set you must also supply uid->size.
2453                     ; 336 										 ) 
2453                     ; 337 {
2454                     	switch	.text
2455  0366               _PICC_Select:
2457  0366 89            	pushw	x
2458  0367 5218          	subw	sp,#24
2459       00000018      OFST:	set	24
2462                     ; 341 	uint8_t cascadeLevel = 1;
2464  0369 a601          	ld	a,#1
2465  036b 6b04          	ld	(OFST-20,sp),a
2467                     ; 382 	if (validBits > 80) {
2469  036d 7b1d          	ld	a,(OFST+5,sp)
2470  036f a151          	cp	a,#81
2471  0371 2504          	jrult	L3621
2472                     ; 383 		return STATUS_INVALID;
2474  0373 a606          	ld	a,#6
2476  0375 2019          	jra	L261
2477  0377               L3621:
2478                     ; 387 	MFRC_ClearRegBitMask(CollReg, 0x80);		// ValuesAfterColl=1 => Bits received after collision are cleared.
2480  0377 ae1c80        	ldw	x,#7296
2481  037a cd01bd        	call	_MFRC_ClearRegBitMask
2483                     ; 390 	uidComplete = 0;
2485  037d 0f03          	clr	(OFST-21,sp)
2488  037f acd206d2      	jpf	L7621
2489  0383               L5621:
2490                     ; 393 		switch (cascadeLevel) {
2492  0383 7b04          	ld	a,(OFST-20,sp)
2494                     ; 414 				break;
2496  0385 4a            	dec	a
2497  0386 270b          	jreq	L3701
2498  0388 4a            	dec	a
2499  0389 2722          	jreq	L5701
2500  038b 4a            	dec	a
2501  038c 273b          	jreq	L7701
2502  038e               L1011:
2503                     ; 412 			default:
2503                     ; 413 				return STATUS_INTERNAL_ERROR;
2505  038e a605          	ld	a,#5
2507  0390               L261:
2509  0390 5b1a          	addw	sp,#26
2510  0392 81            	ret
2511  0393               L3701:
2512                     ; 394 			case 1:
2512                     ; 395 				buffer[0] = PICC_CMD_SEL_CL1;
2514  0393 a693          	ld	a,#147
2515  0395 6b0f          	ld	(OFST-9,sp),a
2517                     ; 396 				uidIndex = 0;
2519  0397 0f07          	clr	(OFST-17,sp)
2521                     ; 397 				useCascadeTag = validBits && uid0->size > 4;	// When we know that the UID has more than 4 bytes
2523  0399 0d1d          	tnz	(OFST+5,sp)
2524  039b 270b          	jreq	L26
2525  039d 1e19          	ldw	x,(OFST+1,sp)
2526  039f f6            	ld	a,(x)
2527  03a0 a105          	cp	a,#5
2528  03a2 2504          	jrult	L26
2529  03a4 a601          	ld	a,#1
2530  03a6 2001          	jra	L46
2531  03a8               L26:
2532  03a8 4f            	clr	a
2533  03a9               L46:
2534  03a9 6b0d          	ld	(OFST-11,sp),a
2536                     ; 398 				break;
2538  03ab 2026          	jra	L5721
2539  03ad               L5701:
2540                     ; 400 			case 2:
2540                     ; 401 				buffer[0] = PICC_CMD_SEL_CL2;
2542  03ad a695          	ld	a,#149
2543  03af 6b0f          	ld	(OFST-9,sp),a
2545                     ; 402 				uidIndex = 3;
2547  03b1 a603          	ld	a,#3
2548  03b3 6b07          	ld	(OFST-17,sp),a
2550                     ; 403 				useCascadeTag = validBits && uid0->size > 7;	// When we know that the UID has more than 7 bytes
2552  03b5 0d1d          	tnz	(OFST+5,sp)
2553  03b7 270b          	jreq	L66
2554  03b9 1e19          	ldw	x,(OFST+1,sp)
2555  03bb f6            	ld	a,(x)
2556  03bc a108          	cp	a,#8
2557  03be 2504          	jrult	L66
2558  03c0 a601          	ld	a,#1
2559  03c2 2001          	jra	L07
2560  03c4               L66:
2561  03c4 4f            	clr	a
2562  03c5               L07:
2563  03c5 6b0d          	ld	(OFST-11,sp),a
2565                     ; 404 				break;
2567  03c7 200a          	jra	L5721
2568  03c9               L7701:
2569                     ; 406 			case 3:
2569                     ; 407 				buffer[0] = PICC_CMD_SEL_CL3;
2571  03c9 a697          	ld	a,#151
2572  03cb 6b0f          	ld	(OFST-9,sp),a
2574                     ; 408 				uidIndex = 6;
2576  03cd a606          	ld	a,#6
2577  03cf 6b07          	ld	(OFST-17,sp),a
2579                     ; 409 				useCascadeTag = 0;						// Never used in CL3.
2581  03d1 0f0d          	clr	(OFST-11,sp)
2583                     ; 410 				break;
2585  03d3               L5721:
2586                     ; 418 		currentLevelKnownBits = validBits - (8 * uidIndex);
2588  03d3 7b07          	ld	a,(OFST-17,sp)
2589  03d5 48            	sll	a
2590  03d6 48            	sll	a
2591  03d7 48            	sll	a
2592  03d8 101d          	sub	a,(OFST+5,sp)
2593  03da 40            	neg	a
2594  03db 6b0e          	ld	(OFST-10,sp),a
2596                     ; 419 		if (currentLevelKnownBits < 0) {
2598  03dd 9c            	rvf
2599  03de 7b0e          	ld	a,(OFST-10,sp)
2600  03e0 a100          	cp	a,#0
2601  03e2 2e02          	jrsge	L7721
2602                     ; 420 			currentLevelKnownBits = 0;
2604  03e4 0f0e          	clr	(OFST-10,sp)
2606  03e6               L7721:
2607                     ; 423 		index = 2; // destination index in buffer[]
2609  03e6 a602          	ld	a,#2
2610  03e8 6b18          	ld	(OFST+0,sp),a
2612                     ; 424 		if (useCascadeTag) {
2614  03ea 0d0d          	tnz	(OFST-11,sp)
2615  03ec 2714          	jreq	L1031
2616                     ; 425 			buffer[index++] = PICC_CMD_CT;
2618  03ee 96            	ldw	x,sp
2619  03ef 1c000f        	addw	x,#OFST-9
2620  03f2 1f01          	ldw	(OFST-23,sp),x
2622  03f4 7b18          	ld	a,(OFST+0,sp)
2623  03f6 97            	ld	xl,a
2624  03f7 0c18          	inc	(OFST+0,sp)
2626  03f9 9f            	ld	a,xl
2627  03fa 5f            	clrw	x
2628  03fb 97            	ld	xl,a
2629  03fc 72fb01        	addw	x,(OFST-23,sp)
2630  03ff a688          	ld	a,#136
2631  0401 f7            	ld	(x),a
2632  0402               L1031:
2633                     ; 427 		bytesToCopy = currentLevelKnownBits / 8 + (currentLevelKnownBits % 8 ? 1 : 0); // The number of bytes needed to represent the known bits for this level.
2635  0402 7b0e          	ld	a,(OFST-10,sp)
2636  0404 5f            	clrw	x
2637  0405 4d            	tnz	a
2638  0406 2a01          	jrpl	L47
2639  0408 53            	cplw	x
2640  0409               L47:
2641  0409 97            	ld	xl,a
2642  040a a608          	ld	a,#8
2643  040c cd0000        	call	c_smodx
2645  040f a30000        	cpw	x,#0
2646  0412 2704          	jreq	L27
2647  0414 a601          	ld	a,#1
2648  0416 2001          	jra	L67
2649  0418               L27:
2650  0418 4f            	clr	a
2651  0419               L67:
2652  0419 6b02          	ld	(OFST-22,sp),a
2654  041b 7b0e          	ld	a,(OFST-10,sp)
2655  041d ae0008        	ldw	x,#8
2656  0420 51            	exgw	x,y
2657  0421 5f            	clrw	x
2658  0422 4d            	tnz	a
2659  0423 2a01          	jrpl	L001
2660  0425 5a            	decw	x
2661  0426               L001:
2662  0426 02            	rlwa	x,a
2663  0427 cd0000        	call	c_idiv
2665  042a 9f            	ld	a,xl
2666  042b 1b02          	add	a,(OFST-22,sp)
2667  042d 6b0b          	ld	(OFST-13,sp),a
2669                     ; 428 		if (bytesToCopy) {
2671  042f 0d0b          	tnz	(OFST-13,sp)
2672  0431 2748          	jreq	L3031
2673                     ; 429 			maxBytes = useCascadeTag ? 3 : 4; // Max 4 bytes in each Cascade Level. Only 3 left if we use the Cascade Tag
2675  0433 0d0d          	tnz	(OFST-11,sp)
2676  0435 2704          	jreq	L201
2677  0437 a603          	ld	a,#3
2678  0439 2002          	jra	L401
2679  043b               L201:
2680  043b a604          	ld	a,#4
2681  043d               L401:
2682  043d 6b06          	ld	(OFST-18,sp),a
2684                     ; 430 			if (bytesToCopy > maxBytes) {
2686  043f 7b0b          	ld	a,(OFST-13,sp)
2687  0441 1106          	cp	a,(OFST-18,sp)
2688  0443 2304          	jrule	L5031
2689                     ; 431 				bytesToCopy = maxBytes;
2691  0445 7b06          	ld	a,(OFST-18,sp)
2692  0447 6b0b          	ld	(OFST-13,sp),a
2694  0449               L5031:
2695                     ; 433 			for (count = 0; count < bytesToCopy; count++) {
2697  0449 0f0c          	clr	(OFST-12,sp)
2700  044b 2028          	jra	L3131
2701  044d               L7031:
2702                     ; 434 				buffer[index++] = uid0->uidByte[uidIndex + count];
2704  044d 96            	ldw	x,sp
2705  044e 1c000f        	addw	x,#OFST-9
2706  0451 1f01          	ldw	(OFST-23,sp),x
2708  0453 7b18          	ld	a,(OFST+0,sp)
2709  0455 97            	ld	xl,a
2710  0456 0c18          	inc	(OFST+0,sp)
2712  0458 9f            	ld	a,xl
2713  0459 5f            	clrw	x
2714  045a 97            	ld	xl,a
2715  045b 72fb01        	addw	x,(OFST-23,sp)
2716  045e 89            	pushw	x
2717  045f 7b1b          	ld	a,(OFST+3,sp)
2718  0461 97            	ld	xl,a
2719  0462 7b1c          	ld	a,(OFST+4,sp)
2720  0464 1b0e          	add	a,(OFST-10,sp)
2721  0466 2401          	jrnc	L601
2722  0468 5c            	incw	x
2723  0469               L601:
2724  0469 1b09          	add	a,(OFST-15,sp)
2725  046b 2401          	jrnc	L011
2726  046d 5c            	incw	x
2727  046e               L011:
2728  046e 02            	rlwa	x,a
2729  046f e601          	ld	a,(1,x)
2730  0471 85            	popw	x
2731  0472 f7            	ld	(x),a
2732                     ; 433 			for (count = 0; count < bytesToCopy; count++) {
2734  0473 0c0c          	inc	(OFST-12,sp)
2736  0475               L3131:
2739  0475 7b0c          	ld	a,(OFST-12,sp)
2740  0477 110b          	cp	a,(OFST-13,sp)
2741  0479 25d2          	jrult	L7031
2742  047b               L3031:
2743                     ; 438 		if (useCascadeTag) {
2745  047b 0d0d          	tnz	(OFST-11,sp)
2746  047d 2706          	jreq	L7131
2747                     ; 439 			currentLevelKnownBits += 8;
2749  047f 7b0e          	ld	a,(OFST-10,sp)
2750  0481 ab08          	add	a,#8
2751  0483 6b0e          	ld	(OFST-10,sp),a
2753  0485               L7131:
2754                     ; 443 		selectDone = 0;
2756  0485 0f06          	clr	(OFST-18,sp)
2758  0487               L1231:
2759                     ; 446 			if (currentLevelKnownBits >= 32) { // All UID bits in this Cascade Level are known. This is a SELECT.
2761  0487 9c            	rvf
2762  0488 7b0e          	ld	a,(OFST-10,sp)
2763  048a a120          	cp	a,#32
2764  048c 2f3c          	jrslt	L7231
2765                     ; 448 				buffer[1] = 0x70; // NVB - Number of Valid Bits: Seven whole bytes
2767  048e a670          	ld	a,#112
2768  0490 6b10          	ld	(OFST-8,sp),a
2770                     ; 450 				buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5];
2772  0492 7b11          	ld	a,(OFST-7,sp)
2773  0494 1812          	xor	a,(OFST-6,sp)
2774  0496 1813          	xor	a,(OFST-5,sp)
2775  0498 1814          	xor	a,(OFST-4,sp)
2776  049a 6b15          	ld	(OFST-3,sp),a
2778                     ; 452 				result = MFRC_CalculateCRC(buffer, 7, &buffer[7]);
2780  049c 96            	ldw	x,sp
2781  049d 1c0016        	addw	x,#OFST-2
2782  04a0 89            	pushw	x
2783  04a1 4b07          	push	#7
2784  04a3 96            	ldw	x,sp
2785  04a4 1c0012        	addw	x,#OFST-6
2786  04a7 cd01d4        	call	_MFRC_CalculateCRC
2788  04aa 5b03          	addw	sp,#3
2789  04ac 6b18          	ld	(OFST+0,sp),a
2791                     ; 453 				if (result != STATUS_OK) {
2793  04ae 0d18          	tnz	(OFST+0,sp)
2794  04b0 2706          	jreq	L1331
2795                     ; 454 					return result;
2797  04b2 7b18          	ld	a,(OFST+0,sp)
2799  04b4 ac900390      	jpf	L261
2800  04b8               L1331:
2801                     ; 456 				txLastBits		= 0; // 0 => All 8 bits are valid.
2803  04b8 0f0a          	clr	(OFST-14,sp)
2805                     ; 457 				bufferUsed		= 9;
2807  04ba a609          	ld	a,#9
2808  04bc 6b0b          	ld	(OFST-13,sp),a
2810                     ; 459 				responseBuffer	= &buffer[6];
2812  04be 96            	ldw	x,sp
2813  04bf 1c0015        	addw	x,#OFST-3
2814  04c2 1f08          	ldw	(OFST-16,sp),x
2816                     ; 460 				responseLength	= 3;
2818  04c4 a603          	ld	a,#3
2819  04c6 6b05          	ld	(OFST-19,sp),a
2822  04c8 2058          	jra	L3331
2823  04ca               L7231:
2824                     ; 464 				txLastBits		= currentLevelKnownBits % 8;
2826  04ca 7b0e          	ld	a,(OFST-10,sp)
2827  04cc ae0008        	ldw	x,#8
2828  04cf 51            	exgw	x,y
2829  04d0 5f            	clrw	x
2830  04d1 4d            	tnz	a
2831  04d2 2a01          	jrpl	L211
2832  04d4 5a            	decw	x
2833  04d5               L211:
2834  04d5 02            	rlwa	x,a
2835  04d6 cd0000        	call	c_idiv
2837  04d9 909f          	ld	a,yl
2838  04db 6b0a          	ld	(OFST-14,sp),a
2840                     ; 465 				count			= currentLevelKnownBits / 8;	// Number of whole bytes in the UID part.
2842  04dd 7b0e          	ld	a,(OFST-10,sp)
2843  04df ae0008        	ldw	x,#8
2844  04e2 51            	exgw	x,y
2845  04e3 5f            	clrw	x
2846  04e4 4d            	tnz	a
2847  04e5 2a01          	jrpl	L411
2848  04e7 5a            	decw	x
2849  04e8               L411:
2850  04e8 02            	rlwa	x,a
2851  04e9 cd0000        	call	c_idiv
2853  04ec 9f            	ld	a,xl
2854  04ed 6b0c          	ld	(OFST-12,sp),a
2856                     ; 466 				index			= 2 + count;					// Number of whole bytes: SEL + NVB + UIDs
2858  04ef 7b0c          	ld	a,(OFST-12,sp)
2859  04f1 ab02          	add	a,#2
2860  04f3 6b18          	ld	(OFST+0,sp),a
2862                     ; 467 				buffer[1]		= (index << 4) + txLastBits;	// NVB - Number of Valid Bits
2864  04f5 7b18          	ld	a,(OFST+0,sp)
2865  04f7 97            	ld	xl,a
2866  04f8 a610          	ld	a,#16
2867  04fa 42            	mul	x,a
2868  04fb 9f            	ld	a,xl
2869  04fc 1b0a          	add	a,(OFST-14,sp)
2870  04fe 6b10          	ld	(OFST-8,sp),a
2872                     ; 468 				bufferUsed		= index + (txLastBits ? 1 : 0);
2874  0500 0d0a          	tnz	(OFST-14,sp)
2875  0502 2704          	jreq	L611
2876  0504 a601          	ld	a,#1
2877  0506 2001          	jra	L021
2878  0508               L611:
2879  0508 4f            	clr	a
2880  0509               L021:
2881  0509 1b18          	add	a,(OFST+0,sp)
2882  050b 6b0b          	ld	(OFST-13,sp),a
2884                     ; 470 				responseBuffer	= &buffer[index];
2886  050d 96            	ldw	x,sp
2887  050e 1c000f        	addw	x,#OFST-9
2888  0511 9f            	ld	a,xl
2889  0512 5e            	swapw	x
2890  0513 1b18          	add	a,(OFST+0,sp)
2891  0515 2401          	jrnc	L221
2892  0517 5c            	incw	x
2893  0518               L221:
2894  0518 02            	rlwa	x,a
2895  0519 1f08          	ldw	(OFST-16,sp),x
2896  051b 01            	rrwa	x,a
2898                     ; 471 				responseLength	= sizeof(buffer) - index;
2900  051c a609          	ld	a,#9
2901  051e 1018          	sub	a,(OFST+0,sp)
2902  0520 6b05          	ld	(OFST-19,sp),a
2904  0522               L3331:
2905                     ; 475 			rxAlign = txLastBits;											// Having a separate variable is overkill. But it makes the next line easier to read.
2907  0522 7b0a          	ld	a,(OFST-14,sp)
2908  0524 6b0d          	ld	(OFST-11,sp),a
2910                     ; 476 			MFRC_WriteReg(BitFramingReg, (rxAlign << 4) + txLastBits);	// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
2912  0526 7b0d          	ld	a,(OFST-11,sp)
2913  0528 97            	ld	xl,a
2914  0529 a610          	ld	a,#16
2915  052b 42            	mul	x,a
2916  052c 9f            	ld	a,xl
2917  052d 1b0a          	add	a,(OFST-14,sp)
2918  052f ae1a00        	ldw	x,#6656
2919  0532 97            	ld	xl,a
2920  0533 cd0000        	call	_MFRC_WriteReg
2922                     ; 479 			result = MFRC_TransceiveData(buffer, bufferUsed, responseBuffer, &responseLength, &txLastBits, rxAlign, 0);
2924  0536 4b00          	push	#0
2925  0538 7b0e          	ld	a,(OFST-10,sp)
2926  053a 88            	push	a
2927  053b 96            	ldw	x,sp
2928  053c 1c000c        	addw	x,#OFST-12
2929  053f 89            	pushw	x
2930  0540 96            	ldw	x,sp
2931  0541 1c0009        	addw	x,#OFST-15
2932  0544 89            	pushw	x
2933  0545 1e0e          	ldw	x,(OFST-10,sp)
2934  0547 89            	pushw	x
2935  0548 7b13          	ld	a,(OFST-5,sp)
2936  054a 88            	push	a
2937  054b 96            	ldw	x,sp
2938  054c 1c0018        	addw	x,#OFST+0
2939  054f cd02d0        	call	_MFRC_TransceiveData
2941  0552 5b09          	addw	sp,#9
2942  0554 6b18          	ld	(OFST+0,sp),a
2944                     ; 480 			if (result == STATUS_COLLISION) { // More than one PICC in the field => collision.
2946  0556 7b18          	ld	a,(OFST+0,sp)
2947  0558 a102          	cp	a,#2
2948  055a 2703          	jreq	L461
2949  055c cc0607        	jp	L5331
2950  055f               L461:
2951                     ; 481 				valueOfCollReg = MFRC_ReadReg(CollReg); // CollReg[7..0] bits are: ValuesAfterColl reserved CollPosNotValid CollPos[4:0]
2953  055f a61c          	ld	a,#28
2954  0561 cd00b2        	call	_MFRC_ReadReg
2956  0564 6b0d          	ld	(OFST-11,sp),a
2958                     ; 482 				if (valueOfCollReg & 0x20) { // CollPosNotValid
2960  0566 7b0d          	ld	a,(OFST-11,sp)
2961  0568 a520          	bcp	a,#32
2962  056a 2706          	jreq	L7331
2963                     ; 483 					return STATUS_COLLISION; // Without a valid collision position we cannot continue
2965  056c a602          	ld	a,#2
2967  056e ac900390      	jpf	L261
2968  0572               L7331:
2969                     ; 485 				collisionPos = valueOfCollReg & 0x1F; // Values 0-31, 0 means bit 32.
2971  0572 7b0d          	ld	a,(OFST-11,sp)
2972  0574 a41f          	and	a,#31
2973  0576 6b18          	ld	(OFST+0,sp),a
2975                     ; 486 				if (collisionPos == 0) {
2977  0578 0d18          	tnz	(OFST+0,sp)
2978  057a 2604          	jrne	L1431
2979                     ; 487 					collisionPos = 32;
2981  057c a620          	ld	a,#32
2982  057e 6b18          	ld	(OFST+0,sp),a
2984  0580               L1431:
2985                     ; 489 				if (collisionPos <= currentLevelKnownBits) { // No progress - should not happen 
2987  0580 9c            	rvf
2988  0581 7b18          	ld	a,(OFST+0,sp)
2989  0583 5f            	clrw	x
2990  0584 97            	ld	xl,a
2991  0585 7b0e          	ld	a,(OFST-10,sp)
2992  0587 905f          	clrw	y
2993  0589 4d            	tnz	a
2994  058a 2a02          	jrpl	L421
2995  058c 9053          	cplw	y
2996  058e               L421:
2997  058e 9097          	ld	yl,a
2998  0590 90bf00        	ldw	c_y,y
2999  0593 b300          	cpw	x,c_y
3000  0595 2c06          	jrsgt	L3431
3001                     ; 490 					return STATUS_INTERNAL_ERROR;
3003  0597 a605          	ld	a,#5
3005  0599 ac900390      	jpf	L261
3006  059d               L3431:
3007                     ; 493 				currentLevelKnownBits	= collisionPos;
3009  059d 7b18          	ld	a,(OFST+0,sp)
3010  059f 6b0e          	ld	(OFST-10,sp),a
3012                     ; 494 				count			= currentLevelKnownBits % 8; // The bit to modify
3014  05a1 7b0e          	ld	a,(OFST-10,sp)
3015  05a3 ae0008        	ldw	x,#8
3016  05a6 51            	exgw	x,y
3017  05a7 5f            	clrw	x
3018  05a8 4d            	tnz	a
3019  05a9 2a01          	jrpl	L621
3020  05ab 5a            	decw	x
3021  05ac               L621:
3022  05ac 02            	rlwa	x,a
3023  05ad cd0000        	call	c_idiv
3025  05b0 909f          	ld	a,yl
3026  05b2 6b0c          	ld	(OFST-12,sp),a
3028                     ; 495 				checkBit		= (currentLevelKnownBits - 1) % 8;
3030  05b4 7b0e          	ld	a,(OFST-10,sp)
3031  05b6 5f            	clrw	x
3032  05b7 4d            	tnz	a
3033  05b8 2a01          	jrpl	L031
3034  05ba 53            	cplw	x
3035  05bb               L031:
3036  05bb 97            	ld	xl,a
3037  05bc 5a            	decw	x
3038  05bd a608          	ld	a,#8
3039  05bf cd0000        	call	c_smodx
3041  05c2 01            	rrwa	x,a
3042  05c3 6b0d          	ld	(OFST-11,sp),a
3043  05c5 02            	rlwa	x,a
3045                     ; 496 				index			= 1 + (currentLevelKnownBits / 8) + (count ? 1 : 0); // First byte is index 0.
3047  05c6 0d0c          	tnz	(OFST-12,sp)
3048  05c8 2704          	jreq	L231
3049  05ca a601          	ld	a,#1
3050  05cc 2001          	jra	L431
3051  05ce               L231:
3052  05ce 4f            	clr	a
3053  05cf               L431:
3054  05cf 6b02          	ld	(OFST-22,sp),a
3056  05d1 7b0e          	ld	a,(OFST-10,sp)
3057  05d3 ae0008        	ldw	x,#8
3058  05d6 51            	exgw	x,y
3059  05d7 5f            	clrw	x
3060  05d8 4d            	tnz	a
3061  05d9 2a01          	jrpl	L631
3062  05db 5a            	decw	x
3063  05dc               L631:
3064  05dc 02            	rlwa	x,a
3065  05dd cd0000        	call	c_idiv
3067  05e0 9f            	ld	a,xl
3068  05e1 1b02          	add	a,(OFST-22,sp)
3069  05e3 4c            	inc	a
3070  05e4 6b18          	ld	(OFST+0,sp),a
3072                     ; 497 				buffer[index]	|= (1 << checkBit);
3074  05e6 96            	ldw	x,sp
3075  05e7 1c000f        	addw	x,#OFST-9
3076  05ea 9f            	ld	a,xl
3077  05eb 5e            	swapw	x
3078  05ec 1b18          	add	a,(OFST+0,sp)
3079  05ee 2401          	jrnc	L041
3080  05f0 5c            	incw	x
3081  05f1               L041:
3082  05f1 02            	rlwa	x,a
3083  05f2 7b0d          	ld	a,(OFST-11,sp)
3084  05f4 905f          	clrw	y
3085  05f6 9097          	ld	yl,a
3086  05f8 a601          	ld	a,#1
3087  05fa 905d          	tnzw	y
3088  05fc 2705          	jreq	L241
3089  05fe               L441:
3090  05fe 48            	sll	a
3091  05ff 905a          	decw	y
3092  0601 26fb          	jrne	L441
3093  0603               L241:
3094  0603 fa            	or	a,(x)
3095  0604 f7            	ld	(x),a
3097  0605 201b          	jra	L5431
3098  0607               L5331:
3099                     ; 499 			else if (result != STATUS_OK) {
3101  0607 0d18          	tnz	(OFST+0,sp)
3102  0609 2706          	jreq	L7431
3103                     ; 500 				return result;
3105  060b 7b18          	ld	a,(OFST+0,sp)
3107  060d ac900390      	jpf	L261
3108  0611               L7431:
3109                     ; 503 				if (currentLevelKnownBits >= 32) { // This was a SELECT.
3111  0611 9c            	rvf
3112  0612 7b0e          	ld	a,(OFST-10,sp)
3113  0614 a120          	cp	a,#32
3114  0616 2f06          	jrslt	L3531
3115                     ; 504 					selectDone = 1; // No more anticollision 
3117  0618 a601          	ld	a,#1
3118  061a 6b06          	ld	(OFST-18,sp),a
3121  061c 2004          	jra	L5431
3122  061e               L3531:
3123                     ; 509 					currentLevelKnownBits = 32;
3125  061e a620          	ld	a,#32
3126  0620 6b0e          	ld	(OFST-10,sp),a
3128  0622               L5431:
3129                     ; 444 		while (!selectDone) {
3131  0622 0d06          	tnz	(OFST-18,sp)
3132  0624 2603          	jrne	L661
3133  0626 cc0487        	jp	L1231
3134  0629               L661:
3135                     ; 518 		index			= (buffer[2] == PICC_CMD_CT) ? 3 : 2; // source index in buffer[]
3137  0629 7b11          	ld	a,(OFST-7,sp)
3138  062b a188          	cp	a,#136
3139  062d 2604          	jrne	L641
3140  062f a603          	ld	a,#3
3141  0631 2002          	jra	L051
3142  0633               L641:
3143  0633 a602          	ld	a,#2
3144  0635               L051:
3145  0635 6b18          	ld	(OFST+0,sp),a
3147                     ; 519 		bytesToCopy		= (buffer[2] == PICC_CMD_CT) ? 3 : 4;
3149  0637 7b11          	ld	a,(OFST-7,sp)
3150  0639 a188          	cp	a,#136
3151  063b 2604          	jrne	L251
3152  063d a603          	ld	a,#3
3153  063f 2002          	jra	L451
3154  0641               L251:
3155  0641 a604          	ld	a,#4
3156  0643               L451:
3157  0643 6b0b          	ld	(OFST-13,sp),a
3159                     ; 520 		for (count = 0; count < bytesToCopy; count++) {
3161  0645 0f0c          	clr	(OFST-12,sp)
3164  0647 202d          	jra	L3631
3165  0649               L7531:
3166                     ; 521 			uid0->uidByte[uidIndex + count] = buffer[index++];
3168  0649 7b19          	ld	a,(OFST+1,sp)
3169  064b 97            	ld	xl,a
3170  064c 7b1a          	ld	a,(OFST+2,sp)
3171  064e 1b0c          	add	a,(OFST-12,sp)
3172  0650 2401          	jrnc	L651
3173  0652 5c            	incw	x
3174  0653               L651:
3175  0653 1b07          	add	a,(OFST-17,sp)
3176  0655 2401          	jrnc	L061
3177  0657 5c            	incw	x
3178  0658               L061:
3179  0658 02            	rlwa	x,a
3180  0659 9096          	ldw	y,sp
3181  065b 72a9000f      	addw	y,#OFST-9
3182  065f 1701          	ldw	(OFST-23,sp),y
3184  0661 7b18          	ld	a,(OFST+0,sp)
3185  0663 9097          	ld	yl,a
3186  0665 0c18          	inc	(OFST+0,sp)
3188  0667 909f          	ld	a,yl
3189  0669 905f          	clrw	y
3190  066b 9097          	ld	yl,a
3191  066d 72f901        	addw	y,(OFST-23,sp)
3192  0670 90f6          	ld	a,(y)
3193  0672 e701          	ld	(1,x),a
3194                     ; 520 		for (count = 0; count < bytesToCopy; count++) {
3196  0674 0c0c          	inc	(OFST-12,sp)
3198  0676               L3631:
3201  0676 7b0c          	ld	a,(OFST-12,sp)
3202  0678 110b          	cp	a,(OFST-13,sp)
3203  067a 25cd          	jrult	L7531
3204                     ; 525 		if (responseLength != 3 || txLastBits != 0) { // SAK must be exactly 24 bits (1 byte + CRC_A).
3206  067c 7b05          	ld	a,(OFST-19,sp)
3207  067e a103          	cp	a,#3
3208  0680 2604          	jrne	L1731
3210  0682 0d0a          	tnz	(OFST-14,sp)
3211  0684 2706          	jreq	L7631
3212  0686               L1731:
3213                     ; 526 			return STATUS_ERROR;
3215  0686 a601          	ld	a,#1
3217  0688 ac900390      	jpf	L261
3218  068c               L7631:
3219                     ; 529 		result = MFRC_CalculateCRC(responseBuffer, 1, &buffer[2]);
3221  068c 96            	ldw	x,sp
3222  068d 1c0011        	addw	x,#OFST-7
3223  0690 89            	pushw	x
3224  0691 4b01          	push	#1
3225  0693 1e0b          	ldw	x,(OFST-13,sp)
3226  0695 cd01d4        	call	_MFRC_CalculateCRC
3228  0698 5b03          	addw	sp,#3
3229  069a 6b18          	ld	(OFST+0,sp),a
3231                     ; 530 		if (result != STATUS_OK) {
3233  069c 0d18          	tnz	(OFST+0,sp)
3234  069e 2706          	jreq	L3731
3235                     ; 531 			return result;
3237  06a0 7b18          	ld	a,(OFST+0,sp)
3239  06a2 ac900390      	jpf	L261
3240  06a6               L3731:
3241                     ; 533 		if ((buffer[2] != responseBuffer[1]) || (buffer[3] != responseBuffer[2])) {
3243  06a6 1e08          	ldw	x,(OFST-16,sp)
3244  06a8 e601          	ld	a,(1,x)
3245  06aa 1111          	cp	a,(OFST-7,sp)
3246  06ac 2608          	jrne	L7731
3248  06ae 1e08          	ldw	x,(OFST-16,sp)
3249  06b0 e602          	ld	a,(2,x)
3250  06b2 1112          	cp	a,(OFST-6,sp)
3251  06b4 2706          	jreq	L5731
3252  06b6               L7731:
3253                     ; 534 			return STATUS_CRC_WRONG;
3255  06b6 a607          	ld	a,#7
3257  06b8 ac900390      	jpf	L261
3258  06bc               L5731:
3259                     ; 536 		if (responseBuffer[0] & 0x04) { // Cascade bit set - UID not complete yes
3261  06bc 1e08          	ldw	x,(OFST-16,sp)
3262  06be f6            	ld	a,(x)
3263  06bf a504          	bcp	a,#4
3264  06c1 2704          	jreq	L1041
3265                     ; 537 			cascadeLevel++;
3267  06c3 0c04          	inc	(OFST-20,sp)
3270  06c5 200b          	jra	L7621
3271  06c7               L1041:
3272                     ; 540 			uidComplete = 1;
3274  06c7 a601          	ld	a,#1
3275  06c9 6b03          	ld	(OFST-21,sp),a
3277                     ; 541 			uid0->sak = responseBuffer[0];
3279  06cb 1e08          	ldw	x,(OFST-16,sp)
3280  06cd f6            	ld	a,(x)
3281  06ce 1e19          	ldw	x,(OFST+1,sp)
3282  06d0 e70b          	ld	(11,x),a
3283  06d2               L7621:
3284                     ; 391 	while (!uidComplete) {
3286  06d2 0d03          	tnz	(OFST-21,sp)
3287  06d4 2603          	jrne	L071
3288  06d6 cc0383        	jp	L5621
3289  06d9               L071:
3290                     ; 546 	uid0->size = 3 * cascadeLevel + 1;
3292  06d9 7b04          	ld	a,(OFST-20,sp)
3293  06db 97            	ld	xl,a
3294  06dc a603          	ld	a,#3
3295  06de 42            	mul	x,a
3296  06df 9f            	ld	a,xl
3297  06e0 4c            	inc	a
3298  06e1 1e19          	ldw	x,(OFST+1,sp)
3299  06e3 f7            	ld	(x),a
3300                     ; 548 	return STATUS_OK;
3302  06e4 4f            	clr	a
3304  06e5 ac900390      	jpf	L261
3352                     ; 556 StatusCode PICC_HaltA() 
3352                     ; 557 {
3353                     	switch	.text
3354  06e9               _PICC_HaltA:
3356  06e9 5205          	subw	sp,#5
3357       00000005      OFST:	set	5
3360                     ; 562 	buffer[0] = PICC_CMD_HLTA;
3362  06eb a650          	ld	a,#80
3363  06ed 6b01          	ld	(OFST-4,sp),a
3365                     ; 563 	buffer[1] = 0;
3367  06ef 0f02          	clr	(OFST-3,sp)
3369                     ; 565 	result = MFRC_CalculateCRC(buffer, 2, &buffer[2]);
3371  06f1 96            	ldw	x,sp
3372  06f2 1c0003        	addw	x,#OFST-2
3373  06f5 89            	pushw	x
3374  06f6 4b02          	push	#2
3375  06f8 96            	ldw	x,sp
3376  06f9 1c0004        	addw	x,#OFST-1
3377  06fc cd01d4        	call	_MFRC_CalculateCRC
3379  06ff 5b03          	addw	sp,#3
3380  0701 6b05          	ld	(OFST+0,sp),a
3382                     ; 566 	if (result != STATUS_OK) {
3384  0703 0d05          	tnz	(OFST+0,sp)
3385  0705 2704          	jreq	L7241
3386                     ; 567 	return result;
3388  0707 7b05          	ld	a,(OFST+0,sp)
3390  0709 201e          	jra	L471
3391  070b               L7241:
3392                     ; 574 	result = MFRC_TransceiveData(buffer, sizeof(buffer), 0, 0, 0, 0, 0);
3394  070b 4b00          	push	#0
3395  070d 4b00          	push	#0
3396  070f 5f            	clrw	x
3397  0710 89            	pushw	x
3398  0711 5f            	clrw	x
3399  0712 89            	pushw	x
3400  0713 5f            	clrw	x
3401  0714 89            	pushw	x
3402  0715 4b04          	push	#4
3403  0717 96            	ldw	x,sp
3404  0718 1c000a        	addw	x,#OFST+5
3405  071b cd02d0        	call	_MFRC_TransceiveData
3407  071e 5b09          	addw	sp,#9
3408  0720 6b05          	ld	(OFST+0,sp),a
3410                     ; 575 	if (result == STATUS_TIMEOUT) {
3412  0722 7b05          	ld	a,(OFST+0,sp)
3413  0724 a103          	cp	a,#3
3414  0726 2604          	jrne	L1341
3415                     ; 576 		return STATUS_OK;
3417  0728 4f            	clr	a
3419  0729               L471:
3421  0729 5b05          	addw	sp,#5
3422  072b 81            	ret
3423  072c               L1341:
3424                     ; 578 	if (result == STATUS_OK) { // That is ironically NOT ok in this case ;-)
3426  072c 0d05          	tnz	(OFST+0,sp)
3427  072e 2604          	jrne	L3341
3428                     ; 579 		return STATUS_ERROR;
3430  0730 a601          	ld	a,#1
3432  0732 20f5          	jra	L471
3433  0734               L3341:
3434                     ; 581 	return result;
3436  0734 7b05          	ld	a,(OFST+0,sp)
3438  0736 20f1          	jra	L471
3552                     ; 596 StatusCode PCD_Authenticate(uint8_t command,		///< PICC_CMD_MF_AUTH_KEY_A or PICC_CMD_MF_AUTH_KEY_B
3552                     ; 597 														uint8_t blockAddr, 	///< The block number. See numbering in the comments in the .h file.
3552                     ; 598 														MIFARE_Key *key,	///< Pointer to the Crypteo1 key to use (6 bytes)
3552                     ; 599 														Uid_str *uid0			///< Pointer to Uid struct. The first 4 bytes of the UID is used.
3552                     ; 600 											) {
3553                     	switch	.text
3554  0738               _PCD_Authenticate:
3556  0738 89            	pushw	x
3557  0739 520e          	subw	sp,#14
3558       0000000e      OFST:	set	14
3561                     ; 601 	uint8_t waitIRq = 0x10;		// IdleIRq
3563  073b a610          	ld	a,#16
3564  073d 6b01          	ld	(OFST-13,sp),a
3566                     ; 606 	sendData[0] = command;
3568  073f 9e            	ld	a,xh
3569  0740 6b02          	ld	(OFST-12,sp),a
3571                     ; 607 	sendData[1] = blockAddr;
3573  0742 9f            	ld	a,xl
3574  0743 6b03          	ld	(OFST-11,sp),a
3576                     ; 608 	for (i = 0; i < MF_KEY_SIZE; i++) {	// 6 key bytes
3578  0745 0f0e          	clr	(OFST+0,sp)
3580  0747               L7151:
3581                     ; 609 		sendData[2+i] = key->keyByte[i];
3583  0747 96            	ldw	x,sp
3584  0748 1c0004        	addw	x,#OFST-10
3585  074b 9f            	ld	a,xl
3586  074c 5e            	swapw	x
3587  074d 1b0e          	add	a,(OFST+0,sp)
3588  074f 2401          	jrnc	L002
3589  0751 5c            	incw	x
3590  0752               L002:
3591  0752 02            	rlwa	x,a
3592  0753 89            	pushw	x
3593  0754 7b15          	ld	a,(OFST+7,sp)
3594  0756 97            	ld	xl,a
3595  0757 7b16          	ld	a,(OFST+8,sp)
3596  0759 1b10          	add	a,(OFST+2,sp)
3597  075b 2401          	jrnc	L202
3598  075d 5c            	incw	x
3599  075e               L202:
3600  075e 02            	rlwa	x,a
3601  075f f6            	ld	a,(x)
3602  0760 85            	popw	x
3603  0761 f7            	ld	(x),a
3604                     ; 608 	for (i = 0; i < MF_KEY_SIZE; i++) {	// 6 key bytes
3606  0762 0c0e          	inc	(OFST+0,sp)
3610  0764 7b0e          	ld	a,(OFST+0,sp)
3611  0766 a106          	cp	a,#6
3612  0768 25dd          	jrult	L7151
3613                     ; 615 	for (i = 0; i < 4; i++) {				// The last 4 bytes of the UID
3615  076a 0f0e          	clr	(OFST+0,sp)
3617  076c               L5251:
3618                     ; 616 		sendData[8+i] = uid0->uidByte[i+uid0->size-4];
3620  076c 96            	ldw	x,sp
3621  076d 1c000a        	addw	x,#OFST-4
3622  0770 9f            	ld	a,xl
3623  0771 5e            	swapw	x
3624  0772 1b0e          	add	a,(OFST+0,sp)
3625  0774 2401          	jrnc	L402
3626  0776 5c            	incw	x
3627  0777               L402:
3628  0777 02            	rlwa	x,a
3629  0778 89            	pushw	x
3630  0779 1e17          	ldw	x,(OFST+9,sp)
3631  077b f6            	ld	a,(x)
3632  077c 5f            	clrw	x
3633  077d 1b10          	add	a,(OFST+2,sp)
3634  077f 2401          	jrnc	L602
3635  0781 5c            	incw	x
3636  0782               L602:
3637  0782 02            	rlwa	x,a
3638  0783 1d0004        	subw	x,#4
3639  0786 72fb17        	addw	x,(OFST+9,sp)
3640  0789 e601          	ld	a,(1,x)
3641  078b 85            	popw	x
3642  078c f7            	ld	(x),a
3643                     ; 615 	for (i = 0; i < 4; i++) {				// The last 4 bytes of the UID
3645  078d 0c0e          	inc	(OFST+0,sp)
3649  078f 7b0e          	ld	a,(OFST+0,sp)
3650  0791 a104          	cp	a,#4
3651  0793 25d7          	jrult	L5251
3652                     ; 620 	return MFRC_CommunicateWithPICC(MFRC_MFAuthent, waitIRq, &sendData[0], sizeof(sendData), 0, 0, 0, 0, 0);
3654  0795 4b00          	push	#0
3655  0797 4b00          	push	#0
3656  0799 5f            	clrw	x
3657  079a 89            	pushw	x
3658  079b 5f            	clrw	x
3659  079c 89            	pushw	x
3660  079d 5f            	clrw	x
3661  079e 89            	pushw	x
3662  079f 4b0c          	push	#12
3663  07a1 96            	ldw	x,sp
3664  07a2 1c000b        	addw	x,#OFST-3
3665  07a5 89            	pushw	x
3666  07a6 7b0c          	ld	a,(OFST-2,sp)
3667  07a8 ae0e00        	ldw	x,#3584
3668  07ab 97            	ld	xl,a
3669  07ac ad0c          	call	_MFRC_CommunicateWithPICC
3671  07ae 5b0b          	addw	sp,#11
3674  07b0 5b10          	addw	sp,#16
3675  07b2 81            	ret
3699                     ; 627 void PCD_StopCrypto1() {
3700                     	switch	.text
3701  07b3               _PCD_StopCrypto1:
3705                     ; 629 	MFRC_ClearRegBitMask(Status2Reg, 0x08); // Status2Reg[7..0] bits are: TempSensClear I2CForceHS reserved reserved MFCrypto1On ModemState[2:0]
3707  07b3 ae1008        	ldw	x,#4104
3708  07b6 cd01bd        	call	_MFRC_ClearRegBitMask
3710                     ; 630 } // End PCD_StopCrypto1()
3713  07b9 81            	ret
3915                     ; 632 StatusCode MFRC_CommunicateWithPICC(uint8_t command,		///< The command to execute. One of the MFRC_Command enums.
3915                     ; 633 													uint8_t waitIRq,		///< The bits in the ComIrqReg register that signals successful completion of the command.
3915                     ; 634 													uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
3915                     ; 635 													uint8_t sendLen,		///< Number of uint8_ts to transfer to the FIFO.
3915                     ; 636 													uint8_t *backData,		///< nullptr or pointer to buffer if data should be read back after executing the command.
3915                     ; 637 													uint8_t *backLen,		///< In: Max number of uint8_ts to write to *backData. Out: The number of uint8_ts returned.
3915                     ; 638 													uint8_t *validBits,	///< In/Out: The number of valid bits in the last uint8_t. 0 for 8 valid bits.
3915                     ; 639 													uint8_t rxAlign,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
3915                     ; 640 													bool checkCRC		///< In: True => The last two uint8_ts of the response is assumed to be a CRC_A that must be validated.
3915                     ; 641 									 ) 
3915                     ; 642 {
3916                     	switch	.text
3917  07ba               _MFRC_CommunicateWithPICC:
3919  07ba 89            	pushw	x
3920  07bb 5206          	subw	sp,#6
3921       00000006      OFST:	set	6
3924                     ; 649 	uint8_t txLastBits = validBits ? *validBits : 0;
3926  07bd 1e12          	ldw	x,(OFST+12,sp)
3927  07bf 2705          	jreq	L412
3928  07c1 1e12          	ldw	x,(OFST+12,sp)
3929  07c3 f6            	ld	a,(x)
3930  07c4 2001          	jra	L612
3931  07c6               L412:
3932  07c6 4f            	clr	a
3933  07c7               L612:
3934  07c7 6b05          	ld	(OFST-1,sp),a
3936                     ; 650 	uint8_t bitFraming = (rxAlign << 4) + txLastBits;		// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
3938  07c9 7b14          	ld	a,(OFST+14,sp)
3939  07cb 97            	ld	xl,a
3940  07cc a610          	ld	a,#16
3941  07ce 42            	mul	x,a
3942  07cf 9f            	ld	a,xl
3943  07d0 1b05          	add	a,(OFST-1,sp)
3944  07d2 6b05          	ld	(OFST-1,sp),a
3946                     ; 653 	*backData = 0;
3948  07d4 1e0e          	ldw	x,(OFST+8,sp)
3949  07d6 7f            	clr	(x)
3950                     ; 654 	*backLen = 0;
3952  07d7 1e10          	ldw	x,(OFST+10,sp)
3953  07d9 7f            	clr	(x)
3954                     ; 655 	*validBits = 0;
3956  07da 1e12          	ldw	x,(OFST+12,sp)
3957  07dc 7f            	clr	(x)
3958                     ; 656 	rxAlign = 0;
3960  07dd 0f14          	clr	(OFST+14,sp)
3961                     ; 657 	checkCRC = 0;
3963  07df 0f15          	clr	(OFST+15,sp)
3964                     ; 659 	MFRC_WriteReg(CommandReg, MFRC_Idle);			// Stop any active command.
3966  07e1 ae0200        	ldw	x,#512
3967  07e4 cd0000        	call	_MFRC_WriteReg
3969                     ; 660 	MFRC_WriteReg(ComIrqReg, 0x7F);					// Clear all seven interrupt request bits
3971  07e7 ae087f        	ldw	x,#2175
3972  07ea cd0000        	call	_MFRC_WriteReg
3974                     ; 661 	MFRC_WriteReg(FIFOLevelReg, 0x80);				// FlushBuffer = 1, FIFO initialization
3976  07ed ae1480        	ldw	x,#5248
3977  07f0 cd0000        	call	_MFRC_WriteReg
3979                     ; 662 	MFRC_WriteRegs(FIFODataReg, sendData, sendLen);	// Write sendData to the FIFO
3981  07f3 7b0d          	ld	a,(OFST+7,sp)
3982  07f5 88            	push	a
3983  07f6 1e0c          	ldw	x,(OFST+6,sp)
3984  07f8 89            	pushw	x
3985  07f9 a612          	ld	a,#18
3986  07fb cd0050        	call	_MFRC_WriteRegs
3988  07fe 5b03          	addw	sp,#3
3989                     ; 663 	MFRC_WriteReg(BitFramingReg, bitFraming);		// Bit adjustments
3991  0800 7b05          	ld	a,(OFST-1,sp)
3992  0802 ae1a00        	ldw	x,#6656
3993  0805 97            	ld	xl,a
3994  0806 cd0000        	call	_MFRC_WriteReg
3996                     ; 664 	MFRC_WriteReg(CommandReg, command);				// Execute the command
3998  0809 7b07          	ld	a,(OFST+1,sp)
3999  080b ae0200        	ldw	x,#512
4000  080e 97            	ld	xl,a
4001  080f cd0000        	call	_MFRC_WriteReg
4003                     ; 665 	if (command == MFRC_Transceive) {
4005  0812 7b07          	ld	a,(OFST+1,sp)
4006  0814 a10c          	cp	a,#12
4007  0816 2606          	jrne	L5661
4008                     ; 666 		MFRC_SetRegBitMask(BitFramingReg, 0x80);	// StartSend=1, transmission of data starts
4010  0818 ae1a80        	ldw	x,#6784
4011  081b cd01a6        	call	_MFRC_SetRegBitMask
4013  081e               L5661:
4014                     ; 673 	for (i = 2000; i > 0; i--) {
4016  081e ae07d0        	ldw	x,#2000
4017  0821 1f03          	ldw	(OFST-3,sp),x
4019  0823               L7661:
4020                     ; 674 		n = MFRC_ReadReg(ComIrqReg);	// ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq LoAlertIRq ErrIRq TimerIRq
4022  0823 a608          	ld	a,#8
4023  0825 cd00b2        	call	_MFRC_ReadReg
4025  0828 6b06          	ld	(OFST+0,sp),a
4027                     ; 675 		if (n & waitIRq) {					// One of the interrupts that signal success has been set.
4029  082a 7b06          	ld	a,(OFST+0,sp)
4030  082c 1508          	bcp	a,(OFST+2,sp)
4031  082e 2615          	jrne	L3761
4032                     ; 676 			break;
4034                     ; 678 		if (n & 0x01) {						// Timer interrupt - nothing received in 25ms
4036  0830 7b06          	ld	a,(OFST+0,sp)
4037  0832 a501          	bcp	a,#1
4038  0834 2704          	jreq	L7761
4039                     ; 679 			return STATUS_TIMEOUT;
4041  0836 a603          	ld	a,#3
4043  0838 2011          	jra	L022
4044  083a               L7761:
4045                     ; 673 	for (i = 2000; i > 0; i--) {
4047  083a 1e03          	ldw	x,(OFST-3,sp)
4048  083c 1d0001        	subw	x,#1
4049  083f 1f03          	ldw	(OFST-3,sp),x
4053  0841 1e03          	ldw	x,(OFST-3,sp)
4054  0843 26de          	jrne	L7661
4055  0845               L3761:
4056                     ; 683 	if (i == 0) {
4058  0845 1e03          	ldw	x,(OFST-3,sp)
4059  0847 2605          	jrne	L1071
4060                     ; 684 		return STATUS_TIMEOUT;
4062  0849 a603          	ld	a,#3
4064  084b               L022:
4066  084b 5b08          	addw	sp,#8
4067  084d 81            	ret
4068  084e               L1071:
4069                     ; 688 	errorRegValue = MFRC_ReadReg(ErrorReg); // ErrorReg[7..0] bits are: WrErr TempErr reserved BufferOvfl CollErr CRCErr ParityErr ProtocolErr
4071  084e a60c          	ld	a,#12
4072  0850 cd00b2        	call	_MFRC_ReadReg
4074  0853 6b05          	ld	(OFST-1,sp),a
4076                     ; 689 	if (errorRegValue & 0x13) {	 // BufferOvfl ParityErr ProtocolErr
4078  0855 7b05          	ld	a,(OFST-1,sp)
4079  0857 a513          	bcp	a,#19
4080  0859 2704          	jreq	L3071
4081                     ; 690 		return STATUS_ERROR;
4083  085b a601          	ld	a,#1
4085  085d 20ec          	jra	L022
4086  085f               L3071:
4087                     ; 693 	_validBits = 0;
4089  085f 0f06          	clr	(OFST+0,sp)
4091                     ; 696 	if (backData && backLen) {
4093  0861 1e0e          	ldw	x,(OFST+8,sp)
4094  0863 273e          	jreq	L5071
4096  0865 1e10          	ldw	x,(OFST+10,sp)
4097  0867 273a          	jreq	L5071
4098                     ; 697 		uint8_t n = MFRC_ReadReg(FIFOLevelReg);	// Number of uint8_ts in the FIFO
4100  0869 a614          	ld	a,#20
4101  086b cd00b2        	call	_MFRC_ReadReg
4103  086e 6b06          	ld	(OFST+0,sp),a
4105                     ; 698 		if (n > *backLen) {
4107  0870 1e10          	ldw	x,(OFST+10,sp)
4108  0872 f6            	ld	a,(x)
4109  0873 1106          	cp	a,(OFST+0,sp)
4110  0875 2404          	jruge	L7071
4111                     ; 699 			return STATUS_NO_ROOM;
4113  0877 a604          	ld	a,#4
4115  0879 20d0          	jra	L022
4116  087b               L7071:
4117                     ; 701 		*backLen = n;											// Number of uint8_ts returned
4119  087b 7b06          	ld	a,(OFST+0,sp)
4120  087d 1e10          	ldw	x,(OFST+10,sp)
4121  087f f7            	ld	(x),a
4122                     ; 702 		MFRC_ReadRegs(FIFODataReg, n, backData, rxAlign);	// Get received data from FIFO
4124  0880 7b14          	ld	a,(OFST+14,sp)
4125  0882 88            	push	a
4126  0883 1e0f          	ldw	x,(OFST+9,sp)
4127  0885 89            	pushw	x
4128  0886 7b09          	ld	a,(OFST+3,sp)
4129  0888 ae1200        	ldw	x,#4608
4130  088b 97            	ld	xl,a
4131  088c cd00f6        	call	_MFRC_ReadRegs
4133  088f 5b03          	addw	sp,#3
4134                     ; 703 		_validBits = MFRC_ReadReg(ControlReg) & 0x07;		// RxLastBits[2:0] indicates the number of valid bits in the last received uint8_t. If this value is 000b, the whole uint8_t is valid.
4136  0891 a618          	ld	a,#24
4137  0893 cd00b2        	call	_MFRC_ReadReg
4139  0896 a407          	and	a,#7
4140  0898 6b06          	ld	(OFST+0,sp),a
4142                     ; 704 		if (validBits) {
4144  089a 1e12          	ldw	x,(OFST+12,sp)
4145  089c 2705          	jreq	L5071
4146                     ; 705 			*validBits = _validBits;
4148  089e 7b06          	ld	a,(OFST+0,sp)
4149  08a0 1e12          	ldw	x,(OFST+12,sp)
4150  08a2 f7            	ld	(x),a
4151  08a3               L5071:
4152                     ; 710 	if (errorRegValue & 0x08) {		// CollErr
4154  08a3 7b05          	ld	a,(OFST-1,sp)
4155  08a5 a508          	bcp	a,#8
4156  08a7 2704          	jreq	L3171
4157                     ; 711 		return STATUS_COLLISION;
4159  08a9 a602          	ld	a,#2
4161  08ab 209e          	jra	L022
4162  08ad               L3171:
4163                     ; 715 	if (backData && backLen && checkCRC) {
4165  08ad 1e0e          	ldw	x,(OFST+8,sp)
4166  08af 276b          	jreq	L5171
4168  08b1 1e10          	ldw	x,(OFST+10,sp)
4169  08b3 2767          	jreq	L5171
4171  08b5 0d15          	tnz	(OFST+15,sp)
4172  08b7 2763          	jreq	L5171
4173                     ; 717 		if (*backLen == 1 && _validBits == 4) {
4175  08b9 1e10          	ldw	x,(OFST+10,sp)
4176  08bb f6            	ld	a,(x)
4177  08bc a101          	cp	a,#1
4178  08be 260a          	jrne	L7171
4180  08c0 7b06          	ld	a,(OFST+0,sp)
4181  08c2 a104          	cp	a,#4
4182  08c4 2604          	jrne	L7171
4183                     ; 718 			return STATUS_MIFARE_NACK;
4185  08c6 a6ff          	ld	a,#255
4187  08c8 2081          	jpf	L022
4188  08ca               L7171:
4189                     ; 721 		if (*backLen < 2 || _validBits != 0) {
4191  08ca 1e10          	ldw	x,(OFST+10,sp)
4192  08cc f6            	ld	a,(x)
4193  08cd a102          	cp	a,#2
4194  08cf 2504          	jrult	L3271
4196  08d1 0d06          	tnz	(OFST+0,sp)
4197  08d3 2706          	jreq	L1271
4198  08d5               L3271:
4199                     ; 722 			return STATUS_CRC_WRONG;
4201  08d5 a607          	ld	a,#7
4203  08d7 ac4b084b      	jpf	L022
4204  08db               L1271:
4205                     ; 725 		status = MFRC_CalculateCRC(&backData[0], *backLen - 2, &controlBuffer[0]);
4207  08db 96            	ldw	x,sp
4208  08dc 1c0001        	addw	x,#OFST-5
4209  08df 89            	pushw	x
4210  08e0 1e12          	ldw	x,(OFST+12,sp)
4211  08e2 f6            	ld	a,(x)
4212  08e3 a002          	sub	a,#2
4213  08e5 88            	push	a
4214  08e6 1e11          	ldw	x,(OFST+11,sp)
4215  08e8 cd01d4        	call	_MFRC_CalculateCRC
4217  08eb 5b03          	addw	sp,#3
4218  08ed 6b05          	ld	(OFST-1,sp),a
4220                     ; 726 		if (status != STATUS_OK) {
4222  08ef 0d05          	tnz	(OFST-1,sp)
4223  08f1 2706          	jreq	L5271
4224                     ; 727 			return status;
4226  08f3 7b05          	ld	a,(OFST-1,sp)
4228  08f5 ac4b084b      	jpf	L022
4229  08f9               L5271:
4230                     ; 729 		if ((backData[*backLen - 2] != controlBuffer[0]) || (backData[*backLen - 1] != controlBuffer[1])) {
4232  08f9 1e10          	ldw	x,(OFST+10,sp)
4233  08fb f6            	ld	a,(x)
4234  08fc 5f            	clrw	x
4235  08fd 97            	ld	xl,a
4236  08fe 5a            	decw	x
4237  08ff 5a            	decw	x
4238  0900 72fb0e        	addw	x,(OFST+8,sp)
4239  0903 f6            	ld	a,(x)
4240  0904 1101          	cp	a,(OFST-5,sp)
4241  0906 260e          	jrne	L1371
4243  0908 1e10          	ldw	x,(OFST+10,sp)
4244  090a f6            	ld	a,(x)
4245  090b 5f            	clrw	x
4246  090c 97            	ld	xl,a
4247  090d 5a            	decw	x
4248  090e 72fb0e        	addw	x,(OFST+8,sp)
4249  0911 f6            	ld	a,(x)
4250  0912 1102          	cp	a,(OFST-4,sp)
4251  0914 2706          	jreq	L5171
4252  0916               L1371:
4253                     ; 730 			return STATUS_CRC_WRONG;
4255  0916 a607          	ld	a,#7
4257  0918 ac4b084b      	jpf	L022
4258  091c               L5171:
4259                     ; 733 	return STATUS_OK;
4261  091c 4f            	clr	a
4263  091d ac4b084b      	jpf	L022
4330                     ; 736 StatusCode MIFARE_Read(uint8_t blockAddr, 	///< MIFARE Classic: The block (0-0xff) number. MIFARE Ultralight: The first page to return data from.
4330                     ; 737 											uint8_t *buffer,		///< The buffer to store the data in
4330                     ; 738 											uint8_t *bufferSize	///< Buffer size, at least 18 uint8_ts. Also number of uint8_ts returned if STATUS_OK.
4330                     ; 739 										) 
4330                     ; 740 {
4331                     	switch	.text
4332  0921               _MIFARE_Read:
4334  0921 88            	push	a
4335  0922 88            	push	a
4336       00000001      OFST:	set	1
4339                     ; 744 	if ((buffer == 0) || (*bufferSize < 18)) {
4341  0923 1e05          	ldw	x,(OFST+4,sp)
4342  0925 2707          	jreq	L7671
4344  0927 1e07          	ldw	x,(OFST+6,sp)
4345  0929 f6            	ld	a,(x)
4346  092a a112          	cp	a,#18
4347  092c 2404          	jruge	L5671
4348  092e               L7671:
4349                     ; 745 		return STATUS_NO_ROOM;
4351  092e a604          	ld	a,#4
4353  0930 2021          	jra	L422
4354  0932               L5671:
4355                     ; 749 	buffer[0] = PICC_CMD_MF_READ;
4357  0932 1e05          	ldw	x,(OFST+4,sp)
4358  0934 a630          	ld	a,#48
4359  0936 f7            	ld	(x),a
4360                     ; 750 	buffer[1] = blockAddr;
4362  0937 7b02          	ld	a,(OFST+1,sp)
4363  0939 1e05          	ldw	x,(OFST+4,sp)
4364  093b e701          	ld	(1,x),a
4365                     ; 752 	result = MFRC_CalculateCRC(buffer, 2, &buffer[2]);
4367  093d 1e05          	ldw	x,(OFST+4,sp)
4368  093f 5c            	incw	x
4369  0940 5c            	incw	x
4370  0941 89            	pushw	x
4371  0942 4b02          	push	#2
4372  0944 1e08          	ldw	x,(OFST+7,sp)
4373  0946 cd01d4        	call	_MFRC_CalculateCRC
4375  0949 5b03          	addw	sp,#3
4376  094b 6b01          	ld	(OFST+0,sp),a
4378                     ; 753 	if (result != STATUS_OK) {
4380  094d 0d01          	tnz	(OFST+0,sp)
4381  094f 2704          	jreq	L1771
4382                     ; 754 		return result;
4384  0951 7b01          	ld	a,(OFST+0,sp)
4386  0953               L422:
4388  0953 85            	popw	x
4389  0954 81            	ret
4390  0955               L1771:
4391                     ; 758 	return MFRC_TransceiveData(buffer, 4, buffer, bufferSize, 0, 0, 1);
4393  0955 4b01          	push	#1
4394  0957 4b00          	push	#0
4395  0959 5f            	clrw	x
4396  095a 89            	pushw	x
4397  095b 1e0b          	ldw	x,(OFST+10,sp)
4398  095d 89            	pushw	x
4399  095e 1e0b          	ldw	x,(OFST+10,sp)
4400  0960 89            	pushw	x
4401  0961 4b04          	push	#4
4402  0963 1e0e          	ldw	x,(OFST+13,sp)
4403  0965 cd02d0        	call	_MFRC_TransceiveData
4405  0968 5b09          	addw	sp,#9
4407  096a 20e7          	jra	L422
4534                     ; 766 PICC_Type PICC_GetType(uint8_t sak		///< The SAK byte returned from PICC_Select().
4534                     ; 767 										) {
4535                     	switch	.text
4536  096c               L3_PICC_GetType:
4538  096c 88            	push	a
4539       00000000      OFST:	set	0
4542                     ; 772 	sak &= 0x7F;
4544  096d 7b01          	ld	a,(OFST+1,sp)
4545  096f a47f          	and	a,#127
4546  0971 6b01          	ld	(OFST+1,sp),a
4547                     ; 773 	switch (sak) {
4549  0973 7b01          	ld	a,(OFST+1,sp)
4551                     ; 784 		default:	return PICC_TYPE_UNKNOWN;
4552  0975 4d            	tnz	a
4553  0976 2739          	jreq	L3002
4554  0978 4a            	dec	a
4555  0979 2740          	jreq	L7002
4556  097b a003          	sub	a,#3
4557  097d 271e          	jreq	L3771
4558  097f a004          	sub	a,#4
4559  0981 2724          	jreq	L7771
4560  0983 4a            	dec	a
4561  0984 271c          	jreq	L5771
4562  0986 a007          	sub	a,#7
4563  0988 272c          	jreq	L5002
4564  098a 4a            	dec	a
4565  098b 2729          	jreq	L5002
4566  098d a007          	sub	a,#7
4567  098f 271b          	jreq	L1002
4568  0991 a008          	sub	a,#8
4569  0993 272b          	jreq	L1102
4570  0995 a020          	sub	a,#32
4571  0997 272c          	jreq	L3102
4572  0999               L5102:
4575  0999 4f            	clr	a
4578  099a 5b01          	addw	sp,#1
4579  099c 81            	ret
4580  099d               L3771:
4581                     ; 774 		case 0x04:	return PICC_TYPE_NOT_COMPLETE;	// UID not complete
4583  099d a6ff          	ld	a,#255
4586  099f 5b01          	addw	sp,#1
4587  09a1 81            	ret
4588  09a2               L5771:
4589                     ; 775 		case 0x09:	return PICC_TYPE_MIFARE_MINI;
4591  09a2 a603          	ld	a,#3
4594  09a4 5b01          	addw	sp,#1
4595  09a6 81            	ret
4596  09a7               L7771:
4597                     ; 776 		case 0x08:	return PICC_TYPE_MIFARE_1K;
4599  09a7 a604          	ld	a,#4
4602  09a9 5b01          	addw	sp,#1
4603  09ab 81            	ret
4604  09ac               L1002:
4605                     ; 777 		case 0x18:	return PICC_TYPE_MIFARE_4K;
4607  09ac a605          	ld	a,#5
4610  09ae 5b01          	addw	sp,#1
4611  09b0 81            	ret
4612  09b1               L3002:
4613                     ; 778 		case 0x00:	return PICC_TYPE_MIFARE_UL;
4615  09b1 a606          	ld	a,#6
4618  09b3 5b01          	addw	sp,#1
4619  09b5 81            	ret
4620  09b6               L5002:
4621                     ; 779 		case 0x10:
4621                     ; 780 		case 0x11:	return PICC_TYPE_MIFARE_PLUS;
4623  09b6 a607          	ld	a,#7
4626  09b8 5b01          	addw	sp,#1
4627  09ba 81            	ret
4628  09bb               L7002:
4629                     ; 781 		case 0x01:	return PICC_TYPE_TNP3XXX;
4631  09bb a609          	ld	a,#9
4634  09bd 5b01          	addw	sp,#1
4635  09bf 81            	ret
4636  09c0               L1102:
4637                     ; 782 		case 0x20:	return PICC_TYPE_ISO_14443_4;
4639  09c0 a601          	ld	a,#1
4642  09c2 5b01          	addw	sp,#1
4643  09c4 81            	ret
4644  09c5               L3102:
4645                     ; 783 		case 0x40:	return PICC_TYPE_ISO_18092;
4647  09c5 a602          	ld	a,#2
4650  09c7 5b01          	addw	sp,#1
4651  09c9 81            	ret
4687                     ; 792 void PCD_DumpVersionToSerial() {
4688                     	switch	.text
4689  09ca               _PCD_DumpVersionToSerial:
4691  09ca 88            	push	a
4692       00000001      OFST:	set	1
4695                     ; 794 	uint8_t v = MFRC_ReadReg(VersionReg);
4697  09cb a66e          	ld	a,#110
4698  09cd cd00b2        	call	_MFRC_ReadReg
4700                     ; 810 } // End PCD_DumpVersionToSerial()
4703  09d0 84            	pop	a
4704  09d1 81            	ret
4762                     ; 906 bool PICC_IsNewCardPresent(void) 
4762                     ; 907 {
4763                     	switch	.text
4764  09d2               _PICC_IsNewCardPresent:
4766  09d2 5204          	subw	sp,#4
4767       00000004      OFST:	set	4
4770                     ; 909 	uint8_t bufferSize = sizeof(bufferATQA);
4772  09d4 a602          	ld	a,#2
4773  09d6 6b03          	ld	(OFST-1,sp),a
4775                     ; 913 	MFRC_WriteReg(TxModeReg, 0x00);
4777  09d8 ae2400        	ldw	x,#9216
4778  09db cd0000        	call	_MFRC_WriteReg
4780                     ; 914 	MFRC_WriteReg(RxModeReg, 0x00);
4782  09de ae2600        	ldw	x,#9728
4783  09e1 cd0000        	call	_MFRC_WriteReg
4785                     ; 916 	MFRC_WriteReg(ModWidthReg, 0x26);
4787  09e4 ae4826        	ldw	x,#18470
4788  09e7 cd0000        	call	_MFRC_WriteReg
4790                     ; 918 	result = PICC_RequestA(bufferATQA, &bufferSize);
4792  09ea 96            	ldw	x,sp
4793  09eb 1c0003        	addw	x,#OFST-1
4794  09ee 89            	pushw	x
4795  09ef 96            	ldw	x,sp
4796  09f0 1c0003        	addw	x,#OFST-1
4797  09f3 cd02f7        	call	_PICC_RequestA
4799  09f6 85            	popw	x
4800  09f7 6b04          	ld	(OFST+0,sp),a
4802                     ; 919 	return ((result == STATUS_OK) || (result == STATUS_COLLISION));
4804  09f9 0d04          	tnz	(OFST+0,sp)
4805  09fb 2706          	jreq	L632
4806  09fd 7b04          	ld	a,(OFST+0,sp)
4807  09ff a102          	cp	a,#2
4808  0a01 2604          	jrne	L432
4809  0a03               L632:
4810  0a03 a601          	ld	a,#1
4811  0a05 2001          	jra	L042
4812  0a07               L432:
4813  0a07 4f            	clr	a
4814  0a08               L042:
4817  0a08 5b04          	addw	sp,#4
4818  0a0a 81            	ret
4842                     	xdef	_count
4843                     	xdef	_PICC_IsNewCardPresent
4844                     	xdef	_PCD_DumpVersionToSerial
4845                     	xdef	_MIFARE_Read
4846                     	xdef	_PCD_StopCrypto1
4847                     	xdef	_PCD_Authenticate
4848                     	xdef	_PICC_HaltA
4849                     	xdef	_PICC_Select
4850                     	xdef	_PICC_REQA_or_WUPA
4851                     	xdef	_PICC_WakeupA
4852                     	xdef	_PICC_RequestA
4853                     	xdef	_MFRC_CommunicateWithPICC
4854                     	xdef	_MFRC_TransceiveData
4855                     	xdef	_MFRC_AntennaOn
4856                     	xdef	_MFRC_Reset
4857                     	xdef	_MFRC_Init
4858                     	xdef	_MFRC_CalculateCRC
4859                     	xdef	_MFRC_ClearRegBitMask
4860                     	xdef	_MFRC_SetRegBitMask
4861                     	xdef	_MFRC_ReadRegs
4862                     	xdef	_MFRC_ReadReg
4863                     	xdef	_MFRC_WriteRegs
4864                     	xdef	_MFRC_WriteReg
4865                     	xref	_delay_us
4866                     	xref	_SPI_Transceive
4867                     	xref	_SPI_GetFlagStatus
4868                     	xref	_SPI_ReceiveData
4869                     	xref	_SPI_SendData
4870                     	xref	_GPIO_ResetBits
4871                     	xref	_GPIO_SetBits
4872                     	xref.b	c_x
4873                     	xref.b	c_y
4892                     	xref	c_smodx
4893                     	xref	c_idiv
4894                     	end
