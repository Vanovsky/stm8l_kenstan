   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  15                     	bsct
  16  0000               _pack_dat:
  17  0000 48454c4c4f21  	dc.b	"HELLO!",0
  18  0007               _pack_len:
  19  0007 06            	dc.b	6
  20  0008               _Buff:
  21  0008 00            	dc.b	0
  54                     ; 9 void CLK_config(void)
  54                     ; 10 {
  56                     	switch	.text
  57  0000               _CLK_config:
  61                     ; 14 	CLK_SYSCLKSourceSwitchCmd(ENABLE);
  63  0000 a601          	ld	a,#1
  64  0002 cd0000        	call	_CLK_SYSCLKSourceSwitchCmd
  66                     ; 15   CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
  68  0005 a601          	ld	a,#1
  69  0007 cd0000        	call	_CLK_SYSCLKSourceConfig
  71                     ; 16   CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
  73  000a 4f            	clr	a
  74  000b cd0000        	call	_CLK_SYSCLKDivConfig
  77  000e               L52:
  78                     ; 17   while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}
  80  000e cd0000        	call	_CLK_GetSYSCLKSource
  82  0011 a101          	cp	a,#1
  83  0013 26f9          	jrne	L52
  84                     ; 18 }
  87  0015 81            	ret
 121                     ; 20 void delay_us(uint32_t del_us)
 121                     ; 21 {
 122                     	switch	.text
 123  0016               _delay_us:
 125       00000000      OFST:	set	0
 128  0016               L15:
 129                     ; 23 	while(del_us--);
 131  0016 96            	ldw	x,sp
 132  0017 1c0003        	addw	x,#OFST+3
 133  001a cd0000        	call	c_ltor
 135  001d 96            	ldw	x,sp
 136  001e 1c0003        	addw	x,#OFST+3
 137  0021 a601          	ld	a,#1
 138  0023 cd0000        	call	c_lgsbc
 140  0026 cd0000        	call	c_lrzmp
 142  0029 26eb          	jrne	L15
 143                     ; 24 }
 146  002b 81            	ret
 172                     ; 26 void GPIO_config(void)
 172                     ; 27 {
 173                     	switch	.text
 174  002c               _GPIO_config:
 178                     ; 32 	GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast );	// SCK
 180  002c 4bf0          	push	#240
 181  002e 4b20          	push	#32
 182  0030 ae5005        	ldw	x,#20485
 183  0033 cd0000        	call	_GPIO_Init
 185  0036 85            	popw	x
 186                     ; 33 	GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast );	// MOSI
 188  0037 4bf0          	push	#240
 189  0039 4b40          	push	#64
 190  003b ae5005        	ldw	x,#20485
 191  003e cd0000        	call	_GPIO_Init
 193  0041 85            	popw	x
 194                     ; 34 	GPIO_Init(GPIOD, MFRC_CS_Pin, GPIO_Mode_Out_PP_High_Fast );	// CSN
 196  0042 4bf0          	push	#240
 197  0044 4b10          	push	#16
 198  0046 ae500f        	ldw	x,#20495
 199  0049 cd0000        	call	_GPIO_Init
 201  004c 85            	popw	x
 202                     ; 35 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 204  004d 4b10          	push	#16
 205  004f ae500f        	ldw	x,#20495
 206  0052 cd0000        	call	_GPIO_SetBits
 208  0055 84            	pop	a
 209                     ; 38 	GPIO_Init(GPIOD, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast );	// Soft Reset Pin
 211  0056 4bf0          	push	#240
 212  0058 4b20          	push	#32
 213  005a ae500f        	ldw	x,#20495
 214  005d cd0000        	call	_GPIO_Init
 216  0060 85            	popw	x
 217                     ; 39 	GPIO_Init(GPIOE, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast );	// Green LED
 219  0061 4bf0          	push	#240
 220  0063 4b80          	push	#128
 221  0065 ae5014        	ldw	x,#20500
 222  0068 cd0000        	call	_GPIO_Init
 224  006b 85            	popw	x
 225                     ; 40 	GPIO_Init(GPIOC, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast );	//  Blue LED
 227  006c 4bf0          	push	#240
 228  006e 4b80          	push	#128
 229  0070 ae500a        	ldw	x,#20490
 230  0073 cd0000        	call	_GPIO_Init
 232  0076 85            	popw	x
 233                     ; 42 	GPIO_ResetBits(GPIOE, GPIO_Pin_7);
 235  0077 4b80          	push	#128
 236  0079 ae5014        	ldw	x,#20500
 237  007c cd0000        	call	_GPIO_ResetBits
 239  007f 84            	pop	a
 240                     ; 43 	GPIO_ResetBits(GPIOC, GPIO_Pin_7);
 242  0080 4b80          	push	#128
 243  0082 ae500a        	ldw	x,#20490
 244  0085 cd0000        	call	_GPIO_ResetBits
 246  0088 84            	pop	a
 247                     ; 47 }
 250  0089 81            	ret
 275                     ; 49 void SoftResetRFID(void)
 275                     ; 50 {
 276                     	switch	.text
 277  008a               _SoftResetRFID:
 281                     ; 51 	GPIO_ResetBits(GPIOD, GPIO_Pin_5);
 283  008a 4b20          	push	#32
 284  008c ae500f        	ldw	x,#20495
 285  008f cd0000        	call	_GPIO_ResetBits
 287  0092 84            	pop	a
 288                     ; 52 	GPIO_SetBits(GPIOD, GPIO_Pin_5);
 290  0093 4b20          	push	#32
 291  0095 ae500f        	ldw	x,#20495
 292  0098 cd0000        	call	_GPIO_SetBits
 294  009b 84            	pop	a
 295                     ; 53 }
 298  009c 81            	ret
 322                     ; 55 void EXTI_config(void)
 322                     ; 56 {
 323                     	switch	.text
 324  009d               _EXTI_config:
 328                     ; 57 	EXTI_DeInit();
 330  009d cd0000        	call	_EXTI_DeInit
 332                     ; 63 }
 335  00a0 81            	ret
 359                     ; 65 void ITC_config(void)
 359                     ; 66 {
 360                     	switch	.text
 361  00a1               _ITC_config:
 365                     ; 69 	ITC_SetSoftwarePriority(TIM2_UPD_OVF_TRG_BRK_IRQn, ITC_PriorityLevel_3);
 367  00a1 ae1303        	ldw	x,#4867
 368  00a4 cd0000        	call	_ITC_SetSoftwarePriority
 370                     ; 70 }
 373  00a7 81            	ret
 400                     ; 72 void SPI_config(void)
 400                     ; 73 {
 401                     	switch	.text
 402  00a8               _SPI_config:
 406                     ; 74 	SPI_DeInit(SPI1);	
 408  00a8 ae5200        	ldw	x,#20992
 409  00ab cd0000        	call	_SPI_DeInit
 411                     ; 78 	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);	
 413  00ae ae0401        	ldw	x,#1025
 414  00b1 cd0000        	call	_CLK_PeripheralClockConfig
 416                     ; 80 	SPI_Init(SPI1, 
 416                     ; 81 						SPI_FirstBit_MSB,
 416                     ; 82 						SPI_BaudRatePrescaler_32,
 416                     ; 83 						SPI_Mode_Master,
 416                     ; 84 						//SPI_Mode_Slave,
 416                     ; 85 						SPI_CPOL_Low,
 416                     ; 86 						SPI_CPHA_1Edge, 
 416                     ; 87 						SPI_Direction_2Lines_FullDuplex,
 416                     ; 88 						SPI_NSS_Soft, 
 416                     ; 89 						0);
 418  00b4 4b00          	push	#0
 419  00b6 4b02          	push	#2
 420  00b8 4b00          	push	#0
 421  00ba 4b00          	push	#0
 422  00bc 4b00          	push	#0
 423  00be 4b04          	push	#4
 424  00c0 4b20          	push	#32
 425  00c2 4b00          	push	#0
 426  00c4 ae5200        	ldw	x,#20992
 427  00c7 cd0000        	call	_SPI_Init
 429  00ca 5b08          	addw	sp,#8
 430                     ; 91 	SPI_Cmd(SPI1, ENABLE);
 432  00cc 4b01          	push	#1
 433  00ce ae5200        	ldw	x,#20992
 434  00d1 cd0000        	call	_SPI_Cmd
 436  00d4 84            	pop	a
 437                     ; 92 }
 440  00d5 81            	ret
 468                     ; 94 void I2C_config(void)
 468                     ; 95 {
 469                     	switch	.text
 470  00d6               _I2C_config:
 474                     ; 96 	I2C_DeInit(I2C1);
 476  00d6 ae5210        	ldw	x,#21008
 477  00d9 cd0000        	call	_I2C_DeInit
 479                     ; 100 	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
 481  00dc ae0301        	ldw	x,#769
 482  00df cd0000        	call	_CLK_PeripheralClockConfig
 484                     ; 103   CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_2);
 486  00e2 a601          	ld	a,#1
 487  00e4 cd0000        	call	_CLK_SYSCLKDivConfig
 489                     ; 106   I2C_Init(I2C1, 
 489                     ; 107 					 I2C_SPEED, 
 489                     ; 108 					 0xA0,
 489                     ; 109            I2C_Mode_I2C, 
 489                     ; 110 					 I2C_DutyCycle_2,
 489                     ; 111            I2C_Ack_Enable, 
 489                     ; 112 					 I2C_AcknowledgedAddress_7bit);
 491  00e7 4b00          	push	#0
 492  00e9 4b04          	push	#4
 493  00eb 4b00          	push	#0
 494  00ed 4b00          	push	#0
 495  00ef ae00a0        	ldw	x,#160
 496  00f2 89            	pushw	x
 497  00f3 ae86a0        	ldw	x,#34464
 498  00f6 89            	pushw	x
 499  00f7 ae0001        	ldw	x,#1
 500  00fa 89            	pushw	x
 501  00fb ae5210        	ldw	x,#21008
 502  00fe cd0000        	call	_I2C_Init
 504  0101 5b0a          	addw	sp,#10
 505                     ; 117 	I2C_Cmd(I2C1, ENABLE);
 507  0103 4b01          	push	#1
 508  0105 ae5210        	ldw	x,#21008
 509  0108 cd0000        	call	_I2C_Cmd
 511  010b 84            	pop	a
 512                     ; 118 }
 515  010c 81            	ret
 563                     ; 120 void SPI_Send(uint8_t* arrData, uint8_t arrCount)
 563                     ; 121 {
 564                     	switch	.text
 565  010d               _SPI_Send:
 567  010d 89            	pushw	x
 568       00000000      OFST:	set	0
 571                     ; 122 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 573  010e 4b10          	push	#16
 574  0110 ae500f        	ldw	x,#20495
 575  0113 cd0000        	call	_GPIO_ResetBits
 577  0116 84            	pop	a
 579  0117 2020          	jra	L161
 580  0119               L761:
 581                     ; 125 		while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 583  0119 4b02          	push	#2
 584  011b ae5200        	ldw	x,#20992
 585  011e cd0000        	call	_SPI_GetFlagStatus
 587  0121 5b01          	addw	sp,#1
 588  0123 4d            	tnz	a
 589  0124 27f3          	jreq	L761
 590                     ; 126 		SPI_SendData(SPI1, *arrData++);
 592  0126 1e01          	ldw	x,(OFST+1,sp)
 593  0128 1c0001        	addw	x,#1
 594  012b 1f01          	ldw	(OFST+1,sp),x
 595  012d 1d0001        	subw	x,#1
 596  0130 f6            	ld	a,(x)
 597  0131 88            	push	a
 598  0132 ae5200        	ldw	x,#20992
 599  0135 cd0000        	call	_SPI_SendData
 601  0138 84            	pop	a
 602  0139               L161:
 603                     ; 123 	while(arrCount--)
 605  0139 7b05          	ld	a,(OFST+5,sp)
 606  013b 0a05          	dec	(OFST+5,sp)
 607  013d 4d            	tnz	a
 608  013e 26d9          	jrne	L761
 610  0140               L571:
 611                     ; 128 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 613  0140 4b80          	push	#128
 614  0142 ae5200        	ldw	x,#20992
 615  0145 cd0000        	call	_SPI_GetFlagStatus
 617  0148 5b01          	addw	sp,#1
 618  014a 4d            	tnz	a
 619  014b 26f3          	jrne	L571
 620                     ; 129 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 622  014d 4b10          	push	#16
 623  014f ae500f        	ldw	x,#20495
 624  0152 cd0000        	call	_GPIO_SetBits
 626  0155 84            	pop	a
 627                     ; 130 }
 630  0156 85            	popw	x
 631  0157 81            	ret
 677                     ; 132 uint8_t SPI_Transceive(uint8_t TransmitData)
 677                     ; 133 {
 678                     	switch	.text
 679  0158               _SPI_Transceive:
 681  0158 88            	push	a
 682  0159 88            	push	a
 683       00000001      OFST:	set	1
 686  015a               L522:
 687                     ; 136 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 689  015a 4b02          	push	#2
 690  015c ae5200        	ldw	x,#20992
 691  015f cd0000        	call	_SPI_GetFlagStatus
 693  0162 5b01          	addw	sp,#1
 694  0164 4d            	tnz	a
 695  0165 27f3          	jreq	L522
 696                     ; 137 	SPI_SendData(SPI1, TransmitData);
 698  0167 7b02          	ld	a,(OFST+1,sp)
 699  0169 88            	push	a
 700  016a ae5200        	ldw	x,#20992
 701  016d cd0000        	call	_SPI_SendData
 703  0170 84            	pop	a
 704                     ; 139 	ReceiveData = SPI_ReceiveData(SPI1);
 706  0171 ae5200        	ldw	x,#20992
 707  0174 cd0000        	call	_SPI_ReceiveData
 709  0177 6b01          	ld	(OFST+0,sp),a
 712  0179               L532:
 713                     ; 140 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 715  0179 4b80          	push	#128
 716  017b ae5200        	ldw	x,#20992
 717  017e cd0000        	call	_SPI_GetFlagStatus
 719  0181 5b01          	addw	sp,#1
 720  0183 4d            	tnz	a
 721  0184 26f3          	jrne	L532
 722                     ; 142 	return ReceiveData;
 724  0186 7b01          	ld	a,(OFST+0,sp)
 727  0188 85            	popw	x
 728  0189 81            	ret
 766                     ; 145 void SPI_SimpleSend(uint8_t arrData)
 766                     ; 146 {
 767                     	switch	.text
 768  018a               _SPI_SimpleSend:
 770  018a 88            	push	a
 771       00000000      OFST:	set	0
 774                     ; 147 	GPIO_ResetBits(GPIOD, MFRC_CS_Pin);
 776  018b 4b10          	push	#16
 777  018d ae500f        	ldw	x,#20495
 778  0190 cd0000        	call	_GPIO_ResetBits
 780  0193 84            	pop	a
 782  0194               L162:
 783                     ; 148 	while(!(SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE)));
 785  0194 4b02          	push	#2
 786  0196 ae5200        	ldw	x,#20992
 787  0199 cd0000        	call	_SPI_GetFlagStatus
 789  019c 5b01          	addw	sp,#1
 790  019e 4d            	tnz	a
 791  019f 27f3          	jreq	L162
 792                     ; 149 	SPI_SendData(SPI1, arrData);
 794  01a1 7b01          	ld	a,(OFST+1,sp)
 795  01a3 88            	push	a
 796  01a4 ae5200        	ldw	x,#20992
 797  01a7 cd0000        	call	_SPI_SendData
 799  01aa 84            	pop	a
 801  01ab               L762:
 802                     ; 150 	while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY));
 804  01ab 4b80          	push	#128
 805  01ad ae5200        	ldw	x,#20992
 806  01b0 cd0000        	call	_SPI_GetFlagStatus
 808  01b3 5b01          	addw	sp,#1
 809  01b5 4d            	tnz	a
 810  01b6 26f3          	jrne	L762
 811                     ; 151 	GPIO_SetBits(GPIOD, MFRC_CS_Pin);
 813  01b8 4b10          	push	#16
 814  01ba ae500f        	ldw	x,#20495
 815  01bd cd0000        	call	_GPIO_SetBits
 817  01c0 84            	pop	a
 818                     ; 152 }
 821  01c1 84            	pop	a
 822  01c2 81            	ret
 851                     ; 154 void TIM2_config(void)
 851                     ; 155 {
 852                     	switch	.text
 853  01c3               _TIM2_config:
 857                     ; 156 	TIM2_DeInit();	
 859  01c3 cd0000        	call	_TIM2_DeInit
 861                     ; 160 	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);	
 863  01c6 ae0001        	ldw	x,#1
 864  01c9 cd0000        	call	_CLK_PeripheralClockConfig
 866                     ; 161 	TIM2_TimeBaseInit(TIM2_Prescaler_128, TIM2_CounterMode_Up, 31250);
 868  01cc ae7a12        	ldw	x,#31250
 869  01cf 89            	pushw	x
 870  01d0 ae0700        	ldw	x,#1792
 871  01d3 cd0000        	call	_TIM2_TimeBaseInit
 873  01d6 85            	popw	x
 874                     ; 162 	TIM2_ARRPreloadConfig(ENABLE);		
 876  01d7 a601          	ld	a,#1
 877  01d9 cd0000        	call	_TIM2_ARRPreloadConfig
 879                     ; 163 	TIM2_ITConfig(TIM2_IT_Update, ENABLE);
 881  01dc ae0101        	ldw	x,#257
 882  01df cd0000        	call	_TIM2_ITConfig
 884                     ; 165 	TIM2_Cmd(ENABLE);
 886  01e2 a601          	ld	a,#1
 887  01e4 cd0000        	call	_TIM2_Cmd
 889                     ; 166 }
 892  01e7 81            	ret
 935                     ; 173 int main()
 935                     ; 174 {
 936                     	switch	.text
 937  01e8               _main:
 939  01e8 89            	pushw	x
 940       00000002      OFST:	set	2
 943                     ; 175 	int i = 0;
 945                     ; 181 	enableInterrupts();
 948  01e9 9a            rim
 950                     ; 182 	CLK_config();
 953  01ea cd0000        	call	_CLK_config
 955                     ; 183 	TIM2_config();
 957  01ed add4          	call	_TIM2_config
 959                     ; 184 	GPIO_config();
 961  01ef cd002c        	call	_GPIO_config
 963                     ; 187 	SPI_config();
 965  01f2 cd00a8        	call	_SPI_config
 967                     ; 190 	MFRC_Init();
 969  01f5 cd0000        	call	_MFRC_Init
 971                     ; 191 	delay_us(1);
 973  01f8 ae0001        	ldw	x,#1
 974  01fb 89            	pushw	x
 975  01fc ae0000        	ldw	x,#0
 976  01ff 89            	pushw	x
 977  0200 cd0016        	call	_delay_us
 979  0203 5b04          	addw	sp,#4
 980  0205               L123:
 981                     ; 196 		Buff = MFRC_ReadReg(FIFODataReg);
 983  0205 a612          	ld	a,#18
 984  0207 cd0000        	call	_MFRC_ReadReg
 986  020a b708          	ld	_Buff,a
 987                     ; 197 		delay_us(200);
 989  020c ae00c8        	ldw	x,#200
 990  020f 89            	pushw	x
 991  0210 ae0000        	ldw	x,#0
 992  0213 89            	pushw	x
 993  0214 cd0016        	call	_delay_us
 995  0217 5b04          	addw	sp,#4
 997  0219 20ea          	jra	L123
1080                     	switch	.ubsct
1081  0000               _buffer2:
1082  0000 000000000000  	ds.b	18
1083                     	xdef	_buffer2
1084  0012               _buffer1:
1085  0012 000000000000  	ds.b	18
1086                     	xdef	_buffer1
1087                     	xdef	_Buff
1088  0024               _nope_pack:
1089  0024 000000000000  	ds.b	25
1090                     	xdef	_nope_pack
1091  003d               _rfid_buf:
1092  003d 000000000000  	ds.b	25
1093                     	xdef	_rfid_buf
1094                     	xdef	_pack_len
1095                     	xdef	_pack_dat
1096                     	xdef	_delay_us
1097                     	xdef	_main
1098                     	xdef	_SPI_Transceive
1099                     	xdef	_SPI_SimpleSend
1100                     	xdef	_SPI_Send
1101                     	xdef	_TIM2_config
1102                     	xdef	_I2C_config
1103                     	xdef	_SPI_config
1104                     	xdef	_ITC_config
1105                     	xdef	_EXTI_config
1106                     	xdef	_SoftResetRFID
1107                     	xdef	_GPIO_config
1108                     	xdef	_CLK_config
1109                     	xref	_MFRC_Init
1110                     	xref	_MFRC_ReadReg
1111                     	xref	_TIM2_ITConfig
1112                     	xref	_TIM2_Cmd
1113                     	xref	_TIM2_ARRPreloadConfig
1114                     	xref	_TIM2_TimeBaseInit
1115                     	xref	_TIM2_DeInit
1116                     	xref	_SPI_GetFlagStatus
1117                     	xref	_SPI_ReceiveData
1118                     	xref	_SPI_SendData
1119                     	xref	_SPI_Cmd
1120                     	xref	_SPI_Init
1121                     	xref	_SPI_DeInit
1122                     	xref	_ITC_SetSoftwarePriority
1123                     	xref	_I2C_Cmd
1124                     	xref	_I2C_Init
1125                     	xref	_I2C_DeInit
1126                     	xref	_GPIO_ResetBits
1127                     	xref	_GPIO_SetBits
1128                     	xref	_GPIO_Init
1129                     	xref	_EXTI_DeInit
1130                     	xref	_CLK_PeripheralClockConfig
1131                     	xref	_CLK_SYSCLKSourceSwitchCmd
1132                     	xref	_CLK_SYSCLKDivConfig
1133                     	xref	_CLK_GetSYSCLKSource
1134                     	xref	_CLK_SYSCLKSourceConfig
1154                     	xref	c_lrzmp
1155                     	xref	c_lgsbc
1156                     	xref	c_ltor
1157                     	end
