   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  44                     ; 55 INTERRUPT_HANDLER(NonHandledInterrupt,0)
  44                     ; 56 {
  45                     	switch	.text
  46  0000               f_NonHandledInterrupt:
  50                     ; 60 }
  53  0000 80            	iret
  75                     ; 70 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  75                     ; 71 {
  76                     	switch	.text
  77  0001               f_TRAP_IRQHandler:
  81                     ; 75 }
  84  0001 80            	iret
 106                     ; 81 INTERRUPT_HANDLER(FLASH_IRQHandler,1)
 106                     ; 82 {
 107                     	switch	.text
 108  0002               f_FLASH_IRQHandler:
 112                     ; 86 }
 115  0002 80            	iret
 138                     ; 92 INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler,2)
 138                     ; 93 {
 139                     	switch	.text
 140  0003               f_DMA1_CHANNEL0_1_IRQHandler:
 144                     ; 97 }
 147  0003 80            	iret
 170                     ; 103 INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler,3)
 170                     ; 104 {
 171                     	switch	.text
 172  0004               f_DMA1_CHANNEL2_3_IRQHandler:
 176                     ; 108 }
 179  0004 80            	iret
 202                     ; 114 INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler,4)
 202                     ; 115 {
 203                     	switch	.text
 204  0005               f_RTC_CSSLSE_IRQHandler:
 208                     ; 119 }
 211  0005 80            	iret
 234                     ; 125 INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
 234                     ; 126 {
 235                     	switch	.text
 236  0006               f_EXTIE_F_PVD_IRQHandler:
 240                     ; 130 }
 243  0006 80            	iret
 265                     ; 137 INTERRUPT_HANDLER(EXTIB_G_IRQHandler,6)
 265                     ; 138 {
 266                     	switch	.text
 267  0007               f_EXTIB_G_IRQHandler:
 271                     ; 142 }
 274  0007 80            	iret
 296                     ; 149 INTERRUPT_HANDLER(EXTID_H_IRQHandler,7)
 296                     ; 150 {
 297                     	switch	.text
 298  0008               f_EXTID_H_IRQHandler:
 302                     ; 154 }
 305  0008 80            	iret
 327                     ; 161 INTERRUPT_HANDLER(EXTI0_IRQHandler,8)
 327                     ; 162 {
 328                     	switch	.text
 329  0009               f_EXTI0_IRQHandler:
 333                     ; 168 }
 336  0009 80            	iret
 360                     ; 175 INTERRUPT_HANDLER(EXTI1_IRQHandler,9)
 360                     ; 176 {
 361                     	switch	.text
 362  000a               f_EXTI1_IRQHandler:
 364  000a 8a            	push	cc
 365  000b 84            	pop	a
 366  000c a4bf          	and	a,#191
 367  000e 88            	push	a
 368  000f 86            	pop	cc
 369  0010 3b0002        	push	c_x+2
 370  0013 be00          	ldw	x,c_x
 371  0015 89            	pushw	x
 372  0016 3b0002        	push	c_y+2
 373  0019 be00          	ldw	x,c_y
 374  001b 89            	pushw	x
 377                     ; 180 	GPIO_SetBits(GPIOC, GPIO_Pin_7);
 379  001c 4b80          	push	#128
 380  001e ae500a        	ldw	x,#20490
 381  0021 cd0000        	call	_GPIO_SetBits
 383  0024 84            	pop	a
 384                     ; 181 	EXTI_ClearITPendingBit(EXTI_IT_Pin1); /* ������� �������� ������� �����������!!!*/
 386  0025 ae0002        	ldw	x,#2
 387  0028 cd0000        	call	_EXTI_ClearITPendingBit
 389                     ; 182 }
 392  002b 85            	popw	x
 393  002c bf00          	ldw	c_y,x
 394  002e 320002        	pop	c_y+2
 395  0031 85            	popw	x
 396  0032 bf00          	ldw	c_x,x
 397  0034 320002        	pop	c_x+2
 398  0037 80            	iret
 420                     ; 189 INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
 420                     ; 190 {
 421                     	switch	.text
 422  0038               f_EXTI2_IRQHandler:
 426                     ; 194 }
 429  0038 80            	iret
 451                     ; 201 INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
 451                     ; 202 {
 452                     	switch	.text
 453  0039               f_EXTI3_IRQHandler:
 457                     ; 206 }
 460  0039 80            	iret
 482                     ; 213 INTERRUPT_HANDLER(EXTI4_IRQHandler,12)
 482                     ; 214 {
 483                     	switch	.text
 484  003a               f_EXTI4_IRQHandler:
 488                     ; 218 }
 491  003a 80            	iret
 513                     ; 225 INTERRUPT_HANDLER(EXTI5_IRQHandler,13)
 513                     ; 226 {
 514                     	switch	.text
 515  003b               f_EXTI5_IRQHandler:
 519                     ; 230 }
 522  003b 80            	iret
 544                     ; 237 INTERRUPT_HANDLER(EXTI6_IRQHandler,14)
 544                     ; 238 {
 545                     	switch	.text
 546  003c               f_EXTI6_IRQHandler:
 550                     ; 242 }
 553  003c 80            	iret
 575                     ; 249 INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
 575                     ; 250 {
 576                     	switch	.text
 577  003d               f_EXTI7_IRQHandler:
 581                     ; 254 }
 584  003d 80            	iret
 606                     ; 260 INTERRUPT_HANDLER(LCD_AES_IRQHandler,16)
 606                     ; 261 {
 607                     	switch	.text
 608  003e               f_LCD_AES_IRQHandler:
 612                     ; 265 }
 615  003e 80            	iret
 638                     ; 271 INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler,17)
 638                     ; 272 {
 639                     	switch	.text
 640  003f               f_SWITCH_CSS_BREAK_DAC_IRQHandler:
 644                     ; 276 }
 647  003f 80            	iret
 670                     ; 283 INTERRUPT_HANDLER(ADC1_COMP_IRQHandler,18)
 670                     ; 284 {
 671                     	switch	.text
 672  0040               f_ADC1_COMP_IRQHandler:
 676                     ; 288 }
 679  0040 80            	iret
 681                     	bsct
 682  0000               _tgl:
 683  0000 01            	dc.b	1
 710                     ; 296 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
 710                     ; 297 {
 711                     	switch	.text
 712  0041               f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler:
 714  0041 8a            	push	cc
 715  0042 84            	pop	a
 716  0043 a4bf          	and	a,#191
 717  0045 88            	push	a
 718  0046 86            	pop	cc
 719  0047 3b0002        	push	c_x+2
 720  004a be00          	ldw	x,c_x
 721  004c 89            	pushw	x
 722  004d 3b0002        	push	c_y+2
 723  0050 be00          	ldw	x,c_y
 724  0052 89            	pushw	x
 727                     ; 301 	tgl++;
 729  0053 3c00          	inc	_tgl
 730                     ; 302 	if (tgl % 2 == 0) 
 732  0055 b600          	ld	a,_tgl
 733  0057 a501          	bcp	a,#1
 734  0059 260b          	jrne	L362
 735                     ; 305 		GPIO_SetBits(GPIOE, GPIO_Pin_7);		// GREEN LED
 737  005b 4b80          	push	#128
 738  005d ae5014        	ldw	x,#20500
 739  0060 cd0000        	call	_GPIO_SetBits
 741  0063 84            	pop	a
 743  0064 2009          	jra	L562
 744  0066               L362:
 745                     ; 310 		GPIO_ResetBits(GPIOE, GPIO_Pin_7);
 747  0066 4b80          	push	#128
 748  0068 ae5014        	ldw	x,#20500
 749  006b cd0000        	call	_GPIO_ResetBits
 751  006e 84            	pop	a
 752  006f               L562:
 753                     ; 317 	if (tgl > 253) tgl = 1;
 755  006f b600          	ld	a,_tgl
 756  0071 a1fe          	cp	a,#254
 757  0073 2504          	jrult	L762
 760  0075 35010000      	mov	_tgl,#1
 761  0079               L762:
 762                     ; 318 	TIM2_ClearITPendingBit(TIM2_IT_Update);	/* ������� �������� ������� �����������!!!*/
 764  0079 a601          	ld	a,#1
 765  007b cd0000        	call	_TIM2_ClearITPendingBit
 767                     ; 319 }
 770  007e 85            	popw	x
 771  007f bf00          	ldw	c_y,x
 772  0081 320002        	pop	c_y+2
 773  0084 85            	popw	x
 774  0085 bf00          	ldw	c_x,x
 775  0087 320002        	pop	c_x+2
 776  008a 80            	iret
 799                     ; 326 INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler,20)
 799                     ; 327 {
 800                     	switch	.text
 801  008b               f_TIM2_CC_USART2_RX_IRQHandler:
 805                     ; 331 }
 808  008b 80            	iret
 832                     ; 339 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
 832                     ; 340 {
 833                     	switch	.text
 834  008c               f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler:
 838                     ; 344 }
 841  008c 80            	iret
 864                     ; 350 INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler,22)
 864                     ; 351 {
 865                     	switch	.text
 866  008d               f_TIM3_CC_USART3_RX_IRQHandler:
 870                     ; 355 }
 873  008d 80            	iret
 896                     ; 361 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler,23)
 896                     ; 362 {
 897                     	switch	.text
 898  008e               f_TIM1_UPD_OVF_TRG_COM_IRQHandler:
 902                     ; 366 }
 905  008e 80            	iret
 927                     ; 372 INTERRUPT_HANDLER(TIM1_CC_IRQHandler,24)
 927                     ; 373 {
 928                     	switch	.text
 929  008f               f_TIM1_CC_IRQHandler:
 933                     ; 377 }
 936  008f 80            	iret
 959                     ; 384 INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler,25)
 959                     ; 385 {
 960                     	switch	.text
 961  0090               f_TIM4_UPD_OVF_TRG_IRQHandler:
 965                     ; 389 }
 968  0090 80            	iret
 990                     ; 395 INTERRUPT_HANDLER(SPI1_IRQHandler,26)
 990                     ; 396 {
 991                     	switch	.text
 992  0091               f_SPI1_IRQHandler:
 996                     ; 401 }
 999  0091 80            	iret
1023                     ; 408 INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler,27)
1023                     ; 409 {
1024                     	switch	.text
1025  0092               f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler:
1029                     ; 413 }
1032  0092 80            	iret
1055                     ; 420 INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
1055                     ; 421 {
1056                     	switch	.text
1057  0093               f_USART1_RX_TIM5_CC_IRQHandler:
1061                     ; 425 }
1064  0093 80            	iret
1087                     ; 432 INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler,29)
1087                     ; 433 {
1088                     	switch	.text
1089  0094               f_I2C1_SPI2_IRQHandler:
1093                     ; 437 }
1096  0094 80            	iret
1119                     	xdef	_tgl
1120                     	xdef	f_I2C1_SPI2_IRQHandler
1121                     	xdef	f_USART1_RX_TIM5_CC_IRQHandler
1122                     	xdef	f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler
1123                     	xdef	f_SPI1_IRQHandler
1124                     	xdef	f_TIM4_UPD_OVF_TRG_IRQHandler
1125                     	xdef	f_TIM1_CC_IRQHandler
1126                     	xdef	f_TIM1_UPD_OVF_TRG_COM_IRQHandler
1127                     	xdef	f_TIM3_CC_USART3_RX_IRQHandler
1128                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler
1129                     	xdef	f_TIM2_CC_USART2_RX_IRQHandler
1130                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler
1131                     	xdef	f_ADC1_COMP_IRQHandler
1132                     	xdef	f_SWITCH_CSS_BREAK_DAC_IRQHandler
1133                     	xdef	f_LCD_AES_IRQHandler
1134                     	xdef	f_EXTI7_IRQHandler
1135                     	xdef	f_EXTI6_IRQHandler
1136                     	xdef	f_EXTI5_IRQHandler
1137                     	xdef	f_EXTI4_IRQHandler
1138                     	xdef	f_EXTI3_IRQHandler
1139                     	xdef	f_EXTI2_IRQHandler
1140                     	xdef	f_EXTI1_IRQHandler
1141                     	xdef	f_EXTI0_IRQHandler
1142                     	xdef	f_EXTID_H_IRQHandler
1143                     	xdef	f_EXTIB_G_IRQHandler
1144                     	xdef	f_EXTIE_F_PVD_IRQHandler
1145                     	xdef	f_RTC_CSSLSE_IRQHandler
1146                     	xdef	f_DMA1_CHANNEL2_3_IRQHandler
1147                     	xdef	f_DMA1_CHANNEL0_1_IRQHandler
1148                     	xdef	f_FLASH_IRQHandler
1149                     	xdef	f_TRAP_IRQHandler
1150                     	xdef	f_NonHandledInterrupt
1151                     	xref	_TIM2_ClearITPendingBit
1152                     	xref	_GPIO_ResetBits
1153                     	xref	_GPIO_SetBits
1154                     	xref	_EXTI_ClearITPendingBit
1155                     	xref.b	c_x
1156                     	xref.b	c_y
1175                     	end
