   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
 174                     ; 135 void ADC_DeInit(ADC_TypeDef* ADCx)
 174                     ; 136 {
 176                     	switch	.text
 177  0000               _ADC_DeInit:
 181                     ; 138   ADCx->CR1 =  ADC_CR1_RESET_VALUE;
 183  0000 7f            	clr	(x)
 184                     ; 139   ADCx->CR2 =  ADC_CR2_RESET_VALUE;
 186  0001 6f01          	clr	(1,x)
 187                     ; 140   ADCx->CR3 =  ADC_CR3_RESET_VALUE;
 189  0003 a61f          	ld	a,#31
 190  0005 e702          	ld	(2,x),a
 191                     ; 143   ADCx->SR =  (uint8_t)~ADC_SR_RESET_VALUE;
 193  0007 a6ff          	ld	a,#255
 194  0009 e703          	ld	(3,x),a
 195                     ; 146   ADCx->HTRH =  ADC_HTRH_RESET_VALUE;
 197  000b a60f          	ld	a,#15
 198  000d e706          	ld	(6,x),a
 199                     ; 147   ADCx->HTRL =  ADC_HTRL_RESET_VALUE;
 201  000f a6ff          	ld	a,#255
 202  0011 e707          	ld	(7,x),a
 203                     ; 150   ADCx->LTRH =  ADC_LTRH_RESET_VALUE;
 205  0013 6f08          	clr	(8,x)
 206                     ; 151   ADCx->LTRL =  ADC_LTRL_RESET_VALUE;
 208  0015 6f09          	clr	(9,x)
 209                     ; 154   ADCx->SQR[0] =  ADC_SQR1_RESET_VALUE;
 211  0017 6f0a          	clr	(10,x)
 212                     ; 155   ADCx->SQR[1] =  ADC_SQR2_RESET_VALUE;
 214  0019 6f0b          	clr	(11,x)
 215                     ; 156   ADCx->SQR[2] =  ADC_SQR3_RESET_VALUE;
 217  001b 6f0c          	clr	(12,x)
 218                     ; 157   ADCx->SQR[3] =  ADC_SQR4_RESET_VALUE;
 220  001d 6f0d          	clr	(13,x)
 221                     ; 160   ADCx->TRIGR[0] =  ADC_TRIGR1_RESET_VALUE;
 223  001f 6f0e          	clr	(14,x)
 224                     ; 161   ADCx->TRIGR[1] =  ADC_TRIGR2_RESET_VALUE;
 226  0021 6f0f          	clr	(15,x)
 227                     ; 162   ADCx->TRIGR[2] =  ADC_TRIGR3_RESET_VALUE;
 229  0023 6f10          	clr	(16,x)
 230                     ; 163   ADCx->TRIGR[3] =  ADC_TRIGR4_RESET_VALUE;
 232  0025 6f11          	clr	(17,x)
 233                     ; 164 }
 236  0027 81            	ret
 383                     ; 186 void ADC_Init(ADC_TypeDef* ADCx,
 383                     ; 187               ADC_ConversionMode_TypeDef ADC_ConversionMode,
 383                     ; 188               ADC_Resolution_TypeDef ADC_Resolution,
 383                     ; 189               ADC_Prescaler_TypeDef ADC_Prescaler)
 383                     ; 190 {
 384                     	switch	.text
 385  0028               _ADC_Init:
 387  0028 89            	pushw	x
 388       00000000      OFST:	set	0
 391                     ; 192   assert_param(IS_ADC_CONVERSION_MODE(ADC_ConversionMode));
 393                     ; 193   assert_param(IS_ADC_RESOLUTION(ADC_Resolution));
 395                     ; 194   assert_param(IS_ADC_PRESCALER(ADC_Prescaler));
 397                     ; 197   ADCx->CR1 &= (uint8_t)~(ADC_CR1_CONT | ADC_CR1_RES);
 399  0029 f6            	ld	a,(x)
 400  002a a49b          	and	a,#155
 401  002c f7            	ld	(x),a
 402                     ; 200   ADCx->CR1 |= (uint8_t)((uint8_t)ADC_ConversionMode | (uint8_t)ADC_Resolution);
 404  002d 7b05          	ld	a,(OFST+5,sp)
 405  002f 1a06          	or	a,(OFST+6,sp)
 406  0031 fa            	or	a,(x)
 407  0032 f7            	ld	(x),a
 408                     ; 203   ADCx->CR2 &= (uint8_t)~(ADC_CR2_PRESC);
 410  0033 e601          	ld	a,(1,x)
 411  0035 a47f          	and	a,#127
 412  0037 e701          	ld	(1,x),a
 413                     ; 206   ADCx->CR2 |= (uint8_t) ADC_Prescaler;
 415  0039 e601          	ld	a,(1,x)
 416  003b 1a07          	or	a,(OFST+7,sp)
 417  003d e701          	ld	(1,x),a
 418                     ; 207 }
 421  003f 85            	popw	x
 422  0040 81            	ret
 489                     ; 216 void ADC_Cmd(ADC_TypeDef* ADCx,
 489                     ; 217              FunctionalState NewState)
 489                     ; 218 {
 490                     	switch	.text
 491  0041               _ADC_Cmd:
 493  0041 89            	pushw	x
 494       00000000      OFST:	set	0
 497                     ; 220   assert_param(IS_FUNCTIONAL_STATE(NewState));
 499                     ; 222   if (NewState != DISABLE)
 501  0042 0d05          	tnz	(OFST+5,sp)
 502  0044 2706          	jreq	L142
 503                     ; 225     ADCx->CR1 |= ADC_CR1_ADON;
 505  0046 f6            	ld	a,(x)
 506  0047 aa01          	or	a,#1
 507  0049 f7            	ld	(x),a
 509  004a 2006          	jra	L342
 510  004c               L142:
 511                     ; 230     ADCx->CR1 &= (uint8_t)~ADC_CR1_ADON;
 513  004c 1e01          	ldw	x,(OFST+1,sp)
 514  004e f6            	ld	a,(x)
 515  004f a4fe          	and	a,#254
 516  0051 f7            	ld	(x),a
 517  0052               L342:
 518                     ; 232 }
 521  0052 85            	popw	x
 522  0053 81            	ret
 560                     ; 239 void ADC_SoftwareStartConv(ADC_TypeDef* ADCx)
 560                     ; 240 {
 561                     	switch	.text
 562  0054               _ADC_SoftwareStartConv:
 566                     ; 242   ADCx->CR1 |= ADC_CR1_START;
 568  0054 f6            	ld	a,(x)
 569  0055 aa02          	or	a,#2
 570  0057 f7            	ld	(x),a
 571                     ; 243 }
 574  0058 81            	ret
 702                     ; 261 void ADC_ExternalTrigConfig(ADC_TypeDef* ADCx,
 702                     ; 262                             ADC_ExtEventSelection_TypeDef ADC_ExtEventSelection,
 702                     ; 263                             ADC_ExtTRGSensitivity_TypeDef ADC_ExtTRGSensitivity)
 702                     ; 264 {
 703                     	switch	.text
 704  0059               _ADC_ExternalTrigConfig:
 706  0059 89            	pushw	x
 707       00000000      OFST:	set	0
 710                     ; 266   assert_param(IS_ADC_EXT_EVENT_SELECTION(ADC_ExtEventSelection));
 712                     ; 267   assert_param(IS_ADC_EXT_TRG_SENSITIVITY(ADC_ExtTRGSensitivity));
 714                     ; 270   ADCx->CR2 &= (uint8_t)~(ADC_CR2_TRIGEDGE | ADC_CR2_EXTSEL);
 716  005a e601          	ld	a,(1,x)
 717  005c a487          	and	a,#135
 718  005e e701          	ld	(1,x),a
 719                     ; 274   ADCx->CR2 |= (uint8_t)( (uint8_t)ADC_ExtTRGSensitivity | \
 719                     ; 275                           (uint8_t)ADC_ExtEventSelection);
 721  0060 7b06          	ld	a,(OFST+6,sp)
 722  0062 1a05          	or	a,(OFST+5,sp)
 723  0064 ea01          	or	a,(1,x)
 724  0066 e701          	ld	(1,x),a
 725                     ; 276 }
 728  0068 85            	popw	x
 729  0069 81            	ret
1043                     ; 339 void ADC_AnalogWatchdogChannelSelect(ADC_TypeDef* ADCx,
1043                     ; 340                                      ADC_AnalogWatchdogSelection_TypeDef ADC_AnalogWatchdogSelection)
1043                     ; 341 {
1044                     	switch	.text
1045  006a               _ADC_AnalogWatchdogChannelSelect:
1047  006a 89            	pushw	x
1048       00000000      OFST:	set	0
1051                     ; 343   assert_param(IS_ADC_ANALOGWATCHDOG_SELECTION(ADC_AnalogWatchdogSelection));
1053                     ; 346   ADCx->CR3 &= ((uint8_t)~ADC_CR3_CHSEL);
1055  006b e602          	ld	a,(2,x)
1056  006d a4e0          	and	a,#224
1057  006f e702          	ld	(2,x),a
1058                     ; 349   ADCx->CR3 |= (uint8_t)ADC_AnalogWatchdogSelection;
1060  0071 e602          	ld	a,(2,x)
1061  0073 1a05          	or	a,(OFST+5,sp)
1062  0075 e702          	ld	(2,x),a
1063                     ; 350 }
1066  0077 85            	popw	x
1067  0078 81            	ret
1123                     ; 361 void ADC_AnalogWatchdogThresholdsConfig(ADC_TypeDef* ADCx, uint16_t HighThreshold, uint16_t LowThreshold)
1123                     ; 362 {
1124                     	switch	.text
1125  0079               _ADC_AnalogWatchdogThresholdsConfig:
1127  0079 89            	pushw	x
1128       00000000      OFST:	set	0
1131                     ; 364   assert_param(IS_ADC_THRESHOLD(HighThreshold));
1133                     ; 365   assert_param(IS_ADC_THRESHOLD(LowThreshold));
1135                     ; 368   ADCx->HTRH = (uint8_t)(HighThreshold >> 8);
1137  007a 7b05          	ld	a,(OFST+5,sp)
1138  007c 1e01          	ldw	x,(OFST+1,sp)
1139  007e e706          	ld	(6,x),a
1140                     ; 369   ADCx->HTRL = (uint8_t)(HighThreshold);
1142  0080 7b06          	ld	a,(OFST+6,sp)
1143  0082 1e01          	ldw	x,(OFST+1,sp)
1144  0084 e707          	ld	(7,x),a
1145                     ; 372   ADCx->LTRH = (uint8_t)(LowThreshold >> 8);
1147  0086 7b07          	ld	a,(OFST+7,sp)
1148  0088 1e01          	ldw	x,(OFST+1,sp)
1149  008a e708          	ld	(8,x),a
1150                     ; 373   ADCx->LTRL = (uint8_t)(LowThreshold);
1152  008c 7b08          	ld	a,(OFST+8,sp)
1153  008e 1e01          	ldw	x,(OFST+1,sp)
1154  0090 e709          	ld	(9,x),a
1155                     ; 374 }
1158  0092 85            	popw	x
1159  0093 81            	ret
1226                     ; 412 void ADC_AnalogWatchdogConfig(ADC_TypeDef* ADCx,
1226                     ; 413                               ADC_AnalogWatchdogSelection_TypeDef ADC_AnalogWatchdogSelection,
1226                     ; 414                               uint16_t HighThreshold,
1226                     ; 415                               uint16_t LowThreshold)
1226                     ; 416 {
1227                     	switch	.text
1228  0094               _ADC_AnalogWatchdogConfig:
1230  0094 89            	pushw	x
1231       00000000      OFST:	set	0
1234                     ; 418   assert_param(IS_ADC_ANALOGWATCHDOG_SELECTION(ADC_AnalogWatchdogSelection));
1236                     ; 419   assert_param(IS_ADC_THRESHOLD(HighThreshold));
1238                     ; 420   assert_param(IS_ADC_THRESHOLD(LowThreshold));
1240                     ; 423   ADCx->CR3 &= ((uint8_t)~ADC_CR3_CHSEL);
1242  0095 e602          	ld	a,(2,x)
1243  0097 a4e0          	and	a,#224
1244  0099 e702          	ld	(2,x),a
1245                     ; 426   ADCx->CR3 |= (uint8_t)ADC_AnalogWatchdogSelection;
1247  009b e602          	ld	a,(2,x)
1248  009d 1a05          	or	a,(OFST+5,sp)
1249  009f e702          	ld	(2,x),a
1250                     ; 429   ADCx->HTRH = (uint8_t)(HighThreshold >> 8);
1252  00a1 7b06          	ld	a,(OFST+6,sp)
1253  00a3 1e01          	ldw	x,(OFST+1,sp)
1254  00a5 e706          	ld	(6,x),a
1255                     ; 430   ADCx->HTRL = (uint8_t)(HighThreshold);
1257  00a7 7b07          	ld	a,(OFST+7,sp)
1258  00a9 1e01          	ldw	x,(OFST+1,sp)
1259  00ab e707          	ld	(7,x),a
1260                     ; 433   ADCx->LTRH = (uint8_t)(LowThreshold >> 8);
1262  00ad 7b08          	ld	a,(OFST+8,sp)
1263  00af 1e01          	ldw	x,(OFST+1,sp)
1264  00b1 e708          	ld	(8,x),a
1265                     ; 434   ADCx->LTRL = (uint8_t)LowThreshold;
1267  00b3 7b09          	ld	a,(OFST+9,sp)
1268  00b5 1e01          	ldw	x,(OFST+1,sp)
1269  00b7 e709          	ld	(9,x),a
1270                     ; 435 }
1273  00b9 85            	popw	x
1274  00ba 81            	ret
1309                     ; 474 void ADC_TempSensorCmd(FunctionalState NewState)
1309                     ; 475 {
1310                     	switch	.text
1311  00bb               _ADC_TempSensorCmd:
1315                     ; 477   assert_param(IS_FUNCTIONAL_STATE(NewState));
1317                     ; 479   if (NewState != DISABLE)
1319  00bb 4d            	tnz	a
1320  00bc 2706          	jreq	L175
1321                     ; 482     ADC1->TRIGR[0] |= (uint8_t)(ADC_TRIGR1_TSON);
1323  00be 721a534e      	bset	21326,#5
1325  00c2 2004          	jra	L375
1326  00c4               L175:
1327                     ; 487     ADC1->TRIGR[0] &= (uint8_t)(~ADC_TRIGR1_TSON);
1329  00c4 721b534e      	bres	21326,#5
1330  00c8               L375:
1331                     ; 489 }
1334  00c8 81            	ret
1369                     ; 497 void ADC_VrefintCmd(FunctionalState NewState)
1369                     ; 498 {
1370                     	switch	.text
1371  00c9               _ADC_VrefintCmd:
1375                     ; 500   assert_param(IS_FUNCTIONAL_STATE(NewState));
1377                     ; 502   if (NewState != DISABLE)
1379  00c9 4d            	tnz	a
1380  00ca 2706          	jreq	L316
1381                     ; 505     ADC1->TRIGR[0] |= (uint8_t)(ADC_TRIGR1_VREFINTON);
1383  00cc 7218534e      	bset	21326,#4
1385  00d0 2004          	jra	L516
1386  00d2               L316:
1387                     ; 510     ADC1->TRIGR[0] &= (uint8_t)(~ADC_TRIGR1_VREFINTON);
1389  00d2 7219534e      	bres	21326,#4
1390  00d6               L516:
1391                     ; 512 }
1394  00d6 81            	ret
1706                     ; 583 void ADC_ChannelCmd(ADC_TypeDef* ADCx, ADC_Channel_TypeDef ADC_Channels, FunctionalState NewState)
1706                     ; 584 {
1707                     	switch	.text
1708  00d7               _ADC_ChannelCmd:
1710  00d7 89            	pushw	x
1711  00d8 88            	push	a
1712       00000001      OFST:	set	1
1715                     ; 585   uint8_t regindex = 0;
1717                     ; 587   assert_param(IS_FUNCTIONAL_STATE(NewState));
1719                     ; 589   regindex = (uint8_t)((uint16_t)ADC_Channels >> 8);
1721  00d9 7b06          	ld	a,(OFST+5,sp)
1722  00db 6b01          	ld	(OFST+0,sp),a
1724                     ; 591   if (NewState != DISABLE)
1726  00dd 0d08          	tnz	(OFST+7,sp)
1727  00df 270f          	jreq	L367
1728                     ; 594     ADCx->SQR[regindex] |= (uint8_t)(ADC_Channels);
1730  00e1 01            	rrwa	x,a
1731  00e2 1b01          	add	a,(OFST+0,sp)
1732  00e4 2401          	jrnc	L23
1733  00e6 5c            	incw	x
1734  00e7               L23:
1735  00e7 02            	rlwa	x,a
1736  00e8 e60a          	ld	a,(10,x)
1737  00ea 1a07          	or	a,(OFST+6,sp)
1738  00ec e70a          	ld	(10,x),a
1740  00ee 2012          	jra	L567
1741  00f0               L367:
1742                     ; 599     ADCx->SQR[regindex] &= (uint8_t)(~(uint8_t)(ADC_Channels));
1744  00f0 7b02          	ld	a,(OFST+1,sp)
1745  00f2 97            	ld	xl,a
1746  00f3 7b03          	ld	a,(OFST+2,sp)
1747  00f5 1b01          	add	a,(OFST+0,sp)
1748  00f7 2401          	jrnc	L43
1749  00f9 5c            	incw	x
1750  00fa               L43:
1751  00fa 02            	rlwa	x,a
1752  00fb 7b07          	ld	a,(OFST+6,sp)
1753  00fd 43            	cpl	a
1754  00fe e40a          	and	a,(10,x)
1755  0100 e70a          	ld	(10,x),a
1756  0102               L567:
1757                     ; 601 }
1760  0102 5b03          	addw	sp,#3
1761  0104 81            	ret
1911                     ; 625 void ADC_SamplingTimeConfig(ADC_TypeDef* ADCx,
1911                     ; 626                             ADC_Group_TypeDef ADC_GroupChannels,
1911                     ; 627                             ADC_SamplingTime_TypeDef ADC_SamplingTime)
1911                     ; 628 {
1912                     	switch	.text
1913  0105               _ADC_SamplingTimeConfig:
1915  0105 89            	pushw	x
1916       00000000      OFST:	set	0
1919                     ; 630   assert_param(IS_ADC_GROUP(ADC_GroupChannels));
1921                     ; 631   assert_param(IS_ADC_SAMPLING_TIME_CYCLES(ADC_SamplingTime));
1923                     ; 633   if ( ADC_GroupChannels != ADC_Group_SlowChannels)
1925  0106 0d05          	tnz	(OFST+5,sp)
1926  0108 2712          	jreq	L3501
1927                     ; 636     ADCx->CR3 &= (uint8_t)~ADC_CR3_SMPT2;
1929  010a e602          	ld	a,(2,x)
1930  010c a41f          	and	a,#31
1931  010e e702          	ld	(2,x),a
1932                     ; 637     ADCx->CR3 |= (uint8_t)(ADC_SamplingTime << 5);
1934  0110 7b06          	ld	a,(OFST+6,sp)
1935  0112 4e            	swap	a
1936  0113 48            	sll	a
1937  0114 a4e0          	and	a,#224
1938  0116 ea02          	or	a,(2,x)
1939  0118 e702          	ld	(2,x),a
1941  011a 2010          	jra	L5501
1942  011c               L3501:
1943                     ; 642     ADCx->CR2 &= (uint8_t)~ADC_CR2_SMPT1;
1945  011c 1e01          	ldw	x,(OFST+1,sp)
1946  011e e601          	ld	a,(1,x)
1947  0120 a4f8          	and	a,#248
1948  0122 e701          	ld	(1,x),a
1949                     ; 643     ADCx->CR2 |= (uint8_t)ADC_SamplingTime;
1951  0124 1e01          	ldw	x,(OFST+1,sp)
1952  0126 e601          	ld	a,(1,x)
1953  0128 1a06          	or	a,(OFST+6,sp)
1954  012a e701          	ld	(1,x),a
1955  012c               L5501:
1956                     ; 645 }
1959  012c 85            	popw	x
1960  012d 81            	ret
2027                     ; 691 void ADC_SchmittTriggerConfig(ADC_TypeDef* ADCx, ADC_Channel_TypeDef ADC_Channels,
2027                     ; 692                               FunctionalState NewState)
2027                     ; 693 {
2028                     	switch	.text
2029  012e               _ADC_SchmittTriggerConfig:
2031  012e 89            	pushw	x
2032  012f 88            	push	a
2033       00000001      OFST:	set	1
2036                     ; 694   uint8_t regindex = 0;
2038                     ; 696   assert_param(IS_FUNCTIONAL_STATE(NewState));
2040                     ; 698   regindex = (uint8_t)((uint16_t)ADC_Channels >> 8);
2042  0130 7b06          	ld	a,(OFST+5,sp)
2043  0132 6b01          	ld	(OFST+0,sp),a
2045                     ; 700   if (NewState != DISABLE)
2047  0134 0d08          	tnz	(OFST+7,sp)
2048  0136 2710          	jreq	L3111
2049                     ; 703     ADCx->TRIGR[regindex] &= (uint8_t)(~(uint8_t)ADC_Channels);
2051  0138 01            	rrwa	x,a
2052  0139 1b01          	add	a,(OFST+0,sp)
2053  013b 2401          	jrnc	L24
2054  013d 5c            	incw	x
2055  013e               L24:
2056  013e 02            	rlwa	x,a
2057  013f 7b07          	ld	a,(OFST+6,sp)
2058  0141 43            	cpl	a
2059  0142 e40e          	and	a,(14,x)
2060  0144 e70e          	ld	(14,x),a
2062  0146 2011          	jra	L5111
2063  0148               L3111:
2064                     ; 708     ADCx->TRIGR[regindex] |= (uint8_t)(ADC_Channels);
2066  0148 7b02          	ld	a,(OFST+1,sp)
2067  014a 97            	ld	xl,a
2068  014b 7b03          	ld	a,(OFST+2,sp)
2069  014d 1b01          	add	a,(OFST+0,sp)
2070  014f 2401          	jrnc	L44
2071  0151 5c            	incw	x
2072  0152               L44:
2073  0152 02            	rlwa	x,a
2074  0153 e60e          	ld	a,(14,x)
2075  0155 1a07          	or	a,(OFST+6,sp)
2076  0157 e70e          	ld	(14,x),a
2077  0159               L5111:
2078                     ; 710 }
2081  0159 5b03          	addw	sp,#3
2082  015b 81            	ret
2129                     ; 717 uint16_t ADC_GetConversionValue(ADC_TypeDef* ADCx)
2129                     ; 718 {
2130                     	switch	.text
2131  015c               _ADC_GetConversionValue:
2133  015c 89            	pushw	x
2134  015d 89            	pushw	x
2135       00000002      OFST:	set	2
2138                     ; 719   uint16_t tmpreg = 0;
2140                     ; 722   tmpreg = (uint16_t)(ADCx->DRH);
2142  015e e604          	ld	a,(4,x)
2143  0160 5f            	clrw	x
2144  0161 97            	ld	xl,a
2145  0162 1f01          	ldw	(OFST-1,sp),x
2147                     ; 723   tmpreg = (uint16_t)((uint16_t)((uint16_t)tmpreg << 8) | ADCx->DRL);
2149  0164 1e01          	ldw	x,(OFST-1,sp)
2150  0166 1603          	ldw	y,(OFST+1,sp)
2151  0168 90e605        	ld	a,(5,y)
2152  016b 02            	rlwa	x,a
2153  016c 1f01          	ldw	(OFST-1,sp),x
2155                     ; 726   return (uint16_t)(tmpreg) ;
2157  016e 1e01          	ldw	x,(OFST-1,sp)
2160  0170 5b04          	addw	sp,#4
2161  0172 81            	ret
2208                     ; 760 void ADC_DMACmd(ADC_TypeDef* ADCx, FunctionalState NewState)
2208                     ; 761 {
2209                     	switch	.text
2210  0173               _ADC_DMACmd:
2212  0173 89            	pushw	x
2213       00000000      OFST:	set	0
2216                     ; 763   assert_param(IS_FUNCTIONAL_STATE(NewState));
2218                     ; 765   if (NewState != DISABLE)
2220  0174 0d05          	tnz	(OFST+5,sp)
2221  0176 2708          	jreq	L7611
2222                     ; 768     ADCx->SQR[0] &= (uint8_t)~ADC_SQR1_DMAOFF;
2224  0178 e60a          	ld	a,(10,x)
2225  017a a47f          	and	a,#127
2226  017c e70a          	ld	(10,x),a
2228  017e 2008          	jra	L1711
2229  0180               L7611:
2230                     ; 773     ADCx->SQR[0] |= ADC_SQR1_DMAOFF;
2232  0180 1e01          	ldw	x,(OFST+1,sp)
2233  0182 e60a          	ld	a,(10,x)
2234  0184 aa80          	or	a,#128
2235  0186 e70a          	ld	(10,x),a
2236  0188               L1711:
2237                     ; 775 }
2240  0188 85            	popw	x
2241  0189 81            	ret
2325                     ; 831 void ADC_ITConfig(ADC_TypeDef* ADCx, ADC_IT_TypeDef ADC_IT, FunctionalState NewState)
2325                     ; 832 {
2326                     	switch	.text
2327  018a               _ADC_ITConfig:
2329  018a 89            	pushw	x
2330       00000000      OFST:	set	0
2333                     ; 834   assert_param(IS_FUNCTIONAL_STATE(NewState));
2335                     ; 835   assert_param(IS_ADC_IT(ADC_IT));
2337                     ; 837   if (NewState != DISABLE)
2339  018b 0d06          	tnz	(OFST+6,sp)
2340  018d 2706          	jreq	L5321
2341                     ; 840     ADCx->CR1 |= (uint8_t) ADC_IT;
2343  018f f6            	ld	a,(x)
2344  0190 1a05          	or	a,(OFST+5,sp)
2345  0192 f7            	ld	(x),a
2347  0193 2007          	jra	L7321
2348  0195               L5321:
2349                     ; 845     ADCx->CR1 &= (uint8_t)(~ADC_IT);
2351  0195 1e01          	ldw	x,(OFST+1,sp)
2352  0197 7b05          	ld	a,(OFST+5,sp)
2353  0199 43            	cpl	a
2354  019a f4            	and	a,(x)
2355  019b f7            	ld	(x),a
2356  019c               L7321:
2357                     ; 847 }
2360  019c 85            	popw	x
2361  019d 81            	ret
2466                     ; 859 FlagStatus ADC_GetFlagStatus(ADC_TypeDef* ADCx, ADC_FLAG_TypeDef ADC_FLAG)
2466                     ; 860 {
2467                     	switch	.text
2468  019e               _ADC_GetFlagStatus:
2470  019e 89            	pushw	x
2471  019f 88            	push	a
2472       00000001      OFST:	set	1
2475                     ; 861   FlagStatus flagstatus = RESET;
2477                     ; 864   assert_param(IS_ADC_GET_FLAG(ADC_FLAG));
2479                     ; 867   if ((ADCx->SR & ADC_FLAG) != (uint8_t)RESET)
2481  01a0 e603          	ld	a,(3,x)
2482  01a2 1506          	bcp	a,(OFST+5,sp)
2483  01a4 2706          	jreq	L3131
2484                     ; 870     flagstatus = SET;
2486  01a6 a601          	ld	a,#1
2487  01a8 6b01          	ld	(OFST+0,sp),a
2490  01aa 2002          	jra	L5131
2491  01ac               L3131:
2492                     ; 875     flagstatus = RESET;
2494  01ac 0f01          	clr	(OFST+0,sp)
2496  01ae               L5131:
2497                     ; 879   return  flagstatus;
2499  01ae 7b01          	ld	a,(OFST+0,sp)
2502  01b0 5b03          	addw	sp,#3
2503  01b2 81            	ret
2550                     ; 892 void ADC_ClearFlag(ADC_TypeDef* ADCx,
2550                     ; 893                    ADC_FLAG_TypeDef ADC_FLAG)
2550                     ; 894 {
2551                     	switch	.text
2552  01b3               _ADC_ClearFlag:
2554  01b3 89            	pushw	x
2555       00000000      OFST:	set	0
2558                     ; 896   assert_param(IS_ADC_CLEAR_FLAG(ADC_FLAG));
2560                     ; 899   ADCx->SR = (uint8_t)~ADC_FLAG;
2562  01b4 7b05          	ld	a,(OFST+5,sp)
2563  01b6 43            	cpl	a
2564  01b7 1e01          	ldw	x,(OFST+1,sp)
2565  01b9 e703          	ld	(3,x),a
2566                     ; 900 }
2569  01bb 85            	popw	x
2570  01bc 81            	ret
2646                     ; 912 ITStatus ADC_GetITStatus(ADC_TypeDef* ADCx,
2646                     ; 913                          ADC_IT_TypeDef ADC_IT)
2646                     ; 914 {
2647                     	switch	.text
2648  01bd               _ADC_GetITStatus:
2650  01bd 89            	pushw	x
2651  01be 5203          	subw	sp,#3
2652       00000003      OFST:	set	3
2655                     ; 915   ITStatus itstatus = RESET;
2657                     ; 916   uint8_t itmask = 0, enablestatus = 0;
2661                     ; 919   assert_param(IS_ADC_GET_IT(ADC_IT));
2663                     ; 922   itmask = (uint8_t)(ADC_IT >> 3);
2665  01c0 7b08          	ld	a,(OFST+5,sp)
2666  01c2 44            	srl	a
2667  01c3 44            	srl	a
2668  01c4 44            	srl	a
2669  01c5 6b03          	ld	(OFST+0,sp),a
2671                     ; 923   itmask =  (uint8_t)((uint8_t)((uint8_t)(itmask & (uint8_t)0x10) >> 2) | \
2671                     ; 924                                 (uint8_t)(itmask & (uint8_t)0x03));
2673  01c7 7b03          	ld	a,(OFST+0,sp)
2674  01c9 a403          	and	a,#3
2675  01cb 6b01          	ld	(OFST-2,sp),a
2677  01cd 7b03          	ld	a,(OFST+0,sp)
2678  01cf a410          	and	a,#16
2679  01d1 44            	srl	a
2680  01d2 44            	srl	a
2681  01d3 1a01          	or	a,(OFST-2,sp)
2682  01d5 6b03          	ld	(OFST+0,sp),a
2684                     ; 927   enablestatus = (uint8_t)(ADCx->CR1 & (uint8_t)ADC_IT) ;
2686  01d7 f6            	ld	a,(x)
2687  01d8 1408          	and	a,(OFST+5,sp)
2688  01da 6b02          	ld	(OFST-1,sp),a
2690                     ; 930   if (((ADCx->SR & itmask) != (uint8_t)RESET) && enablestatus)
2692  01dc e603          	ld	a,(3,x)
2693  01de 1503          	bcp	a,(OFST+0,sp)
2694  01e0 270a          	jreq	L3041
2696  01e2 0d02          	tnz	(OFST-1,sp)
2697  01e4 2706          	jreq	L3041
2698                     ; 933     itstatus = SET;
2700  01e6 a601          	ld	a,#1
2701  01e8 6b03          	ld	(OFST+0,sp),a
2704  01ea 2002          	jra	L5041
2705  01ec               L3041:
2706                     ; 938     itstatus = RESET;
2708  01ec 0f03          	clr	(OFST+0,sp)
2710  01ee               L5041:
2711                     ; 942   return  itstatus;
2713  01ee 7b03          	ld	a,(OFST+0,sp)
2716  01f0 5b05          	addw	sp,#5
2717  01f2 81            	ret
2774                     ; 955 void ADC_ClearITPendingBit(ADC_TypeDef* ADCx,
2774                     ; 956                            ADC_IT_TypeDef ADC_IT)
2774                     ; 957 {
2775                     	switch	.text
2776  01f3               _ADC_ClearITPendingBit:
2778  01f3 89            	pushw	x
2779  01f4 89            	pushw	x
2780       00000002      OFST:	set	2
2783                     ; 958   uint8_t itmask = 0;
2785                     ; 961   assert_param(IS_ADC_IT(ADC_IT));
2787                     ; 964   itmask = (uint8_t)(ADC_IT >> 3);
2789  01f5 7b07          	ld	a,(OFST+5,sp)
2790  01f7 44            	srl	a
2791  01f8 44            	srl	a
2792  01f9 44            	srl	a
2793  01fa 6b02          	ld	(OFST+0,sp),a
2795                     ; 965   itmask =  (uint8_t)((uint8_t)(((uint8_t)(itmask & (uint8_t)0x10)) >> 2) | \
2795                     ; 966                                  (uint8_t)(itmask & (uint8_t)0x03));
2797  01fc 7b02          	ld	a,(OFST+0,sp)
2798  01fe a403          	and	a,#3
2799  0200 6b01          	ld	(OFST-1,sp),a
2801  0202 7b02          	ld	a,(OFST+0,sp)
2802  0204 a410          	and	a,#16
2803  0206 44            	srl	a
2804  0207 44            	srl	a
2805  0208 1a01          	or	a,(OFST-1,sp)
2806  020a 6b02          	ld	(OFST+0,sp),a
2808                     ; 969   ADCx->SR = (uint8_t)~itmask;
2810  020c 7b02          	ld	a,(OFST+0,sp)
2811  020e 43            	cpl	a
2812  020f 1e03          	ldw	x,(OFST+1,sp)
2813  0211 e703          	ld	(3,x),a
2814                     ; 970 }
2817  0213 5b04          	addw	sp,#4
2818  0215 81            	ret
2831                     	xdef	_ADC_ClearITPendingBit
2832                     	xdef	_ADC_GetITStatus
2833                     	xdef	_ADC_ClearFlag
2834                     	xdef	_ADC_GetFlagStatus
2835                     	xdef	_ADC_ITConfig
2836                     	xdef	_ADC_DMACmd
2837                     	xdef	_ADC_GetConversionValue
2838                     	xdef	_ADC_SchmittTriggerConfig
2839                     	xdef	_ADC_SamplingTimeConfig
2840                     	xdef	_ADC_ChannelCmd
2841                     	xdef	_ADC_VrefintCmd
2842                     	xdef	_ADC_TempSensorCmd
2843                     	xdef	_ADC_AnalogWatchdogConfig
2844                     	xdef	_ADC_AnalogWatchdogThresholdsConfig
2845                     	xdef	_ADC_AnalogWatchdogChannelSelect
2846                     	xdef	_ADC_ExternalTrigConfig
2847                     	xdef	_ADC_SoftwareStartConv
2848                     	xdef	_ADC_Cmd
2849                     	xdef	_ADC_Init
2850                     	xdef	_ADC_DeInit
2869                     	end
