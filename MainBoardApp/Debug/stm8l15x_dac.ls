   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.10 - 06 Jul 2017
   3                     ; Generator (Limited) V4.4.7 - 05 Oct 2017
  43                     ; 142 void DAC_DeInit(void)
  43                     ; 143 {
  45                     	switch	.text
  46  0000               _DAC_DeInit:
  50                     ; 145   DAC->CH1CR1 = DAC_CR1_RESET_VALUE;
  52  0000 725f5380      	clr	21376
  53                     ; 146   DAC->CH1CR2 = DAC_CR2_RESET_VALUE;
  55  0004 725f5381      	clr	21377
  56                     ; 149   DAC->CH2CR1 = DAC_CR1_RESET_VALUE;
  58  0008 725f5382      	clr	21378
  59                     ; 150   DAC->CH2CR2 = DAC_CR2_RESET_VALUE;
  61  000c 725f5383      	clr	21379
  62                     ; 153   DAC->SWTRIGR = DAC_SWTRIGR_RESET_VALUE;
  64  0010 725f5384      	clr	21380
  65                     ; 156   DAC->SR = (uint8_t)~DAC_SR_RESET_VALUE;
  67  0014 35ff5385      	mov	21381,#255
  68                     ; 159   DAC->CH1RDHRH = DAC_RDHRH_RESET_VALUE;
  70  0018 725f5388      	clr	21384
  71                     ; 160   DAC->CH1RDHRL = DAC_RDHRL_RESET_VALUE;
  73  001c 725f5389      	clr	21385
  74                     ; 161   DAC->CH1LDHRH = DAC_LDHRH_RESET_VALUE;
  76  0020 725f538c      	clr	21388
  77                     ; 162   DAC->CH1LDHRL = DAC_LDHRL_RESET_VALUE;
  79  0024 725f538d      	clr	21389
  80                     ; 163   DAC->CH1DHR8 = DAC_DHR8_RESET_VALUE;
  82  0028 725f5390      	clr	21392
  83                     ; 166   DAC->CH2RDHRH = DAC_RDHRH_RESET_VALUE;
  85  002c 725f5394      	clr	21396
  86                     ; 167   DAC->CH2RDHRL = DAC_RDHRL_RESET_VALUE;
  88  0030 725f5395      	clr	21397
  89                     ; 168   DAC->CH2LDHRH = DAC_LDHRH_RESET_VALUE;
  91  0034 725f5398      	clr	21400
  92                     ; 169   DAC->CH2LDHRL = DAC_LDHRL_RESET_VALUE;
  94  0038 725f5399      	clr	21401
  95                     ; 170   DAC->CH2DHR8 = DAC_DHR8_RESET_VALUE;
  97  003c 725f539c      	clr	21404
  98                     ; 173   DAC->DCH1RDHRH = DAC_RDHRH_RESET_VALUE;
 100  0040 725f53a0      	clr	21408
 101                     ; 174   DAC->DCH1RDHRL = DAC_RDHRL_RESET_VALUE;
 103  0044 725f53a1      	clr	21409
 104                     ; 175   DAC->DCH2RDHRH = DAC_RDHRH_RESET_VALUE;
 106  0048 725f53a2      	clr	21410
 107                     ; 176   DAC->DCH2RDHRL = DAC_RDHRL_RESET_VALUE;
 109  004c 725f53a3      	clr	21411
 110                     ; 179   DAC->DCH1LDHRH = DAC_LDHRH_RESET_VALUE;
 112  0050 725f53a4      	clr	21412
 113                     ; 180   DAC->DCH1LDHRL = DAC_LDHRL_RESET_VALUE;
 115  0054 725f53a5      	clr	21413
 116                     ; 181   DAC->DCH2LDHRH = DAC_LDHRH_RESET_VALUE;
 118  0058 725f53a6      	clr	21414
 119                     ; 182   DAC->DCH2LDHRL = DAC_LDHRL_RESET_VALUE;
 121  005c 725f53a7      	clr	21415
 122                     ; 185   DAC->DCH1DHR8 = DAC_DHR8_RESET_VALUE;
 124  0060 725f53a8      	clr	21416
 125                     ; 186   DAC->DCH2DHR8 = DAC_DHR8_RESET_VALUE;
 127  0064 725f53a9      	clr	21417
 128                     ; 187 }
 131  0068 81            	ret
 290                     ; 208 void DAC_Init(DAC_Channel_TypeDef DAC_Channel,
 290                     ; 209               DAC_Trigger_TypeDef DAC_Trigger,
 290                     ; 210               DAC_OutputBuffer_TypeDef DAC_OutputBuffer)
 290                     ; 211 {
 291                     	switch	.text
 292  0069               _DAC_Init:
 294  0069 89            	pushw	x
 295  006a 5203          	subw	sp,#3
 296       00000003      OFST:	set	3
 299                     ; 212   uint8_t tmpreg = 0;
 301                     ; 213   uint16_t tmpreg2 = 0;
 303                     ; 216   assert_param(IS_DAC_CHANNEL(DAC_Channel));
 305                     ; 217   assert_param(IS_DAC_TRIGGER(DAC_Trigger));
 307                     ; 218   assert_param(IS_DAC_OUTPUT_BUFFER_STATE(DAC_OutputBuffer));
 309                     ; 221   tmpreg2 =  (uint16_t)((uint8_t)((uint8_t)DAC_Channel << 1));
 311  006c 9e            	ld	a,xh
 312  006d 48            	sll	a
 313  006e 5f            	clrw	x
 314  006f 97            	ld	xl,a
 315  0070 1f01          	ldw	(OFST-2,sp),x
 317                     ; 222   tmpreg = *(uint8_t*)((uint16_t)(DAC_BASE + CR1_Offset + tmpreg2));
 319  0072 1e01          	ldw	x,(OFST-2,sp)
 320  0074 d65380        	ld	a,(21376,x)
 321  0077 6b03          	ld	(OFST+0,sp),a
 323                     ; 225   tmpreg &= (uint8_t)~(DAC_CR1_BOFF | DAC_CR1_TEN | DAC_CR1_TSEL );
 325  0079 7b03          	ld	a,(OFST+0,sp)
 326  007b a4c1          	and	a,#193
 327  007d 6b03          	ld	(OFST+0,sp),a
 329                     ; 228   tmpreg |= (uint8_t)(DAC_OutputBuffer);
 331  007f 7b03          	ld	a,(OFST+0,sp)
 332  0081 1a08          	or	a,(OFST+5,sp)
 333  0083 6b03          	ld	(OFST+0,sp),a
 335                     ; 232   if (DAC_Trigger != DAC_Trigger_None)
 337  0085 7b05          	ld	a,(OFST+2,sp)
 338  0087 a130          	cp	a,#48
 339  0089 2708          	jreq	L511
 340                     ; 235     tmpreg |= (uint8_t)(DAC_CR1_TEN | DAC_Trigger) ;
 342  008b 7b05          	ld	a,(OFST+2,sp)
 343  008d aa04          	or	a,#4
 344  008f 1a03          	or	a,(OFST+0,sp)
 345  0091 6b03          	ld	(OFST+0,sp),a
 347  0093               L511:
 348                     ; 239   *(uint8_t*)((uint16_t)(DAC_BASE + CR1_Offset + tmpreg2)) = (uint8_t)tmpreg;
 350  0093 7b03          	ld	a,(OFST+0,sp)
 351  0095 1e01          	ldw	x,(OFST-2,sp)
 352  0097 d75380        	ld	(21376,x),a
 353                     ; 240 }
 356  009a 5b05          	addw	sp,#5
 357  009c 81            	ret
 431                     ; 254 void DAC_Cmd(DAC_Channel_TypeDef DAC_Channel, FunctionalState NewState)
 431                     ; 255 {
 432                     	switch	.text
 433  009d               _DAC_Cmd:
 435  009d 89            	pushw	x
 436  009e 89            	pushw	x
 437       00000002      OFST:	set	2
 440                     ; 256   uint16_t cr1addr = 0;
 442                     ; 258   assert_param(IS_DAC_CHANNEL(DAC_Channel));
 444                     ; 259   assert_param(IS_FUNCTIONAL_STATE(NewState));
 446                     ; 262   cr1addr = DAC_BASE + CR1_Offset + (uint8_t)((uint8_t)DAC_Channel << 1);
 448  009f 9e            	ld	a,xh
 449  00a0 48            	sll	a
 450  00a1 5f            	clrw	x
 451  00a2 97            	ld	xl,a
 452  00a3 1c5380        	addw	x,#21376
 453  00a6 1f01          	ldw	(OFST-1,sp),x
 455                     ; 264   if (NewState != DISABLE)
 457  00a8 0d04          	tnz	(OFST+2,sp)
 458  00aa 2708          	jreq	L551
 459                     ; 267     (*(uint8_t*)(cr1addr)) |= DAC_CR1_EN;
 461  00ac 1e01          	ldw	x,(OFST-1,sp)
 462  00ae f6            	ld	a,(x)
 463  00af aa01          	or	a,#1
 464  00b1 f7            	ld	(x),a
 466  00b2 2006          	jra	L751
 467  00b4               L551:
 468                     ; 272     (*(uint8_t*)(cr1addr)) &= (uint8_t) ~(DAC_CR1_EN);
 470  00b4 1e01          	ldw	x,(OFST-1,sp)
 471  00b6 f6            	ld	a,(x)
 472  00b7 a4fe          	and	a,#254
 473  00b9 f7            	ld	(x),a
 474  00ba               L751:
 475                     ; 274 }
 478  00ba 5b04          	addw	sp,#4
 479  00bc 81            	ret
 525                     ; 286 void DAC_SoftwareTriggerCmd(DAC_Channel_TypeDef DAC_Channel, FunctionalState NewState)
 525                     ; 287 {
 526                     	switch	.text
 527  00bd               _DAC_SoftwareTriggerCmd:
 529  00bd 89            	pushw	x
 530       00000000      OFST:	set	0
 533                     ; 289   assert_param(IS_DAC_CHANNEL(DAC_Channel));
 535                     ; 290   assert_param(IS_FUNCTIONAL_STATE(NewState));
 537                     ; 292   if (NewState != DISABLE)
 539  00be 9f            	ld	a,xl
 540  00bf 4d            	tnz	a
 541  00c0 2714          	jreq	L302
 542                     ; 295     DAC->SWTRIGR |= (uint8_t)(DAC_SWTRIGR_SWTRIG1 << DAC_Channel);
 544  00c2 9e            	ld	a,xh
 545  00c3 5f            	clrw	x
 546  00c4 97            	ld	xl,a
 547  00c5 a601          	ld	a,#1
 548  00c7 5d            	tnzw	x
 549  00c8 2704          	jreq	L41
 550  00ca               L61:
 551  00ca 48            	sll	a
 552  00cb 5a            	decw	x
 553  00cc 26fc          	jrne	L61
 554  00ce               L41:
 555  00ce ca5384        	or	a,21380
 556  00d1 c75384        	ld	21380,a
 558  00d4 2014          	jra	L502
 559  00d6               L302:
 560                     ; 300     DAC->SWTRIGR &= (uint8_t)~((uint8_t)(DAC_SWTRIGR_SWTRIG1 << DAC_Channel));
 562  00d6 7b01          	ld	a,(OFST+1,sp)
 563  00d8 5f            	clrw	x
 564  00d9 97            	ld	xl,a
 565  00da a601          	ld	a,#1
 566  00dc 5d            	tnzw	x
 567  00dd 2704          	jreq	L02
 568  00df               L22:
 569  00df 48            	sll	a
 570  00e0 5a            	decw	x
 571  00e1 26fc          	jrne	L22
 572  00e3               L02:
 573  00e3 43            	cpl	a
 574  00e4 c45384        	and	a,21380
 575  00e7 c75384        	ld	21380,a
 576  00ea               L502:
 577                     ; 302 }
 580  00ea 85            	popw	x
 581  00eb 81            	ret
 617                     ; 311 void DAC_DualSoftwareTriggerCmd(FunctionalState NewState)
 617                     ; 312 {
 618                     	switch	.text
 619  00ec               _DAC_DualSoftwareTriggerCmd:
 623                     ; 314   assert_param(IS_FUNCTIONAL_STATE(NewState));
 625                     ; 316   if (NewState != DISABLE)
 627  00ec 4d            	tnz	a
 628  00ed 270a          	jreq	L522
 629                     ; 319     DAC->SWTRIGR |= (DAC_SWTRIGR_SWTRIG1 | DAC_SWTRIGR_SWTRIG2) ;
 631  00ef c65384        	ld	a,21380
 632  00f2 aa03          	or	a,#3
 633  00f4 c75384        	ld	21380,a
 635  00f7 2008          	jra	L722
 636  00f9               L522:
 637                     ; 324     DAC->SWTRIGR &= (uint8_t)~(DAC_SWTRIGR_SWTRIG1 | DAC_SWTRIGR_SWTRIG2);
 639  00f9 c65384        	ld	a,21380
 640  00fc a4fc          	and	a,#252
 641  00fe c75384        	ld	21380,a
 642  0101               L722:
 643                     ; 326 }
 646  0101 81            	ret
 731                     ; 342 void DAC_WaveGenerationCmd(DAC_Channel_TypeDef DAC_Channel,
 731                     ; 343                            DAC_Wave_TypeDef DAC_Wave,
 731                     ; 344                            FunctionalState NewState)
 731                     ; 345 {
 732                     	switch	.text
 733  0102               _DAC_WaveGenerationCmd:
 735  0102 89            	pushw	x
 736  0103 88            	push	a
 737       00000001      OFST:	set	1
 740                     ; 346   uint8_t tmpreg = 0;
 742                     ; 349   assert_param(IS_DAC_CHANNEL(DAC_Channel));
 744                     ; 350   assert_param(IS_DAC_WAVE(DAC_Wave));
 746                     ; 351   assert_param(IS_FUNCTIONAL_STATE(NewState));
 748                     ; 354   tmpreg = (uint8_t)((*(uint8_t*)(uint16_t)(DAC_BASE + CR1_Offset + (uint8_t)((uint8_t)DAC_Channel << 1))) & (uint8_t)~(DAC_CR1_WAVEN));
 750  0104 9e            	ld	a,xh
 751  0105 48            	sll	a
 752  0106 5f            	clrw	x
 753  0107 97            	ld	xl,a
 754  0108 d65380        	ld	a,(21376,x)
 755  010b a43f          	and	a,#63
 756  010d 6b01          	ld	(OFST+0,sp),a
 758                     ; 356   if (NewState != DISABLE)
 760  010f 0d06          	tnz	(OFST+5,sp)
 761  0111 2706          	jreq	L372
 762                     ; 358     tmpreg |= (uint8_t)(DAC_Wave);
 764  0113 7b01          	ld	a,(OFST+0,sp)
 765  0115 1a03          	or	a,(OFST+2,sp)
 766  0117 6b01          	ld	(OFST+0,sp),a
 768  0119               L372:
 769                     ; 362   (*(uint8_t*) (uint16_t)(DAC_BASE + CR1_Offset +  (uint8_t)((uint8_t)DAC_Channel << 1))) = tmpreg;
 771  0119 7b02          	ld	a,(OFST+1,sp)
 772  011b 48            	sll	a
 773  011c 5f            	clrw	x
 774  011d 97            	ld	xl,a
 775  011e 7b01          	ld	a,(OFST+0,sp)
 776  0120 d75380        	ld	(21376,x),a
 777                     ; 364 }
 780  0123 5b03          	addw	sp,#3
 781  0125 81            	ret
 947                     ; 388 void DAC_SetNoiseWaveLFSR(DAC_Channel_TypeDef DAC_Channel, DAC_LFSRUnmask_TypeDef DAC_LFSRUnmask)
 947                     ; 389 {
 948                     	switch	.text
 949  0126               _DAC_SetNoiseWaveLFSR:
 951  0126 89            	pushw	x
 952  0127 5203          	subw	sp,#3
 953       00000003      OFST:	set	3
 956                     ; 390   uint8_t tmpreg = 0;
 958                     ; 391   uint16_t cr2addr = 0;
 960                     ; 394   assert_param(IS_DAC_CHANNEL(DAC_Channel));
 962                     ; 395   assert_param(IS_DAC_LFSR_UNMASK_TRIANGLE_AMPLITUDE(DAC_LFSRUnmask));
 964                     ; 398   cr2addr = (uint16_t)(DAC_BASE + CR2_Offset + (uint8_t)((uint8_t)DAC_Channel << 1));
 966  0129 9e            	ld	a,xh
 967  012a 48            	sll	a
 968  012b 5f            	clrw	x
 969  012c 97            	ld	xl,a
 970  012d 1c5381        	addw	x,#21377
 971  0130 1f02          	ldw	(OFST-1,sp),x
 973                     ; 399   tmpreg = (uint8_t)((*(uint8_t*)(cr2addr)) & (uint8_t)~(DAC_CR2_MAMPx));
 975  0132 1e02          	ldw	x,(OFST-1,sp)
 976  0134 f6            	ld	a,(x)
 977  0135 a4f0          	and	a,#240
 978  0137 6b01          	ld	(OFST-2,sp),a
 980                     ; 402   (*(uint8_t*)(cr2addr)) = (uint8_t)( tmpreg | DAC_LFSRUnmask);
 982  0139 7b01          	ld	a,(OFST-2,sp)
 983  013b 1a05          	or	a,(OFST+2,sp)
 984  013d 1e02          	ldw	x,(OFST-1,sp)
 985  013f f7            	ld	(x),a
 986                     ; 403 }
 989  0140 5b05          	addw	sp,#5
 990  0142 81            	ret
1157                     ; 427 void DAC_SetTriangleWaveAmplitude(DAC_Channel_TypeDef DAC_Channel, DAC_TriangleAmplitude_TypeDef DAC_TriangleAmplitude)
1157                     ; 428 {
1158                     	switch	.text
1159  0143               _DAC_SetTriangleWaveAmplitude:
1161  0143 89            	pushw	x
1162  0144 5203          	subw	sp,#3
1163       00000003      OFST:	set	3
1166                     ; 429   uint8_t tmpreg = 0;
1168                     ; 430   uint16_t cr2addr = 0;
1170                     ; 433   assert_param(IS_DAC_CHANNEL(DAC_Channel));
1172                     ; 434   assert_param(IS_DAC_LFSR_UNMASK_TRIANGLE_AMPLITUDE(DAC_TriangleAmplitude));
1174                     ; 438   cr2addr = (uint16_t)(DAC_BASE + CR2_Offset + (uint8_t)((uint8_t)DAC_Channel << 1));
1176  0146 9e            	ld	a,xh
1177  0147 48            	sll	a
1178  0148 5f            	clrw	x
1179  0149 97            	ld	xl,a
1180  014a 1c5381        	addw	x,#21377
1181  014d 1f02          	ldw	(OFST-1,sp),x
1183                     ; 439   tmpreg = (uint8_t)((*(uint8_t*)(cr2addr)) & (uint8_t)~(DAC_CR2_MAMPx));
1185  014f 1e02          	ldw	x,(OFST-1,sp)
1186  0151 f6            	ld	a,(x)
1187  0152 a4f0          	and	a,#240
1188  0154 6b01          	ld	(OFST-2,sp),a
1190                     ; 442   (*(uint8_t*)(cr2addr)) = (uint8_t)( tmpreg | DAC_TriangleAmplitude);
1192  0156 7b01          	ld	a,(OFST-2,sp)
1193  0158 1a05          	or	a,(OFST+2,sp)
1194  015a 1e02          	ldw	x,(OFST-1,sp)
1195  015c f7            	ld	(x),a
1196                     ; 443 }
1199  015d 5b05          	addw	sp,#5
1200  015f 81            	ret
1272                     ; 455 void DAC_SetChannel1Data(DAC_Align_TypeDef DAC_Align, uint16_t DAC_Data)
1272                     ; 456 {
1273                     	switch	.text
1274  0160               _DAC_SetChannel1Data:
1276  0160 88            	push	a
1277       00000000      OFST:	set	0
1280                     ; 458   assert_param(IS_DAC_ALIGN(DAC_Align));
1282                     ; 460   if (DAC_Align != DAC_Align_8b_R)
1284  0161 a108          	cp	a,#8
1285  0163 2712          	jreq	L505
1286                     ; 463     *(uint8_t*)((uint16_t)(DAC_BASE + CH1RDHRH_Offset + DAC_Align )) = (uint8_t)(((uint16_t)DAC_Data) >> 8);
1288  0165 5f            	clrw	x
1289  0166 97            	ld	xl,a
1290  0167 7b04          	ld	a,(OFST+4,sp)
1291  0169 d75388        	ld	(21384,x),a
1292                     ; 464     *(uint8_t*)((uint16_t)(DAC_BASE + CH1RDHRH_Offset + 1 + DAC_Align )) = (uint8_t)DAC_Data;
1294  016c 7b01          	ld	a,(OFST+1,sp)
1295  016e 5f            	clrw	x
1296  016f 97            	ld	xl,a
1297  0170 7b05          	ld	a,(OFST+5,sp)
1298  0172 d75389        	ld	(21385,x),a
1300  0175 2005          	jra	L705
1301  0177               L505:
1302                     ; 469     assert_param(IS_DAC_DATA_08R(DAC_Data));
1304                     ; 472     DAC->CH1DHR8 = (uint8_t)(DAC_Data);
1306  0177 7b05          	ld	a,(OFST+5,sp)
1307  0179 c75390        	ld	21392,a
1308  017c               L705:
1309                     ; 474 }
1312  017c 84            	pop	a
1313  017d 81            	ret
1358                     ; 486 void DAC_SetChannel2Data(DAC_Align_TypeDef DAC_Align, uint16_t DAC_Data)
1358                     ; 487 {
1359                     	switch	.text
1360  017e               _DAC_SetChannel2Data:
1362  017e 88            	push	a
1363       00000000      OFST:	set	0
1366                     ; 489   assert_param(IS_DAC_ALIGN(DAC_Align));
1368                     ; 491   if (DAC_Align != DAC_Align_8b_R)
1370  017f a108          	cp	a,#8
1371  0181 2712          	jreq	L335
1372                     ; 494     *(uint8_t*)((uint16_t)(DAC_BASE + CH2RDHRH_Offset + DAC_Align )) = (uint8_t)(((uint16_t)DAC_Data) >> 8);
1374  0183 5f            	clrw	x
1375  0184 97            	ld	xl,a
1376  0185 7b04          	ld	a,(OFST+4,sp)
1377  0187 d75394        	ld	(21396,x),a
1378                     ; 495     *(uint8_t*)((uint16_t)(DAC_BASE + CH2RDHRH_Offset + 1 + DAC_Align )) = (uint8_t)DAC_Data;
1380  018a 7b01          	ld	a,(OFST+1,sp)
1381  018c 5f            	clrw	x
1382  018d 97            	ld	xl,a
1383  018e 7b05          	ld	a,(OFST+5,sp)
1384  0190 d75395        	ld	(21397,x),a
1386  0193 2005          	jra	L535
1387  0195               L335:
1388                     ; 500     assert_param(IS_DAC_DATA_08R(DAC_Data));
1390                     ; 503     DAC->CH2DHR8 = (uint8_t)(DAC_Data);
1392  0195 7b05          	ld	a,(OFST+5,sp)
1393  0197 c7539c        	ld	21404,a
1394  019a               L535:
1395                     ; 505 }
1398  019a 84            	pop	a
1399  019b 81            	ret
1462                     ; 522 void DAC_SetDualChannelData(DAC_Align_TypeDef DAC_Align, uint16_t DAC_Data2, uint16_t DAC_Data1)
1462                     ; 523 {
1463                     	switch	.text
1464  019c               _DAC_SetDualChannelData:
1466  019c 88            	push	a
1467  019d 89            	pushw	x
1468       00000002      OFST:	set	2
1471                     ; 524   uint16_t dchxrdhrhaddr = 0;
1473                     ; 527   assert_param(IS_DAC_ALIGN(DAC_Align));
1475                     ; 529   if (DAC_Align != DAC_Align_8b_R)
1477  019e a108          	cp	a,#8
1478  01a0 2727          	jreq	L175
1479                     ; 532     dchxrdhrhaddr = (uint16_t)(DAC_BASE + DCH1RDHRH_Offset + DAC_Align);
1481  01a2 a653          	ld	a,#83
1482  01a4 97            	ld	xl,a
1483  01a5 a6a0          	ld	a,#160
1484  01a7 1b03          	add	a,(OFST+1,sp)
1485  01a9 2401          	jrnc	L24
1486  01ab 5c            	incw	x
1487  01ac               L24:
1488  01ac 02            	rlwa	x,a
1489  01ad 1f01          	ldw	(OFST-1,sp),x
1490  01af 01            	rrwa	x,a
1492                     ; 535     *(uint8_t*)(uint16_t)dchxrdhrhaddr = (uint8_t)(((uint16_t)DAC_Data1) >> 8);
1494  01b0 7b08          	ld	a,(OFST+6,sp)
1495  01b2 1e01          	ldw	x,(OFST-1,sp)
1496  01b4 f7            	ld	(x),a
1497                     ; 536     *(uint8_t*)(uint16_t)(dchxrdhrhaddr + 1) = (uint8_t)DAC_Data1;
1499  01b5 7b09          	ld	a,(OFST+7,sp)
1500  01b7 1e01          	ldw	x,(OFST-1,sp)
1501  01b9 e701          	ld	(1,x),a
1502                     ; 537     *(uint8_t*)(uint16_t)(dchxrdhrhaddr + 2) = (uint8_t)(((uint16_t)DAC_Data2) >> 8);
1504  01bb 7b06          	ld	a,(OFST+4,sp)
1505  01bd 1e01          	ldw	x,(OFST-1,sp)
1506  01bf e702          	ld	(2,x),a
1507                     ; 538     *(uint8_t*)(uint16_t)(dchxrdhrhaddr + 3) = (uint8_t)DAC_Data2;
1509  01c1 7b07          	ld	a,(OFST+5,sp)
1510  01c3 1e01          	ldw	x,(OFST-1,sp)
1511  01c5 e703          	ld	(3,x),a
1513  01c7 200a          	jra	L375
1514  01c9               L175:
1515                     ; 543     assert_param(IS_DAC_DATA_08R(DAC_Data1 | DAC_Data2));
1517                     ; 546     DAC->DCH1DHR8 = (uint8_t)(DAC_Data1);
1519  01c9 7b09          	ld	a,(OFST+7,sp)
1520  01cb c753a8        	ld	21416,a
1521                     ; 547     DAC->DCH2DHR8 = (uint8_t)(DAC_Data2);
1523  01ce 7b07          	ld	a,(OFST+5,sp)
1524  01d0 c753a9        	ld	21417,a
1525  01d3               L375:
1526                     ; 549 }
1529  01d3 5b03          	addw	sp,#3
1530  01d5 81            	ret
1584                     ; 559 uint16_t DAC_GetDataOutputValue(DAC_Channel_TypeDef DAC_Channel)
1584                     ; 560 {
1585                     	switch	.text
1586  01d6               _DAC_GetDataOutputValue:
1588  01d6 89            	pushw	x
1589       00000002      OFST:	set	2
1592                     ; 561   uint16_t outputdata = 0;
1594                     ; 562   uint16_t tmp = 0;
1596                     ; 565   assert_param(IS_DAC_CHANNEL(DAC_Channel));
1598                     ; 567   if ( DAC_Channel ==  DAC_Channel_1)
1600  01d7 4d            	tnz	a
1601  01d8 2619          	jrne	L326
1602                     ; 570     tmp = (uint16_t)((uint16_t)DAC->CH1DORH << 8);
1604  01da c653ac        	ld	a,21420
1605  01dd 5f            	clrw	x
1606  01de 97            	ld	xl,a
1607  01df 4f            	clr	a
1608  01e0 02            	rlwa	x,a
1609  01e1 1f01          	ldw	(OFST-1,sp),x
1611                     ; 571     outputdata = (uint16_t)(tmp | (DAC->CH1DORL));
1613  01e3 c653ad        	ld	a,21421
1614  01e6 5f            	clrw	x
1615  01e7 97            	ld	xl,a
1616  01e8 01            	rrwa	x,a
1617  01e9 1a02          	or	a,(OFST+0,sp)
1618  01eb 01            	rrwa	x,a
1619  01ec 1a01          	or	a,(OFST-1,sp)
1620  01ee 01            	rrwa	x,a
1621  01ef 1f01          	ldw	(OFST-1,sp),x
1624  01f1 2017          	jra	L526
1625  01f3               L326:
1626                     ; 576     tmp = (uint16_t)((uint16_t)DAC->CH2DORH << 8);
1628  01f3 c653b0        	ld	a,21424
1629  01f6 5f            	clrw	x
1630  01f7 97            	ld	xl,a
1631  01f8 4f            	clr	a
1632  01f9 02            	rlwa	x,a
1633  01fa 1f01          	ldw	(OFST-1,sp),x
1635                     ; 577     outputdata = (uint16_t)(tmp | (DAC->CH2DORL));
1637  01fc c653b1        	ld	a,21425
1638  01ff 5f            	clrw	x
1639  0200 97            	ld	xl,a
1640  0201 01            	rrwa	x,a
1641  0202 1a02          	or	a,(OFST+0,sp)
1642  0204 01            	rrwa	x,a
1643  0205 1a01          	or	a,(OFST-1,sp)
1644  0207 01            	rrwa	x,a
1645  0208 1f01          	ldw	(OFST-1,sp),x
1647  020a               L526:
1648                     ; 581   return (uint16_t)outputdata;
1650  020a 1e01          	ldw	x,(OFST-1,sp)
1653  020c 5b02          	addw	sp,#2
1654  020e 81            	ret
1708                     ; 613 void DAC_DMACmd(DAC_Channel_TypeDef DAC_Channel, FunctionalState NewState)
1708                     ; 614 {
1709                     	switch	.text
1710  020f               _DAC_DMACmd:
1712  020f 89            	pushw	x
1713  0210 89            	pushw	x
1714       00000002      OFST:	set	2
1717                     ; 615   uint16_t cr2addr = 0;
1719                     ; 618   assert_param(IS_DAC_CHANNEL(DAC_Channel));
1721                     ; 619   assert_param(IS_FUNCTIONAL_STATE(NewState));
1723                     ; 622   cr2addr = DAC_BASE + CR2_Offset + (uint8_t)((uint8_t)DAC_Channel << 1);
1725  0211 9e            	ld	a,xh
1726  0212 48            	sll	a
1727  0213 5f            	clrw	x
1728  0214 97            	ld	xl,a
1729  0215 1c5381        	addw	x,#21377
1730  0218 1f01          	ldw	(OFST-1,sp),x
1732                     ; 624   if (NewState != DISABLE)
1734  021a 0d04          	tnz	(OFST+2,sp)
1735  021c 2708          	jreq	L556
1736                     ; 627     (*(uint8_t*)(cr2addr)) |= DAC_CR2_DMAEN;
1738  021e 1e01          	ldw	x,(OFST-1,sp)
1739  0220 f6            	ld	a,(x)
1740  0221 aa10          	or	a,#16
1741  0223 f7            	ld	(x),a
1743  0224 2006          	jra	L756
1744  0226               L556:
1745                     ; 632     (*(uint8_t*)(cr2addr)) &= (uint8_t)~(DAC_CR2_DMAEN);
1747  0226 1e01          	ldw	x,(OFST-1,sp)
1748  0228 f6            	ld	a,(x)
1749  0229 a4ef          	and	a,#239
1750  022b f7            	ld	(x),a
1751  022c               L756:
1752                     ; 634 }
1755  022c 5b04          	addw	sp,#4
1756  022e 81            	ret
1833                     ; 667 void DAC_ITConfig(DAC_Channel_TypeDef DAC_Channel, DAC_IT_TypeDef DAC_IT, FunctionalState NewState)
1833                     ; 668 {
1834                     	switch	.text
1835  022f               _DAC_ITConfig:
1837  022f 89            	pushw	x
1838  0230 89            	pushw	x
1839       00000002      OFST:	set	2
1842                     ; 669   uint16_t cr2addr = 0;
1844                     ; 672   assert_param(IS_DAC_CHANNEL(DAC_Channel));
1846                     ; 673   assert_param(IS_FUNCTIONAL_STATE(NewState));
1848                     ; 674   assert_param(IS_DAC_IT(DAC_IT));
1850                     ; 677   cr2addr = DAC_BASE + CR2_Offset + (uint8_t)((uint8_t)DAC_Channel << 1);
1852  0231 9e            	ld	a,xh
1853  0232 48            	sll	a
1854  0233 5f            	clrw	x
1855  0234 97            	ld	xl,a
1856  0235 1c5381        	addw	x,#21377
1857  0238 1f01          	ldw	(OFST-1,sp),x
1859                     ; 679   if (NewState != DISABLE)
1861  023a 0d07          	tnz	(OFST+5,sp)
1862  023c 2708          	jreq	L127
1863                     ; 682     (*(uint8_t*)(cr2addr)) |=  (uint8_t)(DAC_IT);
1865  023e 1e01          	ldw	x,(OFST-1,sp)
1866  0240 f6            	ld	a,(x)
1867  0241 1a04          	or	a,(OFST+2,sp)
1868  0243 f7            	ld	(x),a
1870  0244 2007          	jra	L327
1871  0246               L127:
1872                     ; 687     (*(uint8_t*)(cr2addr)) &= (uint8_t)(~(DAC_IT));
1874  0246 1e01          	ldw	x,(OFST-1,sp)
1875  0248 7b04          	ld	a,(OFST+2,sp)
1876  024a 43            	cpl	a
1877  024b f4            	and	a,(x)
1878  024c f7            	ld	(x),a
1879  024d               L327:
1880                     ; 689 }
1883  024d 5b04          	addw	sp,#4
1884  024f 81            	ret
1982                     ; 704 FlagStatus DAC_GetFlagStatus(DAC_Channel_TypeDef DAC_Channel, DAC_FLAG_TypeDef DAC_FLAG)
1982                     ; 705 {
1983                     	switch	.text
1984  0250               _DAC_GetFlagStatus:
1986  0250 89            	pushw	x
1987  0251 88            	push	a
1988       00000001      OFST:	set	1
1991                     ; 706   FlagStatus flagstatus = RESET;
1993                     ; 707   uint8_t flag = 0;
1995                     ; 710   assert_param(IS_DAC_CHANNEL(DAC_Channel));
1997                     ; 711   assert_param(IS_DAC_FLAG(DAC_FLAG));
1999                     ; 713   flag = (uint8_t)(DAC_FLAG << DAC_Channel);
2001  0252 9e            	ld	a,xh
2002  0253 5f            	clrw	x
2003  0254 97            	ld	xl,a
2004  0255 7b03          	ld	a,(OFST+2,sp)
2005  0257 5d            	tnzw	x
2006  0258 2704          	jreq	L45
2007  025a               L65:
2008  025a 48            	sll	a
2009  025b 5a            	decw	x
2010  025c 26fc          	jrne	L65
2011  025e               L45:
2012  025e 6b01          	ld	(OFST+0,sp),a
2014                     ; 716   if ((DAC->SR & flag ) != (uint8_t)RESET)
2016  0260 c65385        	ld	a,21381
2017  0263 1501          	bcp	a,(OFST+0,sp)
2018  0265 2706          	jreq	L577
2019                     ; 719     flagstatus = SET;
2021  0267 a601          	ld	a,#1
2022  0269 6b01          	ld	(OFST+0,sp),a
2025  026b 2002          	jra	L777
2026  026d               L577:
2027                     ; 724     flagstatus = RESET;
2029  026d 0f01          	clr	(OFST+0,sp)
2031  026f               L777:
2032                     ; 728   return  flagstatus;
2034  026f 7b01          	ld	a,(OFST+0,sp)
2037  0271 5b03          	addw	sp,#3
2038  0273 81            	ret
2092                     ; 742 void DAC_ClearFlag(DAC_Channel_TypeDef DAC_Channel, DAC_FLAG_TypeDef DAC_FLAG)
2092                     ; 743 {
2093                     	switch	.text
2094  0274               _DAC_ClearFlag:
2096  0274 89            	pushw	x
2097  0275 88            	push	a
2098       00000001      OFST:	set	1
2101                     ; 744   uint8_t flag = 0;
2103                     ; 747   assert_param(IS_DAC_CHANNEL(DAC_Channel));
2105                     ; 748   assert_param(IS_DAC_FLAG(DAC_FLAG));
2107                     ; 751   flag = (uint8_t)(DAC_FLAG << DAC_Channel);
2109  0276 9e            	ld	a,xh
2110  0277 5f            	clrw	x
2111  0278 97            	ld	xl,a
2112  0279 7b03          	ld	a,(OFST+2,sp)
2113  027b 5d            	tnzw	x
2114  027c 2704          	jreq	L26
2115  027e               L46:
2116  027e 48            	sll	a
2117  027f 5a            	decw	x
2118  0280 26fc          	jrne	L46
2119  0282               L26:
2120  0282 6b01          	ld	(OFST+0,sp),a
2122                     ; 754   DAC->SR = (uint8_t)(~flag);
2124  0284 7b01          	ld	a,(OFST+0,sp)
2125  0286 43            	cpl	a
2126  0287 c75385        	ld	21381,a
2127                     ; 755 }
2130  028a 5b03          	addw	sp,#3
2131  028c 81            	ret
2214                     ; 770 ITStatus DAC_GetITStatus(DAC_Channel_TypeDef DAC_Channel, DAC_IT_TypeDef DAC_IT)
2214                     ; 771 {
2215                     	switch	.text
2216  028d               _DAC_GetITStatus:
2218  028d 89            	pushw	x
2219  028e 89            	pushw	x
2220       00000002      OFST:	set	2
2223                     ; 772   ITStatus itstatus = RESET;
2225                     ; 773   uint8_t enablestatus = 0;
2227                     ; 774   uint8_t flagstatus = 0;
2229                     ; 775   uint8_t tempreg = 0;
2231                     ; 778   assert_param(IS_DAC_CHANNEL(DAC_Channel));
2233                     ; 779   assert_param(IS_DAC_IT(DAC_IT));
2235                     ; 782   tempreg = *(uint8_t*)(uint16_t)(DAC_BASE + CR2_Offset + (uint8_t)((uint8_t)DAC_Channel << 2));
2237  028f 9e            	ld	a,xh
2238  0290 48            	sll	a
2239  0291 48            	sll	a
2240  0292 5f            	clrw	x
2241  0293 97            	ld	xl,a
2242  0294 d65381        	ld	a,(21377,x)
2243  0297 6b02          	ld	(OFST+0,sp),a
2245                     ; 783   enablestatus = (uint8_t)( tempreg & (uint8_t)((uint8_t)DAC_IT << DAC_Channel));
2247  0299 7b03          	ld	a,(OFST+1,sp)
2248  029b 5f            	clrw	x
2249  029c 97            	ld	xl,a
2250  029d 7b04          	ld	a,(OFST+2,sp)
2251  029f 5d            	tnzw	x
2252  02a0 2704          	jreq	L07
2253  02a2               L27:
2254  02a2 48            	sll	a
2255  02a3 5a            	decw	x
2256  02a4 26fc          	jrne	L27
2257  02a6               L07:
2258  02a6 1402          	and	a,(OFST+0,sp)
2259  02a8 6b01          	ld	(OFST-1,sp),a
2261                     ; 784   flagstatus = (uint8_t)(DAC->SR & (uint8_t)(DAC_IT >> ((uint8_t)0x05 - DAC_Channel)));
2263  02aa a605          	ld	a,#5
2264  02ac 1003          	sub	a,(OFST+1,sp)
2265  02ae 5f            	clrw	x
2266  02af 97            	ld	xl,a
2267  02b0 7b04          	ld	a,(OFST+2,sp)
2268  02b2 5d            	tnzw	x
2269  02b3 2704          	jreq	L47
2270  02b5               L67:
2271  02b5 44            	srl	a
2272  02b6 5a            	decw	x
2273  02b7 26fc          	jrne	L67
2274  02b9               L47:
2275  02b9 c45385        	and	a,21381
2276  02bc 6b02          	ld	(OFST+0,sp),a
2278                     ; 787   if (((flagstatus) != (uint8_t)RESET) && enablestatus)
2280  02be 0d02          	tnz	(OFST+0,sp)
2281  02c0 270a          	jreq	L1701
2283  02c2 0d01          	tnz	(OFST-1,sp)
2284  02c4 2706          	jreq	L1701
2285                     ; 790     itstatus = SET;
2287  02c6 a601          	ld	a,#1
2288  02c8 6b02          	ld	(OFST+0,sp),a
2291  02ca 2002          	jra	L3701
2292  02cc               L1701:
2293                     ; 795     itstatus = RESET;
2295  02cc 0f02          	clr	(OFST+0,sp)
2297  02ce               L3701:
2298                     ; 799   return  itstatus;
2300  02ce 7b02          	ld	a,(OFST+0,sp)
2303  02d0 5b04          	addw	sp,#4
2304  02d2 81            	ret
2350                     ; 813 void DAC_ClearITPendingBit(DAC_Channel_TypeDef DAC_Channel, DAC_IT_TypeDef DAC_IT)
2350                     ; 814 {
2351                     	switch	.text
2352  02d3               _DAC_ClearITPendingBit:
2354  02d3 89            	pushw	x
2355       00000000      OFST:	set	0
2358                     ; 816   assert_param(IS_DAC_CHANNEL(DAC_Channel));
2360                     ; 817   assert_param(IS_DAC_IT(DAC_IT));
2362                     ; 820   DAC->SR = (uint8_t)~(uint8_t)((uint8_t)DAC_IT >> (0x05 - DAC_Channel));
2364  02d4 9e            	ld	a,xh
2365  02d5 a005          	sub	a,#5
2366  02d7 40            	neg	a
2367  02d8 5f            	clrw	x
2368  02d9 97            	ld	xl,a
2369  02da 7b02          	ld	a,(OFST+2,sp)
2370  02dc 5d            	tnzw	x
2371  02dd 2704          	jreq	L201
2372  02df               L401:
2373  02df 44            	srl	a
2374  02e0 5a            	decw	x
2375  02e1 26fc          	jrne	L401
2376  02e3               L201:
2377  02e3 43            	cpl	a
2378  02e4 c75385        	ld	21381,a
2379                     ; 821 }
2382  02e7 85            	popw	x
2383  02e8 81            	ret
2396                     	xdef	_DAC_ClearITPendingBit
2397                     	xdef	_DAC_GetITStatus
2398                     	xdef	_DAC_ClearFlag
2399                     	xdef	_DAC_GetFlagStatus
2400                     	xdef	_DAC_ITConfig
2401                     	xdef	_DAC_DMACmd
2402                     	xdef	_DAC_GetDataOutputValue
2403                     	xdef	_DAC_SetDualChannelData
2404                     	xdef	_DAC_SetChannel2Data
2405                     	xdef	_DAC_SetChannel1Data
2406                     	xdef	_DAC_SetTriangleWaveAmplitude
2407                     	xdef	_DAC_SetNoiseWaveLFSR
2408                     	xdef	_DAC_WaveGenerationCmd
2409                     	xdef	_DAC_DualSoftwareTriggerCmd
2410                     	xdef	_DAC_SoftwareTriggerCmd
2411                     	xdef	_DAC_Cmd
2412                     	xdef	_DAC_Init
2413                     	xdef	_DAC_DeInit
2432                     	end
