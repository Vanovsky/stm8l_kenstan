#include "spi_ini.h"

void SPI_INIT(SPI_TypeDef* SPIx, SPI_NSS_type SPI_NSS, SPI_MODE_type SPI_MODE)
{
	/* SPI Disnable */
	SPIx->CR1 &= ~SPI_CR1_SPE;
	
	/* Set SPI Clock */
	CLK->PCKENR1 |= CLK_PCKENR1_SPI1;
	
	SPIx->CR1 |= SPI_CR1_BR;
	
	/* Data Phase & Polarity - default */
	SPIx->CR1 &= ~SPI_CR1_CPOL;
	SPIx->CR1 &= ~SPI_CR1_CPHA;
	
	SPIx->CR1 &= ~SPI_CR1_LSBFIRST;
	
	/* NSS Harware Mode */
	if (SPI_NSS == SPI_NSS_Software)
	{
		/* NSS Software */
		SPIx->CR2 |= SPI_CR2_SSM;
		SPIx->CR2 |= SPI_CR2_SSI;
	}
	else
	{
		/* NSS Hardware */
		SPIx->CR2 &= ~SPI_CR2_SSM;
		SPIx->CR2 |= SPI_CR2_SSI;
	}
	/* Set Master of Slave Mode */
	if (SPI_MODE != SPI_MODE_Master)
		SPIx->CR1 &= ~SPI_CR1_MSTR;	/* Slave Mode */
		else SPIx->CR1 |= SPI_CR1_MSTR;	/* Master Mode */
		
	/* SPI Enable */
	SPIx->CR1 |= SPI_CR1_SPE;
}

uint8_t SPI_Transfer(SPI_TypeDef* SPIx, uint8_t w_data)
{
	uint8_t r_data;
	
	while((SPIx->SR & SPI_SR_TXE) == 0 ) {} // Waiting for completed transfer (while Tx is not empty)
	GPIO_ResetBits(SPI_NSS_Port, SPI_NSS_Pin);
	SPI1->DR = w_data;
	while((SPIx->SR & SPI_SR_BSY) != 0 ) {}
	GPIO_SetBits(SPI_NSS_Port, SPI_NSS_Pin);
	r_data = SPI1->DR;
	
	return(r_data);
	
}