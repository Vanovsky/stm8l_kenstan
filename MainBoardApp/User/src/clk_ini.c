#include "clk_ini.h"

void CLK_INIT(void)
{
	CLK->ECKCR |= CLK_ECKCR_HSEON;
	/* Wait while HSE is not ready */
	while (!(CLK->ECKCR & CLK_ECKCR_HSERDY));
}