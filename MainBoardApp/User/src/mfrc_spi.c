#include "mfrc_spi.h"

void RF_Deactivate(void) //�������������� RC522
{
	GPIO_SetBits(Port, NSS);
}
 
void RF_Activate(void) ///������������ RC522
{
	GPIO_ResetBits(Port, NSS);
}
/*
void SPI_init(void) //������������� SPI
{
	GPIO_InitTypeDef SPI_port; //��������� ���������
 
	//CSK � MOSI ��� �������������� ������, ��� PUSH - PULL
	SPI_port.GPIO_Pin = SCK | MOSI;
	SPI_port.GPIO_Mode = GPIO_Mode_AF_PP;
	SPI_port.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(Port, &amp;SPI_port);
 
	//MISO - ����, �������� � �������
	SPI_port.GPIO_Pin = MISO;
	SPI_port.GPIO_Mode = GPIO_Mode_IPU;
	SPI_port.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(Port, &amp;SPI_port);
 
	//����� - P/P
	SPI_port.GPIO_Pin = NSS;
	SPI_port.GPIO_Mode = GPIO_Mode_Out_PP;
	SPI_port.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(Port, &amp;SPI_port);
 
	SPI_InitTypeDef SPI;
	//������ �������
	SPI.SPI_Direction 		  = SPI_Direction_2Lines_FullDuplex;
	//stm - ������
	SPI.SPI_Mode 			  = SPI_Mode_Master;
	//����� ������ - 8 ���
	SPI.SPI_DataSize 		  = SPI_DataSize_8b;
	SPI.SPI_CPOL 			  = SPI_CPOL_Low;//���������� 
	SPI.SPI_CPHA 			  = SPI_CPHA_1Edge;//���� 
	SPI.SPI_NSS 			  = SPI_NSS_Soft; //���������� NSS
	SPI.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;//�������� SPI
	SPI.SPI_FirstBit          = SPI_FirstBit_MSB; //�������� ������� ������� ���
 
	SPI_Init(SPIx, &amp;SPI);//������������� SPI
 
	SPI_Cmd(SPIx, ENABLE); //���. SPI
 
	SPI_NSSInternalSoftwareConfig(SPIx, SPI_NSSInternalSoft_Set);//����, ��� �� stm ����, ��� �� ������
 
	//Reset - �� ������ ������
	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin   = RST_Pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_Init(PORT_RST, &amp;GPIO_InitStructure);
 
	GPIO_WriteBit(PORT_RST, RST_Pin,Bit_SET);
	RF_Deactivate();
}
*/

void USART_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
 
        //����������� �����  Rx � Tx
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &amp;GPIO_InitStruct);
 
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &amp;GPIO_InitStruct);
 
        //��������� USART(��������, ����� ������, ���.���� �����, ��������, ����������)
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &amp;USART_InitStructure);
        //���. USART
	USART_Cmd(USART1, ENABLE);
        //��������� ���������� �� USART
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&amp;NVIC_InitStructure);
        //���������� ��� ������ 
	USART_ITConfig(USART_DEBUG, USART_IT_RXNE, ENABLE);
}
 
//���������� ���������� �� USART
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART_DEBUG, USART_IT_RXNE))//����, ��� �� ������ ���������� ��� �� SPI
	{
           USART_ClearITPendingBit(USART_DEBUG, USART_IT_RXNE);
           char cc = (char)USART_ReceiveData(USART_DEBUG);
           printf("adds = 0x%x ", cc);
           char value = Read_MFRC522(cc);  //����� �� ������, ����� �� SPI
           printf("value = 0x%x \r\n", value);
 	}
}

uint8_t sectorKeyA[16][16] = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                             {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},//when you try it again, please change it into your new password
                             {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                             {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                            };
uint8_t sectorNewKeyA[16][16] = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                                {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xff,0x07,0x80,0x69, 0x19,0x84,0x07,0x15,0x76,0x14},
                                {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xff,0x07,0x80,0x69, 0x19,0x33,0x07,0x15,0x34,0x14},
                                {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xff,0x07,0x80,0x69, 0x19,0x33,0x07,0x15,0x34,0x14},
                               };
 
void RF_Process(void)
{
	uint8_t i,tmp;
	uint8_t status;
	uint8_t str[MAX_LEN];
	uint8_t RC_size;
	uint8_t blockAddr;//Select the operating block address, 0-63
 
	//Look for the card, return type
	status = MFRC522_Request(PICC_REQIDL, str);//������ �� ������
	if (status == MI_OK)
	{
		printf("Find out a card 0x%x, 0x%x \r\n", str[0], str[1]);
	}
 
	// Anti-collision, return the card's 4-byte serial number
	status = MFRC522_Anticoll(str);
	memcpy(serNum, str, 5);
	if (status == MI_OK)
	{
		printf("The card's number is: %x%x%x%x\r\n", serNum[0],serNum[1],serNum[2],serNum[3]);
	}
 
	// Election card, return capacity
	RC_size = MFRC522_SelectTag(serNum);
	if (RC_size != 0)
	{
		printf("The size of the card is: %d Kbits\r\n", RC_size);
	}
 
	 //Registration card
	blockAddr = 11;// Data block 11
	status = MFRC522_Auth(PICC_AUTHENT1A, blockAddr, sectorKeyA[blockAddr/4], serNum);// Authentication
	if (status == MI_OK)
	{
		// Write data
		status = MFRC522_Write(blockAddr, sectorNewKeyA[blockAddr/4]);
		printf("Set the new card password, and can modify the data of the Sector %d:", blockAddr/4);
		for (i=0; i&lt;6; i++)
		{
			printf("0x%x, ",sectorNewKeyA[blockAddr/4][i]);
		}
		printf("\r\n");
		blockAddr = blockAddr - 3 ;
		status = MFRC522_Write(blockAddr, writeData);
		if(status == MI_OK)
		{
			printf("The card has  $100 !\r\n");
		}
 
	}
 
	 //Card reader
	blockAddr = 11;// Data block 11
	status = MFRC522_Auth(PICC_AUTHENT1A, blockAddr, sectorNewKeyA[blockAddr/4], serNum);// Authentication
	if (status == MI_OK)
	{
		// Read data
		blockAddr = blockAddr - 3 ;
		status = MFRC522_Read(blockAddr, str);
		if (status == MI_OK)
		{
			printf("Read from the card ,the data is : ");
			for (i=0; i&lt;16; i++)
			{
				printf("%d, ",str[i]);
			}
			printf("\r\n");
		}
	}
 
	 // Consumer(�����������)
	blockAddr = 11;//Data block 11
	status = MFRC522_Auth(PICC_AUTHENT1A, blockAddr, sectorNewKeyA[blockAddr/4], serNum);// Authentication
	if (status == MI_OK)
	{
		// Read data
		blockAddr = blockAddr - 3 ;
		status = MFRC522_Read(blockAddr, str);
		if (status == MI_OK)
		{
			if( str[15] &lt; 1 ) 			
			{ 				
				printf("The money is not enough !\r\n"); 			
			} 			
			else 			
			{ 				
				str[15] = str[15] - moneyConsume; 				status = MFRC522_Write(blockAddr, str); 				
				if(status == MI_OK) 				
				{ 					
					printf("You pay $1. Now, Your money balance is : %d $ \r\n", 
					str[15]); 				
				} 			
			} 		
		} 	
	} 	 //Recharge(�������) 	
	blockAddr = 11;	//Data block 11 	
	status = MFRC522_Auth(PICC_AUTHENT1A, blockAddr, sectorNewKeyA[blockAddr/4], serNum);// Authentication 	
	if (status == MI_OK) 	
	{ 		// Read data 		
		blockAddr = blockAddr - 3 ; 		
		status = MFRC522_Read(blockAddr, str); 		
		if (status == MI_OK) 		
		{ 			
			tmp = (int)(str[15] + moneyAdd) ; 			//printf("Balance %d $\r\n",tmp); 			if( tmp &gt; (char)254 )
			{
				printf("The money of the card can not be more than 255$!\r\n");
			}
			else
			{
				str[15] = str[15] + 2 ;
				status = MFRC522_Write(blockAddr, str);
				if(status == MI_OK)
				{
					printf("Add 10 $. Your money balance is : %d $ \r\n", str[15]);
				}
			}
		}
	}
	MFRC522_Halt();// Put card in sleep mode
}
 
/*
 * Function:Write_MFRC5200
 * Description:Write a byte of data to MFRC522 register
 * Input parameters:addr--register address;val--value to be written
 * Return value:
 */
void Write_MFRC522(uint8_t addr, uint8_t val)
{
        RF_Activate();
 
        // Address format:0XXXXXX0
        SPI_I2S_SendData(SPIx,(addr&lt;&lt;1)&amp;0x7E);
        while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
        SPI_I2S_SendData(SPIx, val);
        while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
        RF_Deactivate();
}
 
/*
 * Function:Read_MFRC522
 * Description:Read a byte of data from MFRC522 register
 * Input parameters:addr--register address
 * Return value: Returns a byte of data
 */
uint8_t Read_MFRC522(uint8_t addr)
{
	uint8_t val;
	RF_Activate();
 
	// Address format:1XXXXXX0
	SPI_I2S_SendData(SPIx, ((addr&lt;&lt;1)&amp;0x7E) | 0x80);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
	val = SPI_I2S_ReceiveData(SPIx);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
	SPI_I2S_SendData(SPIx, 0xFF);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
	val = SPI_I2S_ReceiveData(SPIx);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
 
	RF_Deactivate();
	return val;
}
 
/*
 * Function:SetBitMask
 * Description:Set RC522 register bit
 * Input parameters:reg--register address;mask--set value
 * Return value:
 */
void SetBitMask(uint8_t reg, uint8_t mask)
{
    uint8_t tmp;
    tmp = Read_MFRC522(reg);
    Write_MFRC522(reg, tmp | mask);  // set bit mask
}
 
/*
 * Function:ClearBitMask
 * Description:Clear RC522 register bit
 * Input parameters:reg--register address;mask--clear bit value
 * Return value:
 */
void ClearBitMask(uint8_t reg, uint8_t mask)
{
    uint8_t tmp;
    tmp = Read_MFRC522(reg);
    Write_MFRC522(reg, tmp &amp; (~mask));  // clear bit mask
}
 
/*
 * Function:AntennaOn
 * Description:Power up ready for use, minimum 1ms interval between on/off cycle
 * Input values:
 * Return value:
 */
void AntennaOn(void)
{
        uint8_t temp;
 
        /*
         * TxControlReg - ��������� �������� ����� Tx1 � Tx2
         * 0 � 1 ���� � ������� - ���������� ����� �������� 13,56 ���
         */
        temp = Read_MFRC522(TxControlReg);
        if (!(temp &amp; 0x03))
        {
                SetBitMask(TxControlReg, 0x03);
        }
}
 
/*
 * Function:AntennaOff
 * Description:Power down after use, minimum 1ms interval between on/off cycle
 * Input values:
 * Output value:
 */
void AntennaOff(void)
{
        ClearBitMask(TxControlReg, 0x03);
}
 
/*
 * Function:ResetMFRC522
 * Description:Resets the RC522
 * Input values:
 * Output value:
 */
void MFRC522_Reset(void)
{
	/*
	 * �������� � ��������� ���������� ������,
	 * �� ��������� CommandReg = 0�20 - ���������� ����� ��������� ����.
	 * ������������� 0�FF - Soft Reset
	 */
    Write_MFRC522(CommandReg, PCD_RESETPHASE);
}
 
/*
 * Function:InitMFRC522
 * Description:???RC522
 * Input values:
 * Output value:
 */
void MFRC522_init(void)
{
	//Soft Reset
    MFRC522_Reset();
 
    /*
     * Timer: TPrescaler*TreloadVal/6.78MHz = 24ms
     * TModeReg, TPrescalerReg - ���������� ��������� ��� ����������� �������
     * TModeReg - �������������� ����� �������, � ������ ��� ������� ������� ���� ������������
     * TPrescalerReg - ������� ���� ������������ ������
     */
    Write_MFRC522(TModeReg, 0x8D);//Tauto=1; f(Timer) = 6.78MHz/TPreScaler
    Write_MFRC522(TPrescalerReg, 0x3E);//TModeReg[3..0] + TPrescalerReg
 
    /*
     * �������� ���������� ���������� ����� 16� ���������� �������
     * �������� 24��, ������� ����
     */
    Write_MFRC522(TReloadRegL, 30);
    Write_MFRC522(TReloadRegH, 0);
 
	Write_MFRC522(TxAutoReg, 0x40);//�������� 100% ASK (����������� ���������)
 
	/*
	 * ���������� ���. ������ ������.
	 * �������� ���� ���� ������������, ���������� ����������, � ����������� CRC
	 */
	Write_MFRC522(ModeReg, 0x3D);//CRC initial value of 0x6363
 
	/*
	 * ����������
	 */
	//ClearBitMask(Status2Reg, 0x08);//MFCrypto1On=0
	//Write_MFRC522(RxSelReg, 0x86);//RxWait = RxSelReg[5..0]
	//Write_MFRC522(RFCfgReg, 0x7F);//RxGain = 48dB
 
    AntennaOn();// Power on the antenna
}
 
/*
 * Function:MFRC522_Request
 * Description:Find card and read its type
 * Input values:reqMode--way to find the card;
 *                         TagType--return type of card:
 *                                 0x4400 = Mifare_UltraLight
 *                                0x0400 = Mifare_One(S50)
 *                                0x0200 = Mifare_One(S70)
 *                                0x0800 = Mifare_Pro(X)
 *                                0x4403 = Mifare_DESFire
 * Output value:successful return MI_OK
 */
uint8_t MFRC522_Request(uint8_t reqMode, uint8_t *TagType)
{
        uint8_t status;
        uint backBits;// The received data bits
 
        /*
         * ������������ ��� ����� � �����
         * 0�07���������� ����� ���������� �����, ������� ������ ���� ��������
         */
        Write_MFRC522(BitFramingReg, 0x07);//TxLastBists = BitFramingReg[2..0]
 
        TagType[0] = reqMode;
        /*
         * PCD_TRANSCEIVE - �������, �������� ������ �� FIFO ������ � �������� � ������������ �������������, �������� ����� ��������.
         */
        status = MFRC522_ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &amp;backBits);
 
        if ((status != MI_OK) || (backBits != 0x10))
        {
                status = MI_ERR;
        }
 
        return status;
}
 
/*
 * Function:MFRC522_ToCard
 * Description:RC522 and ISO14443 card communication
 * Input values:command--MF522 command,
 *                         sendData--RC522 data,
 *                         sendLen--Send data length
 *                         backData--Received data is returns,
 *                         backLen--Returns the length of the data bits
 * Output value:Successful return MI_OK
 */
uint8_t MFRC522_ToCard(uint8_t command, uint8_t *sendData, uint8_t sendLen, uint8_t *backData, uint *backLen)
{
    uint8_t status = MI_ERR;
    uint8_t irqEn = 0x00;
    uint8_t waitIRq = 0x00;
    uint8_t lastBits;
    uint8_t n;
    uint i;
 
    switch (command)
    {
        case PCD_AUTHENT://certification card secret
                {
                        irqEn = 0x12;
                        waitIRq = 0x10;
                        break;
                }
                case PCD_TRANSCEIVE://Transmit FIFO data
                {
                        irqEn = 0x77;
                        waitIRq = 0x30;
                        break;
                }
                default:
                        break;
    }
 
    /*
     * CommIEnReg - ����������� ���� ��� ��������� � ���������� ����������� �������� ����������.
     * 0b11110111
     * - ������ �� ������ IRQ ������������� �� ��������� � IRQ � �������� Status1Reg.
     * - ����������� ������� ���������� ����������� (����������� ����� TxIRq), ������� ����� ���������������� �� ������� IRQ.
     * - ����������� ������ �������� ���������� (������� ����� RxIRq), ����� ���� ���������������� �� ������� IRQ.
     * - ����������� � ������ �������� ������� ���������� (����������� ����� IdleIRq), ������� ����� ���������������� �� ������� IRQ.
     * - ����������� ������� ���������� ���������� ����������
     * - ����������� ������ ������ ���������� ���������� (����������� ����� LoAlertIRq), ������� ����� ���������������� �� ������� IRQ
     * - ����������� ������ ������ ����������
     * - ����������� ������ ���������� �������
     */
    Write_MFRC522(CommIEnReg, irqEn|0x80);// Allow the interrupt request
 
    /*
     * CommIrqReg - �������� ������ �� ���������� ���
     */
    ClearBitMask(CommIrqReg, 0x80);//Clear interrupt request bit
 
    /*
     * ������� �����
     */
    SetBitMask(FIFOLevelReg, 0x80);//FlushBuffer=1, initialise FIFO
 
    /*
     * ������� ��������;
     * �������� ���������� ������� �������.
     */
    Write_MFRC522(CommandReg, PCD_IDLE);        //No action; cancel the current command        ???
 
    // Write data to FIFO
    for (i=0; i&lt;sendLen; i++)     
		{                 
			Write_MFRC522(FIFODataReg, sendData[i]);         
		}         // Execute the command         
		Write_MFRC522(CommandReg, command);     
		if (command == PCD_TRANSCEIVE)     
		{                 
			SetBitMask(BitFramingReg, 0x80);                //StartSend=1,transmission of data starts         
		}         // Wait to receive data        
		i = 2000;        //i is for clock frequency adjustment, M1 card maximum waiting time is 25ms        ???     
		do     
		{                 //CommIrqReg[7..0]                 //Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq        
			n = Read_MFRC522(CommIrqReg);         
			i--;     
		}while ((i!=0) &amp;&amp; !(n&amp;0x01) &amp;&amp; !(n&amp;waitIRq));     
		ClearBitMask(BitFramingReg, 0x80);                        
		//StartSend=0     
		if (i != 0)
		{         
			if(!(Read_MFRC522(ErrorReg) &amp; 0x1B))        
			//BufferOvfl Collerr CRCErr ProtecolErr         
			{            
				status = MI_OK;             
				if (n &amp; irqEn &amp; 0x01)             
				{                                 
					status = MI_NOTAGERR;                        //??                         
				}             
				if (command == PCD_TRANSCEIVE)             
				{                        
					n = Read_MFRC522(FIFOLevelReg);                   
					lastBits = Read_MFRC522(ControlReg) &amp; 0x07;                 
					if (lastBits)                 
					{                                         
						*backLen = (n-1)*8 + lastBits;                                 
					}                 
					else                 
					{                                         
						*backLen = n*8;                                 
					}                 
					if (n == 0)                 
					{                                         
						n = 1;                                 
					}                 
					if (n &gt; MAX_LEN)
					{
						n = MAX_LEN;
					}
					// Read received FIFO data
					for (i=0; i&lt;n; i++)
					{
						backData[i] = Read_MFRC522(FIFODataReg);
					}
				}
			}
			else
			{
				status = MI_ERR;
			}
 
    }
	//SetBitMask(ControlReg,0x80);           //timer stops
	//Write_MFRC522(CommandReg, PCD_IDLE);
	return status;
}
 
/*
 * Function:MFRC522_Anticoll
 * Description:Anti-collision detection and card serial number reading
 * Input values:serNum--return the four byte card serial number, the first five bytes of the checksum
 * Output value:Successful return MI_OK
 */
uint8_t MFRC522_Anticoll(uint8_t *serNum)
{
    uint8_t status;
    uint8_t i;
    uint8_t serNumCheck=0;
    uint unLen;
 
    ClearBitMask(Status2Reg, 0x08); //TempSensclear
    ClearBitMask(CollReg,0x80);//ValuesAfterColl
    Write_MFRC522(BitFramingReg, 0x00);//TxLastBists = BitFramingReg[2..0]
 
    serNum[0] = PICC_ANTICOLL;
    serNum[1] = 0x20;
    status = MFRC522_ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &amp;unLen);
 
    if (status == MI_OK)
        {
                // Check card serial number
                for (i=0; i&lt;4; i++)
                {
                         serNumCheck ^= serNum[i];
                }
                if (serNumCheck != serNum[i])
                {
                        status = MI_ERR;
                }
    }
 
    //SetBitMask(CollReg, 0x80);                //ValuesAfterColl=1
    return status;
}
 
/*
 * Function:CalulateCRC
 * Description:Calculate MF522 CRC
 * Input values::pIndata--read CRC data,len--length of data,pOutData--calculated CRC results
 * Output value:
 */
void CalulateCRC(uint8_t *pIndata, uint8_t len, uint8_t *pOutData)
{
    uint8_t i, n;
 
    ClearBitMask(DivIrqReg, 0x04);                        //CRCIrq = 0
    SetBitMask(FIFOLevelReg, 0x80);                        // Clear FIFO pointer
    //Write_MFRC522(CommandReg, PCD_IDLE);
 
        // Write data to FIFO
    for (i=0; i&lt;len; i++)
    {
                Write_MFRC522(FIFODataReg, *(pIndata+i));
        }
    Write_MFRC522(CommandReg, PCD_CALCCRC);
 
        // Wait for CRC calculation
    i = 0xFF;
    do
    {
        n = Read_MFRC522(DivIrqReg);
        i--;
    }
    while ((i!=0) &amp;&amp; !(n&amp;0x04));                        //CRCIrq = 1
 
        // Read CRC calcuation results
    pOutData[0] = Read_MFRC522(CRCResultRegL);
    pOutData[1] = Read_MFRC522(CRCResultRegM);
}
 
/*
 * Function:MFRC522_SelectTag
 * Description:Election card, read memory capacity
 * Input values:serNum--incoming card serial number
 * Output value:Successful return of card capacity
 */
uint8_t MFRC522_SelectTag(uint8_t *serNum)
{
    uint8_t i;
        uint8_t status;
        uint8_t size;
    uint recvBits;
    uint8_t buffer[9];
 
        //ClearBitMask(Status2Reg, 0x08);                        //MFCrypto1On=0
 
    buffer[0] = PICC_SElECTTAG;
    buffer[1] = 0x70;
    for (i=0; i&lt;5; i++)
    {
            buffer[i+2] = *(serNum+i);
    }
        CalulateCRC(buffer, 7, &amp;buffer[7]);                //??
    status = MFRC522_ToCard(PCD_TRANSCEIVE, buffer, 9, buffer, &amp;recvBits);
 
    if ((status == MI_OK) &amp;&amp; (recvBits == 0x18))
    {
                size = buffer[0];
        }
    else
    {
                size = 0;
        }
 
    return size;
}
 
/*
 * Function:MFRC522_Auth
 * Description:Verify card password
 * Input values:authMode--Passowrd authentication mode
                 0x60 = Verify A key
                 0x61 = Verify B key
             BlockAddr--Block address
             Sectorkey--Sector's password
             serNum--Card serial number, 4 bytes
 * Output value:Successful return MI_OK
 */
uint8_t MFRC522_Auth(uint8_t authMode, uint8_t BlockAddr, uint8_t *Sectorkey, uint8_t *serNum)
{
    uint8_t status;
    uint recvBits;
    uint8_t i;
        uint8_t buff[12];
 
    // Verify instruction block address, sector's password and card serial number
    buff[0] = authMode;
    buff[1] = BlockAddr;
    for (i=0; i&lt;6; i++)
    {
                buff[i+2] = *(Sectorkey+i);
        }
    for (i=0; i&lt;4; i++)
    {
                buff[i+8] = *(serNum+i);
        }
    status = MFRC522_ToCard(PCD_AUTHENT, buff, 12, buff, &amp;recvBits);
 
    if ((status != MI_OK) || (!(Read_MFRC522(Status2Reg) &amp; 0x08)))
    {
                status = MI_ERR;
        }
 
    return status;
}
 
/*
 * Function:MFRC522_Read
 * Description:Read block data
 * Input values:blockAddr--block address;recvData--read a block of data
 * Output value:Successful return MI_OK
 */
uint8_t MFRC522_Read(uint8_t blockAddr, uint8_t *recvData)
{
    uint8_t status;
    uint unLen;
 
    recvData[0] = PICC_READ;
    recvData[1] = blockAddr;
    CalulateCRC(recvData,2, &amp;recvData[2]);
    status = MFRC522_ToCard(PCD_TRANSCEIVE, recvData, 4, recvData, &amp;unLen);
 
    if ((status != MI_OK) || (unLen != 0x90))
    {
        status = MI_ERR;
    }
 
    return status;
}
 
/*
 * Function:MFRC522_Write
 * Description:Write block data
 * Input values:blockAddr--block address;writeData--16 bytes of data to be written to block
 * Output value:Successful return MI_OK
 */
uint8_t MFRC522_Write(uint8_t blockAddr, uint8_t *writeData)
{
    uint8_t status;
    uint recvBits;
    uint8_t i;
        uint8_t buff[18];
 
    buff[0] = PICC_WRITE;
    buff[1] = blockAddr;
    CalulateCRC(buff, 2, &amp;buff[2]);
    status = MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &amp;recvBits);
 
    if ((status != MI_OK) || (recvBits != 4) || ((buff[0] &amp; 0x0F) != 0x0A))
    {
                status = MI_ERR;
        }
 
    if (status == MI_OK)
    {
        for (i=0; i&lt;16; i++)                //Write 16 bytes of data to the FIFO
        {
                buff[i] = *(writeData+i);
        }
        CalulateCRC(buff, 16, &amp;buff[16]);
        status = MFRC522_ToCard(PCD_TRANSCEIVE, buff, 18, buff, &amp;recvBits);
 
                if ((status != MI_OK) || (recvBits != 4) || ((buff[0] &amp; 0x0F) != 0x0A))
        {
                        status = MI_ERR;
                }
    }
 
    return status;
}
 
/*
 * Function:MFRC522_Halt
 * Description: Send card to sleep
 * Input values:
 * Output value:
 */
void MFRC522_Halt(void)
{
        uint8_t status;
    uint unLen;
    uint8_t buff[4];
 
    buff[0] = PICC_HALT;
    buff[1] = 0;
    CalulateCRC(buff, 2, &amp;buff[2]);
 
    status = MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff,&amp;unLen);
}