#ifndef GPIO_INI_H
#define GPIO_INI_H

#include "main.h"

#define SPI_NSS_Port 	GPIOB
#define SPI_NSS_Pin  	GPIO_Pin_4

void GPIO_SPI_INIT(void);

#endif
