#ifndef MFRC_SPI_H
#define MFRC_SPI_H 

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

void 		Write_MFRC522(uint8_t addr, uint8_t val);
uint8_t Read_MFRC522(uint8_t addr);
void 		SetBitMask(uint8_t reg, uint8_t mask);
void 		ClearBitMask(uint8_t reg, uint8_t mask);
void 		AntennaOn(void);
void 		AntennaOff(void);
void 		MFRC522_Reset(void);
void 		MFRC522_Init(void);
uint8_t MFRC522_Request(uint8_t reqMode, uint8_t *TagType);
uint8_t MFRC522_ToCard(uint8_t command, uint8_t *sendData, uint8_t sendLen, uint8_t *backData, uint *backLen);
uint8_t MFRC522_Anticoll(uint8_t *serNum);
void 		CalulateCRC(uint8_t *pIndata, uint8_t len, uint8_t *pOutData);
uint8_t MFRC522_SelectTag(uint8_t *serNum);
uint8_t MFRC522_Auth(uint8_t authMode, uint8_t BlockAddr, uint8_t *Sectorkey, uint8_t *serNum);
uint8_t MFRC522_Read(uint8_t blockAddr, uint8_t *recvData);
uint8_t MFRC522_Write(uint8_t blockAddr, uint8_t *writeData);
void 		MFRC522_Halt(void);
void*		memcpy(void *dst, const void *src, size_t len);

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif