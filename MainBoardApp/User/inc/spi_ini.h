#ifndef SPI_INI_H
#define SPI_INI_H

#include "main.h"

typedef uint8_t SPI_NSS_type;
#define SPI_NSS_Software 0
#define SPI_NSS_Hardware 1

typedef uint8_t SPI_MODE_type;
#define SPI_MODE_Slave 0
#define SPI_MODE_Master 1

void SPI_INIT(SPI_TypeDef* SPIx, SPI_NSS_type SPI_NSS, SPI_MODE_type SPI_MODE);
uint8_t SPI_Transfer(SPI_TypeDef* SPIx, uint8_t w_data);

#endif
